package com.wang.mvchain.outer.service.implay;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.commons.codec.digest.DigestUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.common.util.HTTPUtil;
import com.wang.mvchain.outer.enums.TradeEnum;
import com.wang.mvchain.outer.service.ITradeService;

/**
 * Okex的API工具
 * 
 * @author wangsh
 */
public class CoinexTradeServiceImplay extends BaseTradeService implements ITradeService
{
	/* 请求服务的serverUrl */
	private String serverUrl = "https://api.coinex.com" ; 
	
	@Override
	public int findWebsiteId()
	{
		return 24;
	}
	
	/**
	 * 获取签名方式
	 * @param json 是一个JSON字符串
	 * @return
	 */
	private String authSign(Map<String, String> headerMap,Map<String, String> requestMap,Object json)
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			Map<String, String> signMap = new TreeMap<String, String>();
			signMap.putAll(requestMap);
			
			for (Iterator iterator = signMap.entrySet().iterator(); iterator.hasNext();)
			{
				Entry me = (Entry) iterator.next();
				String key = me.getKey() + "" ;
				String value = me.getValue() + ""; 
				if("requestURL".equalsIgnoreCase(key) || key.startsWith("enc_"))
				{
					continue ; 
				}else
				{
					sb.append(key + "=" + value + "&");
				}
			}
			/* 增加密钥 */
			sb.append("secret_key=" + this.authJSON.getString("secret_key")) ; 
			/*
			 * 采用HMacSha256加密
			 * */
			return DigestUtils.md5Hex(sb.toString()).toUpperCase() ; 
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return "" ; 
	}

	/**
	 * 获取用户基本信息
	 * 
	 * @return
	 */
	public JSONObject getUserInfo(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		String methodUrl = "/v1/balance/info" ; 
		
		HTTPUtil httpUtil = new HTTPUtil();
		/* 公共代码 */
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("requestURL", serverUrl + methodUrl);
		requestMap.put("access_id", this.authJSON.getString("access_id"));
		requestMap.put("tonce", this.findReqtime());
		
		/**
		 * 存储Header的一些参数
		 */
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("Content-Type", "application/json");
		
		/* 存储一些交易的参数 */
		
		String sign = authSign(headerMap,requestMap,null);
		headerMap.put("authorization", sign);
		
		String result = "" ; 
		try
		{
			result = httpUtil.methodGet(headerMap,requestMap);
			JSONObject responseJSON = (JSONObject) JSON.parse(result);
			
			if("0".equalsIgnoreCase(responseJSON.getString("code")))
			{
				JSONObject dataRes = responseJSON.getJSONObject("data");
				
				JSONObject dataJSON = new JSONObject();
				
				JSONObject balanceJSON = new JSONObject();
				JSONObject fundJSON = new JSONObject();
				for (Iterator iterator = dataRes.entrySet().iterator(); iterator.hasNext();)
				{
					Entry me = (Entry) iterator.next() ; 
					String key = me.getKey() + ""; 
					JSONObject resTempJSON = (JSONObject) me.getValue() ;
					
					/*
					 * {
					        "frozen":"0",
					        "hold":"0",
					        "id":"9150707",
					        "currency":"BTC",
					        "balance":"0.0049925",
					        "available":"0.0049925",
					        "holds":"0"
					    }
					 * */
					String name = key.toLowerCase();
					/* 这里面有我们要想的币种 */
					balanceJSON.put(name, resTempJSON.get("available"));
					fundJSON.put(name, resTempJSON.get("frozen"));
				}
				
				dataJSON.put("balance", balanceJSON);
				dataJSON.put("fund", fundJSON);
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
				//ConstatFinalUtil.OUTER_LOG.info(this.findid() + "--获取用户信息返回.--" + responseJSON);
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + result);
			}
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取用户信息失败."
					+ requestMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 下单交易
	 * 
	 * @return
	 */
	public JSONObject trade(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = super.trade(paramsMap);
		if(resultJSON != null)
		{
			return resultJSON ; 
		}
		
		resultJSON = new JSONObject() ;
		//拼装目标数据
		String methodUrl = "/v1/order/limit" ; 
		
		HTTPUtil httpUtil = new HTTPUtil();
		/* 公共代码 */
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("access_id", this.authJSON.getString("access_id"));
		requestMap.put("tonce", this.findReqtime());
		
		/**
		 * 存储Header的一些参数
		 */
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("requestURL", serverUrl + methodUrl);
		headerMap.put("Content-Type", "application/json");
		
		/* 存储一些交易的参数 */
		String type = paramsMap.get("type") ; 
		if(TradeEnum.TRADE_TYPE_BUY.getValue() == Integer.valueOf(type))
		{
			/* 买 */
			requestMap.put("type", "buy");
		}else if(TradeEnum.TRADE_TYPE_SELL.getValue() == Integer.valueOf(type))
		{
			/* 卖 */
			requestMap.put("type", "sell");
		}
		/* 交易对 */
		String moneyType = paramsMap.get("moneytype");
		moneyType = this.convertMoneyType(moneyType) ; 
		requestMap.put("market", moneyType);
		/* 价格和数量 */
		requestMap.put("price", paramsMap.get("price"));
		requestMap.put("amount", paramsMap.get("amount"));
		
		String sign = authSign(headerMap,requestMap,null);
		headerMap.put("authorization", sign);
		
		JSONObject reqJSON = new JSONObject() ; 
		reqJSON.putAll(requestMap);
		
		String result = "" ; 
		try
		{
			result = httpUtil.methodPost(headerMap, reqJSON.toJSONString());
			JSONObject responseJSON = (JSONObject) JSON.parse(result);
			
			JSONObject dataResJSON = responseJSON.getJSONObject("data") ;
			if("0".equalsIgnoreCase(responseJSON.getString("code")))
			{
				JSONObject dataJSON = new JSONObject();
				dataJSON.put("order_id", dataResJSON.getString("id"));
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + result);
			}
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "--下单交易返回--原始信息:" + result
				+ ";转换后目标信息:" + resultJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "下单交易失败."
					+ requestMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 撤销订单
	 * 
	 * @return
	 */
	public JSONObject cancelOrder(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = super.cancelOrder(paramsMap);
		if(resultJSON != null)
		{
			return resultJSON ; 
		}
		/* 订单id */
		String order_id = paramsMap.get("order_id") ; 
		//拼装目标数据
		resultJSON = new JSONObject();
		String methodUrl = "/v1/order/pending" ;
		
		HTTPUtil httpUtil = new HTTPUtil();
		/* 公共代码 */
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("requestURL", serverUrl + methodUrl);
		requestMap.put("access_id", this.authJSON.getString("access_id"));
		requestMap.put("tonce", this.findReqtime());
		
		/**
		 * 存储Header的一些参数
		 */
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("Content-Type", "application/json");
		
		/* 存储一些交易的参数 */
		String moneytype = paramsMap.get("moneytype");
		moneytype = this.convertMoneyType(moneytype) ;
		requestMap.put("market", moneytype);
		requestMap.put("id", order_id);
		
		String sign = authSign(headerMap,requestMap,null);
		headerMap.put("authorization", sign);
		
		String result = "" ; 
		try
		{
			result = httpUtil.methodDelete(headerMap, requestMap);
			JSONObject responseJSON = (JSONObject) JSON.parse(result);
			
			if("0".equalsIgnoreCase(responseJSON.get("code") + ""))
			{
				JSONObject dataJSON = new JSONObject();
				dataJSON.put("order_id", order_id);
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + result);
			}
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "--撤销订单返回.--原始信息:" + result
				+ ";转换后目标信息:" + resultJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "撤销订单失败."
					+ paramsMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 获取用户挂单
	 * 
	 * @param args
	 */
	public JSONObject getOrders(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = super.getOrders(paramsMap);
		if(resultJSON != null)
		{
			return resultJSON ; 
		}
		resultJSON = new JSONObject() ;
		//拼装目标数据
		String methodUrl = "/v1/order/pending" ; 
		HTTPUtil httpUtil = new HTTPUtil();
		
		/* 公共代码 */
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("requestURL", serverUrl + methodUrl);
		requestMap.put("access_id", this.authJSON.getString("access_id"));
		requestMap.put("tonce", this.findReqtime());
		
		/**
		 * 存储Header的一些参数
		 */
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("Content-Type", "application/json");
		
		/* 存储一些交易的参数 */
		String moneyType = paramsMap.get("moneytype") ; 
		moneyType = this.convertMoneyType(moneyType);
		/* 存储一些交易的参数 */
		requestMap.put("market", moneyType);
		requestMap.put("trade_type", "0");
		requestMap.put("page", "1");
		requestMap.put("limit", "100");
		
		String sign = authSign(headerMap,requestMap,null);
		headerMap.put("authorization", sign);
		
		String result = "" ; 
		try
		{
			result = httpUtil.methodGet(headerMap , requestMap);
			JSONObject responseJSON = (JSONObject) JSON.parse(result);
			
			if("0".equalsIgnoreCase(responseJSON.getString("code")))
			{
				JSONObject dataResJSON = responseJSON.getJSONObject("data");
				JSONArray dataResArr = dataResJSON.getJSONArray("data");
				
				JSONObject dataJSON = new JSONObject();
				JSONArray ordersResArr = new JSONArray();
				for (Iterator iterator = dataResArr.iterator(); iterator
						.hasNext();)
				{
					JSONObject ordersTemp = (JSONObject) iterator.next();
					JSONObject ordersResJSON = this.convertOrders(moneyType, ordersTemp);
					ordersResArr.add(ordersResJSON);
				}
				
				dataJSON.put("ordersList", ordersResArr);
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
				ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "--获取用户挂单--原始信息:" + result
				+ ";转换后目标信息:" + resultJSON);
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + result);
			}
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取用户挂单失败."
					+ paramsMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 行情API
	 */
	public JSONObject queryTicker(Map<String, String> paramsMap)
	{

		HTTPUtil httpUtil = new HTTPUtil();
		String symbol = paramsMap.get("symbol");
		if ("ltc_cny".equalsIgnoreCase(symbol))
		{
			paramsMap.put("requestURL",
					"https://www.okcoin.com/api/ticker.do?symbol=ltc_cny");
		} else
		{
			paramsMap.put("requestURL", "https://www.okcoin.com/api/ticker.do");
		}

		paramsMap.remove("symbol");

		JSONObject resultJSON = new JSONObject();
		try
		{
			String result = httpUtil.methodGet(paramsMap);
			resultJSON = (JSONObject) JSON.parse(result);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "行情API失败."
					+ paramsMap, e);
		}
		return resultJSON;
	}

	/**
	 * depth(市场深度)
	 */
	public JSONObject queryDepth(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		String moneyType = paramsMap.get("moneytype");
		moneyType = this.convertMoneyType(moneyType);
		
		String methodUrl = "/v1/market/depth" ; 
		
		HTTPUtil httpUtil = new HTTPUtil();
		/* 公共代码 */
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("requestURL", serverUrl + methodUrl);
		requestMap.put("limit", "50");
		requestMap.put("market", moneyType);
		requestMap.put("merge", "0");
		
		/* 增加签名参数 */
		/* 存储一些交易的参数 */
		JSONObject responseJSON = new JSONObject();
		try
		{
			String result = httpUtil.methodGet(requestMap);
			responseJSON = (JSONObject) JSON.parse(result);
			if("0".equalsIgnoreCase(responseJSON.getString("code")))
			{
				JSONObject dataResJSON = responseJSON.getJSONObject("data");
			
				// 拼装符合条件的json数据
				JSONObject dataJSON = new JSONObject();
	
				int count = 0;
				// 按照规则排序的容器
				Map<Double, JSONObject> asksMap = new TreeMap<Double, JSONObject>();
				JSONArray asksResArr = new JSONArray();
				JSONArray asksArr = (JSONArray) dataResJSON.get("asks");
				for (Iterator iterator = asksArr.iterator(); iterator.hasNext();)
				{
					JSONArray asksTempArr = (JSONArray) iterator.next();
					JSONObject askJSON = new JSONObject();
					askJSON.put("count", "1");
					askJSON.put("vol", asksTempArr.get(1) + "");
					askJSON.put("rate", asksTempArr.get(0) + "");
	
					asksMap.put(Double.valueOf(askJSON.get("rate") + ""), askJSON);
				}
	
				for (Iterator iterator = asksMap.entrySet().iterator(); iterator
						.hasNext();)
				{
					Entry me = (Entry) iterator.next();
					double key = (Double) me.getKey();
	
					if (count >= ConstatFinalUtil.PAGE_BATCH_SIZE)
					{
						break;
					}
	
					JSONObject asksTemp = (JSONObject) me.getValue();
	
					asksResArr.add(asksTemp);
					count++;
				}
	
				dataJSON.put("asks", asksResArr);
	
				// 按照规则排序的容器
				count = 0;
				Map<Double, JSONObject> bidsMap = new TreeMap<Double, JSONObject>(
						new Comparator<Double>()
						{
							@Override
							public int compare(Double o1, Double o2)
							{
								return o2.compareTo(o1);
							}
						});
				JSONArray bidsResArr = new JSONArray();
				JSONArray bidsArr = (JSONArray) dataResJSON.get("bids");
				for (Iterator iterator = bidsArr.iterator(); iterator.hasNext();)
				{
					JSONArray bidsTempArr = (JSONArray) iterator.next();
					JSONObject bidsJSON = new JSONObject();
					bidsJSON.put("count", "1");
					bidsJSON.put("vol", bidsTempArr.get(1) + "");
					bidsJSON.put("rate", bidsTempArr.get(0) + "");
	
					bidsMap.put(Double.valueOf(bidsJSON.get("rate") + ""), bidsJSON);
				}
	
				for (Iterator iterator = bidsMap.entrySet().iterator(); iterator
						.hasNext();)
				{
					Entry me = (Entry) iterator.next();
					double key = (Double) me.getKey();
	
					if (count >= ConstatFinalUtil.PAGE_BATCH_SIZE)
					{
						break;
					}
	
					JSONObject bidsTemp = (JSONObject) me.getValue();
	
					bidsResArr.add(bidsTemp);
					count++;
				}
				dataJSON.put("bids", bidsResArr);
	
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
				
				resultJSON.put("data", dataJSON);
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + result);
			}
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "depth(市场深度)失败."
					+ paramsMap, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * trades的格式,历史交易记录
	 */
	public JSONArray queryHistoryTrades(String symbol,
			Map<String, String> paramsMap)
	{

		HTTPUtil httpUtil = new HTTPUtil();

		if ("ltc_cny".equalsIgnoreCase(symbol))
		{
			paramsMap.put("requestURL",
					" https://www.okcoin.com/api/trades.do?symbol=ltc_cny");
		} else
		{
			paramsMap.put("requestURL",
					" https://www.okcoin.com/api/trades.do?");
		}
		JSONArray resultJSON = new JSONArray();
		try
		{
			String result = httpUtil.methodGet(paramsMap);
			resultJSON = (JSONArray) JSON.parse(result);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "历史交易记录失败."
					+ paramsMap, e);
		}
		return resultJSON;
	}

	@Override
	public String toString()
	{
		return "CoinexTradeServiceImplay [findid()=" + findWebsiteId() + "]";
	}

	@Override
	public JSONObject getOrdersSingle(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = super.getOrdersSingle(paramsMap);
		if(resultJSON != null)
		{
			return resultJSON ; 
		}
		
		resultJSON = new JSONObject();
		//拼装目标数据
		String methodUrl = "/v1/order/status" ; 
		
		HTTPUtil httpUtil = new HTTPUtil();
		/* 公共代码 */
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("requestURL", serverUrl + methodUrl);
		requestMap.put("access_id", this.authJSON.getString("access_id"));
		requestMap.put("tonce", this.findReqtime());
		
		/**
		 * 存储Header的一些参数
		 */
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("Content-Type", "application/json");
		
		/* 存储一些交易的参数 */
		String order_id = paramsMap.get("order_id") ;
		String moneyType = paramsMap.get("moneytype") ; 
		requestMap.put("market", this.convertMoneyType(moneyType));
		
		requestMap.put("id", order_id);
//		requestMap.put("page", "1");
//		requestMap.put("limit", "100");
		
		String sign = authSign(headerMap,requestMap,null);
		headerMap.put("authorization", sign);
		
		String result = "" ; 
		try
		{
			result = httpUtil.methodGet(headerMap , requestMap);
			JSONObject responseJSON = (JSONObject) JSON.parse(result);
			if("0".equalsIgnoreCase(responseJSON.getString("code")))
			{
				JSONObject ordersTemp = responseJSON.getJSONObject("data");
				
				JSONObject dataJSON = new JSONObject() ; 
				if((ordersTemp.get("id") + "").equalsIgnoreCase(order_id))
				{
					JSONObject ordersResJSON = this.convertOrders(moneyType, ordersTemp);
					dataJSON.put("orders", ordersResJSON);
				}
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
				ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "--获取订单详情--原始信息:" + result
						+ ";转换后目标信息:" + resultJSON);
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + result);
			}
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取订单详情失败."
					+ paramsMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	@Override
	public String findWebSiteName()
	{
		return "Mxc";
	}

	@Override
	public JSONObject convertOrders(String moneytype, JSONObject ordersTemp)
	{
		try
		{
			JSONObject ordersResJSON = new JSONObject();
			ordersResJSON.put("order_id", ordersTemp.get("id") + "");
			ordersResJSON.put("price", ordersTemp.get("price") + "");
			ordersResJSON.put("amount", ordersTemp.get("amount") + "");
			ordersResJSON.put("deal_amount", ordersTemp.get("deal_amount") + "");
			
			ordersResJSON.put("avg_price", ordersResJSON.get("avg_price"));
			/* 创建订单的时间 */
			String createTime = ordersTemp.get("create_time") + "";
			Date createTimeDate = new Date(Long.valueOf(createTime) * 1000);
			ordersResJSON.put("create_date", this.dateUtil.formatDateTime(createTimeDate));
			
			String symbol = ordersTemp.get("market") + "";
			ordersResJSON.put("symbol", this.parseMoneyType(symbol));
			/* 交易类型 */
			String type = ordersTemp.get("type") + "";
			if("buy".equalsIgnoreCase(type))
			{
				type = TradeEnum.TRADE_TYPE_BUY.getValue() + "" ; 
			}else if("sell".equalsIgnoreCase(type))
			{
				type = TradeEnum.TRADE_TYPE_SELL.getValue() + "" ; 
			}
			ordersResJSON.put("type", type);
			
			/* 默认未成交 */
			String status = TradeEnum.TRADE_STATUS_DEAL_NO.getValue() + ""; 
			/*
			 * -1:已撤销  0:未成交  1:部分成交  2:完全成交 3 撤单处理中
			 * */
			if("cancel".equalsIgnoreCase(ordersTemp.get("status") + ""))
			{
				status = TradeEnum.TRADE_STATUS_CANCEL.getValue() + ""; 
			}else if("not_deal".equalsIgnoreCase(ordersTemp.get("status") + ""))
			{
				status = TradeEnum.TRADE_STATUS_DEAL_NO.getValue() + ""; 
			}else if("part_deal".equalsIgnoreCase(ordersTemp.get("status") + ""))
			{
				status = TradeEnum.TRADE_STATUS_DEAL_PART.getValue() + ""; 
			}else if("done".equalsIgnoreCase(ordersTemp.get("status") + ""))
			{
				status = TradeEnum.TRADE_STATUS_DEALED.getValue() + ""; 
			}
			ordersResJSON.put("status", status);
			return ordersResJSON;
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOG.error("转换订单报错了",e);
		}
		return new JSONObject();
	}

	@Override
	public String convertMoneyType(String moneytypeSelf)
	{
		return moneytypeSelf.toUpperCase().replaceAll("_", "");
	}

	@Override
	public String parseMoneyType(String moneytypeTrade)
	{
		moneytypeTrade = moneytypeTrade.toLowerCase() ; 
		moneytypeTrade = moneytypeTrade.replaceAll("usdt", "_usdt");
		return moneytypeTrade.toLowerCase();
	}
	
	@Override
	public JSONObject findAllTransPair(Map<String, String> paramsMap)
	{
		JSONObject resultJSON =  super.findAllTransPair(paramsMap);
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		String methodUrl = "/v1/market/info" ; 
		
		HTTPUtil httpUtil = new HTTPUtil();
		/* 公共代码 */
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("requestURL", serverUrl + methodUrl);
		String result = "" ; 
		try
		{
			result = httpUtil.methodGet(Collections.EMPTY_MAP,requestMap);
			//ConstatFinalUtil.SYS_LOG.info("---返回:{}",result);
			JSONObject responseJSON = (JSONObject) JSON.parse(result);
			
			if("0".equalsIgnoreCase(responseJSON.getString("code")))
			{
				JSONObject dataResJSON = responseJSON.getJSONObject("data");
				
				JSONObject dataJSON = new JSONObject();
				for (Iterator iterator = dataResJSON.entrySet().iterator(); iterator.hasNext();)
				{
					Entry me = (Entry) iterator.next() ; 
					String key = me.getKey() + "" ; 
					JSONObject resTempJSON = (JSONObject) me.getValue() ; 
					/*
					 * {
				        //交易货币币种,小写;
				        "base_currency":"BTC",
				        //币对名称,小写;
				        "instrument_id":"BTC-USDT",
				        //最小交易数量
				        "min_size":"0.001",
				        //计价货币币种,小写;
				        "quote_currency":"USDT",
				        //交易货币数量精度
				        "size_increment":"0.00000001",
				        //交易价格精度
				        "tick_size":"0.1"
				    }
					 * */
					String trading_name = resTempJSON.getString("trading_name");
					String pricing_name = resTempJSON.getString("pricing_name");
					
					JSONObject pairJSON = new JSONObject();
					
					String moneyType = trading_name + pricing_name;
					moneyType = this.parseMoneyType(moneyType);
					
					if(!moneyType.endsWith("usdt"))
					{
						continue ; 
					}
					
					String[] moneyTypes = moneyType.split("_");
					pairJSON.put("name", moneyType);
					pairJSON.put("min_size", resTempJSON.get("min_amount"));
					pairJSON.put("base_currency", moneyTypes[0]);
					pairJSON.put("quote_currency", moneyTypes[1]);
					pairJSON.put("size_increment", resTempJSON.get("trading_decimal"));
					pairJSON.put("tick_size", resTempJSON.get("pricing_decimal"));
					/* 存储到结果中 */
					dataJSON.put(pairJSON.get("name") + "", pairJSON);
				}
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + result);
			}
			//ConstatFinalUtil.OUTER_LOG.info(this.findid() + "--获取用户信息返回.--" + responseJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取用户信息失败."
					+ requestMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}
}
