package com.wang.mvchain.outer.enums;

/**
 * 交易的枚举
 * 
 * @author wangshMac
 */
public enum TradeEnum
{
	/**
	 * 交易类型
	 * 买还是卖.0:买,1:卖
	 */
	TRADE_TYPE_BUY("买",0),
	TRADE_TYPE_SELL("卖",1),
	
	/**
	 * -1:已撤销  0:未成交(挂单中)  1:部分成交  2:完全成交
	 */
	TRADE_STATUS_CANCEL("已撤销",-1),
	TRADE_STATUS_DEAL_NO("未成交",0),
	TRADE_STATUS_DEAL_PART("部分成交",1),
	TRADE_STATUS_DEALED("完全成交",2);
	
	/* 名字 */
	private String name ;
	/* 值 */
	private int value;

	/**
	 * 枚举的构造方法
	 * 
	 * @param name
	 * @param value
	 */
	private TradeEnum(String name, int value)
	{
		this.name = name;
		this.value = value;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getValue()
	{
		return value;
	}

	public void setValue(int value)
	{
		this.value = value;
	}
}
