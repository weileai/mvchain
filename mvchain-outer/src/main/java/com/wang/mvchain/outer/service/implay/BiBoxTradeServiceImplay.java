package com.wang.mvchain.outer.service.implay;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.commons.codec.digest.HmacUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.common.util.HTTPUtil;
import com.wang.mvchain.outer.enums.TradeEnum;
import com.wang.mvchain.outer.service.ITradeService;

/**
 * Okex的API工具
 * 
 * @author wangsh
 */
public class BiBoxTradeServiceImplay extends BaseTradeService implements ITradeService
{
	/* 请求服务的serverUrl */
	private String serverUrl = "https://api.bibox365.com" ; 
	
	@Override
	public int findWebsiteId()
	{
		return 23;
	}
	
	/**
	 * 获取签名方式
	 * @param json 是一个JSON字符串
	 * @return
	 */
	private String authSign(JSONObject requestJSON)
	{
		JSONObject resultJSON = new JSONObject();
		/* 先对requestJSON进行处理 */
		JSONArray reqArr = new JSONArray();
		reqArr.add(requestJSON);
		resultJSON.put("cmds", reqArr);
		
		resultJSON.put("apikey", this.authJSON.get("apikey"));
		String encodeStr = reqArr.toJSONString() ; 
		//ConstatFinalUtil.SYS_LOG.info("==加密前:{}",encodeStr);
		encodeStr = encodeStr.replace("\"", "\"");
		//ConstatFinalUtil.SYS_LOG.info("==加密前:{}",encodeStr);
		
		/* 加密 */
		String sign = HmacUtils.hmacMd5Hex(this.authJSON.getString("secret"),encodeStr);
		//ConstatFinalUtil.SYS_LOG.info("==加密后:{}",sign);
		resultJSON.put("sign", sign);
		
		reqArr.clear();
		resultJSON.put("cmds", encodeStr);
		
		String resStr = resultJSON.toJSONString() ;
		//ConstatFinalUtil.SYS_LOG.info("==转义前:{}",resStr);
		resStr = resStr.replaceAll("\\\\\\\\", "") ;
		//ConstatFinalUtil.SYS_LOG.info("==转义后:{}",resStr);
		return resStr ; 
	}

	/**
	 * 获取用户基本信息
	 * 
	 * @return
	 */
	public JSONObject getUserInfo(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		String methodUrl = "/v1/transfer" ; 
		
		HTTPUtil httpUtil = new HTTPUtil();
		/**
		 * 存储Header的一些参数
		 */
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("requestURL", serverUrl + methodUrl);
		headerMap.put("content-Type", "application/json; charset=utf-8");
		
		/* 请求的json */
		JSONObject requestJSON = new JSONObject();
		requestJSON.put("cmd", "transfer/assets");
		/* 存储一些交易的参数 */
		JSONObject bodyJSON = new JSONObject();
		bodyJSON.put("select", 1);
		requestJSON.put("body", bodyJSON);
		
		String requestStr = authSign(requestJSON); 
		//String requestStr = requestJSON.toJSONString() ; 
		//requestStr = "{\"apikey\":\"2668fd042349d520080a15e841d46fc635adcc82\",\"cmds\":[\"[{\\\"cmd\\\":\\\"transfer/assets\\\",\\\"body\\\":{\\\"select\\\":1}}]\"],\"sign\":\"e9e11319255442a4c1d8d28757aa2bcf\"}" ; 
		
		String result = "" ; 
		try
		{
			result = httpUtil.methodPost(headerMap,requestStr);
			//ConstatFinalUtil.SYS_LOG.info("结果-->" + result);
			JSONObject resJSON = (JSONObject) JSON.parse(result);
			JSONArray resArr = resJSON.getJSONArray("result");
			JSONObject res0JSON = (JSONObject) resArr.get(0);
			JSONObject res02JSON = (JSONObject) res0JSON.get("result");
			/* 获取账户明细 */
			resArr = (JSONArray) res02JSON.get("assets_list");
			
			JSONObject dataJSON = new JSONObject();
			JSONObject balanceJSON = new JSONObject();
			JSONObject fundJSON = new JSONObject();
			
			for (Iterator iterator = resArr.iterator(); iterator.hasNext();)
			{
				JSONObject resTempJSON = (JSONObject) iterator.next() ; 
				/*
				 * {
				        "frozen":"0",
				        "hold":"0",
				        "id":"9150707",
				        "currency":"BTC",
				        "balance":"0.0049925",
				        "available":"0.0049925",
				        "holds":"0"
				    }
				 * */
				String name = resTempJSON.getString("coin_symbol");
				name = name.toLowerCase();
				
				/* 这里面有我们要想的币种 */
				balanceJSON.put(name, resTempJSON.get("balance"));
				fundJSON.put(name, resTempJSON.get("freeze"));
			}
			
			dataJSON.put("balance", balanceJSON);
			dataJSON.put("fund", fundJSON);
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			//ConstatFinalUtil.OUTER_LOG.info(this.findid() + "--获取用户信息返回.--" + responseJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取用户信息失败."
					+ headerMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 下单交易
	 * 
	 * @return
	 */
	public JSONObject trade(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = super.trade(paramsMap);
		if(resultJSON != null)
		{
			return resultJSON ; 
		}
		//拼装目标数据
		String methodUrl = "/v1/orderpending" ; 
		
		resultJSON = new JSONObject() ;
		HTTPUtil httpUtil = new HTTPUtil();
				
		/**
		 * 存储Header的一些参数
		 */
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("requestURL", serverUrl + methodUrl);
		headerMap.put("content-Type", "application/json; charset=utf-8");
		
		/* 请求的json */
		JSONObject requestJSON = new JSONObject();
		requestJSON.put("cmd", "orderpending/trade");
		/* 存储一些交易的参数 */
		JSONObject bodyJSON = new JSONObject();
		
		/* 存储一些交易的参数 */
		String type = paramsMap.get("type") ; 
		if(TradeEnum.TRADE_TYPE_BUY.getValue() == Integer.valueOf(type))
		{
			/* 买 */
			bodyJSON.put("order_side", "1");
		}else if(TradeEnum.TRADE_TYPE_SELL.getValue() == Integer.valueOf(type))
		{
			/* 买 */
			bodyJSON.put("order_side", "2");
		}
		/* 交易对 */
		String moneyType = paramsMap.get("moneytype");
		moneyType = this.convertMoneyType(moneyType) ; 
		bodyJSON.put("pair", moneyType);
		/* 价格和数量 */
		bodyJSON.put("price", paramsMap.get("price"));
		bodyJSON.put("amount", paramsMap.get("amount"));
		
		bodyJSON.put("account_type", "0");
		bodyJSON.put("order_type", "2");
		
		requestJSON.put("body", bodyJSON);
		
		String requestStr = authSign(requestJSON); 
		
		String result = "" ; 
		try
		{
			result = httpUtil.methodPost(headerMap, requestStr);
			JSONObject responseJSON = (JSONObject) JSON.parse(result);
			
			JSONArray resArr = responseJSON.getJSONArray("result");
			JSONObject res0JSON = (JSONObject) resArr.get(0);
			
			if(res0JSON.get("result") instanceof String)
			{
				String data = res0JSON.get("result") + "";
				JSONObject dataJSON = new JSONObject();
				dataJSON.put("order_id", data);
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
				ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "--下单交易返回--原始信息:" + result
					+ ";转换后目标信息:" + resultJSON);
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + result );
			}
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "下单交易失败."
					+ bodyJSON + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 撤销订单
	 * 
	 * @return
	 */
	public JSONObject cancelOrder(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = super.cancelOrder(paramsMap);
		if(resultJSON != null)
		{
			return resultJSON ; 
		}
		resultJSON = new JSONObject();
		/* 订单id */
		String order_id = paramsMap.get("order_id") ; 
		
		String methodUrl = "/v1/orderpending" ; 
		
		HTTPUtil httpUtil = new HTTPUtil();
		/**
		 * 存储Header的一些参数
		 */
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("requestURL", serverUrl + methodUrl);
		headerMap.put("content-Type", "application/json; charset=utf-8");
		
		/* 请求的json */
		JSONObject requestJSON = new JSONObject();
		requestJSON.put("cmd", "orderpending/cancelTrade");
		/* 存储一些交易的参数 */
		JSONObject bodyJSON = new JSONObject();
		bodyJSON.put("orders_id", order_id);
		requestJSON.put("body", bodyJSON);
		
		String requestStr = authSign(requestJSON); 
		String result = "" ; 
		try
		{
			result = httpUtil.methodPost(headerMap, requestStr);
			JSONObject responseJSON = (JSONObject) JSON.parse(result);
			
			JSONArray resArr = responseJSON.getJSONArray("result");
			JSONObject res0JSON = (JSONObject) resArr.get(0);
			
			if("ok".equalsIgnoreCase(res0JSON.get("result") + ""))
			{
				JSONObject dataJSON = new JSONObject();
				dataJSON.put("order_id", order_id);
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + result );
			}
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "--撤销订单返回.--原始信息:" + result
				+ ";转换后目标信息:" + resultJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "撤销订单失败."
					+ paramsMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 获取用户挂单
	 * 
	 * @param args
	 */
	public JSONObject getOrders(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = super.getOrders(paramsMap);
		if(resultJSON != null)
		{
			return resultJSON ; 
		}
		resultJSON = new JSONObject() ;
		//拼装目标数据
		String methodUrl = "/v1/orderpending" ; 
		
		HTTPUtil httpUtil = new HTTPUtil();
		/**
		 * 存储Header的一些参数
		 */
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("requestURL", serverUrl + methodUrl);
		headerMap.put("content-Type", "application/json");
		
		/* 请求的json */
		JSONObject requestJSON = new JSONObject();
		requestJSON.put("cmd", "orderpending/orderPendingList");
		/* 存储一些交易的参数 */
		JSONObject bodyJSON = new JSONObject();
		
		String moneytype = paramsMap.get("moneytype");
		moneytype = this.convertMoneyType(moneytype);
		
		bodyJSON.put("pair", moneytype);
		bodyJSON.put("account_type", "0");
		bodyJSON.put("page", "1");
		bodyJSON.put("size", "50");
		
		requestJSON.put("body", bodyJSON);
		
		String requestStr = authSign(requestJSON); 
		String result = "" ; 
		try
		{
			result = httpUtil.methodPost(headerMap , requestStr);
			//ConstatFinalUtil.SYS_LOG.info(result);
			JSONObject responseJSON = (JSONObject) JSON.parse(result);
			JSONArray resArr = responseJSON.getJSONArray("result");
			JSONObject res0JSON = (JSONObject) resArr.get(0);
			JSONObject res02JSON = (JSONObject) res0JSON.get("result");
			JSONArray dataResArr = res02JSON.getJSONArray("items");
			
			JSONObject dataJSON = new JSONObject();
			JSONArray ordersResArr = new JSONArray();
			for (Iterator iterator = dataResArr.iterator(); iterator
					.hasNext();)
			{
				JSONObject ordersTemp = (JSONObject) iterator.next();
				JSONObject ordersResJSON = this.convertOrders(moneytype, ordersTemp);
				ordersResArr.add(ordersResJSON);
			}
			
			dataJSON.put("ordersList", ordersResArr);
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "--获取用户挂单--原始信息:" + result
			+ ";转换后目标信息:" + resultJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取用户挂单失败."
					+ paramsMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 行情API
	 */
	public JSONObject queryTicker(Map<String, String> paramsMap)
	{

		HTTPUtil httpUtil = new HTTPUtil();
		String symbol = paramsMap.get("symbol");
		if ("ltc_cny".equalsIgnoreCase(symbol))
		{
			paramsMap.put("requestURL",
					"https://www.okcoin.com/api/ticker.do?symbol=ltc_cny");
		} else
		{
			paramsMap.put("requestURL", "https://www.okcoin.com/api/ticker.do");
		}

		paramsMap.remove("symbol");

		JSONObject resultJSON = new JSONObject();
		try
		{
			String result = httpUtil.methodGet(paramsMap);
			resultJSON = (JSONObject) JSON.parse(result);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "行情API失败."
					+ paramsMap, e);
		}
		return resultJSON;
	}

	/**
	 * depth(市场深度)
	 */
	public JSONObject queryDepth(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		String moneyType = paramsMap.get("moneytype");
		moneyType = this.convertMoneyType(moneyType);
		
		//拼装目标数据
		String methodUrl = "/v1/mdata" ; 
		
		HTTPUtil httpUtil = new HTTPUtil();
		/* 公共代码 */
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("requestURL", serverUrl + methodUrl);
		requestMap.put("cmd", "depth");
		/*cmd=depth&pair=BIX_BTC&size=10*/
		requestMap.put("size", "20");
		requestMap.put("pair", moneyType);
		
		/* 增加签名参数 */
		/* 存储一些交易的参数 */
		JSONObject responseJSON = new JSONObject();
		try
		{
			String result = httpUtil.methodGet(requestMap);
			responseJSON = (JSONObject) JSON.parse(result);
			JSONObject resJSON = (JSONObject) responseJSON.get("result");
			if(resJSON != null)
			{
				// 拼装符合条件的json数据
				JSONObject dataJSON = new JSONObject();
	
				int count = 0;
				// 按照规则排序的容器
				Map<Double, JSONObject> asksMap = new TreeMap<Double, JSONObject>();
				JSONArray asksResArr = new JSONArray();
				JSONArray asksArr = (JSONArray) resJSON.get("asks");
				for (Iterator iterator = asksArr.iterator(); iterator.hasNext();)
				{
					JSONObject asksTempArr = (JSONObject) iterator.next();
					JSONObject askJSON = new JSONObject();
					askJSON.put("count", "1");
					askJSON.put("vol", asksTempArr.get("volume") + "");
					askJSON.put("rate", asksTempArr.get("price") + "");
	
					asksMap.put(Double.valueOf(askJSON.get("rate") + ""), askJSON);
				}
	
				for (Iterator iterator = asksMap.entrySet().iterator(); iterator
						.hasNext();)
				{
					Entry me = (Entry) iterator.next();
					double key = (Double) me.getKey();
	
					if (count >= ConstatFinalUtil.PAGE_BATCH_SIZE)
					{
						break;
					}
	
					JSONObject asksTemp = (JSONObject) me.getValue();
	
					asksResArr.add(asksTemp);
					count++;
				}
	
				dataJSON.put("asks", asksResArr);
	
				// 按照规则排序的容器
				count = 0;
				Map<Double, JSONObject> bidsMap = new TreeMap<Double, JSONObject>(
						new Comparator<Double>()
						{
							@Override
							public int compare(Double o1, Double o2)
							{
								return o2.compareTo(o1);
							}
						});
				JSONArray bidsResArr = new JSONArray();
				JSONArray bidsArr = (JSONArray) resJSON.get("bids");
				for (Iterator iterator = bidsArr.iterator(); iterator.hasNext();)
				{
					JSONObject bidsTempArr = (JSONObject) iterator.next();
					JSONObject bidsJSON = new JSONObject();
					bidsJSON.put("count", "1");
					bidsJSON.put("vol", bidsTempArr.get("volume") + "");
					bidsJSON.put("rate", bidsTempArr.get("price") + "");
	
					bidsMap.put(Double.valueOf(bidsJSON.get("rate") + ""), bidsJSON);
				}
	
				for (Iterator iterator = bidsMap.entrySet().iterator(); iterator
						.hasNext();)
				{
					Entry me = (Entry) iterator.next();
					double key = (Double) me.getKey();
	
					if (count >= ConstatFinalUtil.PAGE_BATCH_SIZE)
					{
						break;
					}
	
					JSONObject bidsTemp = (JSONObject) me.getValue();
	
					bidsResArr.add(bidsTemp);
					count++;
				}
				dataJSON.put("bids", bidsResArr);
	
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
				
				resultJSON.put("data", dataJSON);
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + result );
			}
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "depth(市场深度)失败."
					+ paramsMap, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * trades的格式,历史交易记录
	 */
	public JSONArray queryHistoryTrades(String symbol,
			Map<String, String> paramsMap)
	{

		HTTPUtil httpUtil = new HTTPUtil();

		if ("ltc_cny".equalsIgnoreCase(symbol))
		{
			paramsMap.put("requestURL",
					" https://www.okcoin.com/api/trades.do?symbol=ltc_cny");
		} else
		{
			paramsMap.put("requestURL",
					" https://www.okcoin.com/api/trades.do?");
		}
		JSONArray resultJSON = new JSONArray();
		try
		{
			String result = httpUtil.methodGet(paramsMap);
			resultJSON = (JSONArray) JSON.parse(result);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "历史交易记录失败."
					+ paramsMap, e);
		}
		return resultJSON;
	}

	@Override
	public String toString()
	{
		return "MxcTradeServiceImplay [findid()=" + findWebsiteId() + "]";
	}

	@Override
	public JSONObject getOrdersSingle(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = super.getOrdersSingle(paramsMap);
		if(resultJSON != null)
		{
			return resultJSON ; 
		}
		
		/* 存储一些交易的参数 */
		String order_id = paramsMap.get("order_id") ;
		String moneyType = paramsMap.get("moneytype") ; 
		moneyType = this.convertMoneyType(moneyType) ; 
		
		
		//拼装目标数据
		resultJSON = new JSONObject();
		String methodUrl = "/v1/orderpending" ; 
		
		HTTPUtil httpUtil = new HTTPUtil();
		/**
		 * 存储Header的一些参数
		 */
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("requestURL", serverUrl + methodUrl);
		headerMap.put("content-Type", "application/json; charset=utf-8");
		
		/* 请求的json */
		JSONObject requestJSON = new JSONObject();
		requestJSON.put("cmd", "orderpending/order");
		/* 存储一些交易的参数 */
		JSONObject bodyJSON = new JSONObject();
		
		bodyJSON.put("id", order_id);
		bodyJSON.put("account_type", "0");
		
		requestJSON.put("body", bodyJSON);
		String requestStr = authSign(requestJSON); 
		
		String result = "" ; 
		try
		{
			result = httpUtil.methodPost(headerMap , requestStr);
			JSONObject responseJSON = (JSONObject) JSON.parse(result);
			JSONArray resArr = responseJSON.getJSONArray("result");
			JSONObject result0JSON = resArr.getJSONObject(0);
			JSONObject ordersTemp = result0JSON.getJSONObject("result");
			if(ordersTemp != null)
			{
				JSONObject dataJSON = new JSONObject() ; 
				if((ordersTemp.get("id") + "").equalsIgnoreCase(order_id))
				{
					JSONObject ordersResJSON = this.convertOrders(moneyType, ordersTemp);
					dataJSON.put("orders", ordersResJSON);
				}
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
				ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "--获取订单详情--原始信息:" + result
						+ ";转换后目标信息:" + resultJSON);
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + result );
			}
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取订单详情失败."
					+ paramsMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	@Override
	public String findWebSiteName()
	{
		return "Mxc";
	}

	@Override
	public JSONObject convertOrders(String moneytype, JSONObject ordersTemp)
	{
		try
		{
			JSONObject ordersResJSON = new JSONObject();
			ordersResJSON.put("order_id", ordersTemp.get("id") + "");
			ordersResJSON.put("price", ordersTemp.get("price") + "");
			ordersResJSON.put("amount", ordersTemp.get("amount") + "");
			ordersResJSON.put("deal_amount", ordersTemp.get("deal_amount") + "");
			
			ordersResJSON.put("avg_price", ordersResJSON.get("price"));
			/* 创建订单的时间 */
			String createTime = ordersTemp.get("createdAt") + "";
			ordersResJSON.put("create_date", createTime);
			
			String symbol = ordersTemp.get("pair") + "";
			ordersResJSON.put("symbol", this.parseMoneyType(symbol));
			/* 交易类型 */
			String type = ordersTemp.get("order_side") + "";
			if("1".equalsIgnoreCase(type))
			{
				type = TradeEnum.TRADE_TYPE_BUY.getValue() + "" ; 
			}else if("2".equalsIgnoreCase(type))
			{
				type = TradeEnum.TRADE_TYPE_SELL.getValue() + "" ; 
			}
			ordersResJSON.put("type", type);
			
			/* 默认未成交 */
			String status = TradeEnum.TRADE_STATUS_DEAL_NO.getValue() + ""; 
			/*
			 * 状态，-1 失败 0或1-待成交, 2-部分成交，3-完全成交，4-部分撤销，5-完全撤销，6-待撤销
			 * */
			if("5".equalsIgnoreCase(ordersTemp.get("status") + ""))
			{
				status = TradeEnum.TRADE_STATUS_CANCEL.getValue() + ""; 
			}else if("1".equalsIgnoreCase(ordersTemp.get("status") + "") || "0".equalsIgnoreCase(ordersTemp.get("status") + ""))
			{
				status = TradeEnum.TRADE_STATUS_DEAL_NO.getValue() + ""; 
			}else if("2".equalsIgnoreCase(ordersTemp.get("status") + ""))
			{
				status = TradeEnum.TRADE_STATUS_DEAL_PART.getValue() + ""; 
			}else if("3".equalsIgnoreCase(ordersTemp.get("status") + ""))
			{
				status = TradeEnum.TRADE_STATUS_DEALED.getValue() + ""; 
			}
			ordersResJSON.put("status", status);
			return ordersResJSON;
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOG.error("转换订单报错了",e);
		}
		return new JSONObject();
	}

	@Override
	public String convertMoneyType(String moneytypeSelf)
	{
		return moneytypeSelf.toUpperCase();
	}

	@Override
	public String parseMoneyType(String moneytypeTrade)
	{
		return moneytypeTrade.toLowerCase();
	}
	
	@Override
	public JSONObject findAllTransPair(Map<String, String> paramsMap)
	{
		JSONObject resultJSON =  super.findAllTransPair(paramsMap);
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		String methodUrl = "/v1/mdata" ; 
		
		HTTPUtil httpUtil = new HTTPUtil();
		/* 公共代码 */
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("requestURL", serverUrl + methodUrl);
		requestMap.put("cmd", "pairList");
		String result = "" ; 
		try
		{
			result = httpUtil.methodGet(Collections.EMPTY_MAP,requestMap);
			//ConstatFinalUtil.SYS_LOG.info("---返回:{}",result);
			JSONObject responseJSON = (JSONObject) JSON.parse(result);
			JSONArray resArr = (JSONArray) responseJSON.get("result");
			if(resArr != null && resArr.size() > 0 )
			{
				JSONObject dataJSON = new JSONObject();
				for (Iterator iterator = resArr.iterator(); iterator.hasNext();)
				{
					JSONObject resTempJSON = (JSONObject) iterator.next() ; 
					/*
					 * {
				        //交易货币币种,小写;
				        "base_currency":"BTC",
				        //币对名称,小写;
				        "instrument_id":"BTC-USDT",
				        //最小交易数量
				        "min_size":"0.001",
				        //计价货币币种,小写;
				        "quote_currency":"USDT",
				        //交易货币数量精度
				        "size_increment":"0.00000001",
				        //交易价格精度
				        "tick_size":"0.1"
				    }
					 * */
					JSONObject pairJSON = new JSONObject();
					
					String moneyType = resTempJSON.getString("pair");
					moneyType = this.parseMoneyType(moneyType);
					
					if(!moneyType.endsWith("usdt"))
					{
						continue ; 
					}
					
					String[] moneyTypes = moneyType.split("_");
					pairJSON.put("name", moneyType);
					pairJSON.put("min_size", "0");
					pairJSON.put("base_currency", moneyTypes[0]);
					pairJSON.put("quote_currency", moneyTypes[1]);
					pairJSON.put("size_increment", "0");
					pairJSON.put("tick_size", "0");
					/* 存储到结果中 */
					dataJSON.put(pairJSON.get("name") + "", pairJSON);
				}
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + result );
			}
			//ConstatFinalUtil.OUTER_LOG.info(this.findid() + "--获取用户信息返回.--" + responseJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取用户信息失败."
					+ requestMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}
}
