package com.wang.mvchain.outer.service.implay;

import java.io.IOException;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.commons.codec.digest.HmacUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.common.util.DateUtil;
import com.wang.mvchain.common.util.EncryptUtil;
import com.wang.mvchain.common.util.HTTPUtil;
import com.wang.mvchain.outer.enums.TradeEnum;
import com.wang.mvchain.outer.service.ITradeService;

/**
 * chbtc的API工具
 * 
 * @author wangsh
 *
 */
public class ZbTradeServiceImplay extends BaseTradeService implements ITradeService
{
	/* 服务器的网址
	 * zb.plus
	 * zb.live
	 *  */
	private String serverUrl = "http://api.zb.live" ; 
	private String serverTradeUrl = "https://trade.zb.live" ; 
	
	//登陆验证的信息,可从网站模板中查找
	private JSONObject authJSON;
	/* 日期格式化 */
	private DateUtil dateUtil = new DateUtil();

	@Override
	public int findWebsiteId()
	{
		return 16;
	}

	/**
	 * 获取签名方式
	 * 
	 * @return
	 */
	private String authSign(Map<String, String> condMap)
	{
		StringBuffer sb = new StringBuffer();
		/* 为了让参数调整好顺序 */
		Map<String, String> treeMap = new TreeMap<String, String>();
		treeMap.putAll(condMap);
		for (Iterator<?> iterator = treeMap.entrySet().iterator(); iterator
				.hasNext();)
		{
			Entry me = (Entry) iterator.next();
			String key = me.getKey() + "";
			String value = me.getValue() + "";
			
			if(key.equalsIgnoreCase("requestURL"))
			{
				continue ; 
			}
			
			sb.append(key + "=" + value + "&");
		}
		if (sb.toString().lastIndexOf("&") != -1)
		{
			sb.delete(sb.lastIndexOf("&"), sb.length());
		}
		/* 对密码进行加密 */
		String secretkey = this.authJSON.get("secretkey") + "";
		secretkey = EncryptUtil.digest(secretkey, "SHA");
		
		/* 加密 */
		String sign = HmacUtils.hmacMd5Hex(secretkey, sb.toString());
		return sign; 
	}
	
	/**
	 * 获取用户基本信息
	 * 
	 * @return
	 */
	public JSONObject getUserInfo(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		
		HTTPUtil httpUtil = new HTTPUtil();
		Map<String, String> requestMap = new HashMap<String,String>();
		requestMap.put("requestURL", this.serverTradeUrl + "/api/getAccountInfo");
		requestMap.put("method", "getAccountInfo");
		requestMap.put("accesskey", authJSON.get("accesskey") + "");
		
		String sign = this.authSign(requestMap);
		requestMap.put("sign", sign);

		requestMap.put("reqTime", System.currentTimeMillis() + "");
		JSONObject responseJSON = new JSONObject();
		String result = "" ; 
		try
		{
			result = httpUtil.methodPost(Collections.EMPTY_MAP , requestMap);
			responseJSON = (JSONObject) JSON.parse(result);
			
			if(responseJSON.get("result") != null)
			{
				JSONObject resultResJSON = (JSONObject) responseJSON.get("result");
				JSONArray coinJSON = resultResJSON.getJSONArray("coins"); 
				JSONObject balanceResJSON = new JSONObject();
				JSONObject fundResJSON = new JSONObject();
				
				for (Iterator iterator = coinJSON.iterator(); iterator.hasNext();)
				{
					JSONObject itemJSON = (JSONObject) iterator.next();
					balanceResJSON.put(itemJSON.getString("showName").toLowerCase(), itemJSON.getString("available"));
					fundResJSON.put(itemJSON.getString("showName").toLowerCase(), itemJSON.getString("freez"));
				}
				
				
				JSONObject dataJSON = new JSONObject();
				dataJSON.put("balance", balanceResJSON);
				dataJSON.put("fund", fundResJSON);
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + responseJSON.get("code"));
				ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "获取用户信息返回.----" + responseJSON);
			}
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取用户信息失败."
					+ paramsMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 下单交易
	 * 
	 * @return
	 */
	public JSONObject trade(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = super.trade(paramsMap);
		if(resultJSON != null)
		{
			return resultJSON ; 
		}
		resultJSON = new JSONObject() ;
		
		HTTPUtil httpUtil = new HTTPUtil();
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("requestURL", this.serverTradeUrl + "/api/order");
		
		requestMap.put("method", "order");
		requestMap.put("accesskey", authJSON.get("accesskey") + "");
		
		// 买还是卖.0:买,1:卖
		if (paramsMap.get("type").equalsIgnoreCase(TradeEnum.TRADE_TYPE_BUY.getValue() + ""))
		{
			requestMap.put("tradeType", "1");
		} else if (paramsMap.get("type").equalsIgnoreCase(TradeEnum.TRADE_TYPE_SELL.getValue() + ""))
		{
			requestMap.put("tradeType", "0");
		}
		requestMap.put("price", paramsMap.get("price"));
		requestMap.put("amount", paramsMap.get("amount"));
		
		String moneytype = this.convertMoneyType(paramsMap.get("moneytype"));
		requestMap.put("currency", moneytype + "");
		
		String sign = this.authSign(requestMap);
		requestMap.put("sign", sign);
		
		requestMap.put("reqTime", System.currentTimeMillis() + "");

		String result = "" ; 
		try
		{
			result = httpUtil.methodPost(Collections.EMPTY_MAP , requestMap);
			JSONObject responseJSON = (JSONObject) JSON.parse(result);
			
			if("1000".equalsIgnoreCase(responseJSON.get("code") + ""))
			{
				JSONObject dataJSON = new JSONObject();
				dataJSON.put("order_id", responseJSON.get("id"));
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + responseJSON.get("message"));
			}
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "下单交易返回--原始信息:" + result
				+ ";转换后目标信息:" + resultJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "下单交易失败."
					+ requestMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 撤销订单
	 * 
	 * @return
	 */
	public JSONObject cancelOrder(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = super.cancelOrder(paramsMap);
		if(resultJSON != null)
		{
			return resultJSON ; 
		}
		resultJSON = new JSONObject() ;
		
		HTTPUtil httpUtil = new HTTPUtil();
		Map<String, String> requestMap = new HashMap<String, String>();
		
		requestMap.put("requestURL", this.serverTradeUrl + "/api/cancelOrder");
		
		requestMap.put("method", "order");
		requestMap.put("accesskey", authJSON.get("accesskey") + "");
		
		requestMap.put("id", paramsMap.get("order_id"));
		String moneytype = this.convertMoneyType(paramsMap.get("moneytype"));
		requestMap.put("currency", moneytype + "");
		
		String sign = this.authSign(requestMap);
		requestMap.put("sign", sign);
		
		requestMap.put("reqTime", System.currentTimeMillis() + "");

		JSONObject responseJSON = new JSONObject();
		String result = "" ; 
		try
		{
			result = httpUtil.methodPost(Collections.EMPTY_MAP , requestMap);
			responseJSON = (JSONObject) JSON.parse(result);
			
			if("1000".equalsIgnoreCase(responseJSON.get("code") + ""))
			{
				JSONObject dataJSON = new JSONObject();
				dataJSON.put("order_id", requestMap.get("id"));
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + responseJSON.get("error_code"));
			}
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "撤销订单返回.--原始信息:" + result
				+ ";转换后目标信息:" + resultJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "撤销订单失败."
					+ paramsMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 获取用户挂单
	 * 
	 * @param args
	 */
	public JSONObject getOrders(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = super.getOrders(paramsMap);
		if(resultJSON != null)
		{
			return resultJSON ; 
		}
		resultJSON = new JSONObject() ;
		
		HTTPUtil httpUtil = new HTTPUtil();
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("requestURL", this.serverTradeUrl + "/api/getUnfinishedOrdersIgnoreTradeType");
		requestMap.put("accesskey", authJSON.get("accesskey") + "");
		
		requestMap.put("method", "getUnfinishedOrdersIgnoreTradeType");
		
		String moneytype = paramsMap.get("moneytype") ;
		moneytype = this.convertMoneyType(moneytype);
		requestMap.put("currency", moneytype);
		requestMap.put("pageIndex", "1");
		requestMap.put("pageSize", "10");
		
		String sign = this.authSign(requestMap);
		requestMap.put("sign", sign);

		requestMap.put("reqTime", System.currentTimeMillis() + "");
		String result = "" ; 
		try
		{
			result = httpUtil.methodPost(Collections.EMPTY_MAP , requestMap);
			JSONObject responseJSON = new JSONObject();
			
			if(result.indexOf("code") == -1)
			{
				JSONArray ordersJSON = (JSONArray) JSON.parse(result);
				JSONObject dataJSON = new JSONObject();
				JSONArray ordersResArr = new JSONArray();
				for (Iterator<?> iterator = ordersJSON.iterator(); iterator
						.hasNext();)
				{
					JSONObject ordersTemp = (JSONObject) iterator.next();
					JSONObject ordersResJSON = this.convertOrders(moneytype, ordersTemp);
					ordersResArr.add(ordersResJSON);
				}
				dataJSON.put("ordersList", ordersResArr);
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			}else if(result.indexOf("\"code\":3001") != -1)
			{
				JSONObject dataJSON = new JSONObject();
				JSONArray ordersResArr = new JSONArray();
				dataJSON.put("ordersList", ordersResArr);
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			}
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取用户挂单.--原始信息:" + result
				+ ";转换后目标信息:" + resultJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取用户挂单失败."
					+ paramsMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 行情API
	 */
	public JSONObject queryTicker(Map<String, String> paramsMap)
	{

		HTTPUtil httpUtil = new HTTPUtil();
		String symbol = paramsMap.get("symbol");
		if ("ltc_cny".equalsIgnoreCase(symbol))
		{
			paramsMap.put("requestURL",
					"https://www.okcoin.com/api/ticker.do?symbol=ltc_cny");
		} else
		{
			paramsMap.put("requestURL", "https://www.okcoin.com/api/ticker.do");
		}

		paramsMap.remove("symbol");

		JSONObject resultJSON = new JSONObject();
		try
		{
			String result = httpUtil.methodGet(paramsMap);
			resultJSON = (JSONObject) JSON.parse(result);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "行情API失败."
					+ paramsMap, e);
		}
		return resultJSON;
	}

	/**
	 * depth(市场深度)
	 */
	public JSONObject queryDepth(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		
		HTTPUtil httpUtil = new HTTPUtil();

		Map<String, String> requestMap = new HashMap<String, String>();
		String moneytype = this.convertMoneyType(paramsMap.get("moneytype"));
		requestMap.put("requestURL", serverUrl + "/data/v1/depth?market="+ moneytype +"&size=10");
		JSONObject responseJSON = new JSONObject();
		String result = "" ; 
		try
		{
			result = httpUtil.methodGet(requestMap);
			responseJSON = (JSONObject) JSON.parse(result);

			// 拼装符合条件的json数据
			JSONObject dataJSON = new JSONObject();

			int count = 0;
			// 按照规则排序的容器
			Map<Double, JSONObject> asksMap = new TreeMap<Double, JSONObject>();
			JSONArray asksResArr = new JSONArray();
			JSONArray asksArr = (JSONArray) responseJSON.get("asks");
			for (Iterator<?> iterator = asksArr.iterator(); iterator.hasNext();)
			{
				JSONArray asksTempArr = (JSONArray) iterator.next();
				JSONObject askJSON = new JSONObject();
				askJSON.put("count", "1");
				askJSON.put("vol", asksTempArr.get(1) + "");
				askJSON.put("rate", asksTempArr.get(0) + "");

				asksMap.put(Double.valueOf(askJSON.get("rate") + ""), askJSON);
			}

			for (Iterator<?> iterator = asksMap.entrySet().iterator(); iterator
					.hasNext();)
			{
				Entry<?, ?> me = (Entry<?, ?>) iterator.next();
				double key = (Double) me.getKey();

				if (count >= ConstatFinalUtil.PAGE_BATCH_SIZE)
				{
					break;
				}

				JSONObject asksTemp = (JSONObject) me.getValue();

				asksResArr.add(asksTemp);
				count++;
			}

			dataJSON.put("asks", asksResArr);

			// 按照规则排序的容器
			count = 0;
			Map<Double, JSONObject> bidsMap = new TreeMap<Double, JSONObject>(
					new Comparator<Double>()
					{
						@Override
						public int compare(Double o1, Double o2)
						{
							return o2.compareTo(o1);
						}
					});
			JSONArray bidsResArr = new JSONArray();
			JSONArray bidsArr = (JSONArray) responseJSON.get("bids");
			for (Iterator<?> iterator = bidsArr.iterator(); iterator.hasNext();)
			{
				JSONArray bidsTempArr = (JSONArray) iterator.next();
				JSONObject bidsJSON = new JSONObject();
				bidsJSON.put("count", "1");
				bidsJSON.put("vol", bidsTempArr.get(1) + "");
				bidsJSON.put("rate", bidsTempArr.get(0) + "");

				bidsMap.put(Double.valueOf(bidsJSON.get("rate") + ""), bidsJSON);
			}

			for (Iterator<?> iterator = bidsMap.entrySet().iterator(); iterator
					.hasNext();)
			{
				Entry<?, ?> me = (Entry<?, ?>) iterator.next();
				double key = (Double) me.getKey();

				if (count >= ConstatFinalUtil.PAGE_BATCH_SIZE)
				{
					break;
				}

				JSONObject bidsTemp = (JSONObject) me.getValue();

				bidsResArr.add(bidsTemp);
				count++;
			}
			dataJSON.put("bids", bidsResArr);

			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			resultJSON.put("data", dataJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "depth(市场深度)失败."
					+ paramsMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * trades的格式,历史交易记录
	 */
	public JSONArray queryHistoryTrades(String symbol,
			Map<String, String> paramsMap)
	{

		HTTPUtil httpUtil = new HTTPUtil();

		if ("ltc_cny".equalsIgnoreCase(symbol))
		{
			paramsMap.put("requestURL",
					" https://www.okcoin.com/api/trades.do?symbol=ltc_cny");
		} else
		{
			paramsMap.put("requestURL",
					" https://www.okcoin.com/api/trades.do?");
		}
		JSONArray resultJSON = new JSONArray();
		try
		{
			String result = httpUtil.methodGet(paramsMap);
			resultJSON = (JSONArray) JSON.parse(result);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "历史交易记录失败."
					+ paramsMap, e);
		}
		return resultJSON;
	}

	/**
	 * 获取所有的行情
	 * 
	 * @return
	 */
	public JSONObject getTrade()
	{
		JSONObject resultJSON = new JSONObject();
		String hlUserURL = "http://www.okcoin.com/";
		try
		{
			// 请求服务
			Connection conn = Jsoup.connect(hlUserURL);
			conn.timeout(ConstatFinalUtil.REQ_CONNECT_TIMEOUT);

			conn.header(
					"Cookie",
					"abtest=\"16,34\\|18,41\\|22,60\\|14,29\\|15,33\"; userParamsCookie=trace=g1081; aburl=1; cy=160; __utma=1.543273261.1351234587.1351234587.1351234587.1; __utmz=1.1351234587.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); _hc.v=\"\"22a8bf0b-7caf-468f-ac01-88534d000d02.1351216735\"\"; cye=zhengzhou; is=743412785865");
			conn.header(
					"User-Agent",
					"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727; .NET CLR 3.0.04506.648; .NET CLR 3.5.21022)");

			Document docu = conn.get();

			/* 买入成效价 */
			JSONArray mairuArr = new JSONArray();
			Elements mairuTrades = docu
					.getElementsByAttributeValue("class", "transaction").get(0)
					.child(1).children();
			for (Iterator<?> iterator = mairuTrades.iterator(); iterator.hasNext();)
			{
				Element eleTemp = (Element) iterator.next();
				Elements tdsTemp = eleTemp.children();
				JSONObject mairuTempJSON = new JSONObject();
				// 1买入
				mairuTempJSON.put("type", tdsTemp.get(0).text());
				mairuTempJSON.put("price", tdsTemp.get(1).text());
				mairuTempJSON.put("num", tdsTemp.get(2).text());
				mairuArr.add(mairuTempJSON);
			}
			resultJSON.put("mairu", mairuArr);

			/* 买出成效价 */
			JSONArray maichuArr = new JSONArray();
			Elements maichuTrades = docu
					.getElementsByAttributeValue("class", "transaction").get(1)
					.child(1).children();
			for (Iterator<?> iterator = maichuTrades.iterator(); iterator
					.hasNext();)
			{
				Element eleTemp = (Element) iterator.next();
				Elements tdsTemp = eleTemp.children();
				JSONObject maichuTempJSON = new JSONObject();
				// 1买入
				maichuTempJSON.put("type", tdsTemp.get(0).text());
				maichuTempJSON.put("price", tdsTemp.get(1).text());
				maichuTempJSON.put("num", tdsTemp.get(2).text());
				maichuArr.add(maichuTempJSON);
			}
			resultJSON.put("maichu", maichuArr);

		} catch (IOException e)
		{
			ConstatFinalUtil.OUTER_LOG.error("获取OKcoin的行情失败了", e);
		}
		return resultJSON;
	}

	public void setAuthJSON(JSONObject authJSON)
	{
		this.authJSON = authJSON;
	}

	@Override
	public String toString()
	{
		return "ZbTradeServiceImplay [findid()=" + findWebsiteId() + "]";
	}

	@Override
	public JSONObject getOrdersSingle(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = super.getOrdersSingle(paramsMap);
		if(resultJSON != null)
		{
			return resultJSON ; 
		}
		resultJSON = new JSONObject() ;
		
		HTTPUtil httpUtil = new HTTPUtil();
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("requestURL", this.serverTradeUrl + "/api/getOrder");
		
		requestMap.put("method", "getOrder");
		requestMap.put("accesskey", authJSON.get("accesskey") + "");
		
		requestMap.put("id", paramsMap.get("order_id"));
		
		String moneytype = this.convertMoneyType(paramsMap.get("moneytype"));
		requestMap.put("currency", moneytype + "");
		
		String sign = this.authSign(requestMap);
		requestMap.put("sign", sign);
		
		requestMap.put("reqTime", System.currentTimeMillis() + "");
		String result = "" ; 
		try
		{
			result = httpUtil.methodPost(Collections.EMPTY_MAP , requestMap);
			JSONObject dataJSON = new JSONObject();
			JSONObject ordersTemp = (JSONObject) JSON.parse(result);
			
			JSONObject ordersResJSON = this.convertOrders(moneytype, ordersTemp);
			dataJSON.put("orders", ordersResJSON);
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取订单详情.--原始信息:" + result
				+ ";转换后目标信息:" + resultJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取订单详情失败."
					+ paramsMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	@Override
	public String parseMoneyType(String moneytype)
	{
		return null;
	}

	@Override
	public String findWebSiteName()
	{
		return null;
	}

	@Override
	public JSONObject convertOrders(String moneytype, JSONObject ordersTemp)
	{
		JSONObject ordersResJSON = new JSONObject();
		ordersResJSON.put("order_id", ordersTemp.get("id") + "");
		ordersResJSON.put("price", ordersTemp.get("price") + "");
		ordersResJSON.put("amount", ordersTemp.get("total_amount") + "");
		ordersResJSON.put("deal_amount", ordersTemp.get("trade_amount") + "");
		
		/* avg_price:实际成交的价格 */
		double avgTotalPrice = Double.valueOf(ordersTemp.get("trade_money") + "");
		double deal_amount = Double.valueOf(ordersResJSON.get("deal_amount") + "");
		double avg_price = 0 ;
		if(deal_amount > 0)
		{
			avg_price = avgTotalPrice / deal_amount ; 
		}
		/* 实际价格往高了算 */
		avg_price = this.doubleOperUtil.round(avg_price, 3, RoundingMode.UP.ordinal()) ; 
		ordersResJSON.put("avg_price", avg_price);
		
		//处理下单时间
		Date createDate = new Date(Long.valueOf(ordersTemp.get("trade_date") + ""));
		ordersResJSON.put("create_date", this.dateUtil.formatDateTime(createDate));
		
		String type = ordersTemp.get("type") + "";
		//1/0[buy/sell]
		if("1".equalsIgnoreCase(type))
		{
			type = TradeEnum.TRADE_TYPE_BUY.getValue() + "" ; 
		}else if("0".equalsIgnoreCase(type))
		{
			type = TradeEnum.TRADE_TYPE_SELL.getValue() + "" ; 
		}
		ordersResJSON.put("type", type + "");
		
		/* 状态
		 * 挂单状态(0：待成交,1：取消,2：交易完成,3：待成交未交易部份)
		 *  */
		String status = ordersTemp.get("status") + ""; 
		if("0".equalsIgnoreCase(status))
		{
			status = TradeEnum.TRADE_STATUS_DEAL_NO.getValue() + "" ; 
		}else if("1".equalsIgnoreCase(status))
		{
			status = TradeEnum.TRADE_STATUS_CANCEL.getValue() + "" ; 
		}else if("2".equalsIgnoreCase(status))
		{
			status = TradeEnum.TRADE_STATUS_DEALED.getValue() + "" ; 
		}else if("3".equalsIgnoreCase(status))
		{
			Double amount = ordersResJSON.getDouble("amount");
			if(deal_amount > 0 && deal_amount < amount)
			{
				/* 部分成交 */
				status = TradeEnum.TRADE_STATUS_DEAL_PART.getValue() + "" ; 
			}else
			{
				status = TradeEnum.TRADE_STATUS_DEAL_NO.getValue() + "" ; 
			}
		}
		
		ordersResJSON.put("status", status);
		return ordersResJSON ; 
	}
	
	@Override
	public JSONObject findAllTransPair(Map<String, String> paramsMap)
	{
		JSONObject resultJSON = super.findAllTransPair(paramsMap);
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		String methodUrl = "/data/v1/markets" ; 
		
		HTTPUtil httpUtil = new HTTPUtil();
		/* 公共代码 */
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("requestURL", serverUrl + methodUrl);
		String result = "" ; 
		try
		{
			//result = httpUtil.methodPost(Collections.EMPTY_MAP , paramsMap);
			result = httpUtil.methodGet(Collections.EMPTY_MAP,requestMap);
			JSONObject responseJSON = (JSONObject) JSON.parse(result);
			
			JSONObject dataJSON = new JSONObject();
			for (Iterator iterator = responseJSON.entrySet().iterator(); iterator.hasNext();)
			{
				Entry me = (Entry) iterator.next() ; 
				String key = me.getKey() + ""; 
				JSONObject valJSON = (JSONObject) me.getValue() ; 
				/*
				 * priceScale : 价格小数位数
					amountScale : 数量小数位数
				 * 
				 * {
				        //交易货币币种,小写;
				        "base_currency":"BTC",
				        //币对名称,小写;
				        "instrument_id":"BTC-USDT",
				        //最小交易数量
				        "min_size":"0.001",
				        //计价货币币种,小写;
				        "quote_currency":"USDT",
				        //交易货币数量精度
				        "size_increment":"0.00000001",
				        //交易价格精度
				        "tick_size":"0.1"
				    }
				 * */
				JSONObject pairJSON = new JSONObject();
				String moneyType = key;
				
				if(!moneyType.endsWith("usdt"))
				{
					continue ; 
				}
				
				pairJSON.put("name", moneyType);
				pairJSON.put("min_size", "0");
				pairJSON.put("base_currency", "");
				pairJSON.put("quote_currency", "");
				pairJSON.put("size_increment", valJSON.get("amountScale"));
				pairJSON.put("tick_size", valJSON.get("priceScale"));
				/* 存储到结果中 */
				dataJSON.put(pairJSON.get("name") + "", pairJSON);
			}
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			//ConstatFinalUtil.OUTER_LOG.info(this.findid() + "--获取用户信息返回.--" + responseJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取用户信息失败."
					+ requestMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON ; 
	}
}
