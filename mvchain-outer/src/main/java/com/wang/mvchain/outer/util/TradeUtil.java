package com.wang.mvchain.outer.util;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.outer.service.ITradeService;
import com.wang.mvchain.outer.service.implay.BiBoxTradeServiceImplay;
import com.wang.mvchain.outer.service.implay.BinanceTradeServiceImplay;
import com.wang.mvchain.outer.service.implay.BtcTradeTradeServiceImplay;
import com.wang.mvchain.outer.service.implay.CoinexTradeServiceImplay;
import com.wang.mvchain.outer.service.implay.GateTradeServiceImplay;
import com.wang.mvchain.outer.service.implay.HuobiTradeServiceImplay;
import com.wang.mvchain.outer.service.implay.IdaxTradeServiceImplay;
import com.wang.mvchain.outer.service.implay.MxcTradeServiceImplay;
import com.wang.mvchain.outer.service.implay.OkexTradeServiceImplay;
import com.wang.mvchain.outer.service.implay.ZbTradeServiceImplay;

/**
 * 所有交易站的封装
 * @author wangsh
 *
 */
public class TradeUtil
{
	/* 主币种
	 * 以美元为准,计算的都是美元,也不用抓取汇率了
	 * 如果是以cny为准,需要开启抓取汇率
	 *  */
	public static final int MAIN_COINID = 2 ;
	/*人民币币种id*/
	public static final int CNY_COINID = 3 ;
	/*btc币种id*/
	public static final int BTC_COINID = 1 ;
	/*ltc币种id*/
	public static final int LTC_COINID = 4 ;
	/*usd币种id*/
	public static final int USD_COINID = 2 ;
	
	/* 存储所有btc的交易网站对象 */
	public static final Map<String, ITradeService> tradeServiceMap = new TreeMap<String, ITradeService>();
	
	static 
	{
		/* 将所有的btc交易网站的临时存储 */
		Map<String, ITradeService> tempMap = new HashMap<String, ITradeService>();
		/*tempMap.put("3", new HuobiTradeServiceImplay());
		tempMap.put("4", new BtceTradeServiceImplay());
		tempMap.put("5", new BityesTradeServiceImplay());
		tempMap.put("6", new BitstampTradeServiceImplay());
		tempMap.put("7", new BitfinexTradeServiceImplay());
		tempMap.put("8", new GateTradeServiceImplay());
		tempMap.put("9", new BtcChinaTradeServiceImplay());
		tempMap.put("10", new ZbTradeServiceImplay());
		tempMap.put("11", new BtcTradeTradeServiceImplay());
		tempMap.put("13", new YunbiTradeServiceImplay());*/
		
		tempMap.put("13", new OkexTradeServiceImplay());
		tempMap.put("14", new HuobiTradeServiceImplay());
		tempMap.put("15", new GateTradeServiceImplay());
		tempMap.put("16", new ZbTradeServiceImplay());
		tempMap.put("17", new BtcTradeTradeServiceImplay());
		tempMap.put("18", new BinanceTradeServiceImplay());
		tempMap.put("20", new MxcTradeServiceImplay());
		tempMap.put("22", new IdaxTradeServiceImplay());
		tempMap.put("23", new BiBoxTradeServiceImplay());
		tempMap.put("24", new CoinexTradeServiceImplay());
		
		/*从配置文件中读取*/
		String websiteId = ConstatFinalUtil.SYS_BUNDLE.getString("trade.website.ids");
		String[] websiteIds = websiteId.split(",");
		for (int i = 0; i < websiteIds.length; i++)
		{
			String webId = websiteIds[i];
			if(tempMap.containsKey(webId))
			{
				//如果包含,就放到目录容器中
				tradeServiceMap.put(webId, tempMap.get(webId));
			}
		}
		//一个也木有放,表示木有配置
		if(tradeServiceMap.size() == 0 )
		{
			tradeServiceMap.putAll(tempMap);
		}
	}
}
