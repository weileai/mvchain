package com.wang.mvchain.outer.service.implay;

import java.util.Date;
import java.util.Map;
import java.util.Random;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.common.util.DateUtil;
import com.wang.mvchain.common.util.DoubleOperUtil;
import com.wang.mvchain.common.util.RegexUtil;
import com.wang.mvchain.outer.service.ITradeService;

/**
 * 所有交易类的父类
 * @author wangsh
 *
 */
public abstract class BaseTradeService implements ITradeService 
{
	/**
	 * 日期工具类
	 */
	protected DateUtil dateUtil = new DateUtil();
	protected RegexUtil regexUtil = new RegexUtil() ;
	protected DoubleOperUtil doubleOperUtil = new DoubleOperUtil() ; 
	/**
	 * 登陆验证的信息,可从网站模板中查找
	 */
	protected JSONObject authJSON;
	
	/**
	 * 获取请求时间
	 * 获取nonce
	 * @return
	 */
	protected String findReqtime()
	{
		return System.currentTimeMillis() + "";
	}
	
	public String convertMoneyType(String moneytypeSelf)
	{
		return moneytypeSelf;
	}
	
	/**
	 * 将交易网站中的订单对象转换为系统需要的类型
	 * @param moneytype
	 * @param ordersTemp
	 * @return
	 */
	public abstract JSONObject convertOrders(String moneytype, JSONObject ordersTemp);
	
	@Override
	public String parseMoneyType(String moneytypeTrade)
	{
		return moneytypeTrade;
	}

	public JSONObject getAuthJSON()
	{
		return authJSON;
	}

	public void setAuthJSON(JSONObject authJSON)
	{
		this.authJSON = authJSON;
	}
	
	/**
	 * 全部是模拟查询挂单
	 */
	@Override
	public JSONObject getOrders(Map<String, String> paramsMap)
	{
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		
		//开发版本
		if("1".equalsIgnoreCase(ConstatFinalUtil.SYS_BUNDLE.getString("dev.version")))
		{
			//模拟假数据
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("ordersList", new JSONArray());
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "---模拟下单(getOrders)---" + paramsMap);
			return resultJSON ; 
		}
		return null ; 
	}

	/**
	 * 全部是模拟查询订单
	 */
	@Override
	public JSONObject getOrdersSingle(Map<String, String> paramsMap)
	{
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		
		//开发版本
		if("1".equalsIgnoreCase(ConstatFinalUtil.SYS_BUNDLE.getString("dev.version")))
		{
			//模拟假数据
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("ordersList", new JSONArray());
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "---模拟下单---" + paramsMap);
			return resultJSON ; 
		}
		return null;
	}

	/**
	 * 全部是模拟取消订单
	 */
	@Override
	public JSONObject cancelOrder(Map<String, String> paramsMap)
	{
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		
		//开发版本
		if("1".equalsIgnoreCase(ConstatFinalUtil.SYS_BUNDLE.getString("dev.version")))
		{
			//模拟假数据
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("order_id", paramsMap.get("order_id"));
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "---模拟下单(cancelOrder)---" + paramsMap);
			return resultJSON ; 
		}
		return null;
	}

	/**
	 * 全部是下单
	 */
	@Override
	public JSONObject trade(Map<String, String> paramsMap)
	{
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		//开发版本
		if("1".equalsIgnoreCase(ConstatFinalUtil.SYS_BUNDLE.getString("dev.version")))
		{
			//模拟假数据
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("order_id", new Random().nextInt(10000));
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "---模拟下单(trade)---" + paramsMap);
			return resultJSON;
		}
		return null ; 
	}

	@Override
	public JSONObject findAllTransPair(Map<String, String> paramsMap)
	{
		JSONObject resultJSON = new JSONObject();
		resultJSON.put("code", "1");
		resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("1"));
		return resultJSON;
	}
}
