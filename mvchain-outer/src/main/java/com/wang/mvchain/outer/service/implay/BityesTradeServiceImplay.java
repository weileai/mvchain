package com.wang.mvchain.outer.service.implay;

import java.math.RoundingMode;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.TreeMap;

import org.apache.commons.codec.digest.DigestUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.common.util.HTTPUtil;
import com.wang.mvchain.outer.service.ITradeService;

/**
 * 火币网的国际版
 * 
 * @author wangsh
 *
 */
public class BityesTradeServiceImplay extends BaseTradeService implements ITradeService
{
	//登陆验证的信息,可从网站模板中查找
	private JSONObject authJSON;

	@Override
	public int findWebsiteId()
	{
		return 5;
	}

	/**
	 * 获取签名方式
	 * 
	 * @return
	 */
	private String authSign(Map<String, String> condMap)
	{
		StringBuffer sb = new StringBuffer();
		
		//created10位时间戳
		condMap.put("created", (System.currentTimeMillis() + "").substring(0, 10));
		
		Map<String, String> signMap = new TreeMap<String, String>();
		signMap.putAll(condMap);
		for (Iterator iterator = signMap.entrySet().iterator(); iterator
				.hasNext();)
		{
			Entry me = (Entry) iterator.next();
			String key = me.getKey() + "";
			String value = me.getValue() + "";

			if ("requestURL".equalsIgnoreCase(key))
			{
				continue;
			}

			sb.append(key + "=" + value + "&");
		}
		sb.append("secret_key=" + authJSON.get("secret_key") + "&");
		if (sb.toString().lastIndexOf("&") != -1)
		{
			sb.delete(sb.lastIndexOf("&"), sb.length());
		}
		//ConstatFinalUtil.OUTER_LOG.info(sb);

		return DigestUtils.md5Hex(sb.toString()).toLowerCase();
	}

	/**
	 * 获取用户基本信息
	 * var res = {
			"available_btc_display" : "0.0000",
			"available_cny_display" : "0.26",
			"available_ltc_display" : "0.0000",
			"frozen_btc_display" : "0.0000",
			"frozen_cny_display" : "0.00",
			"frozen_ltc_display" : "0.0000",
			"loan_btc_display" : "0.0000",
			"loan_cny_display" : "0.00",
			"loan_ltc_display" : "0.0000",
			"net_asset" : "0.26",
			"total" : "0.26"
		}
	 * @return
	 */
	public JSONObject getUserInfo(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
				
		HTTPUtil httpUtil = new HTTPUtil();
		paramsMap.put("requestURL", "https://api.bityes.com/apiv2");
		paramsMap.put("access_key", authJSON.get("access_key") + "");
		paramsMap.put("method", "get_account_info");
		String sign = authSign(paramsMap);
		paramsMap.put("sign", sign);

		JSONObject responseJSON = new JSONObject();
		String result = "" ;
		try
		{
			result = httpUtil.methodPost(Collections.EMPTY_MAP , paramsMap);
			responseJSON = (JSONObject) JSON.parse(result);
			
			if(responseJSON == null)
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6"));
				return responseJSON;
			}
			
			//按照接口的格式拼装json
			JSONObject dataJSON = new JSONObject();
			JSONObject balanceJSON = new JSONObject();
			JSONObject fundJSON = new JSONObject();
			
			if(!"null".equalsIgnoreCase(responseJSON.get("available_usd") + ""))
			{
				balanceJSON.put("usd", responseJSON.get("available_usd") + "");
			}else
			{
				balanceJSON.put("usd", "0");
			}
			
			if(!"null".equalsIgnoreCase(responseJSON.get("available_btc") + ""))
			{
				balanceJSON.put("btc", responseJSON.get("available_btc") + "");
			}else
			{
				balanceJSON.put("btc", "0");
			}
			
			if(!"null".equalsIgnoreCase(responseJSON.get("available_ltc") + ""))
			{
				balanceJSON.put("ltc", responseJSON.get("available_ltc") + "");
			}else
			{
				balanceJSON.put("ltc", "0");
			}
			
			if(!"null".equalsIgnoreCase(responseJSON.get("frozen_usd") + ""))
			{
				fundJSON.put("usd", responseJSON.get("frozen_usd") + "");
			}else
			{
				fundJSON.put("usd", "0");
			}
			
			if(!"null".equalsIgnoreCase(responseJSON.get("frozen_btc") + ""))
			{
				fundJSON.put("btc", responseJSON.get("frozen_btc") + "");
			}else
			{
				fundJSON.put("btc", "0");
			}
			
			if(!"null".equalsIgnoreCase(responseJSON.get("frozen_ltc") + ""))
			{
				fundJSON.put("ltc", responseJSON.get("frozen_ltc") + "");
			}else
			{
				fundJSON.put("ltc", "0");
			}
			
			dataJSON.put("balance", balanceJSON);
			dataJSON.put("fund", fundJSON);
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取用户信息失败."
					+ paramsMap + ",返回:" + result + ";原始信息:" + result , e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 下单交易
	 * 
	 * @return
	 */
	public JSONObject trade(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		
		//开发版本
		if("1".equalsIgnoreCase(ConstatFinalUtil.SYS_BUNDLE.getString("dev.version")))
		{
			//模拟假数据
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("order_id", new Random().nextInt(10000));
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "---模拟下单---" + paramsMap);
			return resultJSON ; 
		}
		
		HTTPUtil httpUtil = new HTTPUtil();
		Map<String, String> requestMap = new HashMap<String, String>();
		
		requestMap.put("requestURL", "https://api.bityes.com/apiv2");
		requestMap.put("access_key", authJSON.get("access_key") + "");
		
		String moneytype = paramsMap.get("moneytype") + "";
		if(moneytype.indexOf("_") != -1)
		{
			moneytype = moneytype.substring(0, moneytype.indexOf("_"));
		}
		//币种 1 比特币 2 莱特币
		if("btc".equalsIgnoreCase(moneytype))
		{
			requestMap.put("coin_type", "1");
		}else if("ltc".equalsIgnoreCase(moneytype)) 
		{
			requestMap.put("coin_type", "2");
		}
		
		// 买还是卖.0:买,1:卖
		if ("0".equalsIgnoreCase(paramsMap.get("type")))
		{
			requestMap.put("method", "buy");
		} else if ("1".equalsIgnoreCase(paramsMap.get("type")))
		{
			requestMap.put("method", "sell");
		}
		
		double price = doubleOperUtil.round(Double.valueOf(paramsMap.get("price") + ""), 2, RoundingMode.DOWN.ordinal());
		
		requestMap.put("price", price + "");
		requestMap.put("amount", paramsMap.get("amount") + "");
		
		String sign = authSign(requestMap);
		requestMap.put("sign", sign);
		String result = "" ; 
		try
		{
			result = httpUtil.methodPost(Collections.EMPTY_MAP , requestMap);
			JSONObject responseJSON = (JSONObject) JSON.parse(result);
			
			if(responseJSON == null)
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ",原始信息:" + responseJSON);
				return responseJSON;
			}
			
			if(!"null".equalsIgnoreCase(responseJSON.get("code") + ""))
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ",原始信息:" + responseJSON);
				return resultJSON;
			}
			
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("order_id", responseJSON.get("id"));
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "--下单交易返回--原始信息:" + result
					+ ";转换后目标信息:" + resultJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "下单交易失败."
					+ requestMap + ";原始信息:" + result , e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 撤销订单
	 * 
	 * var res = {
			"code" : 26,
			"msg" : "该委托不存在",
			"result" : "fail",
			"time" : 1417096047
		}
	 * @return
	 */
	public JSONObject cancelOrder(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		//开发版本
		if("1".equalsIgnoreCase(ConstatFinalUtil.SYS_BUNDLE.getString("dev.version")))
		{
			//模拟假数据
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("order_id", paramsMap.get("order_id"));
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "---模拟下单---" + paramsMap);
			return resultJSON ; 
		}
		
		HTTPUtil httpUtil = new HTTPUtil();
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("requestURL", "https://api.bityes.com/apiv2");
		requestMap.put("access_key", authJSON.get("access_key") + "");
		requestMap.put("method", "cancel_order");
		
		String moneytype = paramsMap.get("moneytype") + "";
		if(moneytype.indexOf("_") != -1)
		{
			moneytype = moneytype.substring(0, moneytype.indexOf("_"));
		}
		//币种 1 比特币 2 莱特币
		if("btc".equalsIgnoreCase(moneytype))
		{
			requestMap.put("coin_type", "1");
		}else if("ltc".equalsIgnoreCase(moneytype)) 
		{
			requestMap.put("coin_type", "2");
		}
		requestMap.put("id", paramsMap.get("order_id"));
		String sign = authSign(requestMap);
		requestMap.put("sign", sign);

		JSONObject responseJSON = new JSONObject();
		String result = "" ; 
		try
		{
			result = httpUtil.methodPost(Collections.EMPTY_MAP , requestMap);
			responseJSON = (JSONObject) JSON.parse(result);
			
			if(responseJSON == null)
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6"));
				return responseJSON;
			}
			
			if(!"null".equalsIgnoreCase(responseJSON.get("code") + ""))
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6"));
				return resultJSON;
			}
			
			if("success".equalsIgnoreCase(responseJSON.get("result") + ""))
			{
				JSONObject dataJSON = new JSONObject();
				dataJSON.put("order_id", paramsMap.get("order_id"));
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + responseJSON.get("message"));
				
				ConstatFinalUtil.SYS_LOG.info("---订单取消失败,默认取的是假数据--" + result);
				
				JSONObject dataJSON = new JSONObject();
				dataJSON.put("order_id", "-1");
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			}
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "--取消订单息返回.--原始信息:" + result
					+ ";转换后目标信息:" + resultJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "撤销订单失败."
					+ paramsMap + ";原始信息:" + result , e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 获取用户挂单
	 * 
	 * @param args
	 */
	public JSONObject getOrders(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		
		//开发版本
		if("1".equalsIgnoreCase(ConstatFinalUtil.SYS_BUNDLE.getString("dev.version")))
		{
			//模拟假数据
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("ordersList", new JSONArray());
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "---模拟获取数据---" + paramsMap);
			return resultJSON ; 
		}
		
		HTTPUtil httpUtil = new HTTPUtil();
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("requestURL", "https://api.bityes.com/apiv2");
		requestMap.put("access_key", authJSON.get("access_key") + "");
		requestMap.put("method", "get_orders");
		
		String moneytype = paramsMap.get("moneytype") + "";
		if(moneytype.indexOf("_") != -1)
		{
			moneytype = moneytype.substring(0, moneytype.indexOf("_"));
		}
		//币种 1 比特币 2 莱特币
		if("btc".equalsIgnoreCase(moneytype))
		{
			requestMap.put("coin_type", "1");
		}else if("ltc".equalsIgnoreCase(moneytype)) 
		{
			requestMap.put("coin_type", "2");
		}

		String sign = authSign(requestMap);
		requestMap.put("sign", sign);
		String result = "" ; 
		try
		{
			result = httpUtil.methodPost(Collections.EMPTY_MAP , requestMap);
			JSONObject responseJSON = JSON.parseObject(result);
			//订单列表
			JSONArray responseArr = (JSONArray) responseJSON.get("orders");
			JSONObject dataJSON = new JSONObject();
			JSONArray ordersResArr = new JSONArray();
			for (Iterator iterator = responseArr.iterator(); iterator
					.hasNext();)
			{
				JSONObject ordersTemp = (JSONObject) iterator.next();
				JSONObject ordersResJSON = new JSONObject();
				ordersResJSON.put("order_id", ordersTemp.get("id") + "");
				ordersResJSON.put("price", ordersTemp.get("order_price") + "");
				ordersResJSON.put("amount", ordersTemp.get("order_amount") + "");
				ordersResJSON.put("deal_amount", ordersTemp.get("processed_amount") + "");
				ordersResJSON.put("symbol", moneytype);
				String type = ordersTemp.get("type") + "";
				//1买 2卖
				if("1".equalsIgnoreCase(type))
				{
					type = "0" ; 
				}else if("2".equalsIgnoreCase(type))
				{
					type = "1" ; 
				}
				ordersResJSON.put("type", ordersTemp.get("type") + "");
				ordersResJSON.put("status", ordersTemp.get("status") + "");
				
				ordersResArr.add(ordersResJSON);
			}
			
			dataJSON.put("ordersList", ordersResArr);
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "--获取挂单息返回.--原始信息:" + result
					+ ";转换后目标信息:" + resultJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取用户挂单失败."
					+ paramsMap + ";原始信息:" + result , e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 行情API
	 */
	public JSONObject queryTicker(Map<String, String> paramsMap)
	{

		HTTPUtil httpUtil = new HTTPUtil();
		String symbol = paramsMap.get("symbol");
		if ("ltc_cny".equalsIgnoreCase(symbol))
		{
			paramsMap.put("requestURL",
					"https://www.okcoin.com/api/ticker.do?symbol=ltc_cny");
		} else
		{
			paramsMap.put("requestURL", "https://www.okcoin.com/api/ticker.do");
		}

		paramsMap.remove("symbol");

		JSONObject resultJSON = new JSONObject();
		String result = "" ; 
		try
		{
			result = httpUtil.methodGet(paramsMap);
			resultJSON = (JSONObject) JSON.parse(result);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "行情API失败."
					+ paramsMap + ";原始信息:" + result , e);
		}
		return resultJSON;
	}

	/**
	 * depth(市场深度)
	 */
	public JSONObject queryDepth(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		
		HTTPUtil httpUtil = new HTTPUtil();
		Map<String, String> requestMap = new HashMap<String, String>();
		String moneytype = paramsMap.get("moneytype") + "";
		if("btc_usd".equalsIgnoreCase(moneytype))
		{
			moneytype = "usd_btc";
		}else if("ltc_usd".equalsIgnoreCase(moneytype))
		{
			moneytype = "usd_ltc" ; 
		}
		requestMap.put("requestURL", "https://market.bityes.com/"+ moneytype +"/depth.js?time="+System.currentTimeMillis());

		JSONObject responseJSON = new JSONObject();
		String result = "" ; 
		try
		{
			result = httpUtil.methodGet(requestMap);
			responseJSON = (JSONObject) JSON.parse(result);

			// 拼装符合条件的json数据
			JSONObject dataJSON = new JSONObject();

			int count = 0;
			// 按照规则排序的容器
			Map<Double, JSONObject> asksMap = new TreeMap<Double, JSONObject>();
			JSONArray asksResArr = new JSONArray();
			JSONArray asksArr = (JSONArray) responseJSON.get("asks");
			for (Iterator iterator = asksArr.iterator(); iterator.hasNext();)
			{
				JSONArray asksTempArr = (JSONArray) iterator.next();
				JSONObject askJSON = new JSONObject();
				askJSON.put("count", "1");
				askJSON.put("vol", asksTempArr.get(1) + "");
				askJSON.put("rate", asksTempArr.get(0) + "");

				asksMap.put(Double.valueOf(askJSON.get("rate") + ""), askJSON);
			}

			for (Iterator iterator = asksMap.entrySet().iterator(); iterator
					.hasNext();)
			{
				Entry me = (Entry) iterator.next();
				double key = (Double) me.getKey();

				if (count >= ConstatFinalUtil.PAGE_BATCH_SIZE)
				{
					break;
				}

				JSONObject asksTemp = (JSONObject) me.getValue();

				asksResArr.add(asksTemp);
				count++;
			}

			dataJSON.put("asks", asksResArr);

			// 按照规则排序的容器
			count = 0;
			Map<Double, JSONObject> bidsMap = new TreeMap<Double, JSONObject>(
					new Comparator<Double>()
					{
						@Override
						public int compare(Double o1, Double o2)
						{
							return o2.compareTo(o1);
						}
					});
			JSONArray bidsResArr = new JSONArray();
			JSONArray bidsArr = (JSONArray) responseJSON.get("bids");
			for (Iterator iterator = bidsArr.iterator(); iterator.hasNext();)
			{
				JSONArray bidsTempArr = (JSONArray) iterator.next();
				JSONObject bidsJSON = new JSONObject();
				bidsJSON.put("count", "1");
				bidsJSON.put("vol", bidsTempArr.get(1) + "");
				bidsJSON.put("rate", bidsTempArr.get(0) + "");

				bidsMap.put(Double.valueOf(bidsJSON.get("rate") + ""), bidsJSON);
			}

			for (Iterator iterator = bidsMap.entrySet().iterator(); iterator
					.hasNext();)
			{
				Entry me = (Entry) iterator.next();
				double key = (Double) me.getKey();

				if (count >= ConstatFinalUtil.PAGE_BATCH_SIZE)
				{
					break;
				}

				JSONObject bidsTemp = (JSONObject) me.getValue();

				bidsResArr.add(bidsTemp);
				count++;
			}
			dataJSON.put("bids", bidsResArr);
			
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			
			resultJSON.put("data", dataJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "depth(市场深度)失败."
					+ paramsMap + ";原始信息:" + result , e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * trades的格式,历史交易记录
	 */
	public JSONArray queryHistoryTrades(String symbol,
			Map<String, String> paramsMap)
	{

		HTTPUtil httpUtil = new HTTPUtil();

		if ("ltc_cny".equalsIgnoreCase(symbol))
		{
			paramsMap.put("requestURL",
					" https://www.okcoin.com/api/trades.do?symbol=ltc_cny");
		} else
		{
			paramsMap.put("requestURL",
					" https://www.okcoin.com/api/trades.do?");
		}
		JSONArray resultJSON = new JSONArray();
		String result = "" ; 
		try
		{
			result = httpUtil.methodGet(paramsMap);
			resultJSON = (JSONArray) JSON.parse(result);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "历史交易记录失败."
					+ paramsMap + ";原始信息:" + result , e);
		}
		return resultJSON;
	}

	/**
	 * 获取所有的行情
	 * 
	 * @return
	 */
	public JSONObject getTrade()
	{
		return new JSONObject();
	}

	public void setAuthJSON(JSONObject authJSON)
	{
		this.authJSON = authJSON;
	}

	@Override
	public String convertMoneyType(String moneytype)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toString()
	{
		return "BityesTradeServiceImplay [findid()=" + findWebsiteId() + "]";
	}

	@Override
	public JSONObject getOrdersSingle(Map<String, String> paramsMap)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String parseMoneyType(String moneytype)
	{
		return null;
	}

	@Override
	public String findWebSiteName()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JSONObject convertOrders(String moneytype, JSONObject ordersTemp)
	{
		// TODO Auto-generated method stub
		return null;
	}
}
