package com.wang.mvchain.outer.service.implay;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.TreeMap;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.common.util.EncryptUtil;
import com.wang.mvchain.common.util.HTTPUtil;
import com.wang.mvchain.outer.service.ITradeService;

/**
 * Okcoin的API工具
 * 
 * @author wangsh
 *
 */
public class YunbiTradeServiceImplay extends BaseTradeService implements ITradeService
{
	//登陆验证的信息,可从网站模板中查找
	private JSONObject authJSON;

	@Override
	public int findWebsiteId()
	{
		return 13;
	}

	/**
	 * 获取签名方式
	 * 
	 * @return
	 */
	private String authSign(Map<String, String> condMap)
	{
		StringBuffer sb = new StringBuffer();
		Map<String, String> signMap = new TreeMap<String, String>();
		signMap.putAll(condMap);
		for (Iterator iterator = signMap.entrySet().iterator(); iterator
				.hasNext();)
		{
			Entry me = (Entry) iterator.next();
			String key = me.getKey() + "";
			String value = me.getValue() + "";

			if ("requestURL".equalsIgnoreCase(key) || "canonical_verb".equalsIgnoreCase(key) 
					|| "canonical_uri".equalsIgnoreCase(key))
			{
				continue;
			}

			sb.append(key + "=" + value + "&");
		}

		if (sb.toString().lastIndexOf("&") != -1)
		{
			sb.delete(sb.lastIndexOf("&"), sb.length());
		}
		
		sb.insert(0, condMap.get("canonical_verb") + "|" + condMap.get("canonical_uri") + "|");
		//System.out.println(sb);
		return EncryptUtil.hmacSHA256(sb.toString(), authJSON.get("secret_key") + "");
	}

	/**
	 * 获取用户基本信息
	 * 
	 * @return
	 */
	public JSONObject getUserInfo(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
				
		HTTPUtil httpUtil = new HTTPUtil();
		paramsMap.put("requestURL", "https://yunbi.com:443//api/v2/members/me.json");
		paramsMap.put("access_key", authJSON.get("access_key") + "");
		paramsMap.put("tonce", this.getTimestamp() + "");
		//paramsMap.put("tonce", System.currentTimeMillis() + "");
		
		paramsMap.put("canonical_verb", "GET");
		paramsMap.put("canonical_uri", "/api/v2/members/me.json");
		
		String sign = authSign(paramsMap);
		paramsMap.put("signature", sign);
		
		paramsMap.remove("canonical_verb");
		paramsMap.remove("canonical_uri");

		JSONObject responseJSON = new JSONObject();
		String result = "" ; 
		try
		{
			result = httpUtil.methodGet(paramsMap);
			//ConstatFinalUtil.SYS_LOG.info("----" + res);
			responseJSON = (JSONObject) JSON.parse(result);
			
			JSONArray accountsJSON = (JSONArray) responseJSON.get("accounts");
			if(accountsJSON != null)
			{
				JSONObject dataJSON = new JSONObject();
				
				JSONObject balanceJSON = new JSONObject();
				JSONObject fundJSON = new JSONObject();
				
				for (Iterator iterator = accountsJSON.iterator(); iterator.hasNext();)
				{
					JSONObject jsonTemp = (JSONObject) iterator.next();
					if("cny".equalsIgnoreCase(jsonTemp.get("currency") + ""))
					{
						balanceJSON.put("cny", jsonTemp.get("balance") + "");
						fundJSON.put("cny", jsonTemp.get("locked") + "");
					}
					
					if("btc".equalsIgnoreCase(jsonTemp.get("currency") + ""))
					{
						balanceJSON.put("btc", jsonTemp.get("balance") + "");
						fundJSON.put("btc", jsonTemp.get("locked") + "");
					}
					
					if("ltc".equalsIgnoreCase(jsonTemp.get("currency") + ""))
					{
						balanceJSON.put("ltc", jsonTemp.get("balance") + "");
						fundJSON.put("ltc", jsonTemp.get("locked") + "");
					}
					
					if("eth".equalsIgnoreCase(jsonTemp.get("currency") + ""))
					{
						balanceJSON.put("eth", jsonTemp.get("balance") + "");
						fundJSON.put("eth", jsonTemp.get("locked") + "");
					}
					
					if("etc".equalsIgnoreCase(jsonTemp.get("currency") + ""))
					{
						balanceJSON.put("etc", jsonTemp.get("balance") + "");
						fundJSON.put("etc", jsonTemp.get("locked") + "");
					}
				}
				
				dataJSON.put("balance", balanceJSON);
				dataJSON.put("fund", fundJSON);
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + responseJSON.get("error_code"));
			}
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取用户信息失败."
					+ paramsMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 下单交易
	 * 
	 * @return
	 */
	public JSONObject trade(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		//开发版本
		if("1".equalsIgnoreCase(ConstatFinalUtil.SYS_BUNDLE.getString("dev.version")))
		{
			//模拟假数据
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("order_id", new Random().nextInt(10000));
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "---模拟下单---" + paramsMap);
			return resultJSON ; 
		}
		
		HTTPUtil httpUtil = new HTTPUtil();
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("requestURL", "https://yunbi.com:443//api/v2/orders.json");
		
		requestMap.put("access_key", authJSON.get("access_key") + "");
		requestMap.put("tonce", this.getTimestamp() + "");
		
		// 查询货币的类型
		String market = this.convertMoneyType(paramsMap.get("moneytype"));
		requestMap.put("market", market);
		
		// 买还是卖.0:买,1:卖
		if ("0".equalsIgnoreCase(paramsMap.get("type")))
		{
			requestMap.put("side", "buy");
		} else if ("1".equalsIgnoreCase(paramsMap.get("type")))
		{
			requestMap.put("side", "sell");
		}
		requestMap.put("price", paramsMap.get("price"));
		requestMap.put("volume", paramsMap.get("amount"));

		requestMap.put("canonical_verb", "POST");
		requestMap.put("canonical_uri", "/api/v2/orders.json");
		String sign = authSign(requestMap);
		requestMap.put("signature", sign);
		requestMap.remove("canonical_verb");
		requestMap.remove("canonical_uri");
		String result = "" ; 
		try
		{
			result = httpUtil.methodHttpsPost(Collections.EMPTY_MAP , requestMap);
			JSONObject responseJSON = (JSONObject) JSON.parse(result);
			
			if("wait".equalsIgnoreCase(responseJSON.get("state") + ""))
			{
				JSONObject dataJSON = new JSONObject();
				dataJSON.put("order_id", responseJSON.get("id") + "");
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + responseJSON.get("error_code"));
			}
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "--下单交易返回--原始信息:" + result
				+ ";转换后目标信息:" + resultJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "下单交易失败."
					+ requestMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 撤销订单
	 * 
	 * @return
	 */
	public JSONObject cancelOrder(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		
		//开发版本
		if("1".equalsIgnoreCase(ConstatFinalUtil.SYS_BUNDLE.getString("dev.version")))
		{
			//模拟假数据
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("order_id", paramsMap.get("order_id"));
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "---模拟下单---" + paramsMap);
			return resultJSON ; 
		}
		
		HTTPUtil httpUtil = new HTTPUtil();
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("requestURL", "https://yunbi.com//api/v2/order/delete.json");
		
		requestMap.put("access_key", authJSON.get("access_key") + "");
		requestMap.put("tonce", this.getTimestamp() + "");
		requestMap.put("id", paramsMap.get("order_id"));
		
		requestMap.put("canonical_verb", "POST");
		requestMap.put("canonical_uri", "/api/v2/order/delete.json");

		String sign = authSign(requestMap);
		requestMap.put("signature", sign);
		
		requestMap.remove("canonical_verb");
		requestMap.remove("canonical_uri");
		String result = "" ; 
		try
		{
			JSONObject responseJSON = new JSONObject();
			result = httpUtil.methodPost(Collections.EMPTY_MAP ,requestMap);
			//ConstatFinalUtil.SYS_LOG.info("----" + result);
			responseJSON = (JSONObject) JSON.parse(result);
			
			if("cancel".equalsIgnoreCase(responseJSON.get("state") + ""))
			{
				JSONObject dataJSON = new JSONObject();
				dataJSON.put("order_id", responseJSON.get("id"));
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + responseJSON.get("error_code"));
			}
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "--撤销订单返回.--原始信息:" + result
				+ ";转换后目标信息:" + resultJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "撤销订单失败."
					+ paramsMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 获取用户挂单
	 * 
	 * @param args
	 */
	public JSONObject getOrders(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		
		//开发版本
		if("1".equalsIgnoreCase(ConstatFinalUtil.SYS_BUNDLE.getString("dev.version")))
		{
			//模拟假数据
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("ordersList", new JSONArray());
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "---模拟下单---" + paramsMap);
			return resultJSON ; 
		}
		
		HTTPUtil httpUtil = new HTTPUtil();
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("requestURL", "https://yunbi.com:443//api/v2/orders.json");
		
		requestMap.put("access_key", authJSON.get("access_key") + "");
		requestMap.put("tonce", this.getTimestamp() + "");
		//requestMap.put("order_id", paramsMap.get("order_id"));
		
		// 查询货币的类型
		String market = this.convertMoneyType(paramsMap.get("moneytype"));
		requestMap.put("market", market);
		
		requestMap.put("canonical_verb", "GET");
		requestMap.put("canonical_uri", "/api/v2/orders.json");

		String sign = authSign(requestMap);
		requestMap.put("signature", sign);
		
		requestMap.remove("canonical_verb");
		requestMap.remove("canonical_uri");
		String result = "" ; 
		try
		{
			JSONArray responseArr = new JSONArray();
			result = httpUtil.methodGet(requestMap);
			//ConstatFinalUtil.SYS_LOG.info("----" + result);
			responseArr = (JSONArray) JSON.parse(result);
			
			if(responseArr.size() >= 0)
			{
				JSONObject dataJSON = new JSONObject();
				JSONArray ordersResArr = new JSONArray();
				for (Iterator iterator = responseArr.iterator(); iterator
						.hasNext();)
				{
					JSONObject ordersTemp = (JSONObject) iterator.next();
					JSONObject ordersResJSON = new JSONObject();
					ordersResJSON.put("order_id", ordersTemp.get("id") + "");
					ordersResJSON.put("price", ordersTemp.get("price") + "");
					ordersResJSON.put("amount", ordersTemp.get("volume") + "");
					ordersResJSON.put("deal_amount", ordersTemp.get("executed_volume") + "");
					ordersResJSON.put("create_date", ordersTemp.get("created_at") + "");
					ordersResJSON.put("status", ordersTemp.get("state") + "");
					
					ordersResJSON.put("symbol", paramsMap.get("moneytype"));
					String side = ordersTemp.get("side") + "";
					if("buy".equalsIgnoreCase(side))
					{
						side = "0" ; 
					}else if("sell".equalsIgnoreCase(side))
					{
						side = "1" ; 
					}
					ordersResJSON.put("type", side + "");
					
					ordersResArr.add(ordersResJSON);
				}
				
				dataJSON.put("ordersList", ordersResArr);
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + responseArr);
			}
			
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "--获取用户挂单--原始信息:" + result
				+ ";转换后目标信息:" + resultJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取用户挂单失败."
					+ paramsMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 行情API
	 */
	public JSONObject queryTicker(Map<String, String> paramsMap)
	{

		HTTPUtil httpUtil = new HTTPUtil();
		String symbol = paramsMap.get("symbol");
		if ("ltc_cny".equalsIgnoreCase(symbol))
		{
			paramsMap.put("requestURL",
					"https://www.okcoin.com/api/ticker.do?symbol=ltc_cny");
		} else
		{
			paramsMap.put("requestURL", "https://www.okcoin.com/api/ticker.do");
		}

		paramsMap.remove("symbol");

		JSONObject resultJSON = new JSONObject();
		try
		{
			String result = httpUtil.methodGet(paramsMap);
			resultJSON = (JSONObject) JSON.parse(result);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "行情API失败."
					+ paramsMap, e);
		}
		return resultJSON;
	}

	/**
	 * depth(市场深度)
	 */
	public JSONObject queryDepth(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		
		HTTPUtil httpUtil = new HTTPUtil();

		Map<String, String> requestMap = new HashMap<String, String>();
		//https://yunbi.com:443//api/v2/depth.json?market=btccny
		requestMap.put("requestURL", "https://yunbi.com:443//api/v2/depth.json?");
		// 查询货币的类型
		String market = this.convertMoneyType(paramsMap.get("moneytype"));
		requestMap.put("market", market);
		
		requestMap.put("limit", "20");
		JSONObject responseJSON = new JSONObject();
		try
		{
			String result = httpUtil.methodGet(requestMap);
			//ConstatFinalUtil.SYS_LOG.info("-----" + result);
			responseJSON = (JSONObject) JSON.parse(result);

			// 拼装符合条件的json数据
			JSONObject dataJSON = new JSONObject();

			int count = 0;
			// 按照规则排序的容器
			Map<Double, JSONObject> asksMap = new TreeMap<Double, JSONObject>();
			JSONArray asksResArr = new JSONArray();
			JSONArray asksArr = (JSONArray) responseJSON.get("asks");
			for (Iterator iterator = asksArr.iterator(); iterator.hasNext();)
			{
				JSONArray asksTempArr = (JSONArray) iterator.next();
				JSONObject askJSON = new JSONObject();
				askJSON.put("count", "1");
				askJSON.put("vol", asksTempArr.get(1) + "");
				askJSON.put("rate", asksTempArr.get(0) + "");

				asksMap.put(Double.valueOf(askJSON.get("rate") + ""), askJSON);
			}

			for (Iterator iterator = asksMap.entrySet().iterator(); iterator
					.hasNext();)
			{
				Entry me = (Entry) iterator.next();
				double key = (Double) me.getKey();

				if (count >= ConstatFinalUtil.PAGE_BATCH_SIZE)
				{
					break;
				}

				JSONObject asksTemp = (JSONObject) me.getValue();

				asksResArr.add(asksTemp);
				count++;
			}

			dataJSON.put("asks", asksResArr);

			// 按照规则排序的容器
			count = 0;
			Map<Double, JSONObject> bidsMap = new TreeMap<Double, JSONObject>(
					new Comparator<Double>()
					{
						@Override
						public int compare(Double o1, Double o2)
						{
							return o2.compareTo(o1);
						}
					});
			JSONArray bidsResArr = new JSONArray();
			JSONArray bidsArr = (JSONArray) responseJSON.get("bids");
			for (Iterator iterator = bidsArr.iterator(); iterator.hasNext();)
			{
				JSONArray bidsTempArr = (JSONArray) iterator.next();
				JSONObject bidsJSON = new JSONObject();
				bidsJSON.put("count", "1");
				bidsJSON.put("vol", bidsTempArr.get(1) + "");
				bidsJSON.put("rate", bidsTempArr.get(0) + "");

				bidsMap.put(Double.valueOf(bidsJSON.get("rate") + ""), bidsJSON);
			}

			for (Iterator iterator = bidsMap.entrySet().iterator(); iterator
					.hasNext();)
			{
				Entry me = (Entry) iterator.next();
				double key = (Double) me.getKey();

				if (count >= ConstatFinalUtil.PAGE_BATCH_SIZE)
				{
					break;
				}

				JSONObject bidsTemp = (JSONObject) me.getValue();

				bidsResArr.add(bidsTemp);
				count++;
			}
			dataJSON.put("bids", bidsResArr);

			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			resultJSON.put("data", dataJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "depth(市场深度)失败."
					+ paramsMap, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * trades的格式,历史交易记录
	 */
	public JSONArray queryHistoryTrades(String symbol,
			Map<String, String> paramsMap)
	{

		HTTPUtil httpUtil = new HTTPUtil();

		if ("ltc_cny".equalsIgnoreCase(symbol))
		{
			paramsMap.put("requestURL",
					" https://www.okcoin.com/api/trades.do?symbol=ltc_cny");
		} else
		{
			paramsMap.put("requestURL",
					" https://www.okcoin.com/api/trades.do?");
		}
		JSONArray resultJSON = new JSONArray();
		try
		{
			String result = httpUtil.methodGet(paramsMap);
			resultJSON = (JSONArray) JSON.parse(result);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "历史交易记录失败."
					+ paramsMap, e);
		}
		return resultJSON;
	}

	/**
	 * 获取服务器时间戳
	 * 
	 * @return
	 */
	public String getTimestamp()
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		
		HTTPUtil httpUtil = new HTTPUtil();

		Map<String, String> requestMap = new HashMap<String, String>();
		//https://yunbi.com:443//api/v2/depth.json?market=btccny
		requestMap.put("requestURL", "https://yunbi.com:443//api/v2/timestamp.json");
		try
		{
			Thread.sleep(1000);
			String result = httpUtil.methodGet(requestMap);
			return Long.valueOf(result) * 1000 + "";
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "depth(市场深度)失败."
					+ requestMap, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return "0";
	}

	public void setAuthJSON(JSONObject authJSON)
	{
		this.authJSON = authJSON;
	}

	@Override
	public String convertMoneyType(String moneytype)
	{
		String result = "" ; 
		if("btc_cny".equalsIgnoreCase(moneytype))
		{
			result = "btccny";
		}else if("ltc_cny".equalsIgnoreCase(moneytype))
		{
			result = "ltccny";
		}else if("eth_cny".equalsIgnoreCase(moneytype))
		{
			result = "ethcny";
		}else if("etc_cny".equalsIgnoreCase(moneytype))
		{
			result = "etccny";
		}
		return result;
	}

	@Override
	public String toString()
	{
		return "YunbiTradeServiceImplay [findid()=" + findWebsiteId() + "]";
	}

	@Override
	public JSONObject getOrdersSingle(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		
		//开发版本
		if("1".equalsIgnoreCase(ConstatFinalUtil.SYS_BUNDLE.getString("dev.version")))
		{
			//模拟假数据
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("ordersList", new JSONArray());
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "---模拟下单---" + paramsMap);
			return resultJSON ; 
		}
		
		HTTPUtil httpUtil = new HTTPUtil();
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("requestURL", "https://yunbi.com:443//api/v2/order.json");
		
		requestMap.put("access_key", authJSON.get("access_key") + "");
		requestMap.put("tonce", this.getTimestamp() + "");
		//requestMap.put("order_id", paramsMap.get("order_id"));
		
		requestMap.put("id", paramsMap.get("order_id") + "");
		
		requestMap.put("canonical_verb", "GET");
		requestMap.put("canonical_uri", "/api/v2/order.json");

		String sign = authSign(requestMap);
		requestMap.put("signature", sign);
		
		requestMap.remove("canonical_verb");
		requestMap.remove("canonical_uri");
		String result = "" ; 
		try
		{
			result = httpUtil.methodGet(requestMap);
			//ConstatFinalUtil.SYS_LOG.info("----" + result);
			JSONObject dataJSON = new JSONObject();
			JSONObject ordersTemp = (JSONObject) JSON.parse(result);
			JSONObject ordersResJSON = new JSONObject();
			ordersResJSON.put("order_id", ordersTemp.get("id") + "");
			ordersResJSON.put("price", ordersTemp.get("price") + "");
			ordersResJSON.put("amount", ordersTemp.get("volume") + "");
			ordersResJSON.put("deal_amount", ordersTemp.get("executed_volume") + "");
			ordersResJSON.put("create_date", ordersTemp.get("created_at") + "");
			
			ordersResJSON.put("symbol", paramsMap.get("moneytype"));
			String side = ordersTemp.get("side") + "";
			if("buy".equalsIgnoreCase(side))
			{
				side = "0" ; 
			}else if("sell".equalsIgnoreCase(side))
			{
				side = "1" ; 
			}
			ordersResJSON.put("type", side + "");
			
			String status = ordersTemp.get("state") + ""; 
			if("wait".equalsIgnoreCase(status))
			{
				status = "0" ; 
			}else if("cancel".equalsIgnoreCase(status))
			{
				status = "-1";
			}else if("done".equalsIgnoreCase(status))
			{
				status = "2";
			}
			ordersResJSON.put("status", status + "");
			
			dataJSON.put("orders", ordersResJSON);
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "--获取订单详情--原始信息:" + result
				+ ";转换后目标信息:" + resultJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取订单详情失败."
					+ paramsMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	
	}

	@Override
	public String parseMoneyType(String moneytype)
	{
		return null;
	}

	@Override
	public String findWebSiteName()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JSONObject convertOrders(String moneytype, JSONObject ordersTemp)
	{
		// TODO Auto-generated method stub
		return null;
	}
}
