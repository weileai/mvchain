package com.wang.mvchain.outer.service.implay;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.TreeMap;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.common.util.EncryptUtil;
import com.wang.mvchain.common.util.HTTPUtil;
import com.wang.mvchain.outer.service.ITradeService;

/**
 * bitfinex的API工具
 * 
 * 支持的币种:["btcusd","ltcusd","ltcbtc","drkusd","drkbtc","th1btc"]
 * 
 * @author wangsh
 *
 */
public class BitstampTradeServiceImplay extends BaseTradeService implements ITradeService {
	// 登陆验证的信息,可从网站模板中查找
	private JSONObject authJSON;

	@Override
	public int findWebsiteId() {
		return 6;
	}

	/**
	 * 获取签名方式
	 * 
	 * @return
	 */
	private String authSign(String nonce, String client_id, String api_key, String secret_key) {
		String message = nonce + client_id + api_key;
		return EncryptUtil.hmacSHA256(message, secret_key).toUpperCase();
	}

	/**
	 * 获取用户基本信息
		Account balance
		
		POST https://www.bitstamp.net/api/balance/
		
		Params:
		
		key - API key
		signature - signature
		nonce - nonce
		Returns JSON dictionary:
		
		usd_balance - USD balance
		btc_balance - BTC balance
		usd_reserved - USD reserved in open orders
		btc_reserved - BTC reserved in open orders
		usd_available- USD available for trading
		btc_available - BTC available for trading
		fee - customer trading fee
		{
		    "btc_reserved":"0",
		    "fee":"0.5000",
		    "btc_available":"6.99990000",
		    "usd_reserved":"172.87",
		    "btc_balance":"6.99990000",
		    "usd_balance":"172.87",
		    "usd_available":"0.00"
		}		
	 * @return
	 */
	public JSONObject getUserInfo(Map<String, String> paramsMap) {
		long st = System.currentTimeMillis();
		// 拼装目标数据
		JSONObject resultJSON = new JSONObject();
		Map<String, String> requestMap = new HashMap<String, String>();
		HTTPUtil httpUtil = new HTTPUtil();
		// requestMap.put("request","/api" + method);
		String key = authJSON.getString("Key");
		String secret = authJSON.getString("Secret");
		String nonce = this.findReqtime();
		String client_id = authJSON.getString("client_id");
		String signature = this.authSign(nonce, client_id, key, secret);
		requestMap.put("nonce", nonce);
		requestMap.put("key", authJSON.getString("Key"));
		requestMap.put("signature", signature);

		requestMap.put("requestURL", "https://www.bitstamp.net/api/balance/");

		JSONObject responseJSON = new JSONObject();
		String result = "" ; 
		try
		{
			result = httpUtil.methodHttpsPost(Collections.EMPTY_MAP , requestMap);
//			ConstatFinalUtil.SYS_LOG.info(res + "------>");
			responseJSON = (JSONObject) JSON.parse(result);
			if (responseJSON != null && responseJSON.containsKey("btc_balance")) {
				JSONObject dataJSON = new JSONObject();

				JSONObject fundJSON = new JSONObject();
				fundJSON.put("btc", "0");
				fundJSON.put("ltc", "0");
				// fundJSON.put("cny", "0");
				fundJSON.put("usd", "0");

				JSONObject balanceJSON = new JSONObject();
				if (responseJSON.containsKey("btc_balance")) {
					balanceJSON.put("btc", responseJSON.getString("btc_balance"));
				}
				if (responseJSON.containsKey("usd_balance")) {
					balanceJSON.put("usd", responseJSON.getString("usd_balance"));
				}
				dataJSON.put("balance", balanceJSON);
				dataJSON.put("fund", fundJSON);

				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			} else {
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:"
						+ responseJSON.toJSONString());
			}
			/*ConstatFinalUtil.OUTER_LOG.info(this.findid() + "--获取用户信息返回.--原始信息:" + responseJSON
					+ ";转换后目标信息:" + resultJSON);
			*/
		} catch (Exception e) {
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取用户信息失败." + requestMap + ";原始信息:" + result , e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis();
		resultJSON.put("consume", (ed - st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 下单交易
		Buy limit order
		
		POST https://www.bitstamp.net/api/buy/
		
		Params:
		
		key - API key
		signature - signature
		nonce - nonce
		amount - amount
		price - price
		Returns JSON dictionary representing order:
		
		id - order id
		datetime - date and time
		type - buy or sell (0 - buy; 1 - sell)
		price - price
		amount - amount
		> api.buy_limit_order(c, k, s)
		{
		    'datetime': datetime,   # Datetime placed
		    'id': string,           # Order ID
		    'type': string,         # Order type - one of 
		                            ## api.BUY_LIMIT_ORDER_TYPE_BUY,
		                            ## api.BUY_LIMIT_ORDER_TYPE_SELL
		    'price': decimal,       # Placed order price
		    'amount': decimal       # Placed order amount
		}
				
		Sell limit order
		
		POST https://www.bitstamp.net/api/sell/
		
		Params:
		
		key - API key
		signature - signature
		nonce - nonce
		amount - amount
		price - price
		Returns JSON dictionary representing order:
		
		id - order id
		datetime - date and time
		type - buy or sell (0 - buy; 1 - sell)
		price - price
		amount - amount

		> api.sell_limit_order(c, k, s)
		{
		    'datetime': datetime,   # Datetime placed
		    'id': string,           # Order ID
		    'type': string,         # Order type - one of 
		                            ## api.SELL_LIMIT_ORDER_TYPE_BUY,
		                            ## api.SELL_LIMIT_ORDER_TYPE_SELL
		    'price': decimal,       # Placed order price
		    'amount': decimal       # Placed order amount
		}
		{
		    "price":"1.25",
		    "amount":"1",
		    "type":0,
		    "id":1273070,
		    "datetime":"2013-01-07 08:44:04.656195"
		}						
	 * @return
	 */
	public JSONObject trade(Map<String, String> paramsMap) {
		long st = System.currentTimeMillis();
		// 拼装目标数据
		JSONObject resultJSON = new JSONObject();
		//开发版本
		if("1".equalsIgnoreCase(ConstatFinalUtil.SYS_BUNDLE.getString("dev.version")))
		{
			//模拟假数据
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("order_id", new Random().nextInt(10000));
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "---模拟下单---" + paramsMap);
			return resultJSON ; 
		}		
		Map<String, String> requestMap = new HashMap<String, String>();
		HTTPUtil httpUtil = new HTTPUtil();
		// requestMap.put("request","/api" + method);
		String key = authJSON.getString("Key");
		String secret = authJSON.getString("Secret");
		String nonce = this.findReqtime();
		String client_id = authJSON.getString("client_id");
		String signature = this.authSign(nonce, client_id, key, secret);
		requestMap.put("nonce", nonce);
		requestMap.put("key", authJSON.getString("Key"));
		requestMap.put("signature", signature);
		requestMap.put("amount", paramsMap.get("amount"));
		requestMap.put("price", paramsMap.get("price"));
		// 买还是卖.0:买,1:卖
		if (paramsMap.get("type").equals("0")) {
			requestMap.put("requestURL", "https://www.bitstamp.net/api/buy/");
		} else if (paramsMap.get("type").equals("1")) {
			requestMap.put("requestURL", "https://www.bitstamp.net/api/sell/");
		}
		JSONObject responseJSON = new JSONObject();
		String result = "" ; 
		try
		{
			result = httpUtil.methodHttpsPost(Collections.EMPTY_MAP , requestMap);
			//ConstatFinalUtil.SYS_LOG.info(res + "------>");
			responseJSON = (JSONObject) JSON.parse(result);
			if (responseJSON != null && responseJSON.containsKey("id")) {
				JSONObject dataJSON = new JSONObject();
				dataJSON.put("order_id", responseJSON.get("id"));
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			} else {
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:"
						+ responseJSON.get("error"));
			}
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "--下单交易返回.--原始信息:" + result
					+ ";转换后目标信息:" + resultJSON);
		} catch (Exception e) {
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "下单交易失败." + requestMap + ";原始信息:" + result , e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis();
		resultJSON.put("consume", (ed - st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 撤销订单
		CANCEL ORDERPOST https://www.bitstamp.net/api/cancel_order/Params:
		key - API key
		signature - signature
		nonce - nonce
		id - order ID
		Returns 'true' if order has been found and canceled.
		 
	 * @return
	 */
	public JSONObject cancelOrder(Map<String, String> paramsMap) {
		long st = System.currentTimeMillis();
		// 拼装目标数据
		JSONObject resultJSON = new JSONObject();
		//开发版本
		if("1".equalsIgnoreCase(ConstatFinalUtil.SYS_BUNDLE.getString("dev.version")))
		{
			//模拟假数据
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("order_id", paramsMap.get("order_id"));
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "---模拟下单---" + paramsMap);
			return resultJSON ; 
		}		
		Map<String, String> requestMap = new HashMap<String, String>();
		HTTPUtil httpUtil = new HTTPUtil();
		// requestMap.put("request","/api" + method);
		String key = authJSON.getString("Key");
		String secret = authJSON.getString("Secret");
		String nonce = this.findReqtime();
		String client_id = authJSON.getString("client_id");
		String signature = this.authSign(nonce, client_id, key, secret);
		requestMap.put("nonce", nonce);
		requestMap.put("key", authJSON.getString("Key"));
		requestMap.put("signature", signature);
		requestMap.put("id", paramsMap.get("order_id"));
		requestMap.put("requestURL", "https://www.bitstamp.net/api/cancel_order/");

		String result = "" ; 
		try
		{
			result = httpUtil.methodHttpsPost(Collections.EMPTY_MAP , requestMap);
			//ConstatFinalUtil.SYS_LOG.info(res + "------>");
			if (result.contains("true")) {
				JSONObject dataJSON = new JSONObject();
				dataJSON.put("order_id", paramsMap.get("order_id"));
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			} else {
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + result);
			}
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "--撤销订单返回.--原始信息:" + result
					+ ";转换后目标信息:" + resultJSON);
		} catch (Exception e) {
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "撤销订单失败." + requestMap + ";原始信息:" + result , e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis();
		resultJSON.put("consume", (ed - st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 获取用户挂单
		OPEN ORDERSPOST https://www.bitstamp.net/api/open_orders/Params:
		key - API key
		signature - signature
		nonce - nonce
		Returns JSON list of open orders. Each order is represented as dictionary:
		id - order id
		datetime - date and time
		type - buy or sell (0 - buy; 1 - sell)
		price - price
		amount - amount
		[
		    {
		        "price":"12.93",
		        "amount":"2.63031710",
		        "type":0,
		        "id":1271131,
		        "datetime":"2013-01-06 10:46:41"
		    },,,
		]		
	 * @param args
	 */
	public JSONObject getOrders(Map<String, String> paramsMap) {
		long st = System.currentTimeMillis();
		// 拼装目标数据
		JSONObject resultJSON = new JSONObject();
		//开发版本
		if("1".equalsIgnoreCase(ConstatFinalUtil.SYS_BUNDLE.getString("dev.version")))
		{
			//模拟假数据
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("ordersList", new JSONArray());
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "---模拟下单---" + paramsMap);
			return resultJSON ; 
		}		
		Map<String, String> requestMap = new HashMap<String, String>();
		HTTPUtil httpUtil = new HTTPUtil();
		// requestMap.put("request","/api" + method);
		String key = authJSON.getString("Key");
		String secret = authJSON.getString("Secret");
		String nonce = this.findReqtime();
		String client_id = authJSON.getString("client_id");
		String signature = this.authSign(nonce, client_id, key, secret);
		requestMap.put("nonce", nonce);
		requestMap.put("key", authJSON.getString("Key"));
		requestMap.put("signature", signature);

		requestMap.put("requestURL", "https://www.bitstamp.net/api/open_orders/");

		JSONArray responseJSON = new JSONArray();
		String result = "" ; 
		try
		{
			result = httpUtil.methodHttpsPost(Collections.EMPTY_MAP , requestMap);
			//ConstatFinalUtil.SYS_LOG.info(res + "------>");
			responseJSON = (JSONArray) JSON.parse(result);
			if (responseJSON != null) 
			{
				JSONObject dataJSON = new JSONObject();
				JSONArray ordersResArr = new JSONArray();
				for(Object item:responseJSON){
					JSONObject itemJSON = (JSONObject) item;
					JSONObject ordersResJSON = new JSONObject();
					ordersResJSON.put("order_id", itemJSON.getString("id"));
					ordersResJSON.put("price", itemJSON.getString("price"));
					ordersResJSON.put("amount", itemJSON.getString("amount"));
					ordersResJSON.put("deal_amount", "0");
					ordersResJSON.put("symbol", "");
					String type = itemJSON.getString("type");
					ordersResJSON.put("type", type);
					ordersResJSON.put("status", "0");
					
					ordersResArr.add(ordersResJSON);
				}
				dataJSON.put("ordersList", ordersResArr);
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			} else {
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + result);
			}
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "--获取挂单息返回.--原始信息:" + result
					+ ";转换后目标信息:" + resultJSON);
		} catch (Exception e) {
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取挂单失败." + requestMap + ";原始信息:" + result , e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis();
		resultJSON.put("consume", (ed - st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 行情API
	 */
	public JSONObject queryTicker(Map<String, String> paramsMap) {
		return null;
	}

	/**
	 * depth(市场深度)
		ORDER BOOKGET https://www.bitstamp.net/api/order_book/Params:
		group - group orders with the same price (0 - false; 1 - true). Default: 1.
		Returns JSON dictionary with "bids" and "asks". Each is a list of open orders and each order is represented as a list of price and amount.
		{
		   "timestamp":"1378816304",
		   "bids":[
		      [
		         "123.09",
		         "0.16248274"
		      ],,,
		   ],
		   "asks":[
		      [
		         "123.39",
		         "0.60466812"
		      ],,,
		   ]
		}            		 
	 */
	public JSONObject queryDepth(Map<String, String> paramsMap) {
		long st = System.currentTimeMillis();
		// 拼装目标数据
		JSONObject resultJSON = new JSONObject();
		Map<String, String> requestMap = new HashMap<String, String>();
		HTTPUtil httpUtil = new HTTPUtil();

		requestMap.put("requestURL", "https://www.bitstamp.net/api/order_book/");

		JSONObject responseJSON = new JSONObject();
		String result = "" ; 
		try
		{
			result = httpUtil.methodGet(requestMap);
//			ConstatFinalUtil.SYS_LOG.info(res + "------>");
			responseJSON = (JSONObject) JSON.parse(result);
			if (responseJSON != null && responseJSON.containsKey("bids")) {
				// 拼装符合条件的json数据
				JSONObject dataJSON = new JSONObject();

				int count = 0;
				// 按照规则排序的容器
				Map<Double, JSONObject> asksMap = new TreeMap<Double, JSONObject>();
				JSONArray asksResArr = new JSONArray();
				JSONArray asksArr = (JSONArray) responseJSON.get("asks");
				for (Iterator iterator = asksArr.iterator(); iterator.hasNext();)
				{
					JSONArray asksTempArr = (JSONArray) iterator.next();
					JSONObject askJSON = new JSONObject();
					askJSON.put("count", "1");
					askJSON.put("vol", asksTempArr.get(1) + "");
					askJSON.put("rate", asksTempArr.get(0) + "");

					asksMap.put(Double.valueOf(askJSON.get("rate") + ""), askJSON);
				}

				for (Iterator iterator = asksMap.entrySet().iterator(); iterator
						.hasNext();)
				{
					Entry me = (Entry) iterator.next();
					double key = (Double) me.getKey();

					if (count >= ConstatFinalUtil.PAGE_BATCH_SIZE)
					{
						break;
					}

					JSONObject asksTemp = (JSONObject) me.getValue();

					asksResArr.add(asksTemp);
					count++;
				}

				dataJSON.put("asks", asksResArr);

				// 按照规则排序的容器
				count = 0;
				Map<Double, JSONObject> bidsMap = new TreeMap<Double, JSONObject>(
						new Comparator<Double>()
						{
							@Override
							public int compare(Double o1, Double o2)
							{
								return o2.compareTo(o1);
							}
						});
				JSONArray bidsResArr = new JSONArray();
				JSONArray bidsArr = (JSONArray) responseJSON.get("bids");
				for (Iterator iterator = bidsArr.iterator(); iterator.hasNext();)
				{
					JSONArray bidsTempArr = (JSONArray) iterator.next();
					JSONObject bidsJSON = new JSONObject();
					bidsJSON.put("count", "1");
					bidsJSON.put("vol", bidsTempArr.get(1) + "");
					bidsJSON.put("rate", bidsTempArr.get(0) + "");

					bidsMap.put(Double.valueOf(bidsJSON.get("rate") + ""), bidsJSON);
				}

				for (Iterator iterator = bidsMap.entrySet().iterator(); iterator
						.hasNext();)
				{
					Entry me = (Entry) iterator.next();
					double key = (Double) me.getKey();

					if (count >= ConstatFinalUtil.PAGE_BATCH_SIZE)
					{
						break;
					}

					JSONObject bidsTemp = (JSONObject) me.getValue();

					bidsResArr.add(bidsTemp);
					count++;
				}
				dataJSON.put("bids", bidsResArr);

				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
				resultJSON.put("data", dataJSON);
			} else {
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:"
						+ responseJSON.get("error"));
			}
			//ConstatFinalUtil.OUTER_LOG.info(this.findid() + "--获取用户信息返回.--" + responseJSON);
		} catch (Exception e) {
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "depth(市场深度)失败." + requestMap + ";原始信息:" + result , e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis();
		resultJSON.put("consume", (ed - st) + "毫秒");
		return resultJSON;
	}

	/**
	 * trades的格式,历史交易记录
		USER TRANSACTIONSPOST https://www.bitstamp.net/api/user_transactions/Params:
		key - API key
		signature - signature
		nonce - nonce
		offset - skip that many transactions before beginning to return results. Default: 0.
		limit - limit result to that many transactions. Default: 100.
		sort - sorting by date and time (asc - ascending; desc - descending). Default: desc.
		Returns descending JSON list of transactions. Every transaction (dictionary) contains:
		datetime - date and time
		id - transaction id
		type - transaction type (0 - deposit; 1 - withdrawal; 2 - market trade)
		usd - USD amount
		btc - BTC amount
		fee - transaction fee
		order_id - executed order id
		[
		   {
		      "usd":"-11.37",
		      "btc":"0.08650000",
		      "btc_usd":"131.50",
		      "order_id":6877187,
		      "fee":"0.06",
		      "type":2,
		      "id":1296712,
		      "datetime":"2013-09-02 13:17:49"
		   },,,
		]		
	 */
	public JSONArray queryHistoryTrades() {
		long st = System.currentTimeMillis();
		// 拼装目标数据
		JSONObject resultJSON = new JSONObject();
		Map<String, String> requestMap = new HashMap<String, String>();
		HTTPUtil httpUtil = new HTTPUtil();
		// requestMap.put("request","/api" + method);
		String key = authJSON.getString("Key");
		String secret = authJSON.getString("Secret");
		String nonce = this.findReqtime();
		String client_id = authJSON.getString("client_id");
		String signature = this.authSign(nonce, client_id, key, secret);
		requestMap.put("nonce", nonce);
		requestMap.put("key", authJSON.getString("Key"));
		requestMap.put("signature", signature);
		requestMap.put("offset", "0");
		requestMap.put("limit", "100");
		requestMap.put("sort", "desc");
		requestMap.put("requestURL", "https://www.bitstamp.net/api/user_transactions/");

		JSONArray responseJSON = new JSONArray();
		String result = "" ; 
		try
		{
			result = httpUtil.methodHttpsPost(Collections.EMPTY_MAP , requestMap);
			//ConstatFinalUtil.SYS_LOG.info(res + "------>");
			responseJSON = (JSONArray) JSON.parse(result);
			if (responseJSON != null && responseJSON.size() > 0) {

			} else {

			}
			//ConstatFinalUtil.OUTER_LOG.info(this.findid() + "--获取历史交易记录返回.--" + responseJSON);
		} catch (Exception e) {
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "历史交易记录失败." + requestMap + ";原始信息:" + result , e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis();
		resultJSON.put("consume", (ed - st) + "毫秒");
		return null;
	}

	/**
	 * 获取所有的行情
	 * 
	 * @return
	 */
	public JSONObject getTrade() {
		return null;
	}

	public void setAuthJSON(JSONObject authJSON) {
		this.authJSON = authJSON;
	}

	@Override
	public String convertMoneyType(String moneytype)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toString()
	{
		return "BitstampTradeServiceImplay [findid()=" + findWebsiteId() + "]";
	}

	@Override
	public JSONObject getOrdersSingle(Map<String, String> paramsMap)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String parseMoneyType(String moneytype)
	{
		return null;
	}

	@Override
	public String findWebSiteName()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JSONObject convertOrders(String moneytype, JSONObject ordersTemp)
	{
		// TODO Auto-generated method stub
		return null;
	}
}
