package com.wang.mvchain.outer.service.implay;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.commons.codec.digest.DigestUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.common.util.HTTPUtil;
import com.wang.mvchain.outer.enums.TradeEnum;
import com.wang.mvchain.outer.service.ITradeService;

/**
 * Okex的API工具
 * 
 * @author wangsh
 */
public class MxcTradeServiceImplay extends BaseTradeService implements ITradeService
{
	/* 请求服务的serverUrl
	 * 	mxc.co
	 * 	mxc.ceo
	 *  */
	private String serverUrl = "https://www.mxc.ceo" ; 
	
	@Override
	public int findWebsiteId()
	{
		return 20;
	}
	
	/**
	 * 获取签名方式
	 * @param json 是一个JSON字符串
	 * @return
	 */
	private String authSign(Map<String, String> headerMap,Map<String, String> requestMap,Object json)
	{
		try
		{
			StringBuffer sb = new StringBuffer();
			Map<String, String> signMap = new TreeMap<String, String>();
			signMap.putAll(requestMap);
			
			for (Iterator iterator = signMap.entrySet().iterator(); iterator.hasNext();)
			{
				Entry me = (Entry) iterator.next();
				String key = me.getKey() + "" ;
				String value = me.getValue() + ""; 
				if("requestURL".equalsIgnoreCase(key) || key.startsWith("enc_"))
				{
					continue ; 
				}else
				{
					sb.append(key + "=" + value + "&");
				}
			}
			/* 增加密钥 */
			sb.append("api_secret=" + this.authJSON.getString("api_secret")) ; 
			/*
			 * 采用HMacSha256加密
			 * */
			return DigestUtils.md5Hex(sb.toString()) ; 
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return "" ; 
	}

	/**
	 * 获取用户基本信息
	 * 
	 * @return
	 */
	public JSONObject getUserInfo(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		String methodUrl = "/open/api/v1/private/account/info" ; 
		
		HTTPUtil httpUtil = new HTTPUtil();
		/* 公共代码 */
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("requestURL", serverUrl + methodUrl);
		requestMap.put("api_key", this.authJSON.getString("api_key"));
		requestMap.put("req_time", this.findReqtime());
		
		/**
		 * 存储Header的一些参数
		 */
		Map<String, String> headerMap = new HashMap<String, String>();
		
		/* 存储一些交易的参数 */
		
		String sign = authSign(headerMap,requestMap,null);
		requestMap.put("sign", sign);
		
		String result = "" ; 
		try
		{
			result = httpUtil.methodGet(headerMap,requestMap);
			JSONObject resJSON = (JSONObject) JSON.parse(result);
			
			JSONObject dataJSON = new JSONObject();
			
			JSONObject balanceJSON = new JSONObject();
			JSONObject fundJSON = new JSONObject();
			for (Iterator iterator = resJSON.entrySet().iterator(); iterator.hasNext();)
			{
				Entry me = (Entry) iterator.next() ; 
				String key = me.getKey() + ""; 
				JSONObject resTempJSON = (JSONObject) me.getValue() ;
				
				/*
				 * {
				        "frozen":"0",
				        "hold":"0",
				        "id":"9150707",
				        "currency":"BTC",
				        "balance":"0.0049925",
				        "available":"0.0049925",
				        "holds":"0"
				    }
				 * */
				String name = key.toLowerCase();
				/* 这里面有我们要想的币种 */
				balanceJSON.put(name, resTempJSON.get("available"));
				fundJSON.put(name, resTempJSON.get("frozen"));
			}
			
			dataJSON.put("balance", balanceJSON);
			dataJSON.put("fund", fundJSON);
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			//ConstatFinalUtil.OUTER_LOG.info(this.findid() + "--获取用户信息返回.--" + responseJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取用户信息失败."
					+ requestMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 下单交易
	 * 
	 * @return
	 */
	public JSONObject trade(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = super.trade(paramsMap);
		if(resultJSON != null)
		{
			return resultJSON ; 
		}
		
		resultJSON = new JSONObject() ;
		//拼装目标数据
		String methodUrl = "/open/api/v1/private/order" ; 
		
		HTTPUtil httpUtil = new HTTPUtil();
		/* 公共代码 */
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("requestURL", serverUrl + methodUrl);
		
		requestMap.put("api_key", this.authJSON.getString("api_key"));
		requestMap.put("req_time", this.findReqtime());
		
		Map<String, String> headerMap = new HashMap<String, String>();
		
		/* 存储一些交易的参数 */
		String type = paramsMap.get("type") ; 
		if(TradeEnum.TRADE_TYPE_BUY.getValue() == Integer.valueOf(type))
		{
			/* 买 */
			requestMap.put("trade_type", "1");
		}else if(TradeEnum.TRADE_TYPE_SELL.getValue() == Integer.valueOf(type))
		{
			/* 买 */
			requestMap.put("trade_type", "2");
		}
		/* 交易对 */
		String moneyType = paramsMap.get("moneytype");
		moneyType = this.convertMoneyType(moneyType) ; 
		requestMap.put("market", moneyType);
		/* 价格和数量 */
		requestMap.put("price", paramsMap.get("price"));
		requestMap.put("quantity", paramsMap.get("amount"));
		
		String sign = authSign(headerMap,requestMap , null);
		requestMap.put("sign", sign);
		
		
		String result = "" ; 
		try
		{
			result = httpUtil.methodPost(headerMap, requestMap);
			JSONObject responseJSON = (JSONObject) JSON.parse(result);
			
			String data = responseJSON.getString("data") ;
			if("200".equalsIgnoreCase(responseJSON.get("code") + "") && data != null)
			{
				JSONObject dataJSON = new JSONObject();
				dataJSON.put("order_id", data);
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + responseJSON.get("error_code"));
			}
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "--下单交易返回--原始信息:" + result
				+ ";转换后目标信息:" + resultJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "下单交易失败."
					+ requestMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 撤销订单
	 * 
	 * @return
	 */
	public JSONObject cancelOrder(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = super.cancelOrder(paramsMap);
		if(resultJSON != null)
		{
			return resultJSON ; 
		}
		/* 订单id */
		String order_id = paramsMap.get("order_id") ; 
		//拼装目标数据
		resultJSON = new JSONObject();
		String methodUrl = "/open/api/v1/private/order" ; 
		
		HTTPUtil httpUtil = new HTTPUtil();
		/* 公共代码 */
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("requestURL", serverUrl + methodUrl);
		requestMap.put("api_key", this.authJSON.getString("api_key"));
		requestMap.put("req_time", this.findReqtime());
		
		/**
		 * 存储Header的一些参数
		 */
		Map<String, String> headerMap = new HashMap<String, String>();
		
		/* 存储一些交易的参数 */
		String moneytype = paramsMap.get("moneytype");
		moneytype = this.convertMoneyType(moneytype) ;
		requestMap.put("market", moneytype);
		requestMap.put("trade_no", order_id);
		
		String sign = authSign(headerMap,requestMap,null);
		requestMap.put("sign", sign);
		
		String result = "" ; 
		try
		{
			result = httpUtil.methodDelete(headerMap, requestMap);
			JSONObject responseJSON = (JSONObject) JSON.parse(result);
			
			if("200".equalsIgnoreCase(responseJSON.get("code") + ""))
			{
				JSONObject dataJSON = new JSONObject();
				dataJSON.put("order_id", order_id);
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + responseJSON.get("error_code"));
			}
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "--撤销订单返回.--原始信息:" + result
				+ ";转换后目标信息:" + resultJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "撤销订单失败."
					+ paramsMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 获取用户挂单
	 * 
	 * @param args
	 */
	public JSONObject getOrders(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = super.getOrders(paramsMap);
		if(resultJSON != null)
		{
			return resultJSON ; 
		}
		resultJSON = new JSONObject() ;
		//拼装目标数据
		String methodUrl = "/open/api/v1/private/current/orders" ; 
		
		HTTPUtil httpUtil = new HTTPUtil();
		/* 公共代码 */
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("requestURL", serverUrl + methodUrl);
		requestMap.put("api_key", this.authJSON.getString("api_key"));
		requestMap.put("req_time", this.findReqtime());
		
		/**
		 * 存储Header的一些参数
		 */
		Map<String, String> headerMap = new HashMap<String, String>();
		
		/* 存储一些交易的参数 */
		String moneyType = paramsMap.get("moneytype") ; 
		moneyType = this.convertMoneyType(moneyType);
		
		requestMap.put("market", moneyType);
		requestMap.put("trade_type", "0");
		requestMap.put("page_num", "1");
		requestMap.put("page_size", "10");
		
		String sign = authSign(headerMap,requestMap,null);
		requestMap.put("sign", sign);
				
		String result = "" ; 
		try
		{
			result = httpUtil.methodGet(headerMap , requestMap);
			JSONObject responseJSON = (JSONObject) JSON.parse(result);
			
			if("200".equalsIgnoreCase(responseJSON.getString("code")))
			{
				JSONArray dataResArr = responseJSON.getJSONArray("data");
				
				JSONObject dataJSON = new JSONObject();
				JSONArray ordersResArr = new JSONArray();
				for (Iterator iterator = dataResArr.iterator(); iterator
						.hasNext();)
				{
					JSONObject ordersTemp = (JSONObject) iterator.next();
					JSONObject ordersResJSON = this.convertOrders(moneyType, ordersTemp);
					ordersResArr.add(ordersResJSON);
				}
				
				dataJSON.put("ordersList", ordersResArr);
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
				ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "--获取用户挂单--原始信息:" + result
				+ ";转换后目标信息:" + resultJSON);
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + responseJSON.get("error_code"));
			}
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取用户挂单失败."
					+ paramsMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 行情API
	 */
	public JSONObject queryTicker(Map<String, String> paramsMap)
	{

		HTTPUtil httpUtil = new HTTPUtil();
		String symbol = paramsMap.get("symbol");
		if ("ltc_cny".equalsIgnoreCase(symbol))
		{
			paramsMap.put("requestURL",
					"https://www.okcoin.com/api/ticker.do?symbol=ltc_cny");
		} else
		{
			paramsMap.put("requestURL", "https://www.okcoin.com/api/ticker.do");
		}

		paramsMap.remove("symbol");

		JSONObject resultJSON = new JSONObject();
		try
		{
			String result = httpUtil.methodGet(paramsMap);
			resultJSON = (JSONObject) JSON.parse(result);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "行情API失败."
					+ paramsMap, e);
		}
		return resultJSON;
	}

	/**
	 * depth(市场深度)
	 */
	public JSONObject queryDepth(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		String moneyType = paramsMap.get("moneytype");
		moneyType = this.convertMoneyType(moneyType);
		
		String methodUrl = "/open/api/v1/data/depth" ; 
		
		HTTPUtil httpUtil = new HTTPUtil();
		/* 公共代码 */
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("requestURL", serverUrl + methodUrl);
		requestMap.put("depth", "10");
		requestMap.put("market", moneyType);
		
		/* 增加签名参数 */
		/* 存储一些交易的参数 */
		JSONObject responseJSON = new JSONObject();
		try
		{
			String result = httpUtil.methodGet(requestMap);
			responseJSON = (JSONObject) JSON.parse(result);
			if("200".equalsIgnoreCase(responseJSON.getString("code")))
			{
				JSONObject dataResJSON = responseJSON.getJSONObject("data");
			
				// 拼装符合条件的json数据
				JSONObject dataJSON = new JSONObject();
	
				int count = 0;
				// 按照规则排序的容器
				Map<Double, JSONObject> asksMap = new TreeMap<Double, JSONObject>();
				JSONArray asksResArr = new JSONArray();
				JSONArray asksArr = (JSONArray) dataResJSON.get("asks");
				for (Iterator iterator = asksArr.iterator(); iterator.hasNext();)
				{
					JSONObject asksTempArr = (JSONObject) iterator.next();
					JSONObject askJSON = new JSONObject();
					askJSON.put("count", "1");
					askJSON.put("vol", asksTempArr.get("quantity") + "");
					askJSON.put("rate", asksTempArr.get("price") + "");
	
					asksMap.put(Double.valueOf(askJSON.get("rate") + ""), askJSON);
				}
	
				for (Iterator iterator = asksMap.entrySet().iterator(); iterator
						.hasNext();)
				{
					Entry me = (Entry) iterator.next();
					double key = (Double) me.getKey();
	
					if (count >= ConstatFinalUtil.PAGE_BATCH_SIZE)
					{
						break;
					}
	
					JSONObject asksTemp = (JSONObject) me.getValue();
	
					asksResArr.add(asksTemp);
					count++;
				}
	
				dataJSON.put("asks", asksResArr);
	
				// 按照规则排序的容器
				count = 0;
				Map<Double, JSONObject> bidsMap = new TreeMap<Double, JSONObject>(
						new Comparator<Double>()
						{
							@Override
							public int compare(Double o1, Double o2)
							{
								return o2.compareTo(o1);
							}
						});
				JSONArray bidsResArr = new JSONArray();
				JSONArray bidsArr = (JSONArray) dataResJSON.get("bids");
				for (Iterator iterator = bidsArr.iterator(); iterator.hasNext();)
				{
					JSONObject bidsTempArr = (JSONObject) iterator.next();
					JSONObject bidsJSON = new JSONObject();
					bidsJSON.put("count", "1");
					bidsJSON.put("vol", bidsTempArr.get("quantity") + "");
					bidsJSON.put("rate", bidsTempArr.get("price") + "");
	
					bidsMap.put(Double.valueOf(bidsJSON.get("rate") + ""), bidsJSON);
				}
	
				for (Iterator iterator = bidsMap.entrySet().iterator(); iterator
						.hasNext();)
				{
					Entry me = (Entry) iterator.next();
					double key = (Double) me.getKey();
	
					if (count >= ConstatFinalUtil.PAGE_BATCH_SIZE)
					{
						break;
					}
	
					JSONObject bidsTemp = (JSONObject) me.getValue();
	
					bidsResArr.add(bidsTemp);
					count++;
				}
				dataJSON.put("bids", bidsResArr);
	
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
				
				resultJSON.put("data", dataJSON);
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + responseJSON.get("error_code"));
			}
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "depth(市场深度)失败."
					+ paramsMap, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * trades的格式,历史交易记录
	 */
	public JSONArray queryHistoryTrades(String symbol,
			Map<String, String> paramsMap)
	{

		HTTPUtil httpUtil = new HTTPUtil();

		if ("ltc_cny".equalsIgnoreCase(symbol))
		{
			paramsMap.put("requestURL",
					" https://www.okcoin.com/api/trades.do?symbol=ltc_cny");
		} else
		{
			paramsMap.put("requestURL",
					" https://www.okcoin.com/api/trades.do?");
		}
		JSONArray resultJSON = new JSONArray();
		try
		{
			String result = httpUtil.methodGet(paramsMap);
			resultJSON = (JSONArray) JSON.parse(result);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "历史交易记录失败."
					+ paramsMap, e);
		}
		return resultJSON;
	}

	@Override
	public String toString()
	{
		return "MxcTradeServiceImplay [findid()=" + findWebsiteId() + "]";
	}

	@Override
	public JSONObject getOrdersSingle(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = super.getOrdersSingle(paramsMap);
		if(resultJSON != null)
		{
			return resultJSON ; 
		}
		
		resultJSON = new JSONObject();
		//拼装目标数据
		String methodUrl = "/open/api/v1/private/order" ; 
		
		HTTPUtil httpUtil = new HTTPUtil();
		/* 公共代码 */
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("requestURL", serverUrl + methodUrl);
		requestMap.put("api_key", this.authJSON.getString("api_key"));
		requestMap.put("req_time", this.findReqtime());
		
		/**
		 * 存储Header的一些参数
		 */
		Map<String, String> headerMap = new HashMap<String, String>();
		
		/* 存储一些交易的参数 */
		String order_id = paramsMap.get("order_id") ;
		String moneyType = paramsMap.get("moneytype") ; 
		requestMap.put("market", this.convertMoneyType(moneyType));
		
		requestMap.put("trade_no", order_id);
		
		String sign = authSign(headerMap,requestMap,null);
		requestMap.put("sign", sign);
		
		String result = "" ; 
		try
		{
			result = httpUtil.methodGet(headerMap , requestMap);
			JSONObject responseJSON = (JSONObject) JSON.parse(result);
			JSONObject ordersTemp = responseJSON.getJSONObject("data");
			if("200".equalsIgnoreCase(responseJSON.getString("code")) && ordersTemp != null)
			{
				JSONObject dataJSON = new JSONObject() ; 
				if((ordersTemp.get("id") + "").equalsIgnoreCase(order_id))
				{
					JSONObject ordersResJSON = this.convertOrders(moneyType, ordersTemp);
					dataJSON.put("orders", ordersResJSON);
				}
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
				ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "--获取订单详情--原始信息:" + result
						+ ";转换后目标信息:" + resultJSON);
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + responseJSON.get("error_code"));
			}
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取订单详情失败."
					+ paramsMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	@Override
	public String findWebSiteName()
	{
		return "Mxc";
	}

	@Override
	public JSONObject convertOrders(String moneytype, JSONObject ordersTemp)
	{
		try
		{
			JSONObject ordersResJSON = new JSONObject();
			ordersResJSON.put("order_id", ordersTemp.get("id") + "");
			ordersResJSON.put("price", ordersTemp.get("price") + "");
			ordersResJSON.put("amount", ordersTemp.get("totalQuantity") + "");
			ordersResJSON.put("deal_amount", ordersTemp.get("tradedQuantity") + "");
			
			ordersResJSON.put("avg_price", ordersResJSON.get("price"));
			/* 创建订单的时间 */
			String createTime = ordersTemp.get("createTime") + "";
			ordersResJSON.put("create_date", createTime);
			
			String symbol = ordersTemp.get("market") + "";
			ordersResJSON.put("symbol", this.parseMoneyType(symbol));
			/* 交易类型 */
			String type = ordersTemp.get("type") + "";
			if("1".equalsIgnoreCase(type))
			{
				type = TradeEnum.TRADE_TYPE_BUY.getValue() + "" ; 
			}else if("2".equalsIgnoreCase(type))
			{
				type = TradeEnum.TRADE_TYPE_SELL.getValue() + "" ; 
			}
			ordersResJSON.put("type", type);
			
			/* 默认未成交 */
			String status = TradeEnum.TRADE_STATUS_DEAL_NO.getValue() + ""; 
			/*
			 * -1:已撤销  0:未成交  1:部分成交  2:完全成交 3 撤单处理中
			 * */
			if("4".equalsIgnoreCase(ordersTemp.get("status") + ""))
			{
				status = TradeEnum.TRADE_STATUS_CANCEL.getValue() + ""; 
			}else if("1".equalsIgnoreCase(ordersTemp.get("status") + ""))
			{
				status = TradeEnum.TRADE_STATUS_DEAL_NO.getValue() + ""; 
			}else if("3".equalsIgnoreCase(ordersTemp.get("status") + ""))
			{
				status = TradeEnum.TRADE_STATUS_DEAL_PART.getValue() + ""; 
			}else if("2".equalsIgnoreCase(ordersTemp.get("status") + ""))
			{
				status = TradeEnum.TRADE_STATUS_DEALED.getValue() + ""; 
			}
			ordersResJSON.put("status", status);
			return ordersResJSON;
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOG.error("转换订单报错了",e);
		}
		return new JSONObject();
	}

	@Override
	public String convertMoneyType(String moneytypeSelf)
	{
		return moneytypeSelf.toUpperCase();
	}

	@Override
	public String parseMoneyType(String moneytypeTrade)
	{
		return moneytypeTrade.toLowerCase();
	}
	
	@Override
	public JSONObject findAllTransPair(Map<String, String> paramsMap)
	{
		JSONObject resultJSON =  super.findAllTransPair(paramsMap);
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		String methodUrl = "/open/api/v1/data/markets_info" ; 
		
		HTTPUtil httpUtil = new HTTPUtil();
		/* 公共代码 */
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("requestURL", serverUrl + methodUrl);
		String result = "" ; 
		try
		{
			result = httpUtil.methodGet(Collections.EMPTY_MAP,requestMap);
			//ConstatFinalUtil.SYS_LOG.info("---返回:{}",result);
			JSONObject responseJSON = (JSONObject) JSON.parse(result);
			
			if("200".equalsIgnoreCase(responseJSON.getString("code")))
			{
				JSONObject dataResJSON = responseJSON.getJSONObject("data");
				
				JSONObject dataJSON = new JSONObject();
				for (Iterator iterator = dataResJSON.entrySet().iterator(); iterator.hasNext();)
				{
					Entry me = (Entry) iterator.next() ; 
					String key = me.getKey() + "" ; 
					JSONObject resTempJSON = (JSONObject) me.getValue() ; 
					/*
					 * {
				        //交易货币币种,小写;
				        "base_currency":"BTC",
				        //币对名称,小写;
				        "instrument_id":"BTC-USDT",
				        //最小交易数量
				        "min_size":"0.001",
				        //计价货币币种,小写;
				        "quote_currency":"USDT",
				        //交易货币数量精度
				        "size_increment":"0.00000001",
				        //交易价格精度
				        "tick_size":"0.1"
				    }
					 * */
					JSONObject pairJSON = new JSONObject();
					
					String moneyType = key;
					moneyType = this.parseMoneyType(moneyType);
					
					if(!moneyType.endsWith("usdt"))
					{
						continue ; 
					}
					
					String[] moneyTypes = moneyType.split("_");
					pairJSON.put("name", moneyType);
					pairJSON.put("min_size", resTempJSON.get("minAmount"));
					pairJSON.put("base_currency", moneyTypes[0]);
					pairJSON.put("quote_currency", moneyTypes[1]);
					pairJSON.put("size_increment", resTempJSON.get("quantityScale"));
					pairJSON.put("tick_size", resTempJSON.get("priceScale"));
					/* 存储到结果中 */
					dataJSON.put(pairJSON.get("name") + "", pairJSON);
				}
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + responseJSON.get("error_code"));
			}
			//ConstatFinalUtil.OUTER_LOG.info(this.findid() + "--获取用户信息返回.--" + responseJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取用户信息失败."
					+ requestMap + ";原始信息:" + result, e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}
}
