package com.wang.mvchain.outer.service.implay;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.TreeMap;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.common.util.DateUtil;
import com.wang.mvchain.common.util.EncryptUtil;
import com.wang.mvchain.common.util.HTTPUtil;
import com.wang.mvchain.outer.service.ITradeService;

/**
 * Okcoin的API工具
 * 
 * @author wangsh
 *
 */
public class BtceTradeServiceImplay extends BaseTradeService implements ITradeService
{
	//登陆验证的信息,可从网站模板中查找
	private JSONObject authJSON;
	
	@Override
	public int findWebsiteId()
	{
		return 4;
	}
	
	/**
	 * 获取签名方式
	 * 
	 * @return
	 */
	private String authSign(Map<String, String> condMap)
	{
		StringBuffer sb = new StringBuffer();
		for (Iterator iterator = condMap.entrySet().iterator(); iterator
				.hasNext();)
		{
			Entry me = (Entry) iterator.next();
			String key = me.getKey() + "";
			String value = me.getValue() + "";
			if ("requestURL".equalsIgnoreCase(key))
			{
				continue;
			}
			sb.append(key + "=" + value + "&");
		}
		if (sb.toString().lastIndexOf("&") != -1)
		{
			sb.delete(sb.lastIndexOf("&"), sb.length());
		}
		//ConstatFinalUtil.OUTER_LOG.info(sb);
		return EncryptUtil.hmacSHA512(sb.toString(), authJSON.get("Secret") + "");
	}

	/**
	 * 获取用户基本信息
	 * 
	 * @return
	 */
	public JSONObject getUserInfo(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		
		Map<String, String> requestMap = new HashMap<String, String>();
		HTTPUtil httpUtil = new HTTPUtil();
		requestMap.put("requestURL", "https://btc-e.com/tapi");
		requestMap.put("method", "getInfo");
		requestMap.put("nonce", this.findReqtime());
		
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("Key", authJSON.get("Key") + "");
		String sign = this.authSign(requestMap);
		headerMap.put("Sign", sign);
		JSONObject responseJSON = new JSONObject();
		String result = "" ; 
		try
		{
			result = httpUtil.methodHttpsPost(headerMap, requestMap);
			//ConstatFinalUtil.SYS_LOG.info(res);
			responseJSON = (JSONObject) JSON.parse(result);
			if("1".equalsIgnoreCase(responseJSON.get("success") + ""))
			{
				JSONObject dataJSON = new JSONObject();
				
				JSONObject returnJSON = (JSONObject) responseJSON.get("return");
				JSONObject balanceResJSON = (JSONObject) returnJSON.get("funds");
				
				JSONObject fundJSON = new JSONObject();
				fundJSON.put("btc", "0");
				fundJSON.put("ltc", "0");
				//fundJSON.put("cny", "0");
				fundJSON.put("usd", "0");
				fundJSON.put("eth", "0");
				
				JSONObject balanceJSON = new JSONObject();
				balanceJSON.put("btc", balanceResJSON.get("btc") + "");
				balanceJSON.put("ltc", balanceResJSON.get("ltc") + "");
				//fundJSON.put("cny", "0");
				balanceJSON.put("usd", balanceResJSON.get("usd") + "");
				balanceJSON.put("eth", balanceResJSON.get("eth") + "");
				
				dataJSON.put("balance", balanceJSON);
				dataJSON.put("fund", fundJSON);
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + responseJSON.get("error"));
			}
			//ConstatFinalUtil.OUTER_LOG.info(this.findid() + "--获取用户信息返回.--" + responseJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取用户信息失败."
					+ requestMap + ";原始信息:" + result , e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 下单交易
	 * 
	 * @return
	 */
	public JSONObject trade(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		//开发版本
		if("1".equalsIgnoreCase(ConstatFinalUtil.SYS_BUNDLE.getString("dev.version")))
		{
			//模拟假数据
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("order_id", new Random().nextInt(10000));
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "---模拟下单---" + paramsMap);
			return resultJSON ; 
		}
		
		HTTPUtil httpUtil = new HTTPUtil();
		Map<String, String> requestMap = new HashMap<String, String>();
		requestMap.put("requestURL", "https://btc-e.com/tapi");
		requestMap.put("method", "Trade");
		requestMap.put("nonce", this.findReqtime());
		// 查询货币的类型
		String pair = this.convertMoneyType(paramsMap.get("moneytype"));
		requestMap.put("pair", pair);
		// 买还是卖.0:买,1:卖
		if ("0".equalsIgnoreCase(paramsMap.get("type")))
		{
			requestMap.put("type", "buy");
		} else if ("1".equalsIgnoreCase(paramsMap.get("type")))
		{
			requestMap.put("type", "sell");
		}
		requestMap.put("rate", paramsMap.get("price"));
		requestMap.put("amount", paramsMap.get("amount"));

		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("Key", authJSON.get("Key") + "");
		String sign = this.authSign(requestMap);
		headerMap.put("Sign", sign);
		JSONObject responseJSON = new JSONObject();
		String result = "" ; 
		try
		{
			result = httpUtil.methodHttpsPost(headerMap, requestMap);
			responseJSON = (JSONObject) JSON.parse(result);
			
			if("1".equalsIgnoreCase(responseJSON.get("success") + ""))
			{
				JSONObject returnJSON = (JSONObject) responseJSON.get("return");
				JSONObject dataJSON = new JSONObject();
				dataJSON.put("order_id", returnJSON.get("order_id"));
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + responseJSON.get("error"));
			}
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "--下单交易返回--原始信息:" + result
					+ ";转换后目标信息:" + resultJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "下单交易失败."
					+ requestMap + ";原始信息:" + result , e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 撤销订单
	 * 
	 * @return
	 */
	public JSONObject cancelOrder(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		
		//开发版本
		if("1".equalsIgnoreCase(ConstatFinalUtil.SYS_BUNDLE.getString("dev.version")))
		{
			//模拟假数据
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("order_id", paramsMap.get("order_id"));
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "---模拟下单---" + paramsMap);
			return resultJSON ; 
		}
		
		Map<String, String> requestMap = new HashMap<String, String>();
		HTTPUtil httpUtil = new HTTPUtil();
		requestMap.put("requestURL", "https://btc-e.com/tapi");
		requestMap.put("method", "CancelOrder");
		requestMap.put("nonce", this.findReqtime());
		requestMap.put("order_id", paramsMap.get("order_id") + "");
		
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("Key", authJSON.get("Key") + "");
		String sign = this.authSign(requestMap);
		headerMap.put("Sign", sign);
		JSONObject responseJSON = new JSONObject();
		String result = "" ; 
		try
		{
			result = httpUtil.methodHttpsPost(headerMap , requestMap);
			responseJSON = (JSONObject) JSON.parse(result);
			//ConstatFinalUtil.SYS_LOG.info("=--==>" + response);
			if("1".equalsIgnoreCase(responseJSON.get("success") + ""))
			{
				JSONObject returnJSON = (JSONObject) responseJSON.get("return");
				JSONObject dataJSON = new JSONObject();
				dataJSON.put("order_id", returnJSON.get("order_id"));
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + responseJSON.get("error"));
			}
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "--撤销订单返回.--原始信息:" + result
					+ ";转换后目标信息:" + resultJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "撤销订单失败."
					+ requestMap + ";原始信息:" + result , e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * 获取用户挂单
	 * 
	 * {
			"success":1,
			"return":{
				"343152":{
					"pair":"btc_usd",
					"type":"sell",
					"amount":12.345,
					"rate":485,
					"timestamp_created":1342448420,
					"status":0
				},
				...
			}
		}
	 * 
	 * @param args
	 */
	public JSONObject getOrders(Map<String, String> requestMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		
		//开发版本
		if("1".equalsIgnoreCase(ConstatFinalUtil.SYS_BUNDLE.getString("dev.version")))
		{
			//模拟假数据
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("ordersList", new JSONArray());
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "---模拟下单---" + requestMap);
			return resultJSON ; 
		}
		
		Map<String, String> paramsMap = new HashMap<String, String>();
		HTTPUtil httpUtil = new HTTPUtil();
		paramsMap.put("requestURL", "https://btc-e.com/tapi");
		paramsMap.put("method", "ActiveOrders");
		paramsMap.put("pair", requestMap.get("moneytype") + "");
		paramsMap.put("nonce", this.findReqtime());
		
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("Key", authJSON.get("Key") + "");
		String sign = this.authSign(paramsMap);
		headerMap.put("Sign", sign);
		JSONObject responseJSON = new JSONObject();
		String result = "" ; 
		try
		{
			result = httpUtil.methodHttpsPost(headerMap, paramsMap);
			//ConstatFinalUtil.SYS_LOG.info(res);
			responseJSON = (JSONObject) JSON.parse(result);
			if("1".equalsIgnoreCase(responseJSON.get("success") + ""))
			{
				JSONObject dataJSON = new JSONObject();
				
				JSONArray ordersResArr = new JSONArray();
				JSONObject returnJSON = (JSONObject) responseJSON.get("return");
				for (Iterator iterator = returnJSON.entrySet().iterator(); iterator
						.hasNext();)
				{
					Entry me = (Entry) iterator.next() ; 
					String key = me.getKey() + "" ;
					JSONObject valJSON = (JSONObject) me.getValue() ;
					
					JSONObject ordersResJSON = new JSONObject();
					ordersResJSON.put("order_id", key);
					ordersResJSON.put("price", valJSON.get("rate") + "");
					ordersResJSON.put("amount", valJSON.get("amount") + "");
					//处理下单时间
					Date createDate = new Date(Long.valueOf(valJSON.get("timestamp_created") + "") * 1000);
					ordersResJSON.put("create_date", this.dateUtil.formatDateTime(createDate));
					
					//查询订单信息,获取剩下的已成功交易量
					requestMap.clear();
					requestMap.put("order_id", key);
					double dealAmount = this.findOneOrders(requestMap);
					
					ordersResJSON.put("deal_amount", Double.valueOf(valJSON.get("amount") + "") - dealAmount + "");
					String pair = valJSON.get("pair") + "";
					if("btc_cny".equalsIgnoreCase(pair))
					{
						pair = "1" ; 
					}
					ordersResJSON.put("symbol", pair);
					String type = valJSON.get("type") + "";
					if("buy".equalsIgnoreCase(type))
					{
						type = "0" ; 
					}else if("sell".equalsIgnoreCase(type))
					{
						type = "1" ; 
					}
					ordersResJSON.put("type", valJSON.get("type") + "");
					ordersResJSON.put("status", valJSON.get("status") + "");
					
					ordersResArr.add(ordersResJSON);
				}
				
				dataJSON.put("ordersList", ordersResArr);
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			}else if("0".equalsIgnoreCase(responseJSON.get("success") + "") && "no orders".equalsIgnoreCase(responseJSON.get("error") + ""))
			{
				JSONObject dataJSON = new JSONObject();
				
				JSONArray ordersResArr = new JSONArray();
				dataJSON.put("ordersList", ordersResArr);
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0") + "," + responseJSON.get("error"));
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + responseJSON.get("error"));
			}
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "--获取挂单息返回.--原始信息:" + result
					+ ";转换后目标信息:" + resultJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取挂单失败."
					+ paramsMap + ";原始信息:" + result , e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}
	
	/**
	 * 获取订单的剩余成交量
	 * 
	 * @param args
	 */
	public double findOneOrders(Map<String, String> requestMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		
		Map<String, String> paramsMap = new HashMap<String, String>();
		HTTPUtil httpUtil = new HTTPUtil();
		paramsMap.put("requestURL", "https://btc-e.com/tapi");
		paramsMap.put("method", "OrderInfo");
		paramsMap.put("order_id", requestMap.get("order_id") + "");
		paramsMap.put("nonce", this.findReqtime());
		
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("Key", authJSON.get("Key") + "");
		String sign = this.authSign(paramsMap);
		headerMap.put("Sign", sign);
		JSONObject responseJSON = new JSONObject();
		String result = "" ; 
		try
		{
			result = httpUtil.methodHttpsPost(headerMap, paramsMap);
			responseJSON = (JSONObject) JSON.parse(result);
			if("1".equalsIgnoreCase(responseJSON.get("success") + ""))
			{
				JSONObject returnJSON = (JSONObject) responseJSON.get("return");
				for (Iterator iterator = returnJSON.entrySet().iterator(); iterator
						.hasNext();)
				{
					Entry me = (Entry) iterator.next() ; 
					String key = me.getKey() + "" ;
					JSONObject valJSON = (JSONObject) me.getValue() ;
					
					JSONObject ordersResJSON = new JSONObject();
					ordersResJSON.put("order_id", key);
					ordersResJSON.put("price", valJSON.get("rate") + "");
					ordersResJSON.put("amount", valJSON.get("start_amount") + "");
					
					//查询订单信息,获取剩下的已成功交易量
					ordersResJSON.put("deal_amount", valJSON.get("amount") + "");
					String pair = valJSON.get("pair") + "";
					if("btc_cny".equalsIgnoreCase(pair))
					{
						pair = "1" ; 
					}
					ordersResJSON.put("symbol", pair);
					String type = valJSON.get("type") + "";
					if("buy".equalsIgnoreCase(type))
					{
						type = "0" ; 
					}else if("sell".equalsIgnoreCase(type))
					{
						type = "1" ; 
					}
					ordersResJSON.put("type", valJSON.get("type") + "");
					ordersResJSON.put("status", valJSON.get("status") + "");
					
					return Double.valueOf(valJSON.get("amount") + ""); 
				}
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + responseJSON.get("error"));
			}
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "查询订单详情失败."
					+ paramsMap + ";原始信息:" + result , e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return 0;
	}

	/**
	 * 行情API
	 */
	public JSONObject queryTicker(Map<String, String> paramsMap)
	{

		HTTPUtil httpUtil = new HTTPUtil();
		String symbol = paramsMap.get("symbol");
		if ("ltc_cny".equalsIgnoreCase(symbol))
		{
			paramsMap.put("requestURL",
					"https://www.okcoin.com/api/ticker.do?symbol=ltc_cny");
		} else
		{
			paramsMap.put("requestURL", "https://www.okcoin.com/api/ticker.do");
		}


		JSONObject resultJSON = new JSONObject();
		String result = "" ; 
		try
		{
			result = httpUtil.methodGet(paramsMap);
			resultJSON = (JSONObject) JSON.parse(result);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "行情API失败."
					+ paramsMap + ";原始信息:" + result , e);
		}
		return resultJSON;
	}

	/**
	 * depth(市场深度)
	 */
	public JSONObject queryDepth(Map<String, String> paramsMap)
	{
		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		
		HTTPUtil httpUtil = new HTTPUtil();

		Map<String, String> requestMap = new HashMap<String, String>();
		// 查询货币的类型
		String pair = this.convertMoneyType(paramsMap.get("moneytype"));
		requestMap.put("requestURL", "https://btc-e.com/api/3/depth/" + pair);

		JSONObject responseJSON = new JSONObject();
		String result = "" ; 
		try
		{
			result = httpUtil.methodHttpsPost(Collections.EMPTY_MAP , requestMap);
			responseJSON = (JSONObject) JSON.parse(result);
			responseJSON = (JSONObject) responseJSON.get(paramsMap.get("moneytype"));

			// 拼装符合条件的json数据
			JSONObject dataJSON = new JSONObject();

			int count = 0;
			// 按照规则排序的容器
			Map<Double, JSONObject> asksMap = new TreeMap<Double, JSONObject>();
			JSONArray asksResArr = new JSONArray();
			JSONArray asksArr = (JSONArray) responseJSON.get("asks");
			for (Iterator iterator = asksArr.iterator(); iterator.hasNext();)
			{
				JSONArray asksTempArr = (JSONArray) iterator.next();
				JSONObject askJSON = new JSONObject();
				askJSON.put("count", "1");
				askJSON.put("vol", asksTempArr.get(1) + "");
				askJSON.put("rate", asksTempArr.get(0) + "");

				asksMap.put(Double.valueOf(askJSON.get("rate") + ""), askJSON);
			}

			for (Iterator iterator = asksMap.entrySet().iterator(); iterator
					.hasNext();)
			{
				Entry me = (Entry) iterator.next();
				double key = (Double) me.getKey();

				if (count >= ConstatFinalUtil.PAGE_BATCH_SIZE)
				{
					break;
				}

				JSONObject asksTemp = (JSONObject) me.getValue();

				asksResArr.add(asksTemp);
				count++;
			}

			dataJSON.put("asks", asksResArr);

			// 按照规则排序的容器
			count = 0;
			Map<Double, JSONObject> bidsMap = new TreeMap<Double, JSONObject>(
					new Comparator<Double>()
					{
						@Override
						public int compare(Double o1, Double o2)
						{
							return o2.compareTo(o1);
						}
					});
			JSONArray bidsResArr = new JSONArray();
			JSONArray bidsArr = (JSONArray) responseJSON.get("bids");
			for (Iterator iterator = bidsArr.iterator(); iterator.hasNext();)
			{
				JSONArray bidsTempArr = (JSONArray) iterator.next();
				JSONObject bidsJSON = new JSONObject();
				bidsJSON.put("count", "1");
				bidsJSON.put("vol", bidsTempArr.get(1) + "");
				bidsJSON.put("rate", bidsTempArr.get(0) + "");

				bidsMap.put(Double.valueOf(bidsJSON.get("rate") + ""), bidsJSON);
			}

			for (Iterator iterator = bidsMap.entrySet().iterator(); iterator
					.hasNext();)
			{
				Entry me = (Entry) iterator.next();
				double key = (Double) me.getKey();

				if (count >= ConstatFinalUtil.PAGE_BATCH_SIZE)
				{
					break;
				}

				JSONObject bidsTemp = (JSONObject) me.getValue();

				bidsResArr.add(bidsTemp);
				count++;
			}
			dataJSON.put("bids", bidsResArr);

			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			resultJSON.put("data", dataJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "depth(市场深度)失败."
					+ paramsMap + ";原始信息:" + result , e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	}

	/**
	 * trades的格式,历史交易记录
	 */
	public JSONArray queryHistoryTrades(String symbol,
			Map<String, String> paramsMap)
	{

		HTTPUtil httpUtil = new HTTPUtil();

		if ("ltc_cny".equalsIgnoreCase(symbol))
		{
			paramsMap.put("requestURL",
					" https://www.okcoin.com/api/trades.do?symbol=ltc_cny");
		} else
		{
			paramsMap.put("requestURL",
					" https://www.okcoin.com/api/trades.do?");
		}
		JSONArray resultJSON = new JSONArray();
		String result = "" ; 
		try
		{
			result = httpUtil.methodGet(paramsMap);
			resultJSON = (JSONArray) JSON.parse(result);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "历史交易记录失败."
					+ paramsMap + ";原始信息:" + result , e);
		}
		return resultJSON;
	}

	/**
	 * 获取所有的行情
	 * 
	 * @return
	 */
	public JSONObject getTrade()
	{
		JSONObject resultJSON = new JSONObject();
		String hlUserURL = "http://www.okcoin.com/";
		try
		{
			// 请求服务
			Connection conn = Jsoup.connect(hlUserURL);
			conn.timeout(ConstatFinalUtil.REQ_CONNECT_TIMEOUT);

			conn.header(
					"Cookie",
					"abtest=\"16,34\\|18,41\\|22,60\\|14,29\\|15,33\"; userParamsCookie=trace=g1081; aburl=1; cy=160; __utma=1.543273261.1351234587.1351234587.1351234587.1; __utmz=1.1351234587.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); _hc.v=\"\"22a8bf0b-7caf-468f-ac01-88534d000d02.1351216735\"\"; cye=zhengzhou; is=743412785865");
			conn.header(
					"User-Agent",
					"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727; .NET CLR 3.0.04506.648; .NET CLR 3.5.21022)");

			Document docu = conn.get();

			/* 买入成效价 */
			JSONArray mairuArr = new JSONArray();
			Elements mairuTrades = docu
					.getElementsByAttributeValue("class", "transaction").get(0)
					.child(1).children();
			for (Iterator iterator = mairuTrades.iterator(); iterator.hasNext();)
			{
				Element eleTemp = (Element) iterator.next();
				Elements tdsTemp = eleTemp.children();
				JSONObject mairuTempJSON = new JSONObject();
				// 1买入
				mairuTempJSON.put("type", tdsTemp.get(0).text());
				mairuTempJSON.put("price", tdsTemp.get(1).text());
				mairuTempJSON.put("num", tdsTemp.get(2).text());
				mairuArr.add(mairuTempJSON);
			}
			resultJSON.put("mairu", mairuArr);

			/* 买出成效价 */
			JSONArray maichuArr = new JSONArray();
			Elements maichuTrades = docu
					.getElementsByAttributeValue("class", "transaction").get(1)
					.child(1).children();
			for (Iterator iterator = maichuTrades.iterator(); iterator
					.hasNext();)
			{
				Element eleTemp = (Element) iterator.next();
				Elements tdsTemp = eleTemp.children();
				JSONObject maichuTempJSON = new JSONObject();
				// 1买入
				maichuTempJSON.put("type", tdsTemp.get(0).text());
				maichuTempJSON.put("price", tdsTemp.get(1).text());
				maichuTempJSON.put("num", tdsTemp.get(2).text());
				maichuArr.add(maichuTempJSON);
			}
			resultJSON.put("maichu", maichuArr);

		} catch (IOException e)
		{
			ConstatFinalUtil.OUTER_LOG.error("获取OKcoin的行情失败了" , e);
		}
		return resultJSON;
	}

	public void setAuthJSON(JSONObject authJSON)
	{
		this.authJSON = authJSON;
	}

	@Override
	public String convertMoneyType(String moneytype)
	{
		String result = moneytype ; 
		return result;
	}

	@Override
	public String toString()
	{
		return "BtceTradeServiceImplay [findid()=" + findWebsiteId() + "]";
	}

	@Override
	public JSONObject getOrdersSingle(Map<String, String> requestMap)
	{

		long st = System.currentTimeMillis() ; 
		//拼装目标数据
		JSONObject resultJSON = new JSONObject();
		
		//开发版本
		if("1".equalsIgnoreCase(ConstatFinalUtil.SYS_BUNDLE.getString("dev.version")))
		{
			//模拟假数据
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("ordersList", new JSONArray());
			
			resultJSON.put("data", dataJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "---模拟下单---" + requestMap);
			return resultJSON ; 
		}
		
		Map<String, String> paramsMap = new HashMap<String, String>();
		HTTPUtil httpUtil = new HTTPUtil();
		paramsMap.put("requestURL", "https://btc-e.com/tapi");
		paramsMap.put("method", "OrderInfo");
		paramsMap.put("order_id", requestMap.get("order_id") + "");
		paramsMap.put("nonce", this.findReqtime());
		
		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("Key", authJSON.get("Key") + "");
		String sign = this.authSign(paramsMap);
		headerMap.put("Sign", sign);
		JSONObject responseJSON = new JSONObject();
		String result = "" ; 
		try
		{
			result = httpUtil.methodHttpsPost(headerMap, paramsMap);
			//ConstatFinalUtil.SYS_LOG.info(res);
			responseJSON = (JSONObject) JSON.parse(result);
			if("1".equalsIgnoreCase(responseJSON.get("success") + ""))
			{
				JSONObject dataJSON = new JSONObject();
				
				JSONObject returnJSON = (JSONObject) responseJSON.get("return");
				for (Iterator iterator = returnJSON.entrySet().iterator(); iterator
						.hasNext();)
				{
					Entry me = (Entry) iterator.next() ; 
					String key = me.getKey() + "" ;
					if(key.equalsIgnoreCase(requestMap.get("order_id") + ""))
					{
					
						JSONObject valJSON = (JSONObject) me.getValue() ;
						
						JSONObject ordersResJSON = new JSONObject();
						ordersResJSON.put("order_id", key);
						ordersResJSON.put("price", valJSON.get("rate") + "");
						//总数量
						ordersResJSON.put("amount", valJSON.get("start_amount") + "");
						//处理下单时间
						Date createDate = new Date(Long.valueOf(valJSON.get("timestamp_created") + "") * 1000);
						ordersResJSON.put("create_date", this.dateUtil.formatDateTime(createDate));
						/*已成交量*/
						ordersResJSON.put("deal_amount", Double.valueOf(valJSON.get("start_amount") + "") - 
								Double.valueOf(valJSON.get("amount") + "") + "");
						String pair = valJSON.get("pair") + "";
						if("btc_cny".equalsIgnoreCase(pair))
						{
							pair = "1" ; 
						}
						ordersResJSON.put("symbol", pair);
						String type = valJSON.get("type") + "";
						if("buy".equalsIgnoreCase(type))
						{
							type = "0" ; 
						}else if("sell".equalsIgnoreCase(type))
						{
							type = "1" ; 
						}
						ordersResJSON.put("type", valJSON.get("type") + "");
						/*状态
						 * 0 - active, 1 – executed order, 2 - canceled, 3 – canceled, but was partially executed.
						 * */
						String status = valJSON.get("status") + "";
						if("2".equalsIgnoreCase(status))
						{
							status = "-1";
						}else if("3".equalsIgnoreCase(status))
						{
							status = "1";
						}else if("1".equalsIgnoreCase(status))
						{
							status = "2";
						}
						ordersResJSON.put("status", status);
						
						dataJSON.put("orders", ordersResJSON);
						break ; 
					}
				}
				
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
			}else if("0".equalsIgnoreCase(responseJSON.get("success") + "") && "no orders".equalsIgnoreCase(responseJSON.get("error") + ""))
			{
				JSONObject dataJSON = new JSONObject();
				
				JSONArray ordersResArr = new JSONArray();
				dataJSON.put("orders", ordersResArr);
				resultJSON.put("data", dataJSON);
				resultJSON.put("code", "0");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0") + "," + responseJSON.get("error"));
			}else
			{
				resultJSON.put("code", "-6");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-6") + ";返回数据:" + responseJSON.get("error"));
			}
			ConstatFinalUtil.OUTER_LOG.info(this.findWebsiteId() + "--获取订单详情返回.--原始信息:" + result
					+ ";转换后目标信息:" + resultJSON);
		} catch (Exception e)
		{
			ConstatFinalUtil.OUTER_LOG.error(this.findWebsiteId() + "获取订单详情失败."
					+ paramsMap + ";原始信息:" + result , e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		long ed = System.currentTimeMillis() ;
		resultJSON.put("consume", (ed -st) + "毫秒");
		return resultJSON;
	
	}

	@Override
	public String parseMoneyType(String moneytype)
	{
		return null;
	}

	@Override
	public String findWebSiteName()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JSONObject convertOrders(String moneytype, JSONObject ordersTemp)
	{
		// TODO Auto-generated method stub
		return null;
	}
}
