package com.wang.mvchain.outer.service;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;

/**
 * 所有交易网站的接口
 * 所有的交易网站的例子以FxbtcBtcServiceImplay为主
 * @author wangsh
 *
 */
public interface ITradeService
{
	/**
	 * 获取交易网站的名字
	 * @return
	 */
	String findWebSiteName();
	
	/**
	 * 获取网站来源的编号
	 * @return
	 */
	int findWebsiteId();
	
	/**
	 * 将系统的虚拟币对转换成交易网站需要的
	 * 如:系统:btc_cny,
	 * 交易网站:转换成:btccny
	 * @return
	 */
	String convertMoneyType(String moneytypeSelf);
	
	/**
	 * 将交易网站中的币种转换成 搬砖系统中需要的
	 * @return
	 */
	String parseMoneyType(String moneytypeTrade);
	
	/**
	 * 为用户的认证信息赋值
	 * @param authJSON
	 */
	void setAuthJSON(JSONObject authJSON);
	
	/**
	 * 获取用户信息
	 * 
	 * @param paramsMap
	 * @return	返回JSON数据样例
	 * var response = {
			"code" : "0",
			"info" : "",
			"consumetime" : "",
			"data" : {
				//余额
				"balance" : {
					"btc" : "0.0003",
					"cny" : "0.5129",
					"ltc" : "0"
				},
				//冻结金额
				"fund" : {
					"btc" : "0",
					"cny" : "0.0003",
					"ltc" : "0"
				}
			}
		}
	 */
	JSONObject getUserInfo(Map<String, String> paramsMap);
	
	/**
	 * 获取用户当前挂单
	 * 
	 * @return
	 * 输入参数:
	 * moneytype:标识:ctid;表a_tradetype的id,1:btc_cny,
	 * order_id:订单ID -1:未完成订单，否则查询相应订单号的订单 
	 * 返回数据样例:
	 * var response =  {
			"code" : "0",
			"info" : "",
			"data" : {
				"ordersList" : [ {
					//订单数量
					"amount" : 4.904,
					//实际成交的价格
					"avg_price" : 0,
					//下单日期
					"create_date" : 1416920604000,
					//成交数量
					"deal_amount" : 0,
					//订单id
					"order_id" : 164494253,
					//价格
					"price" : 3000,
					//状态,-1:已撤销  0:未成交  1:部分成交  2:完全成交 3 撤单处理中
					"status" : 0,
					//标识:ctid;表a_tradetype的id,1:btc_cny,
					"symbol" : "btc_cny",
					//买还是卖.0:买,1:卖
					"type" : "sell"
				} ]
			}
		}

	 */
	JSONObject getOrders(Map<String, String> paramsMap);
	
	/**
	 * 获取单条订单的详细信息
	 * @param paramsMap
	 * moneytype:标识:ctid;表a_tradetype的id,1:btc_cny,
	 * order_id:订单的唯一id,-1:查询所有的订单
	 * @return
	 * var response =  {
			"code" : "0",
			"info" : "",
			"data" : {
			 	"orders":
			 	{
					//订单数量
					"amount" : 4.904,
					//实际成交的价格
					"avg_price" : 0,
					//下单日期
					"create_date" : 1416920604000,
					//成交数量
					"deal_amount" : 0,
					//订单id
					"order_id" : 164494253,
					//价格
					"price" : 3000,
					//状态,-1:已撤销  0:未成交  1:部分成交  2:完全成交
					"status" : 0,
					//标识:ctid;表a_tradetype的id,1:btc_cny,
					"symbol" : "btc_cny",
					//买还是卖.0:买,1:卖
					"type" : "sell"
				}
			}
		}
	 */
	JSONObject getOrdersSingle(Map<String, String> paramsMap);
	
	/**
	 * 撤销挂单
	 *
	 * @return
	 * 输入参数:
	 * moneytype:标识:ctid;表a_tradetype的id,1:btc_cny,
	 * order_id:订单ID -1:未完成订单，否则查询相应订单号的订单 
	 * 返回样例:
	 * var response = {
			"code" : "0",
			"consume" : "1542毫秒",
			"data" : {
				"order_id" : ""
			},
			"info" : "成功"
		}
	 */
	JSONObject cancelOrder(Map<String, String> paramsMap);
	
	/**
	 * 下单操作
	 * 
	 * @return
	 * 输入参数:
	 *  moneytype:标识:ctid;表a_tradetype的id,1:btc_cny,
		type:买卖类型：买还是卖.0:买,1:卖
		price:下单价格
		amount:交易数量  
	 * 返回样例:
	 * var response = {
			"code" : "0",
			"consume" : "1542毫秒",
			"data" : {
				"order_id" : ""
			},
			"info" : "成功"
		}
	 */
	JSONObject trade(Map<String, String> paramsMap);
	
	/**
	 * 查询市场深度数据
	 * 
	 * //bids是买入,asks是卖出,
	 * bids:价格从高到低排序
	 * asks:价格从低到高排序
	 * 
	 * 取最新的10条数据即可,所要的排序与网站刚好相反.
	 * @param	paramsMap	moneytype:填写btc_usd,btc_cny字样
	 * @return	对外提供数据格式一样返回的JSON样例
	 * var response = {
			"code" : "0",
			"info" : "",
			"data" : {
				"asks" : [ {
					"count" : "1",
					"rate" : "2271.6",
					"vol" : "12.5984"
				}, {
					"count" : "1",
					"rate" : "2273.15",
					"vol" : "0.307"
				} ],
				"bids" : [ {
					"count" : "1",
					"rate" : "2271.01",
					"vol" : "0.01"
				}, {
					"count" : "1",
					"rate" : "2269.9",
					"vol" : "0.02"
				} ]
			}
		}

	 */
	JSONObject queryDepth(Map<String, String> paramsMap);
	
	/**
	 * 查询当前行情数据
	 * 
	 * @return
	 */
	JSONObject queryTicker(Map<String, String> paramsMap);
	
	/**
	 * 获取所有的交易对
	 * @param paramsMap
	 * @return
{
	"code":"0",
	"info":"",
	"data":
	{
		"name_btc_usdt:
		{
	        //交易货币币种,小写;
	        "base_currency":"BTC",
	        //币对名称,小写;
	        "instrument_id":"BTC-USDT",
	        //最小交易数量
	        "min_size":"0.001",
	        //计价货币币种,小写;
	        "quote_currency":"USDT",
	        //交易货币数量精度
	        "size_increment":"0.00000001",
	        //交易价格精度
	        "tick_size":"0.1"
	    }
	}
}
	 */
	JSONObject findAllTransPair(Map<String, String> paramsMap);
}
