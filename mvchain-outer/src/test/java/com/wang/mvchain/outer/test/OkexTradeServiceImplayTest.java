package com.wang.mvchain.outer.test;


import java.util.Map;
import java.util.TreeMap;

import org.junit.Before;
import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.outer.enums.TradeEnum;
import com.wang.mvchain.outer.service.ITradeService;
import com.wang.mvchain.outer.service.implay.OkexTradeServiceImplay;

public class OkexTradeServiceImplayTest
{
	private ITradeService okBtcService = new OkexTradeServiceImplay();
	
	/**
	 * 为用户的认证信息赋值
	 * @param authJSON
	 */
	@Before
	public void setAuthJSON()
	{
		JSONObject authJSON = new JSONObject();
		
		authJSON.put("api_key", "7bce4fae-2cfe-457d-8573-b621126b122c");
		authJSON.put("secret_key", "F11A0DAF5A8D39C5D5A9F88BF0625B1B");
		
		okBtcService.setAuthJSON(authJSON);
	}
	
	/**
	 * 获取用户基本信息
	 */
	@Test
	public void userInfo()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		JSONObject resultJSON = okBtcService.getUserInfo(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- userInfo ----" + resultJSON);
	}
	
	/**
	 * 获取用户基本信息
	 */
	@Test
	public void trade()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("moneytype", "cmt_usdt");
		/* 买卖类型(buy/sell) */
		paramsMap.put("type", TradeEnum.TRADE_TYPE_SELL.getValue() + "");
		/* 价格 */
		paramsMap.put("price", "100");
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("amount", "20");
		
		JSONObject resultJSON = okBtcService.trade(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- trade ----" + resultJSON);
	}
	
	/**
	 * 撤销订单
	 */
	@Test
	public void cancelOrder()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("order_id", "2714627205639168");
		/* 订单号 */
		paramsMap.put("moneytype", "cmt_usdt");
		
		JSONObject resultJSON = okBtcService.cancelOrder(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- cancelorder ----" + resultJSON);
	}
	
	/**
	 * 获取用户挂单
	 */
	@Test
	public void getOrders()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		paramsMap.put("order_id", "");
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "cmt_usdt");
		
		JSONObject resultJSON = okBtcService.getOrders(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- getorder ----" + resultJSON);
	}
	
	/**
	 * 获取用户挂单
	 */
	@Test
	public void getOrdersSingle()
	{
		this.setAuthJSON();
		Map<String, String> paramsMap = new TreeMap<String, String>();
		paramsMap.put("order_id", "2710579437511680");
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "mith_usdt");
		
		JSONObject resultJSON = okBtcService.getOrdersSingle(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- getorder ----" + resultJSON);
	}
	
	/**
	 * depth(市场深度)
	 */
	@Test
	public void depth()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "cmt_usdt");
		
		JSONObject resultJSON = okBtcService.queryDepth(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- depth ----" + resultJSON);
	}
	

	/**
	 * 查询所有的交易对
	 */
	@Test
	public void findAllTransPair()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		JSONObject resultJSON = okBtcService.findAllTransPair(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- findAllTransPair ----" + resultJSON);
	}
}
