package com.wang.mvchain.outer.test;


import java.util.Map;
import java.util.TreeMap;

import org.junit.Before;
import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.outer.enums.TradeEnum;
import com.wang.mvchain.outer.service.ITradeService;
import com.wang.mvchain.outer.service.implay.BiBoxTradeServiceImplay;

public class BiboxTradeServiceImplayTest
{
	private ITradeService biboxTradeService = new BiBoxTradeServiceImplay();
	
	/**
	 * 为用户的认证信息赋值
	 * @param authJSON
	 */
	@Before
	public void setAuthJSON()
	{
		JSONObject authJSON = new JSONObject();
		
		authJSON.put("apikey", "2668fd042349d520080a15e841d46fc635adcc82");
		authJSON.put("secret", "78715709ce76b2464da506b274f85ed90c6c1742");
		
		biboxTradeService.setAuthJSON(authJSON);
	}
	
	/**
	 * 获取用户基本信息
	 */
	@Test
	public void userInfo()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		JSONObject resultJSON = biboxTradeService.getUserInfo(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- userInfo ----" + resultJSON);
	}
	
	/**
	 * 获取用户基本信息
	 */
	@Test
	public void trade()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("moneytype", "eos_usdt");
		/* 买卖类型(buy/sell) */
		paramsMap.put("type", TradeEnum.TRADE_TYPE_BUY.getValue() + "");
		/* 价格 */
		paramsMap.put("price", "1");
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("amount", "1");
		
		JSONObject resultJSON = biboxTradeService.trade(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- trade ----" + resultJSON);
	}
	
	/**
	 * 撤销订单
	 */
	@Test
	public void cancelOrder()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("order_id", "12197982069435521");
		/* 订单号 */
		paramsMap.put("moneytype", "eos_usdt");
		
		JSONObject resultJSON = biboxTradeService.cancelOrder(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- cancelorder ----" + resultJSON);
	}
	
	/**
	 * 获取用户挂单
	 */
	@Test
	public void getOrders()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		paramsMap.put("order_id", "");
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "eos_usdt");
		
		JSONObject resultJSON = biboxTradeService.getOrders(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- getorder ----" + resultJSON);
	}
	
	/**
	 * 获取用户挂单
	 */
	@Test
	public void getOrdersSingle()
	{
		this.setAuthJSON();
		Map<String, String> paramsMap = new TreeMap<String, String>();
		paramsMap.put("order_id", "12197982069453619");
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "eos_usdt");
		
		JSONObject resultJSON = biboxTradeService.getOrdersSingle(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- getorder ----" + resultJSON);
	}
	
	/**
	 * depth(市场深度)
	 */
	@Test
	public void depth()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "eos_usdt");
		
		JSONObject resultJSON = biboxTradeService.queryDepth(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- depth ----" + resultJSON);
	}
	

	/**
	 * 查询所有的交易对
	 */
	@Test
	public void findAllTransPair()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		JSONObject resultJSON = biboxTradeService.findAllTransPair(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- findAllTransPair ----" + resultJSON);
	}
}
