package com.wang.mvchain.outer.test;


import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.outer.service.ITradeService;
import com.wang.mvchain.outer.service.implay.YunbiTradeServiceImplay;

public class YunbiTradeServiceImplayTest
{
	private ITradeService yunbiService = new YunbiTradeServiceImplay();
	
	/**
	 * 为用户的认证信息赋值
	 * @param authJSON
	 */
	void setAuthJSON()
	{
		JSONObject authJSON = new JSONObject();
		/*
		 * Access Key:hmMGazoofvLUFhadZgCoN3l5SQCC3o4s8OWppAMb	
			Secret Key:W8j7OlmmRvnGiSQf9Cw5w9sRp1z7CeWCB3ntsjtx
		 */
		authJSON.put("access_key", "SWWbLWssYxJ5DkNQiTZpVD4fzqSCYzya91QuEgni");
		authJSON.put("secret_key", "D8xdcWqCid1QtvwIsmoLDI2QVzLKrXjAjMxgIK9g");
		
		yunbiService.setAuthJSON(authJSON);
	}
	
	/**
	 * 获取用户基本信息
	 */
	@Test
	public void userInfo()
	{
		setAuthJSON();
		
		Map<String, String> paramsMap = new TreeMap<String, String>();
		JSONObject resultJSON = yunbiService.getUserInfo(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- userInfo ----" + resultJSON);
	}
	
	/**
	 * 获取用户基本信息
	 */
	@Test
	public void trade()
	{
		this.setAuthJSON();
		Map<String, String> paramsMap = new TreeMap<String, String>();
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("moneytype", "eth_cny");
		/* 买卖类型(buy/sell) */
		paramsMap.put("type", "1");
		/* 价格 */
		paramsMap.put("price", "8000");
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("amount", "1");
		
		JSONObject resultJSON = yunbiService.trade(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- trade ----" + resultJSON);
	}
	
	/**
	 * 撤销订单
	 */
	@Test
	public void cancelorder()
	{
		this.setAuthJSON();
		Map<String, String> paramsMap = new TreeMap<String, String>();
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("order_id", "230663620");
		/* 订单号 */
		paramsMap.put("moneytype", "eth_cny");
		
		JSONObject resultJSON = yunbiService.cancelOrder(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- cancelorder ----" + resultJSON);
	}
	
	/**
	 * 获取用户挂单
	 */
	@Test
	public void getorder()
	{
		this.setAuthJSON();
		Map<String, String> paramsMap = new TreeMap<String, String>();
		paramsMap.put("order_id", "-1");
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "eth_cny");
		
		JSONObject resultJSON = yunbiService.getOrders(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- getorder ----" + resultJSON);
	}
	
	/**
	 * 获取订单详情
	 */
	@Test
	public void getOrdersSingle()
	{
		this.setAuthJSON();
		Map<String, String> paramsMap = new TreeMap<String, String>();
		paramsMap.put("order_id", "232280814");
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "eth_cny");
		
		JSONObject resultJSON = yunbiService.getOrdersSingle(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- getorder ----" + resultJSON);
	}
	
	/**
	 * depth(市场深度)
	 */
	@Test
	public void depth()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "etc_cny");
		
		JSONObject resultJSON = yunbiService.queryDepth(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- depth ----" + resultJSON);
	}
	
	/**
	 * depth(市场深度)
	 */
	@Test
	public void trades()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("since", "5000");
		
		JSONObject resultJSON = yunbiService.trade(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- depth ----" + resultJSON);
		
	}
}
