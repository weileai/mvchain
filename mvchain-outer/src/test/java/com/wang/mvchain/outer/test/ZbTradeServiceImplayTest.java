package com.wang.mvchain.outer.test;


import java.util.Map;
import java.util.TreeMap;

import org.junit.Before;
import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.outer.enums.TradeEnum;
import com.wang.mvchain.outer.service.ITradeService;
import com.wang.mvchain.outer.service.implay.ZbTradeServiceImplay;

public class ZbTradeServiceImplayTest
{
	private ITradeService chbtcService = new ZbTradeServiceImplay();
	
	/**
	 * 为用户的认证信息赋值
	 * 
	 * API访问密钥(Access Key)： 30131e3d-d59f-47de-bb7b-c1574cc8e560
	   API私有密钥(Secret Key)： 7e07f440-1a3d-4a0a-ac37-7d76461dde85 
	 * 
	 * @param authJSON
	 */
	@Before
	public void setAuthJSON()
	{
		JSONObject authJSON = new JSONObject();
		//APPID 11060
		authJSON.put("accesskey", "30131e3d-d59f-47de-bb7b-c1574cc8e560");
		authJSON.put("secretkey", "7e07f440-1a3d-4a0a-ac37-7d76461dde85");
		
		chbtcService.setAuthJSON(authJSON);
	}
	
	/**
	 * 获取用户基本信息
	 */
	@Test
	public void userInfo()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		JSONObject resultJSON = chbtcService.getUserInfo(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- userInfo ----" + resultJSON);
	}
	
	/**
	 * 获取用户基本信息
	 */
	@Test
	public void trade()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("moneytype", "etc_usdt");
		/* 买卖类型(buy/sell) */
		paramsMap.put("type", TradeEnum.TRADE_TYPE_BUY.getValue() + "");
		/* 价格 */
		paramsMap.put("price", "1.001");
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("amount", "0.511");
		
		JSONObject resultJSON = chbtcService.trade(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- trade ----" + resultJSON);
	}
	
	/**
	 * 撤销订单
	 */
	@Test
	public void cancelorder()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("order_id", "20190415381239180");
		/* 订单号 */
		paramsMap.put("moneytype", "etc_usdt");
		
		JSONObject resultJSON = chbtcService.cancelOrder(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- cancelorder ----" + resultJSON);
	}
	
	/**
	 * 获取用户挂单
	 */
	@Test
	public void getorder()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		paramsMap.put("order_id", "-1");
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "etc_usdt");
		
		JSONObject resultJSON = chbtcService.getOrders(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- getorder ----" + resultJSON);
	}
	
	/**
	 * 获取订单详情
	 */
	@Test
	public void getOrdersSingle()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		paramsMap.put("order_id", "2019042416861591");
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "xem_usdt");
		
		JSONObject resultJSON = chbtcService.getOrdersSingle(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- getorder ----" + resultJSON);
	}
	
	
	/**
	 * depth(市场深度)
	 */
	@Test
	public void depth()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "etc_usdt");
		
		JSONObject resultJSON = chbtcService.queryDepth(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- depth ----" + resultJSON);
	}
	
	/**
	 * depth(市场深度)
	 */
	@Test
	public void trades()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("since", "5000");
		
		JSONObject resultJSON = chbtcService.trade(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- depth ----" + resultJSON);
	}
	
	/**
	 * 查询所有的交易对
	 */
	@Test
	public void findAllTransPair()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		JSONObject resultJSON = chbtcService.findAllTransPair(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- findAllTransPair ----" + resultJSON);
	}
}
