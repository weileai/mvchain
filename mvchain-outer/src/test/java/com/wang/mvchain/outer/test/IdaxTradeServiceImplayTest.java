package com.wang.mvchain.outer.test;


import java.util.Map;
import java.util.TreeMap;

import org.junit.Before;
import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.outer.enums.TradeEnum;
import com.wang.mvchain.outer.service.ITradeService;
import com.wang.mvchain.outer.service.implay.IdaxTradeServiceImplay;

public class IdaxTradeServiceImplayTest
{
	private ITradeService idaxTradeService = new IdaxTradeServiceImplay();
	
	/**
	 * 为用户的认证信息赋值
	 * @param authJSON
	 */
	@Before
	public void setAuthJSON()
	{
		JSONObject authJSON = new JSONObject();
		
		authJSON.put("key", "9926a95e836a4268aecce71ca448b06390865c18de414e99a2e77ef4d1788697");
		authJSON.put("secret", "1e3253f4d2ae41118f20dbaaa260c35871467995638240099ac42c9bdcc8e3b7");
		
		idaxTradeService.setAuthJSON(authJSON);
	}
	
	/**
	 * 获取用户基本信息
	 */
	@Test
	public void userInfo()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		JSONObject resultJSON = idaxTradeService.getUserInfo(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- userInfo ----" + resultJSON);
	}
	
	/**
	 * 获取用户基本信息
	 */
	@Test
	public void trade()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("moneytype", "xrp_usdt");
		/* 买卖类型(buy/sell) */
		paramsMap.put("type", TradeEnum.TRADE_TYPE_SELL.getValue() + "");
		/* 价格 */
		paramsMap.put("price", "1");
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("amount", "1");
		
		JSONObject resultJSON = idaxTradeService.trade(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- trade ----" + resultJSON);
	}
	
	/**
	 * 批量交易
	 */
	@Test
	public void tradeBatch()
	{
		String moneyBatch = "btc_usdt,eth_usdt,ltc_usdt,xrp_usdt,etc_usdt,it_usdt,csac_usdt,btd_usdt,doge_usdt,bk_usdt,flc_usdt,lina_usdt,gmb_usdt,dbx_usdt,est_usdt,vex_ust,gfc_usdt,nty_usdt,ttc_usdt,baw_usdt,eve_usdt_,kze_usdt,gunthy_usdt,wicc_usdt,nuls_usdt," ; 
		String[] moneyBatchs = moneyBatch.split(",");
		for (int i = 0; i < moneyBatchs.length; i++)
		{
			String moneyType = moneyBatchs[i];
			
			Map<String, String> paramsMap = new TreeMap<String, String>();
			/* 当前货币兑(btc_cny,ltc_cny) */
			paramsMap.put("moneytype", moneyType);
			/* 买卖类型(buy/sell) */
			paramsMap.put("type", TradeEnum.TRADE_TYPE_SELL.getValue() + "");
			/* 价格 */
			paramsMap.put("price", "1");
			/* 当前货币兑(btc_cny,ltc_cny) */
			paramsMap.put("amount", "1");
			
			JSONObject resultJSON = idaxTradeService.trade(paramsMap);
			ConstatFinalUtil.SYS_LOG.info(moneyType + "---- trade ----" + resultJSON);
			
			try
			{
				Thread.sleep(10000);
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 撤销订单
	 */
	@Test
	public void cancelOrder()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("order_id", "3260601320000644551");
		/* 订单号 */
		paramsMap.put("moneytype", "eth_usdt");
		
		JSONObject resultJSON = idaxTradeService.cancelOrder(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- cancelorder ----" + resultJSON);
	}
	
	/**
	 * 获取用户挂单
	 */
	@Test
	public void getOrders()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		paramsMap.put("order_id", "");
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "btc_usdt");
		
		JSONObject resultJSON = idaxTradeService.getOrders(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- getorder ----" + resultJSON);
	}
	
	/**
	 * 获取用户挂单
	 */
	@Test
	public void getOrdersSingle()
	{
		this.setAuthJSON();
		Map<String, String> paramsMap = new TreeMap<String, String>();
		paramsMap.put("order_id", "3260601300000731351");
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "etc_usdt");
		
		JSONObject resultJSON = idaxTradeService.getOrdersSingle(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- getorder ----" + resultJSON);
	}
	
	/**
	 * depth(市场深度)
	 */
	@Test
	public void depth()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "xrp_usdt");
		
		JSONObject resultJSON = idaxTradeService.queryDepth(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- depth ----" + resultJSON);
	}
	

	/**
	 * 查询所有的交易对
	 */
	@Test
	public void findAllTransPair()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		JSONObject resultJSON = idaxTradeService.findAllTransPair(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- findAllTransPair ----" + resultJSON);
	}
}
