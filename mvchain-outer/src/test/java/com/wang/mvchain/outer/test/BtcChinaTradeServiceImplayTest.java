package com.wang.mvchain.outer.test;

import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.outer.service.ITradeService;
import com.wang.mvchain.outer.service.implay.BtcChinaTradeServiceImplay;

public class BtcChinaTradeServiceImplayTest
{
	private ITradeService btcChinaService = new BtcChinaTradeServiceImplay();
	
	/**
	 * 为用户的认证信息赋值
	 * @param authJSON
	 */
	void setAuthJSON()
	{
		JSONObject authJSON = new JSONObject();
		authJSON.put("Key", "72ec704c-aadc-4ae9-829e-b2e27e42f935");
		authJSON.put("Secret", "98275c6b-fb1b-4a1f-bba1-f068c7379907");
		
		btcChinaService.setAuthJSON(authJSON);
	}
	
	/**
	 * 获取用户基本信息
	 */
	@Test
	public void userInfo()
	{
		setAuthJSON();
		
		Map<String, String> paramsMap = new TreeMap<String, String>();
		JSONObject resultJSON = btcChinaService.getUserInfo(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- userInfo ----" + resultJSON);
	}
	
	/**
	 * 获取用户基本信息
	 */
	@Test
	public void trade()
	{
		this.setAuthJSON();
		Map<String, String> paramsMap = new TreeMap<String, String>();
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("moneytype", "btc_cny");
		/* 买卖类型(buy/sell) */
		paramsMap.put("type", "1");
		/* 价格 */
		paramsMap.put("price", "8000");
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("amount", "0.1");
		
		JSONObject resultJSON = btcChinaService.trade(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- trade ----" + resultJSON);
	}
	
	/**
	 * 撤销订单
	 */
	@Test
	public void cancelorder()
	{
		this.setAuthJSON();
		Map<String, String> paramsMap = new TreeMap<String, String>();
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("order_id", "664432966");
		
		JSONObject resultJSON = btcChinaService.cancelOrder(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- cancelorder ----" + resultJSON);
	}
	
	/**
	 * 获取用户挂单
	 */
	@Test
	public void getorder()
	{
		this.setAuthJSON();
		Map<String, String> paramsMap = new TreeMap<String, String>();
		//paramsMap.put("order_id", "-1");
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "btc_cny");
		
		JSONObject resultJSON = btcChinaService.getOrders(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- getorder ----" + resultJSON);
	}
	
	/**
	 * 获取订单详情
	 */
	@Test
	public void getOrdersSingle()
	{
		this.setAuthJSON();
		Map<String, String> paramsMap = new TreeMap<String, String>();
		paramsMap.put("order_id", "661757991");
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "btc_cny");
		
		JSONObject resultJSON = btcChinaService.getOrdersSingle(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- getorder ----" + resultJSON);
	}
	
	/**
	 * depth(市场深度)
	 */
	@Test
	public void depth()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "btc_usd");
		
		JSONObject resultJSON = btcChinaService.queryDepth(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- depth ----" + resultJSON);
	}
	
	/**
	 * depth(市场深度)
	 */
	@Test
	public void trades()
	{
		this.setAuthJSON();
		Map<String, String> paramsMap = new TreeMap<String, String>();
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("since", "5000");
		
		JSONObject resultJSON = btcChinaService.trade(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- depth ----" + resultJSON);
		
	}
}
