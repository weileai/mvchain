package com.wang.mvchain.outer.test;


import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.outer.service.ITradeService;
import com.wang.mvchain.outer.service.implay.BtceTradeServiceImplay;

public class BtceTradeServiceImplayTest
{
	private ITradeService btceService = new BtceTradeServiceImplay();
	
	/**
	 * 为用户的认证信息赋值
	 * @param authJSON
	 */
	void setAuthJSON()
	{
		JSONObject authJSON = new JSONObject();
		authJSON.put("Key", "MZZZPMBO-VWENVQMM-5XQUD67F-WM93MPJA-LQWH24DV");
		authJSON.put("Secret", "05933071dc583389055925bef4d121b26eeb09bd45fa25b966527ae1fbc4b8d4");
		
		btceService.setAuthJSON(authJSON);
	}
	
	/**
	 * 获取用户基本信息
	 */
	@Test
	public void userInfo()
	{
		setAuthJSON();
		
		Map<String, String> paramsMap = new TreeMap<String, String>();
		JSONObject resultJSON = btceService.getUserInfo(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- userInfo ----" + resultJSON);
	}
	
	/**
	 * 获取用户基本信息
	 */
	@Test
	public void trade()
	{
		this.setAuthJSON();
		Map<String, String> paramsMap = new TreeMap<String, String>();
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("moneytype", "btc_usd");
		/* 买卖类型(buy/sell) */
		paramsMap.put("type", "1");
		/* 价格 */
		paramsMap.put("price", "1000.2");
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("amount", "0.1");
		
		JSONObject resultJSON = btceService.trade(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- trade ----" + resultJSON);
	}
	
	/**
	 * 撤销订单
	 */
	@Test
	public void cancelorder()
	{
		this.setAuthJSON();
		Map<String, String> paramsMap = new TreeMap<String, String>();
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("order_id", "1174972403");
		
		JSONObject resultJSON = btceService.cancelOrder(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- cancelorder ----" + resultJSON);
	}
	
	/**
	 * 获取用户挂单
	 */
	@Test
	public void getorder()
	{
		this.setAuthJSON();
		Map<String, String> paramsMap = new TreeMap<String, String>();
		//paramsMap.put("order_id", "-1");
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "btc_usd");
		
		JSONObject resultJSON = btceService.getOrders(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- getorder ----" + resultJSON);
	}
	
	/**
	 * 获取订单详情
	 */
	@Test
	public void getOrdersSingle()
	{
		this.setAuthJSON();
		Map<String, String> paramsMap = new TreeMap<String, String>();
		paramsMap.put("order_id", "1174972403");
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "btc_usd");
		
		JSONObject resultJSON = btceService.getOrdersSingle(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- getorder ----" + resultJSON);
	}
	
	/**
	 * depth(市场深度)
	 */
	@Test
	public void depth()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "btc_usd");
		
		JSONObject resultJSON = btceService.queryDepth(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- depth ----" + resultJSON);
	}
	
	/**
	 * depth(市场深度)
	 */
	@Test
	public void trades()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("since", "5000");
		
		JSONObject resultJSON = btceService.trade(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- depth ----" + resultJSON);
		
	}
}
