package com.wang.mvchain.outer.test;


import java.util.Map;
import java.util.TreeMap;

import org.junit.Before;
import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.outer.enums.TradeEnum;
import com.wang.mvchain.outer.service.ITradeService;
import com.wang.mvchain.outer.service.implay.BinanceTradeServiceImplay;

public class BinanceTradeServiceImplayTest
{
	private ITradeService binanceTradeService = new BinanceTradeServiceImplay();
	
	/**
	 * 为用户的认证信息赋值
	 * @param authJSON
	 */
	@Before
	public void setAuthJSON()
	{
		JSONObject authJSON = new JSONObject();
		
		authJSON.put("apiKey", "euCXIPafIqyUv6RpTgYBFefrY2D6hdDXssYN7XDeHPTf1SdAiHokVZ1NKMRgZdqG");
		authJSON.put("secretKey", "glHj3i8IoxtR2HTcXdaPll4SgjCNxy2OQexmySQWo1p8FnJbWlr3oPDaKyWSkU83");
		
		binanceTradeService.setAuthJSON(authJSON);
	}
	
	/**
	 * 获取用户基本信息
	 */
	@Test
	public void userInfo()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		JSONObject resultJSON = binanceTradeService.getUserInfo(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- userInfo ----" + resultJSON);
	}
	
	/**
	 * 获取用户基本信息
	 */
	@Test
	public void trade()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("moneytype", "eos_usdt");
		/* 买卖类型(buy/sell) */
		paramsMap.put("type", TradeEnum.TRADE_TYPE_BUY.getValue() + "");
		/* 价格 */
		paramsMap.put("price", "1.0");
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("amount", "2");
		
		JSONObject resultJSON = binanceTradeService.trade(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- trade ----" + resultJSON);
	}
	
	/**
	 * 撤销订单
	 */
	@Test
	public void cancelOrder()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("order_id", "223621869");
		/* 订单号 */
		paramsMap.put("moneytype", "eth_usdt");
		
		JSONObject resultJSON = binanceTradeService.cancelOrder(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- cancelorder ----" + resultJSON);
	}
	
	/**
	 * 获取用户挂单
	 */
	@Test
	public void getOrders()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		paramsMap.put("order_id", "");
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "dos_usdt");
		
		JSONObject resultJSON = binanceTradeService.getOrders(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- getorder ----" + resultJSON);
	}
	
	/**
	 * 获取用户挂单
	 */
	@Test
	public void getOrdersSingle()
	{
		this.setAuthJSON();
		Map<String, String> paramsMap = new TreeMap<String, String>();
		paramsMap.put("order_id", "223621869");
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "aos_usdt");
		
		JSONObject resultJSON = binanceTradeService.getOrdersSingle(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- getorder ----" + resultJSON);
	}
	
	/**
	 * depth(市场深度)
	 */
	@Test
	public void depth()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "btc_usdt");
		
		JSONObject resultJSON = binanceTradeService.queryDepth(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- depth ----" + resultJSON);
	}
	

	/**
	 * 查询所有的交易对
	 */
	@Test
	public void findAllTransPair()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		JSONObject resultJSON = binanceTradeService.findAllTransPair(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- findAllTransPair ----" + resultJSON);
	}
}
