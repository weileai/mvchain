package com.wang.mvchain.outer.test;


import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.outer.service.ITradeService;
import com.wang.mvchain.outer.service.implay.BitstampTradeServiceImplay;

public class BitstampTradeServiceImplayTest
{
	private ITradeService bitstampService = new BitstampTradeServiceImplay();
	
	/**
	 * 为用户的认证信息赋值
	 * @param authJSON
	 */
	void setAuthJSON()
	{
		JSONObject authJSON = new JSONObject();
		authJSON.put("Key", "aps2GNFE7Gg0SNQjXLWT5UYXshZwovKE");
		authJSON.put("Secret", "2WWyLxT29EVrxCrwQwv8x1zLGoY67PDj");
		authJSON.put("client_id", "669353");
		bitstampService.setAuthJSON(authJSON);
	}
	
	/**
	 * 获取用户基本信息
	 */
	@Test
	public void userInfo()
	{
		setAuthJSON();
		
		Map<String, String> paramsMap = new TreeMap<String, String>();
		JSONObject resultJSON = bitstampService.getUserInfo(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- userInfo ----" + resultJSON);
	}
	
	/**
	 * 获取用户基本信息
	 */
	@Test
	public void trade()
	{
		this.setAuthJSON();
		Map<String, String> paramsMap = new TreeMap<String, String>();
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("moneytype", "btc_usd");
		/* 买卖类型(buy/sell) */
		paramsMap.put("type", "1");
		/* 价格 */
		paramsMap.put("price", "400.11");
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("amount", "0.1");
		
		JSONObject resultJSON = bitstampService.trade(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- trade ----" + resultJSON);
	}
	
	/**
	 * 撤销订单
	 */
	@Test
	public void cancelorder()
	{
		this.setAuthJSON();
		Map<String, String> paramsMap = new TreeMap<String, String>();
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("order_id", "50321801");
		
		JSONObject resultJSON = bitstampService.cancelOrder(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- cancelorder ----" + resultJSON);
	}
	
	/**
	 * 获取用户挂单
	 */
	@Test
	public void getorder()
	{
		this.setAuthJSON();
		Map<String, String> paramsMap = new TreeMap<String, String>();
		//paramsMap.put("order_id", "-1");
		//当前货币兑(btc_cny,ltc_cny)
		
		JSONObject resultJSON = bitstampService.getOrders(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- getorder ----" + resultJSON);
	}
	
	/**
	 * depth(市场深度)
	 */
	@Test
	public void depth()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "btc_usd");
		
		JSONObject resultJSON = bitstampService.queryDepth(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- depth ----" + resultJSON);
	}
	
	/**
	 * depth(市场深度)
	 */
	@Test
	public void trades()
	{
		
	}
	/**
	 * 交易历史
	 */
	@Test
	public void queryHistoryTrades()
	{
		
	}	
}
