package com.wang.mvchain.outer.test;

import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.codec.binary.Base64;
import org.junit.Before;
import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.outer.enums.TradeEnum;
import com.wang.mvchain.outer.service.ITradeService;
import com.wang.mvchain.outer.service.implay.HuobiTradeServiceImplay;

public class HuobiTradeServiceImplayTest
{
	private ITradeService huobiService = new HuobiTradeServiceImplay();
	
	/**
	 *API 访问密钥(Access Key) : dc0aece8-bcb9a2f3-0db41aa6-3a258
		API 秘密密钥(Secret Key) : 20f0e0ed-71a40e02-368d5a5b-b3a71
	 * @param authJSON
	 */
	@Before
	public void setAuthJSON()
	{
		JSONObject authJSON = new JSONObject();
		authJSON.put("access_key", "bg2hyw2dfg-bdc33b1f-00437548-7706a");
		authJSON.put("secret_key", "38fb8102-0cf4b38d-78abd07e-8c281");
		
		huobiService.setAuthJSON(authJSON);
	}
	
	/**
	 * 获取用户基本信息
	 */
	@Test
	public void userInfo()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		JSONObject resultJSON = huobiService.getUserInfo(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("var response = " + resultJSON);
	}
	
	/**
	 * 获取用户基本信息
	 */
	@Test
	public void trade()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("moneytype", "etc_usdt");
		/* 买卖类型(buy/sell) */
		paramsMap.put("type", TradeEnum.TRADE_TYPE_BUY.getValue() + "");
		/* 价格 */
		paramsMap.put("price", "1.182");
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("amount", "1.001");
		
		JSONObject resultJSON = huobiService.trade(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- trade ----" + resultJSON);
	}
	
	/**
	 * 撤销订单
	 */
	@Test
	public void cancelorder()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		/* 当前货币兑(btc_cny,ltc_cny) */
		paramsMap.put("order_id", "2400628583");
		/* 订单号 */
		paramsMap.put("moneytype", "etc_usdt");
		
		JSONObject resultJSON = huobiService.cancelOrder(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- cancelorder ----" + resultJSON);
	}
	
	/**
	 * 获取用户挂单
	 */
	@Test
	public void getorder()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		paramsMap.put("order_id", "-1");
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "etc_usdt");
		
		JSONObject resultJSON = huobiService.getOrders(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- getorder ----" + resultJSON);
	}
	
	/**
	 * 获取用户订单详情
	 */
	@Test
	public void getOrdersSingle()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		paramsMap.put("order_id", "2400628583");
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "cmt_usdt");
		
		JSONObject resultJSON = huobiService.getOrdersSingle(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- getOrdersSingle ----" + resultJSON);
	}
	
	/**
	 * depth(市场深度)
	 */
	@Test
	public void depth()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("moneytype", "btc_usdt");
		
		JSONObject resultJSON = huobiService.queryDepth(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("var response = " + resultJSON);
	}
	
	/**
	 * depth(市场深度)
	 */
	@Test
	public void trades()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		//当前货币兑(btc_cny,ltc_cny)
		paramsMap.put("since", "5000");
		
		JSONObject resultJSON = huobiService.trade(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- depth ----" + resultJSON);
	}
	
	/**
	 * 查询所有的交易对
	 */
	@Test
	public void findAllTransPair()
	{
		Map<String, String> paramsMap = new TreeMap<String, String>();
		JSONObject resultJSON = huobiService.findAllTransPair(paramsMap);
		ConstatFinalUtil.SYS_LOG.info("---- findAllTransPair ----" + resultJSON);
	}
}
