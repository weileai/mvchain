package com.wang.mvchain.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

/**
 * 所有处理日期的工具类
 * @author zjx
 *
 */
@Component("dateUtil")
public class DateUtil
{
	private String dateTimeFormat = ConstatFinalUtil.SYS_BUNDLE.getString("datetime.format");
	private String dateFormat = ConstatFinalUtil.SYS_BUNDLE.getString("date.format");
	
	/**
	 * 将日期时间转换为字符串
	 * @param now
	 * @return
	 */
	public String formatDateTime(Date now)
	{
		SimpleDateFormat sdf = new SimpleDateFormat(dateTimeFormat);
		return sdf.format(now);
	}
	
	/**
	 * 将日期转换为字符串
	 * @param now
	 * @return
	 */
	public String formatDate(Date now)
	{
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		return sdf.format(now);
	}
	
	/**
	 * 将日期转换为字符串
	 * @param now
	 * @return
	 */
	public String format(Date now,String patt)
	{
		SimpleDateFormat sdf = new SimpleDateFormat(patt);
		return sdf.format(now);
	}
	
	/**
	 * 将字符串解析为日期时间
	 * @param now
	 * @return
	 */
	public Date parseDateTime(String now)
	{
		SimpleDateFormat sdf = new SimpleDateFormat(dateTimeFormat);
		try
		{
			return sdf.parse(now);
		} catch (Exception e)
		{
		}
		return new Date() ; 
	}
	
	/**
	 * 将字符串解析为日期
	 * @param now
	 * @return
	 */
	public Date parseDate(String now)
	{
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		try
		{
			return sdf.parse(now);
		} catch (Exception e)
		{
		}
		return new Date() ; 
	}
	
	/**
	 * 将字符串解析为日期时间
	 * @param now
	 * @return
	 */
	public Date parse(String now,String patt)
	{
		SimpleDateFormat sdf = new SimpleDateFormat(patt);
		try
		{
			return sdf.parse(now);
		} catch (Exception e)
		{
			
		}
		return new Date() ; 
	}
	
	public static void main(String[] args)
	{
		DateUtil dateUtil = new DateUtil();
		String res = dateUtil.formatDateTime(new Date());
		System.out.println(res);
		
		res = dateUtil.formatDate(new Date());
		System.out.println(res);
	}
}
