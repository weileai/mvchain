package com.wang.mvchain.common.dao;

import java.util.List;
import java.util.Map;

public interface IBaseDAO<T>
{
	/**
	 * 保存一条记录
	 * @param t
	 * @return
	 */
	int save(T t);
	
	/**
	 * 批量保存
	 * @param list
	 * @return
	 */
	int saveBatch(List<T> list);
	
	/**
	 * 更新一条记录
	 * @param t
	 * @return
	 */
	int update(T t);
	
	/**
	 * 批量保存
	 * @param list
	 * @return
	 */
	int updateBatch(List<T> list);
	
	/**
	 * 删除一条记录
	 * @param t
	 * @return
	 */
	int delete(T t);
	
	/**
	 * 删除多条记录
	 * @param t
	 * @return
	 */
	int delete(Map<String,Object> condMap);
	
	/**
	 * 批量执行
	 * @param condMap
	 * @return
	 */
	int executeBatch(Map<String, Object> condMap);
	
	/**
	 * 查询一条记录
	 * @param map
	 * @return
	 */
	T findOne(Map<String, Object> map);
	
	/**
	 * 查询一条记录
	 * @param map
	 * @return
	 */
	List<T> findList(Map<String, Object> map);
}
