package com.wang.mvchain.common.pojo;

import com.alibaba.fastjson.JSONObject;

/**
 * 所有POJO的超类
 * @author wangsh
 *
 */
public abstract class BasePojo<T>
{
	/**
	 * 将对象转换为JSON
	 * @return
	 */
	public abstract JSONObject toJSON();
	
	/**
	 * 从json对象中解析对象
	 * @param souStr
	 * @return
	 */
	protected abstract T parseJSON(JSONObject souJSON);
}
