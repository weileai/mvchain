package com.wang.mvchain.common.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 因为Junit和web窗口的dataSource不一样,所以整出一套baseTest出来
 * @author wangsh
 *
 */
public abstract class BaseTest
{
	protected Logger logger = LogManager.getLogger(BaseTest.class);
	
	protected ApplicationContext ctx = null ; 
	
	public BaseTest()
	{
		logger.info("----initSpring----");
		ctx = new ClassPathXmlApplicationContext(
				"classpath*:spring/applicationContext-*.xml");

	}
}