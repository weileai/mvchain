package com.wang.mvchain.common.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.common.util.DateUtil;
import com.wang.mvchain.common.util.DoubleOperUtil;
import com.wang.mvchain.common.util.FileUtil;
import com.wang.mvchain.common.util.HTTPUtil;
import com.wang.mvchain.common.util.PageInfoUtil;
import com.wang.mvchain.common.util.RegexUtil;

/**
 * controller的超级父类
 * 
 * @author wangshh
 * 
 */
public class BaseController
{
	@Resource
	protected DateUtil dateUtil ;
	@Resource
	protected RegexUtil regexUtil ;
	@Resource
	protected FileUtil fileUtil ;
	@Resource
	protected DoubleOperUtil doubleOperUtil;
	@Resource
	protected HTTPUtil httpUtil ; 

	/**
	 * 获取真实IP
	 * 
	 * @return
	 */
	protected String findRealIpUtil(HttpServletRequest request)
	{
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
		{
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
		{
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
		{
			ip = request.getHeader("x-real-ip");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
		{
			ip = request.getRemoteAddr();
		}
		// String ip = request.getParameter("ip");
		return ip;
	}

	/**
	 * 设置page的信息（默认的值）
	 * 
	 * @param pageinfo
	 * @param cond
	 * @return
	 */
	protected PageInfoUtil setPageInfoCond(String... cond)
	{
		PageInfoUtil pageInfoUtil = new PageInfoUtil();
		int currentPage = 1;
		int pageSize = Integer.valueOf(ConstatFinalUtil.SYS_BUNDLE
				.getString("houtai.pageinfo.pagesize"));
		try
		{
			currentPage = Integer.valueOf(cond[0]);
			pageSize = Integer.valueOf(cond[1]);
		} catch (NumberFormatException e)
		{

		}
		pageInfoUtil.setCurrentPage(currentPage);
		pageInfoUtil.setPageSize(pageSize);
		return pageInfoUtil;
	}
	
	/**
	 * 从session中获取用户或者管理员信息
	 * @return
	 */
	protected Object findObjfromSession(HttpServletRequest request , String type)
	{
		HttpSession session = request.getSession();
		if("1".equalsIgnoreCase(type))
		{
			return session.getAttribute("users");
		}else if("2".equalsIgnoreCase(type))
		{
			return session.getAttribute("admins");
		}
		return null ; 
	}
	
	/**
	 * 返回json字符串
	 * @param request
	 * @param info
	 * @return
	 */
	public JSONObject returnJSON(HttpServletRequest request , String info)
	{
		//处理返回的json
		String callbackType = request.getParameter("callbackType");
		String navTabId = request.getParameter("navTabId");
		String rel = request.getParameter("rel");
		String forwardUrl = request.getParameter("forwardUrl");
		
		if(callbackType == null)
		{
			callbackType = "" ; 
		}
		if(navTabId == null)
		{
			navTabId = "" ; 
		}
		if(rel == null)
		{
			rel = "" ; 
		}
		if(forwardUrl == null)
		{
			forwardUrl = "" ; 
		}
		
		if(request.getAttribute("callbackType") != null)
		{
			callbackType = request.getAttribute("callbackType") + "" ; 
		}
		
		JSONObject resultJSON = new JSONObject();
		resultJSON.put("message", info);
		resultJSON.put("confirmMsg", "");
		resultJSON.put("callbackType", callbackType);
		resultJSON.put("navTabId", navTabId);
		resultJSON.put("rel", rel);
		resultJSON.put("forwardUrl", forwardUrl);
		resultJSON.put("statusCode", "200");
		return resultJSON ; 
	}
	
	/**
	 * 返回json字符串
	 * @param request
	 * @param info
	 * @return
	 * @throws IOException 
	 */
	public JSONObject printOut(HttpServletRequest request , HttpServletResponse response , String returnStr)
	{
		try
		{
			response.setContentType("text/html;charset=UTF-8");
			PrintWriter out = response.getWriter();
			out.print(returnStr);
			out.flush();
			out.close();
		} catch (IOException e)
		{
			ConstatFinalUtil.SYS_LOG.error("为客户端返回信息出错了;" , e);
		}
		return null; 
	}
}
