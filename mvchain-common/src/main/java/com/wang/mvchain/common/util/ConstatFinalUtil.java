package com.wang.mvchain.common.util;

import java.util.Collections;
import java.util.Hashtable;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * 存储所有的常量
 * @author zjx
 *
 */
public class ConstatFinalUtil
{
	public static final ResourceBundle SYS_BUNDLE = ResourceBundle.getBundle("common");
	/* 系统日志对象 */
	public static final Logger SYS_LOG = LogManager.getLogger("SystemLog");
	/* 外部日志对象 */
	public static final Logger OUTER_LOG = LogManager.getLogger("OuterLog");
	/* 外部日志对象 */
	public static final Logger TIMER_LOG = LogManager.getLogger("TimerLog");
	/*默认的编码*/
	public static final String CHARSET = "UTF-8";
	/*默认分隔字符串*/
	public static final String SPLIT_STR = "|-->" ; 
	/*存储一些系统级的配置参数*/
	public static final Map<String, String> SYSPRO_MAP = new Hashtable<String,String>();
	/*存放所有线程的容器*/
	public static Map<String, Map<String, Object>> THREAD_MAP = new Hashtable<String, Map<String,Object>>();
	
	/*处理大数据,每页多少条*/
	public static final int PAGE_BATCH_SIZE = 10 ; 
	/*请求服务器超时*/
	public static final int SECOND = 1000;
	/*请求服务器超时*/
	public static final int REQ_CONNECT_TIMEOUT = 5000;
	/*读取超时*/
	public static final int READ_TIMEOUT = 5000 ;
	/*请求次数*/
	public static final int REQ_COUNT = 3 ;
	/* 请求用户余额的次数 */
	public static long USERS_BALANCE_COUNT = Long.valueOf(1) ; 
	
	/*静态资源文件的JSON*/
	public static JSONObject RESOURCE_JSON = new JSONObject() ; 
	public static JSONObject INFO_JSON = new JSONObject() ; 
	public static JSONObject CONFIG_JSON = new JSONObject() ; 
	
	/**
	 * 静态代码块
	 */
	static
	{
		/* 解析静态的JSON */
		ConstatFinalUtil.SYS_LOG.info("--资源文件开始--");
		FileUtil fileUtil = new FileUtil();
		/*读取jar包里面的配置文件*/
		String resource = fileUtil.readFile(ConstatFinalUtil.class.getResourceAsStream("/resource.json"), 
				ConstatFinalUtil.CHARSET, Collections.EMPTY_MAP);
		RESOURCE_JSON = JSON.parseObject(resource);
		if(RESOURCE_JSON != null)
		{
			INFO_JSON = RESOURCE_JSON.getJSONObject("info");
			CONFIG_JSON = RESOURCE_JSON.getJSONObject("config");
		}
		ConstatFinalUtil.SYS_LOG.info("--资源文件结束--");
	}
	
	/**
	 * 测试方法
	 * @param args
	 */
	public static void main(String[] args)
	{
		System.out.println(ConstatFinalUtil.RESOURCE_JSON);
	}
}
