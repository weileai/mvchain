package com.wang.mvchain.common.util;

import com.alibaba.fastjson.JSONObject;

/**
 * 分页的相关信息
 * @author wangsh
 *
 */
public class PageInfoUtil
{
	// 当前页
	private int currentPage = 1;
	// 每页多少条
	private int pageSize = Integer.valueOf(ConstatFinalUtil.SYS_BUNDLE.getString("houtai.pageinfo.pagesize"));
	// 总共多少页
	private int totalPage;
	//总记录数
	private int totalRecord ; 
	// 上一页
	private int prePage;
	// 下一页
	private int nextPage;
	//当前记录数
	private int currRecord ; 
	//上一页的总记录数
	private int preRecord ;
	//下一页的总记录数
	private int nextRecord ; 
	
	public int getCurrentPage()
	{
		if(this.currentPage < 1 )
		{
			this.currentPage = 1 ; 
		}
		if(this.getTotalPage() > 0 && this.currentPage > this.getTotalPage())
		{
			this.currentPage = this.getTotalPage() ;
		}
		return currentPage;
	}

	public void setCurrentPage(int currentPage)
	{
		this.currentPage = currentPage;
	}

	public int getPageSize()
	{
		if(pageSize <= 0){
			this.pageSize = Integer.valueOf(ConstatFinalUtil.SYS_BUNDLE.getString("houtai.pageinfo.pagesize"));
		}
		return pageSize;
	}

	public void setPageSize(int pageSize)
	{
		this.pageSize = pageSize;
	}

	public int getPrePage()
	{
		this.prePage = this.currentPage - 1;
		if(this.prePage < 1 )
		{
			this.prePage = 1 ; 
		}
		return prePage;
	}

	public int getNextPage()
	{
		this.nextPage = this.currentPage + 1;
		if(this.nextPage > this.getTotalPage() )
		{
			this.nextPage = this.getTotalPage() ; 
		}
		return nextPage;
	}

	public int getTotalPage()
	{
		if(this.getTotalRecord() % this.getPageSize() == 0 )
		{
			this.totalPage = this.getTotalRecord() / this.getPageSize() ;
		}else
		{
			this.totalPage = this.getTotalRecord() / this.getPageSize() + 1 ;
		}
		return totalPage;
	}

	public int getTotalRecord()
	{
		return totalRecord;
	}

	public void setTotalRecord(int totalRecord)
	{
		this.totalRecord = totalRecord;
	}
	
	public int getCurrRecord()
	{
		if(this.getCurrentPage() == 0 )
		{
			return 0 ; 
		}
		this.currRecord =  (this.getCurrentPage() - 1) * this.pageSize ;
		return this.currRecord ; 
	}
	
	public int getPreRecord() {
		this.preRecord = this.getCurrRecord() - this.getPageSize() ; 
		if(this.preRecord < 0 )
		{
			this.preRecord = 0 ; 
		}
		return preRecord;
	}

	public int getNextRecord() {
		this.nextRecord = this.getCurrRecord() + this.getPageSize() ; 
		return nextRecord;
	}
	
	public String toString()
	{
		JSONObject pageInfoJSON = new JSONObject();
		pageInfoJSON.put("currentPage", this.getCurrentPage() + "");
		pageInfoJSON.put("pageSize", this.getPageSize() + "");
		pageInfoJSON.put("totalPage", this.getTotalPage() + "");
		pageInfoJSON.put("totalRecord", this.getTotalRecord() + "");
		pageInfoJSON.put("prePage", this.getPrePage() + "");
		pageInfoJSON.put("nextPage", this.getNextPage() + "");
		return pageInfoJSON + "";
	}

}