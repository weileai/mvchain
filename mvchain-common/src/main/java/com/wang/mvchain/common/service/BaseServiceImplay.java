package com.wang.mvchain.common.service;

import javax.annotation.Resource;

import com.wang.mvchain.common.util.DateUtil;
import com.wang.mvchain.common.util.DoubleOperUtil;
import com.wang.mvchain.common.util.FileUtil;
import com.wang.mvchain.common.util.RedisUtil;
import com.wang.mvchain.common.util.RegexUtil;
import com.wang.mvchain.common.util.SpringEmailUtil;

public class BaseServiceImplay
{
	@Resource
	protected DateUtil dateUtil ;
	@Resource
	protected RegexUtil regexUtil ;
	@Resource
	protected FileUtil fileUtil ;
	@Resource
	protected SpringEmailUtil springEmailUtil ;
	@Resource
	protected DoubleOperUtil doubleOperUtil ;
	@Resource
	protected RedisUtil redisUtil;
}
