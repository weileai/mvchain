package com.wang.mvchain.common.util;

import javax.annotation.Resource;
import javax.mail.internet.MimeMessage;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

/**
 * 使用Spring发邮件的例子
 * 
 * @author Administrator
 * 
 */
@Component("springEmailUtil")
public class SpringEmailUtil 
{
	
	@Resource(name = "javaMailSender")
	private JavaMailSender javaMailSender ; 
	@Resource(name = "mailMessage")
	private SimpleMailMessage simpleMailMessage ;
	
	/**
	 * 发送普通的文本信息 jmail相当于服务器,发送的内容由自己来封装
	 * @param email
	 * @param subject
	 * @param text
	 * @return
	 */
	public boolean sendTextMail(String email, String subject,
			String text) {
		for (int i = 0; i < ConstatFinalUtil.REQ_COUNT; i++)
		{
			try 
			{
				simpleMailMessage.setTo(email);
				simpleMailMessage.setSubject(subject);
				simpleMailMessage.setText(text);
				javaMailSender.send(simpleMailMessage);
				ConstatFinalUtil.SYS_LOG.info(i + ";====普通邮件=====" + email + "邮件发送成功了");
				return true ; 
			} catch (Exception e) {
				ConstatFinalUtil.SYS_LOG.error(i + ";=====" + email + "+ 邮件发送失败了,内容为:"+ 
						subject + "-->" + text , e);
			}
		}
		return false ; 
	}
	
	/**
	 * 发送HTML格式的邮件
	 * @param email
	 * @param subject
	 * @param text
	 * @return
	 */
	public boolean sendHTMLMail(String email , String subject ,
				String text)
	{
		for (int i = 0; i < ConstatFinalUtil.REQ_COUNT; i++)
		{
			try
			{
				MimeMessage message = javaMailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(message ,true, "UTF-8") ;
				helper.setTo(email);
				helper.setFrom(this.simpleMailMessage.getFrom()) ; 
				helper.setSubject(subject);
				helper.setText(text , true);
				javaMailSender.send(message);
				ConstatFinalUtil.SYS_LOG.info(i + ";" + email + "====网页邮件=====" + subject + "邮件发送成功了");
				return true ; 
			} catch (Exception e)
			{
				ConstatFinalUtil.SYS_LOG.error(i + ";" + email + "====网页邮件=====" + subject + ",邮件发送失败了,内容为:"+ 
						subject + "-->" + text , e);
			}
		}
		return false ; 
	}

	/**
	 * 运行测试邮件的方法
	 * @param args
	 */
	public static void main(String[] args) {
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath*:spring/java_applicationContext.xml");
		SpringEmailUtil smail = (SpringEmailUtil) ctx.getBean("springEmailUtil");
		//boolean flag = smail.sendTextMail("wangshanhu186@126.com", "测试邮件", "<a href='http://www.baidu.com'>百度</a><b>你收到了吗?</b>");
		boolean flag = smail.sendHTMLMail("724259669@qq.com", "测试邮件", "<a href='http://www.baidu.com'>百度</a><b>你收到了吗?</b>");
		System.out.println(flag);
	}
}
