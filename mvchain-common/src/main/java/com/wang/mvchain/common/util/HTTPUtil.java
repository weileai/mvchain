package com.wang.mvchain.common.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.springframework.stereotype.Component;

/**
 * HTTP的相关工具类,模拟POST和GET请求
 * 
 * @author wangsh
 *
 */
@Component("httpUtil")
public class HTTPUtil
{
	public class TrustAnyHostnameVerifier implements HostnameVerifier
	{
		public boolean verify(String hostname, SSLSession session)
		{
			// 直接返回true
			return true;
		}
	}

	/**
	 * Post请求
	 * 
	 * @param headerMap
	 *            请求头
	 * @param paramsMap
	 *            请求体参数
	 * @return
	 */
	public String methodPost(Map<String, String> headerMap,
			Map<String, String> paramsMap)
	{
		StringBuffer sb = new StringBuffer();
		//如果是https协议
		if(paramsMap.get("requestURL").startsWith("https"))
		{
			return this.methodHttpsPost(headerMap, paramsMap);
		}
		
		// 连续请求多次
		for (int i = 0; i < ConstatFinalUtil.REQ_COUNT; i++)
		{
			sb.delete(0, sb.length());
			HttpURLConnection connect = null ;
			try
			{
				URL url = new URL(paramsMap.get("requestURL"));
				connect = (HttpURLConnection) url
						.openConnection();
				connect.setConnectTimeout(ConstatFinalUtil.REQ_CONNECT_TIMEOUT);
				connect.setReadTimeout(ConstatFinalUtil.READ_TIMEOUT);
				connect.setDoInput(true);
				connect.setDoOutput(true);
	
				StringBuffer paramssb = new StringBuffer();
				for (Iterator iterator = paramsMap.entrySet().iterator(); iterator
						.hasNext();)
				{
					Map.Entry me = (Map.Entry) iterator.next();
					if (!"requestURL".equalsIgnoreCase(me.getKey() + "") && !"retry".equalsIgnoreCase(me.getKey() + ""))
					{
						paramssb.append(me.getKey() + "=" + me.getValue() + "&");
					}
				}
	
				if (paramssb.toString().endsWith("&"))
				{
					paramssb.delete(paramssb.length() - 1, paramssb.length());
				}
				connect.setRequestProperty(
						"Cookie",
						"Hm_lvt_2d57a0f88eed9744a82604dcfa102e49=1386575661; CNZZDATA5342694=cnzz_eid%3D1753424715-1386575827-http%253A%252F%252Fwww.btctrade.com%26ntime%3D1386750475%26cnzz_a%3D5%26ltime%3D1386750483033%26rtime%3D1; pgv_pvi=8171956224; __utma=252052442.1822116731.1386640814.1386741966.1386750468.3; __utmz=252052442.1386640814.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); wafenterurl=/; wafcookie=51a45a2229469ee92bbd8cc281e98e91; __utmb=252052442.1.10.1386750468; __utmc=252052442; wafverify=afe13eda6d99c7f141d7dd3966b59d9e; USER_PW=ab3f61ee826a95e51734cf7174100382; PHPSESSID=9f2c19d6ffd0ef808ba8bac0b74ab0f3; IESESSION=alive; pgv_si=s1618283520");
				connect.setRequestProperty("User-Agent",
						"Mozilla/5.0 (Windows NT 6.2; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
	
				for (Iterator iterator = headerMap.entrySet().iterator(); iterator
						.hasNext();)
				{
					Entry me = (Entry) iterator.next();
					connect.setRequestProperty(me.getKey() + "", me.getValue() + "");
				}

				
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
						connect.getOutputStream()));
				bw.write(paramssb.toString());
				bw.flush();
				
				BufferedReader br = null ; 
				//响应码成功
				if(connect.getResponseCode() == 200)
				{
					br = new BufferedReader(new InputStreamReader(
								connect.getInputStream(), "UTF-8"));
				}else
				{
					if(connect.getErrorStream() != null)
					{
						br = new BufferedReader(new InputStreamReader(
								connect.getErrorStream(), "UTF-8"));
					}else
					{
						br = new BufferedReader(new InputStreamReader(
								connect.getInputStream(), "UTF-8"));
					}
				}
				
				String line = "";
				while ((line = br.readLine()) != null)
				{
					sb.append(line.trim());
				}
				br.close();
				bw.close();
				
				//正常返回直接退出
				if(connect.getResponseCode() != 200)
				{
					ConstatFinalUtil.SYS_LOG.info("重试次数:{},响应码:{};响应信息:{};返回信息:{};请求头:{};请求体:{}",
							i,connect.getResponseCode(),connect.getResponseMessage(),sb,headerMap,paramsMap);
				}
				break ;
			}catch (Exception e)
			{
				try
				{
					ConstatFinalUtil.SYS_LOG.info("http的Post请求失败了;重试次数:{},响应码:{};返回信息:{};请求头:{};请求体:{}",
							i,connect != null ? connect.getResponseCode() : connect,sb,headerMap,paramsMap,e);
					Thread.sleep(ConstatFinalUtil.PAGE_BATCH_SIZE);
				} catch (Exception e1)
				{
					e1.printStackTrace();
				}
			}
			
			//重试机制
			if(!"true".equalsIgnoreCase(headerMap.get("retry")))
			{
				break ; 
			}
		}
		//没有查询到数据,调用https
		if(sb.length() <= 0)
		{
			return this.methodHttpsPost(headerMap, paramsMap);
		}
		return sb.toString();
	}

	/**
	 * HttpsPost请求
	 * 
	 * @param headerMap
	 *            请求头参数
	 * @param paramsMap
	 *            请求体参数
	 * @return
	 */
	public String methodHttpsPost(Map<String, String> headerMap,
			Map<String, String> paramsMap)
	{
		StringBuffer sb = new StringBuffer();
		// 连续请求多次
		for (int i = 0; i < ConstatFinalUtil.REQ_COUNT; i++)
		{
			sb.delete(0, sb.length());
			HttpsURLConnection connect = null ; 
			try
			{
				// Create a trust manager that does not validate certificate chains
				TrustManager[] trustAllCerts = new TrustManager[]
				{ new X509TrustManager()
				{
					public X509Certificate[] getAcceptedIssuers()
					{
						return null;
					}
	
					public void checkClientTrusted(X509Certificate[] certs,
							String authType)
					{
					}
	
					public void checkServerTrusted(X509Certificate[] certs,
							String authType)
					{
					}
				} };
	
				// Install the all-trusting trust manager
	
				SSLContext sc = SSLContext.getInstance("TLS");
				sc.init(null, trustAllCerts, new SecureRandom());
				HttpsURLConnection
						.setDefaultSSLSocketFactory(sc.getSocketFactory());
	
				URL url = new URL(paramsMap.get("requestURL"));
				connect = (HttpsURLConnection) url
						.openConnection();
				connect.setHostnameVerifier(new TrustAnyHostnameVerifier());
				connect.setConnectTimeout(ConstatFinalUtil.REQ_CONNECT_TIMEOUT);
				connect.setReadTimeout(ConstatFinalUtil.READ_TIMEOUT);
				connect.setDoInput(true);
				connect.setDoOutput(true);
				
				connect.setRequestProperty(
						"Cookie",
						"Hm_lvt_2d57a0f88eed9744a82604dcfa102e49=1386575661; CNZZDATA5342694=cnzz_eid%3D1753424715-1386575827-http%253A%252F%252Fwww.btctrade.com%26ntime%3D1386750475%26cnzz_a%3D5%26ltime%3D1386750483033%26rtime%3D1; pgv_pvi=8171956224; __utma=252052442.1822116731.1386640814.1386741966.1386750468.3; __utmz=252052442.1386640814.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); wafenterurl=/; wafcookie=51a45a2229469ee92bbd8cc281e98e91; __utmb=252052442.1.10.1386750468; __utmc=252052442; wafverify=afe13eda6d99c7f141d7dd3966b59d9e; USER_PW=ab3f61ee826a95e51734cf7174100382; PHPSESSID=9f2c19d6ffd0ef808ba8bac0b74ab0f3; IESESSION=alive; pgv_si=s1618283520");
				connect.setRequestProperty("User-Agent",
						"Mozilla/5.0 (Windows NT 6.2; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
	
				StringBuffer paramssb = new StringBuffer();
				for (Iterator iterator = paramsMap.entrySet().iterator(); iterator
						.hasNext();)
				{
					Map.Entry me = (Map.Entry) iterator.next();
					if (!"requestURL".equalsIgnoreCase(me.getKey() + ""))
					{
						paramssb.append(me.getKey() + "=" + me.getValue() + "&");
					}
				}
	
				if (paramssb.toString().endsWith("&"))
				{
					paramssb.delete(paramssb.length() - 1, paramssb.length());
				}
				// ConstatFinalUtil.SYS_LOG.info("-----"+ paramssb);
				for (Iterator iterator = headerMap.entrySet().iterator(); iterator
						.hasNext();)
				{
					Entry me = (Entry) iterator.next();
					if (!"requestURL".equalsIgnoreCase(headerMap.get("requestURL")))
					{
						connect.setRequestProperty(me.getKey() + "", me.getValue()
								+ "");
					}
				}

			
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
						connect.getOutputStream()));
				bw.write(paramssb.toString());
				bw.flush();

				BufferedReader br = null ; 
				//响应码成功
				if(connect.getResponseCode() == 200)
				{
					br = new BufferedReader(new InputStreamReader(
								connect.getInputStream(), "UTF-8"));
				}else
				{
					if(connect.getErrorStream() != null)
					{
						br = new BufferedReader(new InputStreamReader(
								connect.getErrorStream(), "UTF-8"));
					}else
					{
						br = new BufferedReader(new InputStreamReader(
								connect.getInputStream(), "UTF-8"));
					}
				}
				
				String line = "";
				while ((line = br.readLine()) != null)
				{
					sb.append(line.trim());
				}
				br.close();
				bw.close();
				
				//正常返回直接退出
				if(connect.getResponseCode() != 200)
				{
					ConstatFinalUtil.SYS_LOG.info("重试次数:{},响应码:{};响应信息:{};返回信息:{};请求头:{};请求体:{}",
							i,connect.getResponseCode(),connect.getResponseMessage(),sb,headerMap,paramsMap);
				}
				break ;
			} catch (Exception e)
			{
				try
				{
					ConstatFinalUtil.SYS_LOG.info("https的Post请求失败了;重试次数:{},响应码:{};返回信息:{};请求头:{};请求体:{}",
							i,connect != null ? connect.getResponseCode() : connect,sb,headerMap,paramsMap,e);
					
					Thread.sleep(ConstatFinalUtil.PAGE_BATCH_SIZE);
				} catch (Exception e1)
				{
					e1.printStackTrace();
				}
			}
			
			//重试机制
			if(!"true".equalsIgnoreCase(headerMap.get("retry")))
			{
				break ; 
			}
		}
		return sb.toString();
	}

	/**
	 * Post请求 请求url放到headerMap中
	 * 
	 * @param paramsMap
	 * @return
	 */
	public String methodPost(Map<String, String> headerMap, String postStr)
	{
		StringBuffer sb = new StringBuffer();
		//如果是https协议
		if(headerMap.get("requestURL").startsWith("https"))
		{
			return this.methodHttpsPost(headerMap,postStr);
		}
		// 连续请求多次
		for (int i = 0; i < ConstatFinalUtil.REQ_COUNT; i++)
		{
			sb.delete(0, sb.length());
			HttpURLConnection connect = null ; 
			try
			{
				URL url = new URL(headerMap.get("requestURL"));
				connect = (HttpURLConnection) url
						.openConnection();
				connect.setConnectTimeout(ConstatFinalUtil.REQ_CONNECT_TIMEOUT);
				connect.setReadTimeout(ConstatFinalUtil.READ_TIMEOUT);
				connect.setDoInput(true);
				connect.setDoOutput(true);
				
				connect.setRequestProperty(
						"Cookie",
						"Hm_lvt_2d57a0f88eed9744a82604dcfa102e49=1386575661; CNZZDATA5342694=cnzz_eid%3D1753424715-1386575827-http%253A%252F%252Fwww.btctrade.com%26ntime%3D1386750475%26cnzz_a%3D5%26ltime%3D1386750483033%26rtime%3D1; pgv_pvi=8171956224; __utma=252052442.1822116731.1386640814.1386741966.1386750468.3; __utmz=252052442.1386640814.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); wafenterurl=/; wafcookie=51a45a2229469ee92bbd8cc281e98e91; __utmb=252052442.1.10.1386750468; __utmc=252052442; wafverify=afe13eda6d99c7f141d7dd3966b59d9e; USER_PW=ab3f61ee826a95e51734cf7174100382; PHPSESSID=9f2c19d6ffd0ef808ba8bac0b74ab0f3; IESESSION=alive; pgv_si=s1618283520");
				connect.setRequestProperty("User-Agent",
						"Mozilla/5.0 (Windows NT 6.2; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
	
				for (Iterator iterator = headerMap.entrySet().iterator(); iterator
						.hasNext();)
				{
					Entry me = (Entry) iterator.next();
					if (!"requestURL".equalsIgnoreCase(me.getKey() + ""))
					{
						connect.setRequestProperty(me.getKey() + "", me.getValue()
								+ "");
					}
				}

			
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
						connect.getOutputStream()));
				bw.write(postStr);
				bw.flush();

				BufferedReader br = null ; 
				//响应码成功
				if(connect.getResponseCode() == 200)
				{
					br = new BufferedReader(new InputStreamReader(
								connect.getInputStream(), "UTF-8"));
				}else
				{
					if(connect.getErrorStream() != null)
					{
						br = new BufferedReader(new InputStreamReader(
								connect.getErrorStream(), "UTF-8"));
					}else
					{
						br = new BufferedReader(new InputStreamReader(
								connect.getInputStream(), "UTF-8"));
					}
				}
				
				String line = "";
				while ((line = br.readLine()) != null)
				{
					sb.append(line.trim());
				}
				br.close();
				bw.close();
				
				//正常返回直接退出
				if(connect.getResponseCode() != 200)
				{
					ConstatFinalUtil.SYS_LOG.info("重试次数:{},响应码:{};响应信息:{};返回信息:{};请求头:{};请求体:{}",
							i,connect.getResponseCode(),connect.getResponseMessage(),sb,headerMap,postStr);
				}
				break ;
			} catch (Exception e)
			{
				try
				{
					ConstatFinalUtil.SYS_LOG.info("http的Post请求失败了;重试次数:{},响应码:{};返回信息:{};请求头:{};请求体:{}",
							i,connect != null ? connect.getResponseCode() : connect,sb,headerMap,postStr,e);
					Thread.sleep(ConstatFinalUtil.PAGE_BATCH_SIZE);
				} catch (Exception e1)
				{
					e1.printStackTrace();
				}
				return this.methodHttpsPost(headerMap, postStr);
			}
		}
		
		//没有查询到数据,调用https
		if(sb.length() <= 0)
		{
			return this.methodHttpsPost(headerMap, postStr);
		}
		return sb.toString();
	}

	/**
	 * HttpsPost请求 请求url放到headerMap中
	 * 
	 * @param headerMap
	 *            请求头参数
	 * @param paramsMap
	 * @return
	 */
	public String methodHttpsPost(Map<String, String> headerMap, String postStr)
	{
		StringBuffer sb = new StringBuffer();
		// 连续请求多次
		for (int i = 0; i < ConstatFinalUtil.REQ_COUNT; i++)
		{
			sb.delete(0, sb.length());
			HttpsURLConnection connect = null ; 
			try
			{
				// Create a trust manager that does not validate certificate chains
				TrustManager[] trustAllCerts = new TrustManager[]
				{ new X509TrustManager()
				{
					public X509Certificate[] getAcceptedIssuers()
					{
						return null;
					}
	
					public void checkClientTrusted(X509Certificate[] certs,
							String authType)
					{
					}
	
					public void checkServerTrusted(X509Certificate[] certs,
							String authType)
					{
					}
				} };
	
				// Install the all-trusting trust manager
	
				SSLContext sc = SSLContext.getInstance("TLS");
				sc.init(null, trustAllCerts, new SecureRandom());
				HttpsURLConnection
						.setDefaultSSLSocketFactory(sc.getSocketFactory());
	
				URL url = new URL(headerMap.get("requestURL"));
				connect = (HttpsURLConnection) url
						.openConnection();
				connect.setHostnameVerifier(new TrustAnyHostnameVerifier());
				connect.setConnectTimeout(ConstatFinalUtil.REQ_CONNECT_TIMEOUT);
				connect.setReadTimeout(ConstatFinalUtil.READ_TIMEOUT);
				connect.setDoInput(true);
				connect.setDoOutput(true);
				
				// ConstatFinalUtil.SYS_LOG.info("-----"+ paramssb);
				for (Iterator iterator = headerMap.entrySet().iterator(); iterator
						.hasNext();)
				{
					Entry me = (Entry) iterator.next();
					if (!"requestURL".equalsIgnoreCase(headerMap.get("requestURL")))
					{
						connect.setRequestProperty(me.getKey() + "", me.getValue()
								+ "");
					}
				}
				connect.setRequestProperty(
						"Cookie",
						"Hm_lvt_2d57a0f88eed9744a82604dcfa102e49=1386575661; CNZZDATA5342694=cnzz_eid%3D1753424715-1386575827-http%253A%252F%252Fwww.btctrade.com%26ntime%3D1386750475%26cnzz_a%3D5%26ltime%3D1386750483033%26rtime%3D1; pgv_pvi=8171956224; __utma=252052442.1822116731.1386640814.1386741966.1386750468.3; __utmz=252052442.1386640814.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); wafenterurl=/; wafcookie=51a45a2229469ee92bbd8cc281e98e91; __utmb=252052442.1.10.1386750468; __utmc=252052442; wafverify=afe13eda6d99c7f141d7dd3966b59d9e; USER_PW=ab3f61ee826a95e51734cf7174100382; PHPSESSID=9f2c19d6ffd0ef808ba8bac0b74ab0f3; IESESSION=alive; pgv_si=s1618283520");
				connect.setRequestProperty("User-Agent",
						"Mozilla/5.0 (Windows NT 6.2; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");

			
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
						connect.getOutputStream()));
				bw.write(postStr);
				bw.flush();

				BufferedReader br = null ; 
				//响应码成功
				if(connect.getResponseCode() == 200)
				{
					br = new BufferedReader(new InputStreamReader(
								connect.getInputStream(), "UTF-8"));
				}else
				{
					if(connect.getErrorStream() != null)
					{
						br = new BufferedReader(new InputStreamReader(
								connect.getErrorStream(), "UTF-8"));
					}else
					{
						br = new BufferedReader(new InputStreamReader(
								connect.getInputStream(), "UTF-8"));
					}
				}
				
				String line = "";
				while ((line = br.readLine()) != null)
				{
					sb.append(line.trim());
				}
				br.close();
				bw.close();
				
				//正常返回直接退出
				if(connect.getResponseCode() != 200)
				{
					ConstatFinalUtil.SYS_LOG.info("重试次数:{},响应码:{};响应信息:{};返回信息:{};请求头:{};请求体:{}",
							i,connect.getResponseCode(),connect.getResponseMessage(),sb,headerMap,postStr);
				}
				break ;
			} catch (Exception e)
			{
				try
				{
					ConstatFinalUtil.SYS_LOG.info("http的Post请求失败了;重试次数:{},响应码:{};返回信息:{};请求头:{};请求体:{}",
							i,connect != null ? connect.getResponseCode() : connect,sb,headerMap,postStr,e);
					Thread.sleep(ConstatFinalUtil.PAGE_BATCH_SIZE);
				} catch (Exception e1)
				{
					e1.printStackTrace();
				}
			}
			//重试机制
			if(!"true".equalsIgnoreCase(headerMap.get("retry")))
			{
				break ; 
			}
		} 
		return sb.toString();
	}
	
	/**
	 * Get请求
	 * 
	 * @param paramsMap
	 * @return
	 */
	public String methodGet(Map<String, String> paramsMap)
	{
		StringBuffer sb = new StringBuffer();
		
		// 连续请求多次
		for (int i = 0; i < ConstatFinalUtil.REQ_COUNT; i++)
		{
			sb.delete(0, sb.length());
			StringBuffer paramssb = new StringBuffer();
			HttpURLConnection connect = null ; 
			try
			{
				paramssb.append(paramsMap.get("requestURL"));
				if (!paramssb.toString().endsWith("?") && paramsMap.size() > 1)
				{
					paramssb.append("?");
				}
				for (Iterator iterator = paramsMap.entrySet().iterator(); iterator
						.hasNext();)
				{
					Map.Entry me = (Map.Entry) iterator.next();
					if (!me.getKey().toString().equalsIgnoreCase("requestURL"))
					{
						paramssb.append(me.getKey() + "=" + me.getValue() + "&");
					}
				}
	
				if (paramssb.toString().endsWith("&"))
				{
					paramssb.delete(paramssb.length() - 1, paramssb.length());
				}
				//ConstatFinalUtil.SYS_LOG.info(paramssb.toString());
				URL url = new URL(paramssb.toString());
				connect = (HttpURLConnection) url
						.openConnection();
				connect.setConnectTimeout(ConstatFinalUtil.REQ_CONNECT_TIMEOUT);
				connect.setReadTimeout(ConstatFinalUtil.READ_TIMEOUT);
	
				connect.setAllowUserInteraction(true);
	
				connect.setDoInput(true);
				connect.setDoOutput(false);
	
				connect.setRequestProperty(
						"Cookie",
						"Hm_lvt_4982d57ea12df95a2b24715fb6440726=1398138317; mstuid=1398138316637_9919; userId=10931889; XM_Hd_Start=1; Hm_lvt_7080c6a6aba51276281d5d595b080def=1398143044");
				connect.setRequestProperty(
						"User-Agent",
						"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36");
				
				BufferedReader br = null ; 
				//响应码成功
				if(connect.getResponseCode() == 200)
				{
					br = new BufferedReader(new InputStreamReader(
								connect.getInputStream(), "UTF-8"));
				}else
				{
					if(connect.getErrorStream() != null)
					{
						br = new BufferedReader(new InputStreamReader(
								connect.getErrorStream(), "UTF-8"));
					}else
					{
						br = new BufferedReader(new InputStreamReader(
								connect.getInputStream(), "UTF-8"));
					}
				}
				
				String line = "";
				while ((line = br.readLine()) != null)
				{
					sb.append(line.trim());
				}
				br.close();
				
				//响应码成功
				if(connect.getResponseCode() == 200)
				{
					break ; 
				}else
				{
					ConstatFinalUtil.SYS_LOG.info("重试次数:{},响应码:{};响应信息:{};返回信息:{};请求头:{};",
							i,connect.getResponseCode(),connect.getResponseMessage(),sb,paramsMap);
				}
			} catch (Exception e)
			{
				ConstatFinalUtil.SYS_LOG.error("http的GET请求失败了,请求信息" + paramsMap,
						e);
				try
				{
					ConstatFinalUtil.SYS_LOG.info("http的GET请求失败了;重试次数:{},响应码:{};返回信息:{};请求头:{};",
							i,connect != null ? connect.getResponseCode() : connect,sb,paramsMap,e);
					Thread.sleep(ConstatFinalUtil.PAGE_BATCH_SIZE);
				} catch (Exception e1)
				{
					e1.printStackTrace();
				}
			}
			//重试机制
			if(!"true".equalsIgnoreCase(paramsMap.get("retry")))
			{
				break ; 
			}
		}
		return sb.toString();
	}

	/**
	 * Get请求
	 * 
	 * @param paramsMap
	 * @return
	 */
	public String methodGet(Map<String, String> headerMap,Map<String, String> paramsMap)
	{
		StringBuffer sb = new StringBuffer();
		
		// 连续请求多次
		for (int i = 0; i < ConstatFinalUtil.REQ_COUNT; i++)
		{
			sb.delete(0, sb.length());
			StringBuffer paramssb = new StringBuffer();
			HttpURLConnection connect = null ; 
			try
			{
				paramssb.append(paramsMap.get("requestURL"));
				if (!paramssb.toString().endsWith("?") && paramsMap.size() > 1)
				{
					paramssb.append("?");
				}
				for (Iterator iterator = paramsMap.entrySet().iterator(); iterator
						.hasNext();)
				{
					Map.Entry me = (Map.Entry) iterator.next();
					if (!me.getKey().toString().equalsIgnoreCase("requestURL"))
					{
						paramssb.append(me.getKey() + "=" + me.getValue() + "&");
					}
				}
	
				if (paramssb.toString().endsWith("&"))
				{
					paramssb.delete(paramssb.length() - 1, paramssb.length());
				}
				
				//ConstatFinalUtil.SYS_LOG.info(paramssb.toString());
				
				URL url = new URL(paramssb.toString());
				connect = (HttpURLConnection) url
						.openConnection();
				connect.setConnectTimeout(ConstatFinalUtil.REQ_CONNECT_TIMEOUT);
				connect.setReadTimeout(ConstatFinalUtil.READ_TIMEOUT);
	
				connect.setAllowUserInteraction(true);
	
				connect.setDoInput(true);
				connect.setDoOutput(false);
				
				for (Iterator iterator = headerMap.entrySet().iterator(); iterator
						.hasNext();)
				{
					Entry me = (Entry) iterator.next();
					if (!"requestURL".equalsIgnoreCase(me.getKey() + ""))
					{
						connect.setRequestProperty(me.getKey() + "", me.getValue()
								+ "");
					}
				}
				
				connect.setRequestProperty(
						"Cookie",
						"Hm_lvt_4982d57ea12df95a2b24715fb6440726=1398138317; mstuid=1398138316637_9919; userId=10931889; XM_Hd_Start=1; Hm_lvt_7080c6a6aba51276281d5d595b080def=1398143044");
				connect.setRequestProperty(
						"User-Agent",
						"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36");
				
				BufferedReader br = null ; 
				//响应码成功
				if(connect.getResponseCode() == 200)
				{
					br = new BufferedReader(new InputStreamReader(
								connect.getInputStream(), "UTF-8"));
				}else
				{
					if(connect.getErrorStream() != null)
					{
						br = new BufferedReader(new InputStreamReader(
								connect.getErrorStream(), "UTF-8"));
					}else
					{
						br = new BufferedReader(new InputStreamReader(
								connect.getInputStream(), "UTF-8"));
					}
				}
				
				String line = "";
				while ((line = br.readLine()) != null)
				{
					sb.append(line.trim());
				}
				br.close();
				
				//响应码成功
				if(connect.getResponseCode() == 200)
				{
					break ; 
				}else
				{
					ConstatFinalUtil.SYS_LOG.info("重试次数:{},响应码:{};响应信息:{};返回信息:{};请求头:{};",
							i,connect.getResponseCode(),connect.getResponseMessage(),sb,paramsMap);
				}
			} catch (Exception e)
			{
				ConstatFinalUtil.SYS_LOG.error("http的GET请求失败了,请求信息" + paramsMap,
						e);
				try
				{
					ConstatFinalUtil.SYS_LOG.info("http的GET请求失败了;重试次数:{},响应码:{};返回信息:{};请求头:{};",
							i,connect != null ? connect.getResponseCode() : connect,sb,paramsMap,e);
					Thread.sleep(ConstatFinalUtil.PAGE_BATCH_SIZE);
				} catch (Exception e1)
				{
					e1.printStackTrace();
				}
			}
			//重试机制
			if(!"true".equalsIgnoreCase(paramsMap.get("retry")))
			{
				break ; 
			}
		}
		return sb.toString();
	}
	
	/**
	 * delete请求
	 * 
	 * @param headerMap
	 *            请求头
	 * @param paramsMap
	 *            请求体参数
	 * @return
	 */
	public String methodDelete(Map<String, String> headerMap,
			Map<String, String> paramsMap)
	{
		StringBuffer sb = new StringBuffer();
		// 连续请求多次
		for (int i = 0; i < ConstatFinalUtil.REQ_COUNT; i++)
		{
			sb.delete(0, sb.length());
			StringBuffer paramssb = new StringBuffer();
			HttpURLConnection connect = null ; 
			try
			{
				paramssb.append(paramsMap.get("requestURL"));
				if (!paramssb.toString().endsWith("?") && paramsMap.size() > 1)
				{
					paramssb.append("?");
				}
				for (Iterator iterator = paramsMap.entrySet().iterator(); iterator
						.hasNext();)
				{
					Map.Entry me = (Map.Entry) iterator.next();
					if (!me.getKey().toString().equalsIgnoreCase("requestURL"))
					{
						paramssb.append(me.getKey() + "=" + me.getValue() + "&");
					}
				}
	
				if (paramssb.toString().endsWith("&"))
				{
					paramssb.delete(paramssb.length() - 1, paramssb.length());
				}
				
				//ConstatFinalUtil.SYS_LOG.info(paramssb.toString());
				
				URL url = new URL(paramssb.toString());
				connect = (HttpURLConnection) url
						.openConnection();
				connect.setRequestMethod("DELETE");
				
				connect.setConnectTimeout(ConstatFinalUtil.REQ_CONNECT_TIMEOUT);
				connect.setReadTimeout(ConstatFinalUtil.READ_TIMEOUT);
	
				connect.setAllowUserInteraction(true);
	
				connect.setDoInput(true);
				connect.setDoOutput(false);
				
				for (Iterator iterator = headerMap.entrySet().iterator(); iterator
						.hasNext();)
				{
					Entry me = (Entry) iterator.next();
					if (!"requestURL".equalsIgnoreCase(me.getKey() + ""))
					{
						connect.setRequestProperty(me.getKey() + "", me.getValue()
								+ "");
					}
				}
				
				connect.setRequestProperty(
						"Cookie",
						"Hm_lvt_4982d57ea12df95a2b24715fb6440726=1398138317; mstuid=1398138316637_9919; userId=10931889; XM_Hd_Start=1; Hm_lvt_7080c6a6aba51276281d5d595b080def=1398143044");
				connect.setRequestProperty(
						"User-Agent",
						"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36");
				
				BufferedReader br = null ; 
				//响应码成功
				if(connect.getResponseCode() == 200)
				{
					br = new BufferedReader(new InputStreamReader(
								connect.getInputStream(), "UTF-8"));
				}else
				{
					if(connect.getErrorStream() != null)
					{
						br = new BufferedReader(new InputStreamReader(
								connect.getErrorStream(), "UTF-8"));
					}else
					{
						br = new BufferedReader(new InputStreamReader(
								connect.getInputStream(), "UTF-8"));
					}
				}
				
				String line = "";
				while ((line = br.readLine()) != null)
				{
					sb.append(line.trim());
				}
				br.close();
				
				//响应码成功
				if(connect.getResponseCode() == 200)
				{
					break ; 
				}else
				{
					ConstatFinalUtil.SYS_LOG.info("重试次数:{},响应码:{};响应信息:{};返回信息:{};请求头:{};",
							i,connect.getResponseCode(),connect.getResponseMessage(),sb,paramsMap);
				}
			} catch (Exception e)
			{
				ConstatFinalUtil.SYS_LOG.error("http的GET请求失败了,请求信息" + paramsMap,
						e);
				try
				{
					ConstatFinalUtil.SYS_LOG.info("http的GET请求失败了;重试次数:{},响应码:{};返回信息:{};请求头:{};",
							i,connect != null ? connect.getResponseCode() : connect,sb,paramsMap,e);
					Thread.sleep(ConstatFinalUtil.PAGE_BATCH_SIZE);
				} catch (Exception e1)
				{
					e1.printStackTrace();
				}
			}
			//重试机制
			if(!"true".equalsIgnoreCase(paramsMap.get("retry")))
			{
				break ; 
			}
		}
		return sb.toString();
	}
	
	/**
	 * main方法测试
	 * @param args
	 */
	public static void main(String[] args)
	{
		HTTPUtil httpUtil = new HTTPUtil();
		Map<String, String> paramsMap = new HashMap<String,String>();
		paramsMap.put("requestURL", "http://www.baidu.com/asdf");
		paramsMap.put("retry", "true");
		String result = httpUtil.methodGet(paramsMap);
		System.out.println("----" + result);
	}
}
