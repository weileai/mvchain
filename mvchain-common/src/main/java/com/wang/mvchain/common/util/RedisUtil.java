package com.wang.mvchain.common.util;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/**
 * redis的工具类
 * 里面全部存放json
 * @author wangsh
 */
@Component("redisUtil")
public class RedisUtil
{
	@Resource
	private RedisTemplate redisTemplate ;
	
	/**
	 * 往redis里面放东西
	 * @param key	要合理规划
	 * @param valueJSON	json,里面必须包含创建时间
	 * @return
	 */
	public boolean put(final String key,final String valueJSON)
	{
		try
		{
			return (Boolean)this.redisTemplate.execute(new RedisCallback() {

				@Override
				public Object doInRedis(RedisConnection connection) throws DataAccessException {
					byte[] keyByte = redisTemplate.getStringSerializer().serialize(key) ; 
					connection.set(keyByte, redisTemplate.getStringSerializer().serialize(valueJSON));
					return true;
				}
			});
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOG.error("从redis中存储数据报错了,键为:" + key,e);
		}
		return false ;
	}
	
	/**
	 * 往redis里面放东西
	 * @param key	要合理规划
	 * @param valueJSON	json,里面必须包含创建时间
	 * @param timeout	过期时间,单位是秒
	 * @return
	 */
	public boolean put(final String key,final String valueJSON , final int timeout)
	{
		try
		{
			return (Boolean)this.redisTemplate.execute(new RedisCallback() {

				@Override
				public Object doInRedis(RedisConnection connection) throws DataAccessException {
					byte[] keyByte = redisTemplate.getStringSerializer().serialize(key) ; 
					connection.set(keyByte, redisTemplate.getStringSerializer().serialize(valueJSON));
					//设置一下过期时间
					boolean flag = connection.expire(keyByte, timeout);
					if(!flag)
					{
						ConstatFinalUtil.SYS_LOG.error(key + "--" + timeout + ",redis设置过时时间(毫秒)失败了--" + valueJSON);
					}
					return flag;
				}
			});
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOG.error("从redis中存储数据报错了,键为:" + key,e);
		}
		return false ;
	}
	
	/**
	 * 往redis里面放东西
	 * @param key	要合理规划
	 * @param valueJSON	json,里面必须包含创建时间
	 * @param expDate 旋转的是指定的日期过期
	 * @return
	 */
	public boolean put(final String key,final String valueJSON , final Date expDate)
	{
		try
		{
			return (Boolean)this.redisTemplate.execute(new RedisCallback() {

				@Override
				public Object doInRedis(RedisConnection connection) throws DataAccessException {
					byte[] keyByte = redisTemplate.getStringSerializer().serialize(key) ; 
					connection.set(keyByte, redisTemplate.getStringSerializer().serialize(valueJSON));
					//设置一下过期时间,单位是秒,所以要除上1000
					boolean flag = connection.expireAt(keyByte, expDate.getTime() / 1000);
					if(!flag)
					{
						ConstatFinalUtil.SYS_LOG.error(key + "--" + expDate.toLocaleString() + ",redis设置过时间点失败了--" + valueJSON);
					}
					return flag;
				}
			});
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOG.error("从redis中存储数据报错了,键为:" + key,e);
		}
		return false ;
	}
	
	/**
	 * 从redis里面取数据
	 * @param key 会返回null
	 * @return
	 */
	public Object get(final String key)
	{
		try
		{
			return this.redisTemplate.execute(new RedisCallback() {

				@Override
				public Object doInRedis(RedisConnection connection)
						throws DataAccessException {
					byte[] keyByte = redisTemplate.getStringSerializer().serialize(key);
					if(connection.exists(keyByte))
					{
						byte[] valueByte = connection.get(keyByte);
						return redisTemplate.getStringSerializer().deserialize(valueByte); 
					}
					return null;
				}
			});
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOG.error("从redis中读取数据报错了,键为:" + key,e);
		}
		return null ; 
	}
	
	/**
	 * 删除数据
	 * @param key
	 * @return 返回删除的条数	-1 为键不存在
	 */
	public Object delete(final String key)
	{
		try
		{
			return this.redisTemplate.execute(new RedisCallback() {

				@Override
				public Object doInRedis(RedisConnection connection)
						throws DataAccessException {
					byte[] keyByte = redisTemplate.getStringSerializer().serialize(key);
					if(connection.exists(keyByte))
					{
						long keylon = connection.del(keyByte);
						return keylon ; 
					}
					return -1;
				}
			});
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOG.error("从redis中删除数据报错了,键为:" + key,e);
		}
		return -1 ; 
	}
}