package com.wang.mvchain.common.util;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Random;

import org.springframework.stereotype.Component;

/**
 * 所有正则表达式的工具类
 * @author wangsh
 *
 */
@Component("regexUtil")
public class RegexUtil
{
	private String randStr = "23456789abcdefghijkmnpqrstuvwxyz" ; 
	
	/**
	 * 随机生成字符串
	 * @param length 生成随机数的长度
	 * @return
	 */
	public String proccedRandString(int length)
	{
		String result = "" ; 
		Random rand = new Random();
		for (int i = 0; i < length; i++)
		{
			//随机生成 随机字符串长度(randStr)的任意一个位置
			int res = rand.nextInt(randStr.length());
			char ch = randStr.charAt(res);
			result += ch ; 
		}
		return result ; 
	}
	
	/**
	 * 解析百分比
	 * @return
	 */
	public String parsePercent(double dou)
	{
		DecimalFormat df = new DecimalFormat("##.##%");
		return df.format(dou) ; 
	}
	
	/**
	 * double,小数点保留十位
	 * @return
	 */
	public String parseDouble(double dou)
	{
		DecimalFormat df = new DecimalFormat("#.##########");
		return df.format(dou) ; 
	}
	
	/**
	 * double,小数点保留两位
	 * @return
	 */
	public String parseString(double dou)
	{
		DecimalFormat df = new DecimalFormat("#");
		return df.format(dou) ; 
	}
}
