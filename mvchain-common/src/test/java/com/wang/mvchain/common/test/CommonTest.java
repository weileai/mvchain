package com.wang.mvchain.common.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.common.util.SpringEmailUtil;

public class CommonTest extends BaseTest
{
	private Logger logger = LogManager.getLogger(CommonTest.class);
	
	private SpringEmailUtil springEmailUtil;
	
	@Before
	public void init()
	{
		this.springEmailUtil = (SpringEmailUtil) ctx.getBean("springEmailUtil");
	}
	
	/**
	 * 测试一下框架方法
	 */
	@Test
	public void test()
	{
		ConstatFinalUtil.SYS_LOG.info("----");
	}
	
	/**
	 * 发送邮件的方法
	 */
	@Test
	public void sendHTMLMail()
	{
		String email = "724259669@qq.com";
		String subject = "aaa" ; 
		String text = "测试内容!" ; 
		boolean flag = this.springEmailUtil.sendHTMLMail(email, subject, text);
		logger.info("发信结果:{}",flag);
	}
}
