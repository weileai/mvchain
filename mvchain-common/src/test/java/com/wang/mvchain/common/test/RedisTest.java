package com.wang.mvchain.common.test;

import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;

import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.common.util.RedisUtil;

public class RedisTest extends BaseTest
{
	private RedisTemplate redisTemplate;
	private RedisUtil redisUtil;

	@Before
	public void initSpring()
	{
		this.redisTemplate = (RedisTemplate) ctx.getBean("redisTemplate");
		this.redisUtil = (RedisUtil) ctx.getBean("redisUtil");
	}

	/**
	 * redis保存数据库
	 */
	@Test
	public void save()
	{
		this.redisTemplate.execute(new RedisCallback<Object>()
		{

			@Override
			public Object doInRedis(RedisConnection connection) throws DataAccessException
			{
				connection.set(redisTemplate.getStringSerializer().serialize("wangsh"),
						redisTemplate.getStringSerializer().serialize("liwang"));
				return null;
			}
		});
	}

	/**
	 * 用redis读取一个对象
	 */
	@Test
	public void get()
	{
		this.redisTemplate.execute(new RedisCallback<Object>()
		{

			@Override
			public Object doInRedis(RedisConnection connection) throws DataAccessException
			{
				// byte[] key =
				// redisTemplate.getStringSerializer().serialize("wangsh");
				byte[] key = redisTemplate.getStringSerializer().serialize("poi_poi_1077480BJYHATM");
				// 删除数据
				// long rs = connection.del(key);
				// System.out.println(rs);
				if (connection.exists(key))
				{
					byte[] value = connection.get(key);
					System.out.println(redisTemplate.getStringSerializer().deserialize(value));
				} else
				{
					// System.out.println(rs + "====不存在===" + key);
					System.out.println("-----");
				}
				return null;
			}
		});
	}

	/**
	 * 用redis读取一个对象
	 */
	@Test
	public void delete()
	{
		this.redisTemplate.execute(new RedisCallback<Object>()
		{

			@Override
			public Object doInRedis(RedisConnection connection) throws DataAccessException
			{
				// byte[] key =
				// redisTemplate.getStringSerializer().serialize("wangsh");
				byte[] key = redisTemplate.getStringSerializer().serialize("poi_poi_1077480BJYHATM");
				// 删除数据
				long rs = connection.del(key);
				System.out.println("删除poi:" + rs);

				key = redisTemplate.getStringSerializer().serialize("poi_zhoubian_1077480BJYHATM");
				// 删除数据
				rs = connection.del(key);
				System.out.println("删除poi周边:" + rs);

				// 删除数据
				key = redisTemplate.getStringSerializer().serialize("poi_xiangguan_1077480BJYHATM");
				rs = connection.del(key);
				System.out.println("删除poi相关搜索:" + rs);
				return null;
			}
		});
	}

	@Test
	public void putget()
	{
		String key = "wangsh";
		String value = "aaa";
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, 10);
		System.out.println(calendar.getTime().toLocaleString() + "--->" + calendar.getTimeInMillis());
		boolean flag = this.redisUtil.put(key, value, calendar.getTime());
		ConstatFinalUtil.SYS_LOG.info(flag);
		
		value = (String) this.redisUtil.get(key);
		System.out.println(value);
	}
}
