package com.wang.mvchain.common.test;

import java.io.IOException;
import java.util.Iterator;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * https://zz.fang.anjuke.com/fangyuan/s?m=b&p=2
 */
public class AnjuKeSpider
{
	/**
	 * 测试代码
	 */
	public void list(int page)
	{
		String url = "https://zz.fang.anjuke.com/fangyuan/s?m=b&p=1" ; 
		/* 连接 */
		Connection connection = Jsoup.connect(url);
		try
		{
			/* 获取文档对象 */
			Document document = connection.get() ;
			
			/* 列表 */
			Elements listEles = document.getElementsByClass("item-mod");
			for (Iterator iterator = listEles.iterator(); iterator.hasNext();)
			{
				Element itemEle = (Element) iterator.next();
				/* 图片路径 */
				String imgPath = itemEle.getElementsByClass("F-pic").get(0).getElementsByTag("img").get(0).attr("src");
				
				Element dlEle = itemEle.getElementsByClass("F-info").get(0);
				/* 获取aq链接 */
				Element nameEleA = dlEle.getElementsByTag("a").get(0);
				String name = nameEleA.text();
				String href = nameEleA.attr("href");
				/* 其它 */
				Elements othersEles = dlEle.getElementsByTag("em");
				String others = "" ; 
				for (Iterator iterator2 = othersEles.iterator(); iterator2.hasNext();)
				{
					Element ele = (Element) iterator2.next();
					others += ele.text() + "|" ; 
				}
				/* 城市 */
				Elements cityEles = dlEle.getElementsByTag("dd").get(1).getElementsByTag("a");
				String citys = "" ; 
				for (Iterator iterator2 = cityEles.iterator(); iterator2.hasNext();)
				{
					Element ele = (Element) iterator2.next();
					citys += ele.text() + "|" ; 
				}
				/* 内容 */
				System.out.println("name:" + name + "others:" + others + ";citys:"+ citys +";imgPath:"+ imgPath +";href:" + href);
			}
			System.out.println("=====" + listEles.size());
		} catch (IOException e)
		{
			e.printStackTrace();
		} 
	}
	
	public static void main(String[] args)
	{
		AnjuKeSpider anjuKeSpider = new AnjuKeSpider();
		anjuKeSpider.list(1);
	}
}
