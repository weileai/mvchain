package com.wang.mvchain.system.service;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.PageInfoUtil;
import com.wang.mvchain.system.pojo.ARegion;
import com.wang.mvchain.system.pojo.ASyspro;

/**
 * 系统模块的主要service
 * 
 * @author wangshh
 * 
 */
public interface ISystemService
{
	/*----- 全国地区模块管理开始 -----*/
	/**
	 * 添加一条全国地区
	 * 
	 * @param region
	 * @return
	 */
	JSONObject saveOneRegionService(ARegion region);

	/**
	 * 更新一条全国地区
	 * 
	 * @param region
	 * @return
	 */
	JSONObject updateOneRegionService(ARegion region);

	/**
	 * 删除一条全国地区
	 * 
	 * @param region
	 * @return
	 */
	JSONObject deleteOneRegionService(ARegion region);

	/**
	 * 查询多条全国地区
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	List<ARegion> findCondListRegionService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);

	/**
	 * 查询一条全国地区
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	ARegion findOneRegionService(Map<String, Object> condMap);
	
	/**
	 * 根据当前地区查询节点的字符串描述
	 * 一级节点-->二级节点-->三级节点
	 * @param type
	 * @param regionid
	 * @return
	 */
	List<ARegion> findTreeRegionService(ARegion region);
	/*----- 全国地区模块管理结束 -----*/
	
	/*----- 系统配置模块管理开始 -----*/
	/**
	 * 添加一条系统配置
	 * 
	 * @param syspro
	 * @return
	 */
	JSONObject saveOneSysproService(ASyspro syspro);

	/**
	 * 更新一条系统配置
	 * 
	 * @param syspro
	 * @return
	 */
	JSONObject updateOneSysproService(ASyspro syspro);

	/**
	 * 删除一条系统配置
	 * 
	 * @param syspro
	 * @return
	 */
	JSONObject deleteOneSysproService(ASyspro syspro);

	/**
	 * 查询多条系统配置
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	List<ASyspro> findCondListSysproService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);

	/**
	 * 查询一条系统配置
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	ASyspro findOneSysproService(Map<String, Object> condMap);
	/*----- 系统配置模块管理结束 -----*/
}
