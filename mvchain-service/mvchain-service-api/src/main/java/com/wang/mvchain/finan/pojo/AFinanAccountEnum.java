package com.wang.mvchain.finan.pojo;

/**
 * 管理员的枚举
 * 
 * @author wangsh
 *
 */
public enum AFinanAccountEnum
{
	STATUS_ENABLE(Byte.valueOf("1"), "启用"), 
	STATUS_DISABLE(Byte.valueOf("0"), "禁用"),
	
	ACCOUNTTYPE_ALIPAY(Byte.valueOf("0"), "支付宝"),
	ACCOUNTTYPE_WEIXIN(Byte.valueOf("1"), "微信");

	private byte status;
	private String name;

	private AFinanAccountEnum(byte status, String name)
	{
		this.status = status;
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public String toString()
	{
		return super.toString();
	}
}
