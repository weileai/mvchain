package com.wang.mvchain.users.pojo;

/**
 * 管理员的枚举
 * 
 * @author wangsh
 *
 */
public enum ATradeHistoryEnum
{
	ORDERTYPE_BUY(Byte.valueOf("0"), "买"), 
	ORDERTYPE_SELL(Byte.valueOf("1"), "卖"),
	
	STATUS_UNSTART(Byte.valueOf("0"), "未开始"),
	STATUS_ORDER_KILL(Byte.valueOf("1"), "撤单"),
	STATUS_ORDER_ING(Byte.valueOf("2"), "挂单"),
	STATUS_ORDER_FINISH(Byte.valueOf("3"), "完成");

	private byte status;
	private String name;

	private ATradeHistoryEnum(byte status, String name)
	{
		this.status = status;
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public String toString()
	{
		return super.toString();
	}
}
