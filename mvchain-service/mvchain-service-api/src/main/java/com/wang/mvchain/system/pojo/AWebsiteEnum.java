package com.wang.mvchain.system.pojo;

/**
 * 网站的枚举
 * 
 * @author wangsh
 *
 */
public enum AWebsiteEnum
{
	STATUS_ENABLE(Byte.valueOf("1"), "启用"), 
	STATUS_DISABLE(Byte.valueOf("0"), "禁用");

	private byte status;
	private String name;

	private AWebsiteEnum(byte status, String name)
	{
		this.status = status;
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public String toString()
	{
		return super.toString();
	}
}
