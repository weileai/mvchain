package com.wang.mvchain.users.pojo;

/**
 * 管理员的枚举
 * 
 * @author wangsh
 *
 */
public enum AAdminsEnum
{
	STATUS_ENABLE(Byte.valueOf("1"), "启用"), 
	STATUS_DISABLE(Byte.valueOf("0"), "禁用"),
	
	PAY_UN(Byte.valueOf("0"), "未支付"),
	PAY_ING(Byte.valueOf("1"), "支付中"),
	PAY_SUCCESS(Byte.valueOf("2"), "支付成功"),
	PAY_FAILED(Byte.valueOf("3"), "支付失败");

	private byte status;
	private String name;

	private AAdminsEnum(byte status, String name)
	{
		this.status = status;
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public String toString()
	{
		return super.toString();
	}
}
