package com.wang.mvchain.users.service;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.PageInfoUtil;
import com.wang.mvchain.users.pojo.ATradeHistory;
import com.wang.mvchain.users.pojo.AUsersTask;
import com.wang.mvchain.users.pojo.AUsersTaskOrder;


public interface IOrdersService
{
	/*----- 用户任务表开始 -----*/
	/**
	 * 添加一个用户任务表
	 * 
	 * @param usersTask
	 * @return
	 */
	JSONObject saveOneUsersTaskService(AUsersTask usersTask);

	/**
	 * 更新一个用户任务表
	 * 
	 * @param usersTask
	 * @return
	 */
	JSONObject updateOneUsersTaskService(AUsersTask usersTask);
	
	/**
	 * 更新多个用户任务表
	 * 
	 * @param usersTask
	 * @return
	 */
	JSONObject updateBatchUsersTaskService(String type,AUsersTask usersTask);

	/**
	 * 删除一个用户任务表
	 * 
	 * @param usersTask
	 * @return
	 */
	JSONObject deleteOneUsersTaskService(AUsersTask usersTask);

	/**
	 * 查询一个记录
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	AUsersTask findOneUsersTaskService(Map<String, Object> condMap);

	/**
	 * 查询多条记录
	 * 
	 * @param pageInfoUtil
	 * @param type
	 * @param condMap
	 * @return
	 */
	List<AUsersTask> findCondListUsersTaskService(PageInfoUtil pageInfoUtil,Map<String, Object> condMap);
	
	/**
	 * 查询用户任务的统计记录
	 * @param pageInfoUtil
	 * @param type
	 * @param condMap
	 * @return
	 */
	List findStatMapUsersTaskService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);
	/*----- 用户任务表结束 -----*/
	
	/*----------用户任务订单开始-----------*/

	/**
	 * 添加一个用户任务订单
	 * 
	 * @param usersTaskOrder
	 * @return
	 */
	JSONObject saveOneUsersTaskOrderService(AUsersTaskOrder usersTaskOrder);

	/**
	 * 更新一个用户任务订单
	 * 
	 * @param usersTaskOrder
	 * @return
	 */
	JSONObject updateOneUsersTaskOrderService(AUsersTaskOrder usersTaskOrder);

	/**
	 * 删除一个用户任务订单
	 * 
	 * @param usersTaskOrder
	 * @return
	 */
	JSONObject deleteOneUsersTaskOrderService(AUsersTaskOrder usersTaskOrder);
	
	/**
	 * 删除多个用户任务订单
	 * 
	 * @param usersTaskOrder
	 * @return
	 */
	JSONObject deleteBatchUsersTaskOrderService(Map<String, Object> condMap);

	/**
	 * 查询一个记录
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	AUsersTaskOrder findOneUsersTaskOrderService(Map<String, Object> condMap);

	/**
	 * 查询多条记录
	 * 
	 * @param pageInfoUtil
	 * @param type
	 * @param condMap
	 * @return
	 */
	List<AUsersTaskOrder> findCondListUsersTaskOrderService(PageInfoUtil pageInfoUtil, Map<String, Object> condMap);
	/*----------用户任务订单结束-----------*/
	
	/*----------交易历史开始-----------*/

	/**
	 * 添加一个交易历史
	 * 
	 * @param tradeHistory
	 * @return
	 */
	JSONObject saveOneTradeHistoryService(ATradeHistory tradeHistory);

	/**
	 * 更新一个交易历史
	 * 
	 * @param tradeHistory
	 * @return
	 */
	JSONObject updateOneTradeHistoryService(ATradeHistory tradeHistory);
	
	/**
	 * 更新一个交易历史
	 * 
	 * @param tradeHistory
	 * @return
	 */
	JSONObject updateOneTradeHistoryService(ATradeHistory tradeHistory,Map<String, Object> condMap);

	/**
	 * 删除一个交易历史
	 * 
	 * @param tradeHistory
	 * @return
	 */
	JSONObject deleteOneTradeHistoryService(ATradeHistory tradeHistory);
	
	/**
	 * 查询一个记录
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	ATradeHistory findOneTradeHistoryService(Map<String, Object> condMap);

	/**
	 * 查询多条记录
	 * 
	 * @param pageInfoUtil
	 * @param type
	 * @param condMap
	 * @return
	 */
	List<ATradeHistory> findCondListTradeHistoryService(PageInfoUtil pageInfoUtil, Map<String, Object> condMap);
	/*----------交易历史结束-----------*/
}
