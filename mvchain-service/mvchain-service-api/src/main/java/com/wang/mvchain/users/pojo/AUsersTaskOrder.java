package com.wang.mvchain.users.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.wang.mvchain.system.pojo.ATradeType;

/**
 * 市场深度表,放的是最新的市场行情
 * 
 * @author wangsh
 * 
 */
public class AUsersTaskOrder
{
	/*
	 * 数据库中存储的字段
	 */
	private int id;
	private int utid;
	private int uaid;
	private int ctid ;
	private String ordersn ; 
	private double souprice ; 
	private double price;
	private double tradePrice ; 
	private double tradeedval;
	private double totalval;
	private double totalprice;
	private byte ordertype;
	private double fee ; 
	private byte status;
	private byte priftstatus;
	private Date createtime;
	private Date updatetime;
	private Date pubtime;

	/* 字符串表示 */
	private String ordertypeStr;
	private String statusStr;
	private String priftstatusStr;
	
	/*对象表示*/
	private AUsersTask usersTaskObj;
	private AUsersAccount usersAccountObj;
	private ATradeType tradeTypeObj ; 
	
	/* 存储所有状态的容器 */
	private Map<String,String> enumsMap = new TreeMap<String, String>();

	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		AUsersTaskOrderEnum[] enums = AUsersTaskOrderEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AUsersTaskOrderEnum enumTemp = enums[i];
			String key = enumTemp.toString() ; 
			if(key.lastIndexOf("_") != -1)
			{
				key = key.substring(0,key.lastIndexOf("_")) ; 
			}
			enumsMap.put(key + "-" + enumTemp.getStatus() + "", enumTemp.getName());
		}
		return enumsMap;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public double getTradeedval()
	{
		return tradeedval;
	}

	public void setTradeedval(double tradeedval)
	{
		this.tradeedval = tradeedval;
	}

	public double getTotalval()
	{
		return totalval;
	}

	public void setTotalval(double totalval)
	{
		this.totalval = totalval;
	}

	public double getTotalprice()
	{
		return totalprice;
	}

	public void setTotalprice(double totalprice)
	{
		this.totalprice = totalprice;
	}

	public byte getOrdertype()
	{
		return ordertype;
	}

	public void setOrdertype(byte ordertype)
	{
		this.ordertype = ordertype;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public byte getPriftstatus()
	{
		return priftstatus;
	}

	public void setPriftstatus(byte priftstatus)
	{
		this.priftstatus = priftstatus;
	}

	public Date getCreatetime()
	{
		return createtime;
	}

	public void setCreatetime(Date createtime)
	{
		this.createtime = createtime;
	}

	public Date getUpdatetime()
	{
		return updatetime;
	}

	public void setUpdatetime(Date updatetime)
	{
		this.updatetime = updatetime;
	}

	public Date getPubtime()
	{
		return pubtime;
	}

	public void setPubtime(Date pubtime)
	{
		this.pubtime = pubtime;
	}

	public String getOrdertypeStr()
	{
		/*
		 * 类型:0:买,1:卖
		 */
		AUsersTaskOrderEnum[] usersTaskOrderEnums = AUsersTaskOrderEnum.values();
		for (int i = 0; i < usersTaskOrderEnums.length; i++)
		{
			AUsersTaskOrderEnum usersTaskOrderEnum = usersTaskOrderEnums[i];
			if (usersTaskOrderEnum.toString().startsWith("ORDERTYPE_"))
			{
				// 表示是状态的标识
				if (usersTaskOrderEnum.getStatus() == this.getOrdertype())
				{
					this.ordertypeStr = usersTaskOrderEnum.getName();
					break;
				}
			}
		}
		return ordertypeStr;
	}

	public String getPriftstatusStr()
	{
		/*
		 * 计算状态:1:已计算过收益,0:未计算
		 */
		AUsersTaskOrderEnum[] usersTaskOrderEnums = AUsersTaskOrderEnum.values();
		for (int i = 0; i < usersTaskOrderEnums.length; i++)
		{
			AUsersTaskOrderEnum usersTaskOrderEnum = usersTaskOrderEnums[i];
			if (usersTaskOrderEnum.toString().startsWith("PRIFTSTATUS_"))
			{
				// 表示是状态的标识
				if (usersTaskOrderEnum.getStatus() == this.getPriftstatus())
				{
					this.priftstatusStr = usersTaskOrderEnum.getName();
					break;
				}
			}
		}
		return priftstatusStr;
	}

	public String getStatusStr()
	{
		/*
		 * 状态:0:未开始,1:撤单;2,挂单;3:完成
		 */
		AUsersTaskOrderEnum[] usersTaskOrderEnums = AUsersTaskOrderEnum.values();
		for (int i = 0; i < usersTaskOrderEnums.length; i++)
		{
			AUsersTaskOrderEnum usersTaskOrderEnum = usersTaskOrderEnums[i];
			if (usersTaskOrderEnum.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (usersTaskOrderEnum.getStatus() == this.getStatus())
				{
					this.statusStr = usersTaskOrderEnum.getName();
					break;
				}
			}
		}
		return statusStr;
	}

	public int getUaid()
	{
		return uaid;
	}

	public void setUaid(int uaid)
	{
		this.uaid = uaid;
	}

	public double getPrice()
	{
		return price;
	}

	public void setPrice(double price)
	{
		this.price = price;
	}

	public int getUtid()
	{
		return utid;
	}

	public void setUtid(int utid)
	{
		this.utid = utid;
	}

	public AUsersTask getUsersTaskObj()
	{
		return usersTaskObj;
	}

	public void setUsersTaskObj(AUsersTask usersTaskObj)
	{
		this.usersTaskObj = usersTaskObj;
	}

	public AUsersAccount getUsersAccountObj()
	{
		return usersAccountObj;
	}

	public void setUsersAccountObj(AUsersAccount usersAccountObj)
	{
		this.usersAccountObj = usersAccountObj;
	}

	public String getOrdersn()
	{
		return ordersn;
	}

	public void setOrdersn(String ordersn)
	{
		this.ordersn = ordersn;
	}

	public double getSouprice()
	{
		return souprice;
	}

	public void setSouprice(double souprice)
	{
		this.souprice = souprice;
	}

	public double getFee()
	{
		return fee;
	}

	public void setFee(double fee)
	{
		this.fee = fee;
	}

	public int getCtid()
	{
		return ctid;
	}

	public void setCtid(int ctid)
	{
		this.ctid = ctid;
	}

	public ATradeType getTradeTypeObj()
	{
		return tradeTypeObj;
	}

	public void setTradeTypeObj(ATradeType tradeTypeObj)
	{
		this.tradeTypeObj = tradeTypeObj;
	}

	public double getTradePrice()
	{
		return tradePrice;
	}

	public void setTradePrice(double tradePrice)
	{
		this.tradePrice = tradePrice;
	}
}
