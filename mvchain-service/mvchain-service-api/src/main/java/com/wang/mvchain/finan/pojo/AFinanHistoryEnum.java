package com.wang.mvchain.finan.pojo;

/**
 * 管理员的枚举
 * 
 * @author wangsh
 *
 */
public enum AFinanHistoryEnum
{
	/*状态:0:取消,1:未处理,2:已处理*/
	STATUS_CANCEL(Byte.valueOf("1"), "未处理"), 
	STATUS_UNPROCC(Byte.valueOf("0"), "取消"),
	STATUS_PROCCED(Byte.valueOf("2"), "已处理"), 
	
	/*0:收入;1:支出;*/
	TRADETYPE_IN(Byte.valueOf("0"), "收入"),
	TRADETYPE_OUT(Byte.valueOf("1"), "支出"),
	
	/*0:充值,1:提现;2:收益;*/
	HISTORYTYPE_RECHARGE(Byte.valueOf("0"), "充值"),
	HISTORYTYPE_WITHDRAW(Byte.valueOf("1"), "提现"),
	HISTORYTYPE_PRIFT(Byte.valueOf("2"), "收益");

	private byte status;
	private String name;

	private AFinanHistoryEnum(byte status, String name)
	{
		this.status = status;
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public String toString()
	{
		return super.toString();
	}
}
