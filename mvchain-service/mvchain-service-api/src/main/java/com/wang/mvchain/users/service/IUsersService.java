package com.wang.mvchain.users.service;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.PageInfoUtil;
import com.wang.mvchain.users.pojo.AAdmins;
import com.wang.mvchain.users.pojo.AUsers;
import com.wang.mvchain.users.pojo.AUsersAccount;

public interface IUsersService
{

	/*----------管理员开始-----------*/

	/**
	 * 添加一个管理员
	 * 
	 * @param admin
	 * @return
	 */
	JSONObject saveOneAdminsService(AAdmins admin);

	/**
	 * 更新一个管理员
	 * 
	 * @param admin
	 * @return
	 */
	JSONObject updateOneAdminsService(AAdmins admin);

	/**
	 * 删除一个管理员
	 * 
	 * @param admin
	 * @return
	 */
	JSONObject deleteOneAdminsService(AAdmins admin);

	/**
	 * 查询一个记录
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	AAdmins findOneAdminsService(Map<String, Object> condMap);

	/**
	 * 查询多条记录
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	List<AAdmins> findCondListAdminsService(PageInfoUtil pageInfoUtil, Map<String, Object> condMap);
	/*----------管理员结束-----------*/

	/*----------用户开始-----------*/

	/**
	 * 添加一个用户
	 * 
	 * @param users
	 * @return
	 */
	JSONObject saveOneUsersService(AUsers users);

	/**
	 * 更新一个用户
	 * 
	 * @param users
	 * @return
	 */
	JSONObject updateOneUsersService(AUsers users);

	/**
	 * 删除一个用户
	 * 
	 * @param users
	 * @return
	 */
	JSONObject deleteOneUsersService(AUsers users);

	/**
	 * 查询一个记录
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	AUsers findOneUsersService(Map<String, Object> condMap);

	/**
	 * 查询多条记录
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	List<AUsers> findCondListUsersService(PageInfoUtil pageInfoUtil, Map<String, Object> condMap);
	/*----------用户结束-----------*/

	/*----------用户账户开始-----------*/

	/**
	 * 添加一个用户账户
	 * 
	 * @param usersAccount
	 * @return
	 */
	JSONObject saveOneUsersAccountService(AUsersAccount usersAccount);

	/**
	 * 更新一个用户账户
	 * 
	 * @param usersAccount
	 * @return
	 */
	JSONObject updateOneUsersAccountService(AUsersAccount usersAccount);
	
	/**
	 * 批量更新用户的账户信息
	 * @param type
	 * @param usersAccountList
	 * @return
	 */
	JSONObject updateBatchUsersAccountService(String type , List<AUsersAccount> usersAccountList);
	
	/**
	 * 更新一个用户账户
	 * 
	 * @param usersAccount
	 * @return
	 */
	JSONObject updateBatchUsersAccountService(String type,AUsersAccount usersAccount);

	/**
	 * 删除一个用户账户
	 * 
	 * @param usersAccount
	 * @return
	 */
	JSONObject deleteOneUsersAccountService(AUsersAccount usersAccount);

	/**
	 * 查询一个用户账户
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	AUsersAccount findOneUsersAccountService(Map<String, Object> condMap);

	/**
	 * 查询多条用户账户
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	List<AUsersAccount> findCondListUsersAccountService(PageInfoUtil pageInfoUtil, Map<String, Object> condMap);

	/**
	 * 用户账户统计
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	List<Map<String, Object>> findUsersAccountStatListService(PageInfoUtil pageinfoUtil,Map<String, Object> condMap);
	/*----------用户账户结束-----------*/
}
