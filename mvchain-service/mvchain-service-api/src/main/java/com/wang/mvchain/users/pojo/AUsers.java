package com.wang.mvchain.users.pojo;

import java.util.Date;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.pojo.BasePojo;
import com.wang.mvchain.common.util.DateUtil;

/**
 * 用户的POJO类
 * @author wangsh
 *
 */
public class AUsers extends BasePojo<AUsers>
{
	private int id;
	private int ssoid;
	private String email ; 
	private int maxtask ; 
	private byte prostatus ;
	private double percent ; 
	private byte status ; 
	private Date createtime;
	private Date updatetime;
	
	//字符串表示
	private String ticketStr ;
	
	private String prostatusStr ; 
	private String statusStr ; 

	public int getId()
	{
		return id;
	}

	public int getSsoid()
	{
		return ssoid;
	}

	public void setSsoid(int ssoid)
	{
		this.ssoid = ssoid;
	}

	public Date getCreatetime()
	{
		return createtime;
	}

	public void setCreatetime(Date createtime)
	{
		this.createtime = createtime;
	}

	public Date getUpdatetime()
	{
		return updatetime;
	}

	public void setUpdatetime(Date updatetime)
	{
		this.updatetime = updatetime;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getTicketStr()
	{
		return ticketStr;
	}

	public void setTicketStr(String ticketStr)
	{
		this.ticketStr = ticketStr;
	}

	public byte getProstatus()
	{
		return prostatus;
	}

	public void setProstatus(byte prostatus)
	{
		this.prostatus = prostatus;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getProstatusStr()
	{
		/**
		 * 搬砖程序运行状态:0:关闭,1:开启
		 */
		// 根据状态值获取字符串描述
		AUsersEnum[] usersEnums = AUsersEnum.values();
		for (int i = 0; i < usersEnums.length; i++)
		{
			AUsersEnum usersEnum = usersEnums[i];
			if (usersEnum.toString().startsWith("PROSTATUS_"))
			{
				// 表示是状态的标识
				if (usersEnum.getStatus() == this.getProstatus())
				{
					this.prostatusStr = usersEnum.getName();
					break;
				}
			}
		}
		return prostatusStr;
	}

	public String getStatusStr()
	{
		// 根据状态值获取字符串描述
		AUsersEnum[] usersEnums = AUsersEnum.values();
		for (int i = 0; i < usersEnums.length; i++)
		{
			AUsersEnum usersEnum = usersEnums[i];
			if (usersEnum.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (usersEnum.getStatus() == this.getStatus())
				{
					this.statusStr = usersEnum.getName();
					break;
				}
			}
		}
		return statusStr;
	}

	public int getMaxtask()
	{
		return maxtask;
	}

	public void setMaxtask(int maxtask)
	{
		this.maxtask = maxtask;
	}

	@Override
	public JSONObject toJSON()
	{
		DateUtil dateFormatUtil = new DateUtil();
		JSONObject resultJSON = new JSONObject();
		resultJSON.put("id", this.getId() + "");
		resultJSON.put("ssoid", this.getSsoid() + "");
		resultJSON.put("email", this.getEmail() + "");
		resultJSON.put("maxtask", this.getMaxtask() + "");
		resultJSON.put("prostatus", this.getProstatus() + "");
		resultJSON.put("prostatusStr", this.getProstatusStr() + "");
		resultJSON.put("status", this.getStatus() + "");
		resultJSON.put("statusStr", this.getStatusStr() + "");
		resultJSON.put("createtime", dateFormatUtil.formatDateTime(this.getCreatetime()) + "");
		resultJSON.put("updatetime", dateFormatUtil.formatDateTime(this.getUpdatetime()) + "");
		return resultJSON;
	}

	@Override
	protected AUsers parseJSON(JSONObject souJSON)
	{
		DateUtil dateFormatUtil = new DateUtil();
		if(souJSON.get("id") != null)
		{
			this.setId(Integer.valueOf(souJSON.get("id") + ""));
		}
		if(souJSON.get("ssoid") != null)
		{
			this.setSsoid(Integer.valueOf(souJSON.get("ssoid") + ""));
		}
		if(souJSON.get("email") != null)
		{
			this.setEmail(souJSON.get("email") + "");
		}
		if(souJSON.get("maxtask") != null)
		{
			this.setMaxtask(Integer.valueOf(souJSON.get("maxtask") + ""));
		}
		if(souJSON.get("prostatus") != null)
		{
			this.setStatus(Byte.valueOf(souJSON.get("prostatus") + ""));
		}
		if(souJSON.get("status") != null)
		{
			this.setStatus(Byte.valueOf(souJSON.get("status") + ""));
		}
		if(souJSON.get("createtime") != null)
		{
			this.setCreatetime(dateFormatUtil.parseDateTime(souJSON.get("createtime") + ""));
		}
		if(souJSON.get("updatetime") != null)
		{
			this.setUpdatetime(dateFormatUtil.parseDateTime(souJSON.get("updatetime") + ""));
		}
		return this;
	}

	public double getPercent()
	{
		return percent;
	}

	public void setPercent(double percent)
	{
		this.percent = percent;
	}

}