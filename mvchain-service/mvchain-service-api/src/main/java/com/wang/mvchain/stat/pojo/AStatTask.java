package com.wang.mvchain.stat.pojo;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.pojo.BasePojo;
import com.wang.mvchain.system.pojo.ACoinrate;
import com.wang.mvchain.users.pojo.AUsers;
import com.wang.mvchain.users.pojo.AUsersEnum;

public class AStatTask extends BasePojo
{
	private int id;
	private int usersid;
	private Date currdate;
	private String balance ; 
	private int priftcoinid;
	private double prift;
	private double fee ; 
	private int taskcount;
	private double sellpricemin;
	private double sellpricemax;
	private double buypricemin;
	private double buypricemax;
	private byte flag ; 
	private byte status ; 
	private Date createtime;
	private Date updatetime;
	
	/* 字符串描述 */
	private String flagStr ; 
	private String statusStr ; 
	
	/* 枚举的集合表示 */
	private Map<String, String> statusMap = new HashMap<String, String>();
	private Map<String, String> flagMap = new HashMap<String, String>();
	
	//对象表示
	private AUsers usersObj;
	private ACoinrate priftcoinObj;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getUsersid()
	{
		return usersid;
	}

	public void setUsersid(int usersid)
	{
		this.usersid = usersid;
	}

	public Date getCurrdate()
	{
		return currdate;
	}

	public void setCurrdate(Date currdate)
	{
		this.currdate = currdate;
	}

	public int getPriftcoinid()
	{
		return priftcoinid;
	}

	public void setPriftcoinid(int priftcoinid)
	{
		this.priftcoinid = priftcoinid;
	}

	public double getPrift()
	{
		return prift;
	}

	public void setPrift(double prift)
	{
		this.prift = prift;
	}

	public int getTaskcount()
	{
		return taskcount;
	}

	public void setTaskcount(int taskcount)
	{
		this.taskcount = taskcount;
	}

	public double getSellpricemin()
	{
		return sellpricemin;
	}

	public void setSellpricemin(double sellpricemin)
	{
		this.sellpricemin = sellpricemin;
	}

	public double getSellpricemax()
	{
		return sellpricemax;
	}

	public void setSellpricemax(double sellpricemax)
	{
		this.sellpricemax = sellpricemax;
	}

	public double getBuypricemin()
	{
		return buypricemin;
	}

	public void setBuypricemin(double buypricemin)
	{
		this.buypricemin = buypricemin;
	}

	public double getBuypricemax()
	{
		return buypricemax;
	}

	public void setBuypricemax(double buypricemax)
	{
		this.buypricemax = buypricemax;
	}

	public Date getCreatetime()
	{
		return createtime;
	}

	public void setCreatetime(Date createtime)
	{
		this.createtime = createtime;
	}

	public Date getUpdatetime()
	{
		return updatetime;
	}

	public void setUpdatetime(Date updatetime)
	{
		this.updatetime = updatetime;
	}

	public AUsers getUsersObj()
	{
		return usersObj;
	}

	public void setUsersObj(AUsers usersObj)
	{
		this.usersObj = usersObj;
	}

	public ACoinrate getPriftcoinObj()
	{
		return priftcoinObj;
	}

	public void setPriftcoinObj(ACoinrate priftcoinObj)
	{
		this.priftcoinObj = priftcoinObj;
	}

	public double getFee()
	{
		return fee;
	}

	public void setFee(double fee)
	{
		this.fee = fee;
	}

	public String getBalance()
	{
		return balance;
	}

	public void setBalance(String balance)
	{
		this.balance = balance;
	}
	
	/**
	 * 余额的json对象
	 * @return
	 */
	public JSONObject getBalanceJSON()
	{
		try
		{
			JSONObject resultJSON = (JSONObject) JSONObject.parse(this.balance);
			if(resultJSON == null)
			{
				resultJSON = new JSONObject();
			}
			return resultJSON ; 
		} catch (Exception e)
		{
		}
		return new JSONObject() ;
	}

	@Override
	public JSONObject toJSON()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Object parseJSON(JSONObject souJSON)
	{
		// TODO Auto-generated method stub
		return null;
	}

	public byte getFlag()
	{
		return flag;
	}

	public void setFlag(byte flag)
	{
		this.flag = flag;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	/**
	 * 获取标识
	 * @return
	 */
	public String getFlagStr()
	{
		// 根据状态值获取字符串描述
		AStatTaskEnum[] statTaskEnums = AStatTaskEnum.values();
		for (int i = 0; i < statTaskEnums.length; i++)
		{
			AStatTaskEnum statTaskEnum = statTaskEnums[i];
			if (statTaskEnum.toString().startsWith("FLAG_"))
			{
				// 表示是状态的标识
				if (statTaskEnum.getStatus() == this.getFlag())
				{
					this.flagStr = statTaskEnum.getName();
					break;
				}
			}
		}
		return flagStr;
	}

	/**
	 * 获取状态
	 * @return
	 */
	public String getStatusStr()
	{
		// 根据状态值获取字符串描述
		AStatTaskEnum[] statTaskEnums = AStatTaskEnum.values();
		for (int i = 0; i < statTaskEnums.length; i++)
		{
			AStatTaskEnum statTaskEnum = statTaskEnums[i];
			if (statTaskEnum.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (statTaskEnum.getStatus() == this.getStatus())
				{
					this.statusStr = statTaskEnum.getName();
					break;
				}
			}
		}
		return statusStr;
	}

	/**
	 * 获取状态的map
	 * @return
	 */
	public Map<String, String> getStatusMap()
	{
		// 根据状态值获取字符串描述
		AStatTaskEnum[] statTaskEnums = AStatTaskEnum.values();
		for (int i = 0; i < statTaskEnums.length; i++)
		{
			AStatTaskEnum statTaskEnum = statTaskEnums[i];
			if (statTaskEnum.toString().startsWith("STATUS_"))
			{
				statusMap.put(statTaskEnum.getName(), statTaskEnum.getStatus() + "");
			}
		}
		return statusMap;
	}

	public Map<String, String> getFlagMap()
	{
		// 根据状态值获取字符串描述
		AStatTaskEnum[] statTaskEnums = AStatTaskEnum.values();
		for (int i = 0; i < statTaskEnums.length; i++)
		{
			AStatTaskEnum statTaskEnum = statTaskEnums[i];
			if (statTaskEnum.toString().startsWith("FLAG_"))
			{
				flagMap.put(statTaskEnum.getName(), statTaskEnum.getStatus() + "");
			}
		}
		return flagMap;
	}
}
