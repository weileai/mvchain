package com.wang.mvchain.users.pojo;

import java.util.Date;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.pojo.BasePojo;

/**
 * 管理员的POJO类
 * @author wangsh
 *
 */
public class AAdmins extends BasePojo<AAdmins>
{
	private int id;
	private int ssoid;
	private String email ;
	private byte status ; 
	private Date createtime;
	private Date updatetime;
	
	//字符串表示
	/*单点登陆的令牌*/
	private String ticketStr ;
	/*状态*/
	private String statusStr ;  

	public int getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public int getSsoid()
	{
		return ssoid;
	}

	public void setSsoid(int ssoid)
	{
		this.ssoid = ssoid;
	}

	public Date getCreatetime()
	{
		return createtime;
	}

	public void setCreatetime(Date createtime)
	{
		this.createtime = createtime;
	}

	public Date getUpdatetime()
	{
		return updatetime;
	}

	public void setUpdatetime(Date updatetime)
	{
		this.updatetime = updatetime;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getTicketStr()
	{
		return ticketStr;
	}

	public void setTicketStr(String ticketStr)
	{
		this.ticketStr = ticketStr;
	}

	public String getStatusStr()
	{
		// 根据状态值获取字符串描述
		AAdminsEnum[] adminsEnums = AAdminsEnum.values();
		for (int i = 0; i < adminsEnums.length; i++)
		{
			AAdminsEnum adminsEnum = adminsEnums[i];
			if (adminsEnum.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (adminsEnum.getStatus() == this.getStatus())
				{
					this.statusStr = adminsEnum.getName();
					break;
				}
			}
		}
		return statusStr;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	@Override
	public JSONObject toJSON()
	{
		return null;
	}

	@Override
	protected AAdmins parseJSON(JSONObject souJSON)
	{
		return null;
	}

	@Override
	public String toString()
	{
		return "AAdmins [id=" + id + ", ssoid=" + ssoid + ", email=" + email + ", status=" + status + ", createtime="
				+ createtime + ", updatetime=" + updatetime + ", ticketStr=" + ticketStr + ", statusStr=" + statusStr
				+ "]";
	}
}