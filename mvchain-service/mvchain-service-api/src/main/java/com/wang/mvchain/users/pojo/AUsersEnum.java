package com.wang.mvchain.users.pojo;

/**
 * 管理员的枚举
 * 
 * @author wangsh
 *
 */
public enum AUsersEnum
{
	STATUS_ENABLE(Byte.valueOf("1"), "启用"), 
	STATUS_DISABLE(Byte.valueOf("0"), "禁用"),
	
	PROSTATUS_OPEN(Byte.valueOf("1"), "开启"),
	PROSTATUS_CLOSED(Byte.valueOf("0"), "关闭");

	private byte status;
	private String name;

	private AUsersEnum(byte status, String name)
	{
		this.status = status;
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public String toString()
	{
		return super.toString();
	}
}
