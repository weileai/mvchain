package com.wang.mvchain.users.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.pojo.BasePojo;
import com.wang.mvchain.system.pojo.ATradeType;

/**
 * 用户的POJO类
 * 
 * @author wangsh
 *
 */
public class ATradeHistory extends BasePojo<ATradeHistory>
{
	/*
	 * 数据库中存储的字段
	 */
	private int id;
	private int uaid;
	private int ctid;
	private String ctName;
	private String ordersn;
	private double defprice;
	private double tradePrice;
	private double tradeedval;
	private double totalval;
	private double totalprice;
	private byte ordertype;
	private byte status;
	private Date createtime;
	private Date updatetime;
	private Date pubtime;

	/* 字符串表示 */
	private String ordertypeStr;
	private String statusStr;
	/* 对象的Map */
	private Map<String, String> ordertypeMap = new TreeMap<String, String>();
	private Map<String, String> statusMap = new TreeMap<String, String>();

	/* 对象表示 */
	private AUsersAccount usersAccountObj;
	private ATradeType tradeTypeObj;

	@Override
	public JSONObject toJSON()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ATradeHistory parseJSON(JSONObject souJSON)
	{
		// TODO Auto-generated method stub
		return null;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getUaid()
	{
		return uaid;
	}

	public void setUaid(int uaid)
	{
		this.uaid = uaid;
	}

	public int getCtid()
	{
		return ctid;
	}

	public void setCtid(int ctid)
	{
		this.ctid = ctid;
	}

	public String getCtName()
	{
		return ctName;
	}

	public void setCtName(String ctName)
	{
		this.ctName = ctName;
	}

	public String getOrdersn()
	{
		return ordersn;
	}

	public void setOrdersn(String ordersn)
	{
		this.ordersn = ordersn;
	}

	public double getDefprice()
	{
		return defprice;
	}

	public void setDefprice(double defprice)
	{
		this.defprice = defprice;
	}

	public double getTradePrice()
	{
		return tradePrice;
	}

	public void setTradePrice(double tradePrice)
	{
		this.tradePrice = tradePrice;
	}

	public double getTradeedval()
	{
		return tradeedval;
	}

	public void setTradeedval(double tradeedval)
	{
		this.tradeedval = tradeedval;
	}

	public double getTotalval()
	{
		return totalval;
	}

	public void setTotalval(double totalval)
	{
		this.totalval = totalval;
	}

	public double getTotalprice()
	{
		return totalprice;
	}

	public void setTotalprice(double totalprice)
	{
		this.totalprice = totalprice;
	}

	public byte getOrdertype()
	{
		return ordertype;
	}

	public void setOrdertype(byte ordertype)
	{
		this.ordertype = ordertype;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreatetime()
	{
		return createtime;
	}

	public void setCreatetime(Date createtime)
	{
		this.createtime = createtime;
	}

	public Date getUpdatetime()
	{
		return updatetime;
	}

	public void setUpdatetime(Date updatetime)
	{
		this.updatetime = updatetime;
	}

	public Date getPubtime()
	{
		return pubtime;
	}

	public void setPubtime(Date pubtime)
	{
		this.pubtime = pubtime;
	}

	public AUsersAccount getUsersAccountObj()
	{
		return usersAccountObj;
	}

	public void setUsersAccountObj(AUsersAccount usersAccountObj)
	{
		this.usersAccountObj = usersAccountObj;
	}

	public ATradeType getTradeTypeObj()
	{
		return tradeTypeObj;
	}

	public void setTradeTypeObj(ATradeType tradeTypeObj)
	{
		this.tradeTypeObj = tradeTypeObj;
	}

	public String getOrdertypeStr()
	{
		/*
		 * 类型:0:买,1:卖
		 */
		ATradeHistoryEnum[] usersTaskOrderEnums = ATradeHistoryEnum.values();
		for (int i = 0; i < usersTaskOrderEnums.length; i++)
		{
			ATradeHistoryEnum usersTaskOrderEnum = usersTaskOrderEnums[i];
			if (usersTaskOrderEnum.toString().startsWith("ORDERTYPE_"))
			{
				// 表示是状态的标识
				if (usersTaskOrderEnum.getStatus() == this.getOrdertype())
				{
					this.ordertypeStr = usersTaskOrderEnum.getName();
					break;
				}
			}
		}
		return ordertypeStr;
	}

	public String getStatusStr()
	{
		/*
		 * 状态:0:未开始,1:撤单;2,挂单;3:完成
		 */
		ATradeHistoryEnum[] usersTaskOrderEnums = ATradeHistoryEnum.values();
		for (int i = 0; i < usersTaskOrderEnums.length; i++)
		{
			ATradeHistoryEnum usersTaskOrderEnum = usersTaskOrderEnums[i];
			if (usersTaskOrderEnum.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (usersTaskOrderEnum.getStatus() == this.getStatus())
				{
					this.statusStr = usersTaskOrderEnum.getName();
					break;
				}
			}
		}
		return statusStr;
	}

	public Map<String, String> getOrdertypeMap()
	{
		/*
		 * 类型:0:买,1:卖
		 */
		ATradeHistoryEnum[] tradeHistoryEnums = ATradeHistoryEnum.values();
		for (int i = 0; i < tradeHistoryEnums.length; i++)
		{
			ATradeHistoryEnum tradeHistoryEnum = tradeHistoryEnums[i];
			if (tradeHistoryEnum.toString().startsWith("ORDERTYPE_"))
			{
				this.ordertypeMap.put(tradeHistoryEnum.getStatus() + "",tradeHistoryEnum.getName());
			}
		}
		return ordertypeMap;
	}

	public Map<String, String> getStatusMap()
	{
		/*
		 * 状态:0:未开始,1:撤单;2,挂单;3:完成
		 */
		ATradeHistoryEnum[] tradeHistoryEnums = ATradeHistoryEnum.values();
		for (int i = 0; i < tradeHistoryEnums.length; i++)
		{
			ATradeHistoryEnum tradeHistoryEnum = tradeHistoryEnums[i];
			if (tradeHistoryEnum.toString().startsWith("STATUS_"))
			{
				this.statusMap.put(tradeHistoryEnum.getStatus() + "",tradeHistoryEnum.getName());
			}
		}
		return statusMap;
	}

}