package com.wang.mvchain.users.pojo;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Date;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.pojo.BasePojo;
import com.wang.mvchain.common.util.DateUtil;
import com.wang.mvchain.system.pojo.ACoinrate;
import com.wang.mvchain.system.pojo.AWebsite;

public class AUsersAccount extends BasePojo<AUsersAccount>
{
	private int id;
	private int usersid;
	private int coinid;
	private int wbid;
	private String username;
	private double balance;
	private double fundmoney;
	private double trademinnum ; 
	private double trademaxnum ; 
	private double prift;
	private String authinfo;
	private byte status;
	private Date createtime;
	private Date updatetime;
	private Date pubtime;

	// 字符串表示
	private String statusStr;

	// 对象表示
	private AUsers usersObj;
	private ACoinrate coinrateObj;
	private AWebsite websiteObj;

	public int getId()
	{
		return id;
	}

	public Date getCreatetime()
	{
		return createtime;
	}

	public void setCreatetime(Date createtime)
	{
		this.createtime = createtime;
	}

	public Date getUpdatetime()
	{
		return updatetime;
	}

	public void setUpdatetime(Date updatetime)
	{
		this.updatetime = updatetime;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public int getUsersid()
	{
		return usersid;
	}

	public void setUsersid(int usersid)
	{
		this.usersid = usersid;
	}

	public int getCoinid()
	{
		return coinid;
	}

	public void setCoinid(int coinid)
	{
		this.coinid = coinid;
	}

	public int getWbid()
	{
		return wbid;
	}

	public void setWbid(int wbid)
	{
		this.wbid = wbid;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public double getBalance()
	{
		return balance;
	}

	public void setBalance(double balance)
	{
		this.balance = balance;
	}

	public double getFundmoney()
	{
		return fundmoney;
	}

	public void setFundmoney(double fundmoney)
	{
		this.fundmoney = fundmoney;
	}

	public String getAuthinfo()
	{
		return authinfo;
	}

	public void setAuthinfo(String authinfo)
	{
		this.authinfo = authinfo;
	}

	public AUsers getUsersObj()
	{
		return usersObj;
	}

	public void setUsersObj(AUsers usersObj)
	{
		this.usersObj = usersObj;
	}

	public ACoinrate getCoinrateObj()
	{
		return coinrateObj;
	}

	public void setCoinrateObj(ACoinrate coinrateObj)
	{
		this.coinrateObj = coinrateObj;
	}

	public AWebsite getWebsiteObj()
	{
		return websiteObj;
	}

	public void setWebsiteObj(AWebsite websiteObj)
	{
		this.websiteObj = websiteObj;
	}

	public String getStatusStr()
	{
		/**
		 * 状态:0:禁用,1:启用
		 */
		// 根据状态值获取字符串描述
		AUsersAccountEnum[] usersAccountEnums = AUsersAccountEnum.values();
		for (int i = 0; i < usersAccountEnums.length; i++)
		{
			AUsersAccountEnum usersAccountEnum = usersAccountEnums[i];
			if (usersAccountEnum.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (usersAccountEnum.getStatus() == this.getStatus())
				{
					this.statusStr = usersAccountEnum.getName();
					break;
				}
			}
		}
		return statusStr;
	}

	public Date getPubtime()
	{
		return pubtime;
	}

	public void setPubtime(Date pubtime)
	{
		this.pubtime = pubtime;
	}

	public JSONObject getAuthinfoJSON()
	{
		return (JSONObject) JSON.parse(this.getAuthinfo()); 
	}

	public double getPrift()
	{
		return prift;
	}

	public void setPrift(double prift)
	{
		this.prift = prift;
	}

	public double getTrademaxnum()
	{
		return trademaxnum;
	}

	public void setTrademaxnum(double trademaxnum)
	{
		this.trademaxnum = trademaxnum;
	}

	@Override
	public JSONObject toJSON()
	{
		DateUtil dateFormatUtil = new DateUtil();
		JSONObject resultJSON = new JSONObject();
		resultJSON.put("id", this.getId() + "");
		resultJSON.put("coinid", this.getCoinid() + "");
		resultJSON.put("wbid", this.getWbid() + "");
		resultJSON.put("username", this.getUsername() + "");
		resultJSON.put("balance", this.getBalance() + "");
		resultJSON.put("fundmoney", this.getFundmoney() + "");
		resultJSON.put("trademinnum", this.getTrademinnum() + "");
		resultJSON.put("trademaxnum", this.getTrademaxnum() + "");
		resultJSON.put("status", this.getStatus() + "");
		resultJSON.put("statusStr", this.getStatusStr() + "");
		resultJSON.put("prift", this.getPrift() + "");
		resultJSON.put("authinfo", this.getAuthinfo());
		resultJSON.put("createtime", dateFormatUtil.formatDateTime(this.getCreatetime()) + "");
		resultJSON.put("updatetime", dateFormatUtil.formatDateTime(this.getUpdatetime()) + "");
		
		/* 存储扩展字段 */
		if(usersObj != null)
		{
			resultJSON.put("usersObj", usersObj.toJSON());
		}
		if(coinrateObj != null)
		{
			resultJSON.put("coinrateObj", coinrateObj.toJSON());
		}
		if(websiteObj != null)
		{
			resultJSON.put("websiteObj", websiteObj.toJSON());
		}
		return resultJSON;
	}

	@Override
	public AUsersAccount parseJSON(JSONObject souJSON)
	{
		DateUtil dateFormatUtil = new DateUtil();
		if(souJSON.get("id") != null)
		{
			this.setId(Integer.valueOf(souJSON.get("id") + ""));
		}
		if(souJSON.get("coinid") != null)
		{
			this.setCoinid(Integer.valueOf(souJSON.get("coinid") + ""));
		}
		if(souJSON.get("wbid") != null)
		{
			this.setWbid(Integer.valueOf(souJSON.get("wbid") + ""));
		}
		if(souJSON.get("username") != null)
		{
			this.setUsername(souJSON.get("username") + "");
		}
		if(souJSON.get("status") != null)
		{
			this.setStatus(Byte.valueOf(souJSON.get("status") + ""));
		}
		if(souJSON.get("balance") != null)
		{
			this.setBalance(Double.valueOf(souJSON.get("balance") + ""));
		}
		if(souJSON.get("fundmoney") != null)
		{
			this.setFundmoney(Double.valueOf(souJSON.get("fundmoney") + ""));;
		}
		if(souJSON.get("trademinnum") != null)
		{
			this.setTrademinnum(Double.valueOf(souJSON.get("trademinnum") + ""));
		}
		if(souJSON.get("trademaxnum") != null)
		{
			this.setTrademaxnum(Double.valueOf(souJSON.get("trademaxnum") + ""));
		}
		if(souJSON.get("prift") != null)
		{
			this.setPrift(Double.valueOf(souJSON.get("prift") + ""));
		}
		if(souJSON.get("authinfo") != null)
		{
			this.setAuthinfo(souJSON.get("authinfo") + "");
		}
		if(souJSON.get("createtime") != null)
		{
			this.setCreatetime(dateFormatUtil.parseDateTime(souJSON.get("createtime") + ""));
		}
		if(souJSON.get("updatetime") != null)
		{
			this.setUpdatetime(dateFormatUtil.parseDateTime(souJSON.get("updatetime") + ""));
		}
		
		if(souJSON.get("usersObj") != null)
		{
			AUsers users = new AUsers();
			users = users.parseJSON((JSONObject) souJSON.get("usersObj"));
			this.setUsersObj(users);
		}
		
		if(souJSON.get("coinrateObj") != null)
		{
			this.setUpdatetime(dateFormatUtil.parseDateTime(souJSON.get("coinrateObj") + ""));
			ACoinrate coinrate = new ACoinrate();
			coinrate = coinrate.parseJSON((JSONObject) souJSON.get("coinrateObj"));
			this.setCoinrateObj(coinrate);
		}
		
		if(souJSON.get("websiteObj") != null)
		{
			AWebsite website = new AWebsite();
			website = website.parseJSON((JSONObject) souJSON.get("websiteObj"));
			this.setWebsiteObj(website);
		}
		
		return this;
	}

	public double getTrademinnum()
	{
		return trademinnum;
	}

	public void setTrademinnum(double trademinnum)
	{
		this.trademinnum = trademinnum;
	}
}