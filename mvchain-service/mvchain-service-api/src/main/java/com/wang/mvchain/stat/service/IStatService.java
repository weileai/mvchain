package com.wang.mvchain.stat.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.PageInfoUtil;
import com.wang.mvchain.stat.pojo.AStatTask;

/**
 * 统计的服务接口
 * @author wangsh
 *
 */
public interface IStatService
{
	/*----------用户任务统计开始-----------*/
	/**
	 * 添加一个用户任务统计
	 * 
	 * @param statTask
	 * @return
	 */
	JSONObject insertOneStatTaskService(AStatTask statTask);

	/**
	 * 更新一个用户任务统计
	 * 
	 * @param statTask
	 * @return
	 */
	JSONObject updateOneStatTaskService(AStatTask statTask);

	/**
	 * 删除一个用户任务统计
	 * 
	 * @param statTask
	 * @return
	 */
	JSONObject deleteOneStatTaskService(AStatTask statTask);
	
	/**
	 * 批量一个用户任务统计
	 * 
	 * @param statTask
	 * @return
	 */
	JSONObject deleteBatchStatTaskService(Map<String, Object> map);

	/**
	 * 查询一个记录
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	AStatTask findOneStatTaskService(Map<String, Object> condMap);

	/**
	 * 查询多条记录
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	List<AStatTask> findCondListStatTaskService(PageInfoUtil pageInfo,Map<String, Object> condMap);
	/*----------用户任务统计结束-----------*/
	
	/*----------定时任务统计开始-----------*/
	/**
	 * 计算当天日期的任务统计情况
	 * @param stDate
	 */
	AStatTask updateStatTaskService(Date currDate);
	/*----------定时任务统计结束-----------*/
}
