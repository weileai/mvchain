package com.wang.mvchain.finan.pojo;

import java.util.Date;

import com.wang.mvchain.system.pojo.AWebsite;
import com.wang.mvchain.users.pojo.AUsersTask;

/**
 * 资产账户历史列表
 * 
 * @author wangshMac
 */
public class AFinanHistory
{
	private int id;
	private int assetId;
	private int wbId;
	private int accountId;
	private int taskId;
	private String usersName;
	private double price ;
	private double num ; 
	private double money;
	private double percent;
	private String content;
	private byte tradeType;
	private byte historyType;
	private byte status;
	private Date createtime;
	private Date updatetime;
	private Date pubtime;

	/* 字符串描述 */
	private String statusStr;
	private String tradeTypeStr;
	private String historyTypeStr;
	
	/* 对象描述 */
	private AFinanAsset finanAsset ; 
	private AWebsite website ; 
	private AFinanAccount finanAccount;
	private AUsersTask usersTask ; 
	
	public String getStatusStr()
	{
		// 根据状态值获取字符串描述
		AFinanHistoryEnum[] finanHistoryEnums = AFinanHistoryEnum.values();
		for (int i = 0; i < finanHistoryEnums.length; i++)
		{
			AFinanHistoryEnum finanAccountEnum = finanHistoryEnums[i];
			if (finanAccountEnum.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (finanAccountEnum.getStatus() == this.getStatus())
				{
					this.statusStr = finanAccountEnum.getName();
					break;
				}
			}
		}
		return statusStr;
	}
	
	public String getTradeTypeStr()
	{
		// 根据状态值获取字符串描述
		AFinanHistoryEnum[] finanHistoryEnums = AFinanHistoryEnum.values();
		for (int i = 0; i < finanHistoryEnums.length; i++)
		{
			AFinanHistoryEnum finanAccountEnum = finanHistoryEnums[i];
			if (finanAccountEnum.toString().startsWith("TRADETYPE_"))
			{
				// 表示是状态的标识
				if (finanAccountEnum.getStatus() == this.tradeType)
				{
					this.tradeTypeStr = finanAccountEnum.getName();
					break;
				}
			}
		}
		return tradeTypeStr;
	}
	
	public String getHistoryTypeStr()
	{
		// 根据状态值获取字符串描述
		AFinanHistoryEnum[] finanHistoryEnums = AFinanHistoryEnum.values();
		for (int i = 0; i < finanHistoryEnums.length; i++)
		{
			AFinanHistoryEnum finanAccountEnum = finanHistoryEnums[i];
			if (finanAccountEnum.toString().startsWith("HISTORYTYPE_"))
			{
				// 表示是状态的标识
				if (finanAccountEnum.getStatus() == this.historyType)
				{
					this.historyTypeStr = finanAccountEnum.getName();
					break;
				}
			}
		}
		return historyTypeStr;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getWbId()
	{
		return wbId;
	}

	public void setWbId(int wbId)
	{
		this.wbId = wbId;
	}

	public int getTaskId()
	{
		return taskId;
	}

	public void setTaskId(int taskId)
	{
		this.taskId = taskId;
	}

	public String getUsersName()
	{
		return usersName;
	}

	public void setUsersName(String usersName)
	{
		this.usersName = usersName;
	}

	public double getMoney()
	{
		return money;
	}

	public void setMoney(double money)
	{
		this.money = money;
	}

	public double getPercent()
	{
		return percent;
	}

	public void setPercent(double percent)
	{
		this.percent = percent;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public byte getTradeType()
	{
		return tradeType;
	}

	public void setTradeType(byte tradeType)
	{
		this.tradeType = tradeType;
	}

	public byte getHistoryType()
	{
		return historyType;
	}

	public void setHistoryType(byte historyType)
	{
		this.historyType = historyType;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreatetime()
	{
		return createtime;
	}

	public void setCreatetime(Date createtime)
	{
		this.createtime = createtime;
	}

	public Date getUpdatetime()
	{
		return updatetime;
	}

	public void setUpdatetime(Date updatetime)
	{
		this.updatetime = updatetime;
	}

	public Date getPubtime()
	{
		return pubtime;
	}

	public void setPubtime(Date pubtime)
	{
		this.pubtime = pubtime;
	}

	public AFinanAsset getFinanAsset()
	{
		return finanAsset;
	}

	public void setFinanAsset(AFinanAsset finanAsset)
	{
		this.finanAsset = finanAsset;
	}

	public AWebsite getWebsite()
	{
		return website;
	}

	public void setWebsite(AWebsite website)
	{
		this.website = website;
	}

	public AFinanAccount getFinanAccount()
	{
		return finanAccount;
	}

	public void setFinanAccount(AFinanAccount finanAccount)
	{
		this.finanAccount = finanAccount;
	}

	public AUsersTask getUsersTask()
	{
		return usersTask;
	}

	public void setUsersTask(AUsersTask usersTask)
	{
		this.usersTask = usersTask;
	}

	public double getPrice()
	{
		return price;
	}

	public void setPrice(double price)
	{
		this.price = price;
	}

	public double getNum()
	{
		return num;
	}

	public void setNum(double num)
	{
		this.num = num;
	}

	public int getAssetId()
	{
		return assetId;
	}

	public void setAssetId(int assetId)
	{
		this.assetId = assetId;
	}

	public int getAccountId()
	{
		return accountId;
	}

	public void setAccountId(int accountId)
	{
		this.accountId = accountId;
	}
}
