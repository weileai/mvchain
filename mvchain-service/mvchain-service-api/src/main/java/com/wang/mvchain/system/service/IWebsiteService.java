package com.wang.mvchain.system.service;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.PageInfoUtil;
import com.wang.mvchain.system.pojo.ACoinrate;
import com.wang.mvchain.system.pojo.ADept;
import com.wang.mvchain.system.pojo.ATradeType;
import com.wang.mvchain.system.pojo.AWebsite;

public interface IWebsiteService {

	/*-----网站详情开始-----*/
	/**
	 * 添加一个网站详情
	 * 
	 * @param website
	 * @return
	 */
	JSONObject saveOneWebsiteService(AWebsite website);

	/**
	 * 更新一个网站详情
	 * 
	 * @param website
	 * @return
	 */
	JSONObject updateOneWebsiteService(AWebsite website);

	/**
	 * 删除一个网站详情
	 * 
	 * @param website
	 * @return
	 */
	JSONObject deleteOneWebsiteService(AWebsite website);

	/**
	 * 查询一个网站详情
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	AWebsite findOneWebsiteService(Map<String, Object> condMap);

	/**
	 * 查询多条网站详情
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	List<AWebsite> findCondListWebsiteService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);
	/*-----网站详情结束-----*/
	
	/*-----币种开始-----*/
	/**
	 * 添加一个币种
	 * 
	 * @param coinrate
	 * @return
	 */
	JSONObject saveOneCoinrateService(ACoinrate coinrate);

	/**
	 * 更新一个币种
	 * 
	 * @param coinrate
	 * @return
	 */
	JSONObject updateOneCoinrateService(ACoinrate coinrate);

	/**
	 * 删除一个币种
	 * 
	 * @param coinrate
	 * @return
	 */
	JSONObject deleteOneCoinrateService(ACoinrate coinrate);

	/**
	 * 查询一个币种
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	ACoinrate findOneCoinrateService(Map<String, Object> condMap);

	/**
	 * 查询多条币种
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	List<ACoinrate> findCondListCoinrateService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);
	/*-----币种结束-----*/
	
	/*-----交易类型开始-----*/
	/**
	 * 添加一个交易类型
	 * 
	 * @param tradeType
	 * @return
	 */
	JSONObject saveOneTradeTypeService(ATradeType tradeType);

	/**
	 * 更新一个交易类型
	 * 
	 * @param tradeType
	 * @return
	 */
	JSONObject updateOneTradeTypeService(ATradeType tradeType);
	
	/**
	 * 批量更新一些信息
	 * @param type
	 * @param tradeType
	 * @return
	 */
	JSONObject updateBatchTradeTypeService(String type,ATradeType tradeType);

	/**
	 * 删除一个交易类型
	 * 
	 * @param tradeType
	 * @return
	 */
	JSONObject deleteOneTradeTypeService(ATradeType tradeType);

	/**
	 * 查询一个交易类型
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	ATradeType findOneTradeTypeService(Map<String, Object> condMap);

	/**
	 * 查询多条交易类型
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	List<ATradeType> findCondListTradeTypeService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);
	/*-----交易类型结束-----*/
	
	/*-----市场深度开始-----*/
	/**
	 * 添加一个市场深度
	 * 
	 * @param dept
	 * @return
	 */
	JSONObject saveOneDeptService(ADept dept);

	/**
	 * 更新一个市场深度
	 * 
	 * @param dept
	 * @return
	 */
	JSONObject updateOneDeptService(ADept dept);

	/**
	 * 删除一个市场深度
	 * 
	 * @param dept
	 * @return
	 */
	JSONObject deleteOneDeptService(ADept dept);
	
	/**
	 * 批量操作
	 * 
	 * @param dept
	 * @return
	 */
	JSONObject updateBatchDeptService(String type , List<ADept> deptList);

	/**
	 * 查询一个市场深度
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	ADept findOneDeptService(Map<String, Object> condMap);

	/**
	 * 查询多条市场深度
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	List<ADept> findCondListDeptService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap);
	/*-----市场深度结束-----*/
	/**
	 * 查询所有交易网站共有的交易对
	 * @return
	 */
	Map<String, JSONArray> operFindCommonPairService();
	/**
	 * 查询所有交易网站共有的交易对
	 * @return
	 */
	JSONObject operBatchCoinrateTradeTypeService();
	
	/**
	 * 批量处理网站的所有交易对
	 * ~所有的网站共有的交易对开启,启用;
	 * ~将这些所有的交易对都以json的形式放到website表中
	 * @return
	 */
	JSONObject operBatchWebsiteBatchService();
}
