package com.wang.mvchain.system.pojo;

import java.util.Date;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.pojo.BasePojo;
import com.wang.mvchain.common.util.DateUtil;

/**
 * 市场深度表,放的是最新的市场行情
 * 
 * @author wangsh
 * 
 */
public class ADept extends BasePojo<ADept>
{
	/*
	 * 数据库中存储的字段
	 */
	private int id;
	private int wbid;
	private int ctid;
	private double souprice;
	private double rate;
	private double price;
	private double volnum;
	private double mergevol;
	private int ordersnum;
	private byte ptype;
	private byte otype;
	private Date createtime;
	private Date updatetime;

	/* 字符串表示 */
	private String ptypeStr;

	/* 对象关系 */
	private AWebsite websiteObj;
	private ATradeType tradeTypeObj;
	
	/*辅助对象*/
	/*1,为表示:更新进行中的任务价格*/
	private String updateFlagStr ; 

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getWbid()
	{
		return wbid;
	}

	public void setWbid(int wbid)
	{
		this.wbid = wbid;
	}

	public double getPrice()
	{
		return price;
	}

	public void setPrice(double price)
	{
		this.price = price;
	}

	public double getVolnum()
	{
		return volnum;
	}

	public void setVolnum(double volnum)
	{
		this.volnum = volnum;
	}

	public double getMergevol() {
		return mergevol;
	}

	public void setMergevol(double mergevol) {
		this.mergevol = mergevol;
	}

	public int getOrdersnum()
	{
		return ordersnum;
	}

	public void setOrdersnum(int ordersnum)
	{
		this.ordersnum = ordersnum;
	}

	public byte getPtype()
	{
		return ptype;
	}

	public void setPtype(byte ptype)
	{
		this.ptype = ptype;
	}

	public Date getCreatetime()
	{
		return createtime;
	}

	public void setCreatetime(Date createtime)
	{
		this.createtime = createtime;
	}

	public Date getUpdatetime()
	{
		return updatetime;
	}

	public void setUpdatetime(Date updatetime)
	{
		this.updatetime = updatetime;
	}

	public String getPtypeStr()
	{
		/* 类型:0:买入,1:卖出 */
		ADeptEnum[] deptEnums = ADeptEnum.values();
		for (int i = 0; i < deptEnums.length; i++)
		{
			ADeptEnum deptEnum = deptEnums[i];
			if (deptEnum.toString().startsWith("PTYPE_"))
			{
				// 表示是状态的标识
				if (deptEnum.getStatus() == this.getPtype())
				{
					this.ptypeStr = deptEnum.getName();
					break;
				}
			}
		}
		return ptypeStr;
	}

	public int getCtid()
	{
		return ctid;
	}

	public void setCtid(int ctid)
	{
		this.ctid = ctid;
	}

	public AWebsite getWebsiteObj()
	{
		return websiteObj;
	}

	public void setWebsiteObj(AWebsite websiteObj)
	{
		this.websiteObj = websiteObj;
	}

	public ATradeType getTradeTypeObj()
	{
		return tradeTypeObj;
	}

	public void setTradeTypeObj(ATradeType tradeTypeObj)
	{
		this.tradeTypeObj = tradeTypeObj;
	}

	public byte getOtype()
	{
		return otype;
	}

	public void setOtype(byte otype)
	{
		this.otype = otype;
	}

	public double getSouprice()
	{
		return souprice;
	}

	public void setSouprice(double souprice)
	{
		this.souprice = souprice;
	}

	public double getRate()
	{
		return rate;
	}

	public void setRate(double rate)
	{
		this.rate = rate;
	}

	public String getUpdateFlagStr()
	{
		return updateFlagStr;
	}

	public void setUpdateFlagStr(String updateFlagStr)
	{
		this.updateFlagStr = updateFlagStr;
	}

	@Override
	public JSONObject toJSON()
	{
		DateUtil dateFormatUtil = new DateUtil();
		JSONObject resultJSON = new JSONObject();
		resultJSON.put("id", this.getId() + "");
		resultJSON.put("wbid", this.getWbid() + "");
		resultJSON.put("ctid", this.getCtid() + "");
		resultJSON.put("souprice", this.getSouprice() + "");
		resultJSON.put("price", this.getPrice() + "");
		resultJSON.put("rate", this.getRate() + "");
		resultJSON.put("volnum", this.getVolnum() + "");
		resultJSON.put("mergevol", this.getMergevol() + "");
		resultJSON.put("ordersnum", this.getOrdersnum() + "");
		resultJSON.put("ptype", this.getPtype() + "");
		resultJSON.put("ptypeStr", this.getPtypeStr() + "");
		resultJSON.put("otype", this.getOtype() + "");
		resultJSON.put("createtime", dateFormatUtil.formatDateTime(this.getCreatetime()) + "");
		resultJSON.put("updatetime", dateFormatUtil.formatDateTime(this.getUpdatetime()) + "");
		
		if(this.websiteObj != null)
		{
			resultJSON.put("websiteObj", this.websiteObj.toJSON());
		}
		
		if(this.tradeTypeObj != null)
		{
			resultJSON.put("tradeTypeObj", this.tradeTypeObj.toJSON());
		}
		return resultJSON;
	}

	@Override
	public String toString()
	{
		return "ADept [wbid=" + wbid + ", ctid=" + ctid + ", price=" + price + ", mergevol=" + mergevol + ", ptypeStr="
				+ ptypeStr + "]";
	}

	@Override
	protected ADept parseJSON(JSONObject souJSON)
	{
		return null;
	}
}
