package com.wang.mvchain.system.pojo;

import java.util.Date;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.pojo.BasePojo;
import com.wang.mvchain.common.util.DateUtil;

/**
 * 币种的pojo
 * 
 * @author wangshh
 * 
 */
public class ACoinrate extends BasePojo<ACoinrate>
{
	private int id;
	private String name;
	private String engname;
	private byte prostatus ; 
	private byte status;
	private String extend ; 
	private String content;
	private Date createtime;
	private Date updatetime;
	private Date pubtime;
	
	//字符串表示
	private String statusStr;
	private String prostatusStr ; 
	
	/* 对象表示 */
	private JSONObject extendJSON ;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getEngname()
	{
		return engname;
	}

	public void setEngname(String engname)
	{
		this.engname = engname;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public Date getCreatetime()
	{
		return createtime;
	}

	public void setCreatetime(Date createtime)
	{
		this.createtime = createtime;
	}

	public Date getUpdatetime()
	{
		return updatetime;
	}

	public void setUpdatetime(Date updatetime)
	{
		this.updatetime = updatetime;
	}

	public Date getPubtime()
	{
		return pubtime;
	}

	public void setPubtime(Date pubtime)
	{
		this.pubtime = pubtime;
	}

	public String getStatusStr()
	{
		/*
		 * 状态，0禁用， 1启用
		 */
		ACoinrateEnum[] coinrateEnums = ACoinrateEnum.values();
		for (int i = 0; i < coinrateEnums.length; i++)
		{
			ACoinrateEnum coinrateEnum = coinrateEnums[i];
			if (coinrateEnum.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (coinrateEnum.getStatus() == this.getStatus())
				{
					this.statusStr = coinrateEnum.getName();
					break;
				}
			}
		}
		return statusStr;
	}

	public JSONObject toJSON()
	{
		DateUtil dateFormatUtil = new DateUtil();
		JSONObject resultJSON = new JSONObject();
		resultJSON.put("id", this.getId() + "");
		resultJSON.put("name", this.getName() + "");
		resultJSON.put("engname", this.getEngname() + "");
		resultJSON.put("status", this.getStatus() + "");
		resultJSON.put("statusStr", this.getStatusStr() + "");
		resultJSON.put("content", this.getContent() + "");
		resultJSON.put("createtime", dateFormatUtil.formatDateTime(this.getCreatetime()) + "");
		resultJSON.put("updatetime", dateFormatUtil.formatDateTime(this.getUpdatetime()) + "");
		return resultJSON;
	}

	@Override
	public ACoinrate parseJSON(JSONObject souJSON)
	{
		DateUtil dateFormatUtil = new DateUtil();
		if(souJSON.get("id") != null)
		{
			this.setId(Integer.valueOf(souJSON.get("id") + ""));
		}
		if(souJSON.get("name") != null)
		{
			this.setName(souJSON.get("name") + "");
		}
		if(souJSON.get("engname") != null)
		{
			this.setEngname(souJSON.get("engname") + "");
		}
		if(souJSON.get("content") != null)
		{
			this.setContent(souJSON.get("content") + "");
		}
		if(souJSON.get("status") != null)
		{
			this.setStatus(Byte.valueOf(souJSON.get("status") + ""));
		}
		if(souJSON.get("createtime") != null)
		{
			this.setCreatetime(dateFormatUtil.parseDateTime(souJSON.get("createtime") + ""));
		}
		if(souJSON.get("updatetime") != null)
		{
			this.setUpdatetime(dateFormatUtil.parseDateTime(souJSON.get("updatetime") + ""));
		}
		return this ; 
	}

	public byte getProstatus()
	{
		return prostatus;
	}

	public void setProstatus(byte prostatus)
	{
		this.prostatus = prostatus;
	}

	public String getProstatusStr()
	{
		/*
		 * '搬砖币种开启状态:0:禁用,1:启用
		 */
		ACoinrateEnum[] coinrateEnums = ACoinrateEnum.values();
		for (int i = 0; i < coinrateEnums.length; i++)
		{
			ACoinrateEnum coinrateEnum = coinrateEnums[i];
			if (coinrateEnum.toString().startsWith("PROSTATUS_"))
			{
				// 表示是状态的标识
				if (coinrateEnum.getStatus() == this.getProstatus())
				{
					this.prostatusStr = coinrateEnum.getName();
					break;
				}
			}
		}
		return prostatusStr;
	}

	public String getExtend()
	{
		return extend;
	}

	public void setExtend(String extend)
	{
		this.extend = extend;
	}

	public JSONObject getExtendJSON()
	{
		try
		{
			this.extendJSON = (JSONObject) JSON.parse(this.extend);
			if(extendJSON == null)
			{
				extendJSON = new JSONObject() ; 
			}
			return extendJSON;
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return new JSONObject();
	}
}
