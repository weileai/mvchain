package com.wang.mvchain.system.pojo;

/**
 * 管理员的枚举
 * 
 * @author wangsh
 *
 */
public enum ADeptEnum
{
	PTYPE_BUY(Byte.valueOf("0"), "买"), 
	PTYPE_SELL(Byte.valueOf("1"), "卖");

	private byte status;
	private String name;

	private ADeptEnum(byte status, String name)
	{
		this.status = status;
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public String toString()
	{
		return super.toString();
	}
}
