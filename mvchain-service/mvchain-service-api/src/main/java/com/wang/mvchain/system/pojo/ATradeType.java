package com.wang.mvchain.system.pojo;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.pojo.BasePojo;
import com.wang.mvchain.common.util.DateUtil;

/**
 * 市场深度表,放的是最新的市场行情
 * 
 * @author wangsh
 * 
 */
public class ATradeType extends BasePojo<ATradeType>
{
	/*
	 * 数据库中存储的字段
	 */
	private int id;
	private int fromcoinid;
	private int tocoinid;
	private String name;
	private String content;
	private double rate;
	private byte status;
	private Date createtime;
	private Date updatetime;
	private Date pubtime;

	/* 字符串表示 */
	private String statusStr;

	/* 对象关系 */
	private ACoinrate fromcoinCoinObj;
	private ACoinrate tocoinCoinObj;
	
	/* 存储所有状态的容器 */
	private Map<String,String> enumsMap = new TreeMap<String, String>();

	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		ATradeTypeEnum[] enums = ATradeTypeEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			ATradeTypeEnum enumTemp = enums[i];
			String key = enumTemp.toString() ; 
			if(key.lastIndexOf("_") != -1)
			{
				key = key.substring(0,key.lastIndexOf("_")) ; 
			}
			enumsMap.put(key + "-" + enumTemp.getStatus() + "", enumTemp.getName());
		}
		return enumsMap;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getFromcoinid()
	{
		return fromcoinid;
	}

	public void setFromcoinid(int fromcoinid)
	{
		this.fromcoinid = fromcoinid;
	}

	public int getTocoinid()
	{
		return tocoinid;
	}

	public void setTocoinid(int tocoinid)
	{
		this.tocoinid = tocoinid;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreatetime()
	{
		return createtime;
	}

	public void setCreatetime(Date createtime)
	{
		this.createtime = createtime;
	}

	public Date getUpdatetime()
	{
		return updatetime;
	}

	public void setUpdatetime(Date updatetime)
	{
		this.updatetime = updatetime;
	}

	public Date getPubtime()
	{
		return pubtime;
	}

	public void setPubtime(Date pubtime)
	{
		this.pubtime = pubtime;
	}

	public String getStatusStr()
	{
		/*
		 * 状态:1:启用,0:禁用
		 */
		ATradeTypeEnum[] tradeTypeEnums = ATradeTypeEnum.values();
		for (int i = 0; i < tradeTypeEnums.length; i++)
		{
			ATradeTypeEnum tradeTypeEnum = tradeTypeEnums[i];
			if (tradeTypeEnum.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (tradeTypeEnum.getStatus() == this.getStatus())
				{
					this.statusStr = tradeTypeEnum.getName();
					break;
				}
			}
		}
		return statusStr;
	}

	public double getRate()
	{
		return rate;
	}

	public void setRate(double rate)
	{
		this.rate = rate;
	}

	public ACoinrate getFromcoinCoinObj()
	{
		return fromcoinCoinObj;
	}

	public void setFromcoinCoinObj(ACoinrate fromcoinCoinObj)
	{
		this.fromcoinCoinObj = fromcoinCoinObj;
	}

	public ACoinrate getTocoinCoinObj()
	{
		return tocoinCoinObj;
	}

	public void setTocoinCoinObj(ACoinrate tocoinCoinObj)
	{
		this.tocoinCoinObj = tocoinCoinObj;
	}

	@Override
	public JSONObject toJSON()
	{
		DateUtil dateFormatUtil = new DateUtil();
		JSONObject resultJSON = new JSONObject();
		resultJSON.put("id", this.getId() + "");
		resultJSON.put("fromcoinid", this.getFromcoinid() + "");
		resultJSON.put("tocoinid", this.getTocoinid() + "");
		resultJSON.put("name", this.getName() + "");
		resultJSON.put("content", this.getContent() + "");
		resultJSON.put("rate", this.getRate() + "");
		resultJSON.put("status", this.getStatus() + "");
		resultJSON.put("statusStr", this.getStatusStr() + "");
		resultJSON.put("createtime", dateFormatUtil.formatDateTime(this.getCreatetime()) + "");
		resultJSON.put("updatetime", dateFormatUtil.formatDateTime(this.getUpdatetime()) + "");
		return resultJSON;
	}

	@Override
	protected ATradeType parseJSON(JSONObject souJSON)
	{
		// TODO Auto-generated method stub
		return null;
	}
}
