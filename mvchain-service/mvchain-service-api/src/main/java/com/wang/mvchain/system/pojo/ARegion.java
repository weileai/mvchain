package com.wang.mvchain.system.pojo;

import java.util.Date;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.DateUtil;

/**
 * 全国地区的pojo
 * 
 * @author wangshh
 * 
 */
public class ARegion
{
	private int id;
	private int parentid;
	private String name;
	private String url;
	private String areacode;
	private Byte level;
	private Double lon;
	private Double lat;
	private Byte isleaf;
	private Date createtime;
	private Date updatetime;

	// 对象的表示,关联关系
	private ARegion parentRegion;
	
	//树形分类的字符串表示
	private String treeName ; 
	
	//json的表现方式
	private JSONArray treeJSON ; 
	private JSONArray childrenListJSON ; 

	public int getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Integer getParentid()
	{
		return parentid;
	}

	public void setParentid(Integer parentid)
	{
		this.parentid = parentid;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public String getAreacode()
	{
		return areacode;
	}

	public void setAreacode(String areacode)
	{
		this.areacode = areacode;
	}

	public Byte getLevel()
	{
		return level;
	}

	public void setLevel(Byte level)
	{
		this.level = level;
	}

	public Double getLon()
	{
		return lon;
	}

	public void setLon(Double lon)
	{
		this.lon = lon;
	}

	public Double getLat()
	{
		return lat;
	}

	public void setLat(Double lat)
	{
		this.lat = lat;
	}

	public Byte getIsleaf()
	{
		return isleaf;
	}

	public void setIsleaf(Byte isleaf)
	{
		this.isleaf = isleaf;
	}

	public Date getCreatetime()
	{
		return createtime;
	}

	public void setCreatetime(Date createtime)
	{
		this.createtime = createtime;
	}

	public Date getUpdatetime()
	{
		return updatetime;
	}

	public void setUpdatetime(Date updatetime)
	{
		this.updatetime = updatetime;
	}

	public ARegion getParentRegion()
	{
		return parentRegion;
	}

	public void setParentRegion(ARegion parentRegion)
	{
		this.parentRegion = parentRegion;
	}

	public String getTreeName()
	{
		return treeName;
	}

	public void setTreeName(String treeName)
	{
		this.treeName = treeName;
	}

	/**
	 * 组装成一个json
	 * @return
	 */
	public JSONObject getJson()
	{
		DateUtil dateFormatUtil = new DateUtil();
		JSONObject regTempJSON = new JSONObject();
		regTempJSON.put("id", this.getId() + "");
		regTempJSON.put("parentid", this.getParentid() + "");
		regTempJSON.put("name", this.getName());
		regTempJSON.put("url", this.getUrl());
		regTempJSON.put("areacode", this.getAreacode());
		regTempJSON.put("level", this.getLevel() + "");
		regTempJSON.put("lon", this.getLon() + "");
		regTempJSON.put("lat", this.getLat() + "");
		regTempJSON.put("isleaf", this.getIsleaf() + "");
		regTempJSON.put("createtime", dateFormatUtil.formatDateTime(this.getCreatetime()));
		regTempJSON.put("updatetime", dateFormatUtil.formatDateTime(this.getUpdatetime()));
		regTempJSON.put("treeName", this.getTreeName());
		
		if(treeJSON != null)
		{
			//增加父节点
			regTempJSON.put("treeJSON", treeJSON);
		}
		
		if(childrenListJSON != null)
		{
			regTempJSON.put("childrenListJSON", childrenListJSON);
		}
		
		return regTempJSON ;
	}
	
	/**
	 * 解析一个json
	 * @param souJSON
	 * @return
	 */
	public ARegion parseJson(JSONObject souJSON)
	{
		DateUtil dateFormatUtil = new DateUtil();
		ARegion region = new ARegion();
		region.setId(Integer.valueOf(souJSON.get("id") + ""));
		if(souJSON.get("parentid") != null && !"null".equalsIgnoreCase(souJSON.get("parentid") + ""))
		{
			region.setParentid(Integer.valueOf(souJSON.get("parentid") + ""));
		}
		region.setName(souJSON.get("name") + "");
		region.setUrl(souJSON.get("url") + "");
		region.setAreacode(souJSON.get("areacode") + "");
		region.setLevel(Byte.valueOf(souJSON.get("level") + ""));
		region.setLon(Double.valueOf(souJSON.get("lon") + ""));
		region.setLon(Double.valueOf(souJSON.get("lat") + ""));
		region.setLevel(Byte.valueOf(souJSON.get("isleaf") + ""));
		region.setCreatetime(dateFormatUtil.parseDateTime(souJSON.get("createtime") + ""));
		region.setCreatetime(dateFormatUtil.parseDateTime(souJSON.get("updatetime") + ""));
		region.setAreacode(souJSON.get("treeName") + "");
		
		region.setTreeJSON((JSONArray) souJSON.get("treeJSON"));
		region.setChildrenListJSON((JSONArray) souJSON.get("childrenListJSON"));
		return region ; 
	}

	public JSONArray getTreeJSON()
	{
		return treeJSON;
	}

	public void setTreeJSON(JSONArray treeJSON)
	{
		this.treeJSON = treeJSON;
	}

	public JSONArray getChildrenListJSON()
	{
		return childrenListJSON;
	}

	public void setChildrenListJSON(JSONArray childrenListJSON)
	{
		this.childrenListJSON = childrenListJSON;
	}
}
