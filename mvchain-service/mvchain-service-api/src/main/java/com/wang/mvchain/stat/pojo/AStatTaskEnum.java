package com.wang.mvchain.stat.pojo;

public enum AStatTaskEnum
{
	STATUS_ENABLE(Byte.valueOf("1"), "启用"), 
	STATUS_DISABLE(Byte.valueOf("0"), "禁用"),
	
	FLAG_NONE(Byte.valueOf("0"), "无"),
	FLAG_SNAPSHOT(Byte.valueOf("1"), "快照");

	private byte status;
	private String name;

	private AStatTaskEnum(byte status, String name)
	{
		this.status = status;
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public String toString()
	{
		return super.toString();
	}
}
