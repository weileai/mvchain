package com.wang.mvchain.finan.pojo;

import java.util.Date;

import com.wang.mvchain.system.pojo.ACoinrate;
import com.wang.mvchain.users.pojo.AUsers;

/**
 * 资产账户列表
 * 
 * @author wangshMac
 */
public class AFinanAsset
{
	private int id;
	private int usersid;
	private int coinid;
	private double usdBalance ; 
	private double balance;
	private double percent ;
	private double todayPrift;
	private double totalPrift;
	private double cnyPrift;
	private byte status;
	private Date createtime;
	private Date updatetime;
	private Date pubtime;

	/* 字符串描述 */
	private String statusStr;
	
	/* 对象描述 */
	private AUsers users ; 
	private ACoinrate coinrate ; 

	public String getStatusStr()
	{
		// 根据状态值获取字符串描述
		AFinanAssetEnum[] finanAssetEnumEnums = AFinanAssetEnum.values();
		for (int i = 0; i < finanAssetEnumEnums.length; i++)
		{
			AFinanAssetEnum finanAccountEnum = finanAssetEnumEnums[i];
			if (finanAccountEnum.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (finanAccountEnum.getStatus() == this.getStatus())
				{
					this.statusStr = finanAccountEnum.getName();
					break;
				}
			}
		}
		return statusStr;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getUsersid()
	{
		return usersid;
	}

	public void setUsersid(int usersid)
	{
		this.usersid = usersid;
	}

	public int getCoinid()
	{
		return coinid;
	}

	public void setCoinid(int coinid)
	{
		this.coinid = coinid;
	}

	public double getBalance()
	{
		return balance;
	}

	public void setBalance(double balance)
	{
		this.balance = balance;
	}

	public double getTodayPrift()
	{
		return todayPrift;
	}

	public void setTodayPrift(double todayPrift)
	{
		this.todayPrift = todayPrift;
	}

	public double getTotalPrift()
	{
		return totalPrift;
	}

	public void setTotalPrift(double totalPrift)
	{
		this.totalPrift = totalPrift;
	}

	public double getCnyPrift()
	{
		return cnyPrift;
	}

	public void setCnyPrift(double cnyPrift)
	{
		this.cnyPrift = cnyPrift;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreatetime()
	{
		return createtime;
	}

	public void setCreatetime(Date createtime)
	{
		this.createtime = createtime;
	}

	public Date getUpdatetime()
	{
		return updatetime;
	}

	public void setUpdatetime(Date updatetime)
	{
		this.updatetime = updatetime;
	}

	public Date getPubtime()
	{
		return pubtime;
	}

	public void setPubtime(Date pubtime)
	{
		this.pubtime = pubtime;
	}

	public void setStatusStr(String statusStr)
	{
		this.statusStr = statusStr;
	}

	public AUsers getUsers()
	{
		return users;
	}

	public void setUsers(AUsers users)
	{
		this.users = users;
	}

	public ACoinrate getCoinrate()
	{
		return coinrate;
	}

	public void setCoinrate(ACoinrate coinrate)
	{
		this.coinrate = coinrate;
	}

	public double getPercent()
	{
		return percent;
	}

	public void setPercent(double percent)
	{
		this.percent = percent;
	}

	public double getUsdBalance()
	{
		return usdBalance;
	}

	public void setUsdBalance(double usdBalance)
	{
		this.usdBalance = usdBalance;
	}
}
