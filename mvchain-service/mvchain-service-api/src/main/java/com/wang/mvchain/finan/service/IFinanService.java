package com.wang.mvchain.finan.service;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.PageInfoUtil;
import com.wang.mvchain.finan.pojo.AFinanAccount;
import com.wang.mvchain.finan.pojo.AFinanAsset;
import com.wang.mvchain.finan.pojo.AFinanHistory;
import com.wang.mvchain.stat.pojo.AStatTask;

/**
 * 资产Service
 * @author wangshMac
 *
 */
public interface IFinanService
{
	/*----------提现账户开始-----------*/

	/**
	 * 添加一个提现账户
	 * 
	 * @param finanAccount
	 * @return
	 */
	JSONObject saveOneFinanAccountService(AFinanAccount finanAccount);

	/**
	 * 更新一个提现账户
	 * 
	 * @param finanAccount
	 * @return
	 */
	JSONObject updateOneFinanAccountService(AFinanAccount finanAccount);

	/**
	 * 删除一个提现账户
	 * 
	 * @param finanAccount
	 * @return
	 */
	JSONObject deleteOneFinanAccountService(AFinanAccount finanAccount);

	/**
	 * 查询一个提现账户
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	AFinanAccount findOneFinanAccountService(Map<String, Object> condMap);

	/**
	 * 查询多条提现账户
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	List<AFinanAccount> findCondListFinanAccountService(PageInfoUtil pageInfoUtil, Map<String, Object> condMap);
	/*----------提现账户结束-----------*/
	
	/*----------资产账户开始-----------*/

	/**
	 * 添加一个资产账户
	 * 
	 * @param finanAsset
	 * @return
	 */
	JSONObject saveOneFinanAssetService(AFinanAsset finanAsset);

	/**
	 * 更新一个资产账户
	 * 
	 * @param finanAsset
	 * @return
	 */
	JSONObject updateOneFinanAssetService(AFinanAsset finanAsset);

	/**
	 * 删除一个资产账户
	 * 
	 * @param finanAsset
	 * @return
	 */
	JSONObject deleteOneFinanAssetService(AFinanAsset finanAsset);

	/**
	 * 查询一个资产账户
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	AFinanAsset findOneFinanAssetService(Map<String, Object> condMap);

	/**
	 * 查询多条资产账户
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	List<AFinanAsset> findCondListFinanAssetService(PageInfoUtil pageInfoUtil, Map<String, Object> condMap);
	
	/**
	 * 资产的任务统计
	 * @param condMap
	 * @return
	 */
	List<Map> findStatListService(Map<String, Object> condMap);
	/*----------资产账户结束-----------*/
	
	/*----------资产历史开始-----------*/

	/**
	 * 添加一个资产历史
	 * 
	 * @param finanHistory
	 * @return
	 */
	JSONObject saveOneFinanHistoryService(AFinanHistory finanHistory);

	/**
	 * 更新一个资产历史
	 * 
	 * @param finanHistory
	 * @return
	 */
	JSONObject updateOneFinanHistoryService(AFinanHistory finanHistory);

	/**
	 * 删除一个资产历史
	 * 
	 * @param finanHistory
	 * @return
	 */
	JSONObject deleteOneFinanHistoryService(AFinanHistory finanHistory);

	/**
	 * 查询一个资产历史
	 * 
	 * @param type
	 * @param condMap
	 * @return
	 */
	AFinanHistory findOneFinanHistoryService(Map<String, Object> condMap);

	/**
	 * 查询多条资产历史
	 * 
	 * @param pageInfo
	 * @param type
	 * @param condMap
	 * @return
	 */
	List<AFinanHistory> findCondListFinanHistoryService(PageInfoUtil pageInfoUtil, Map<String, Object> condMap);
	/*----------资产历史结束-----------*/
	
	/*----------其它操作开始-----------*/
	/**
	 * 重新计算所有的百分比
	 */
	void operAssetPercentSyncService();
	
	/**
	 * 根据统计的任务分配收益
	 * @param statTask
	 */
	JSONObject operAssetPriftService(AStatTask statTask);
	/**
	 * 批量对资产做一些统计
	 * @param condMap
	 */
	int operAssetBatchService(Map<String, Object> condMap);
	/*----------其它操作结束-----------*/

}
