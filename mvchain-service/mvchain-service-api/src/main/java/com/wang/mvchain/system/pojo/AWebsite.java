package com.wang.mvchain.system.pojo;

import java.util.Date;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.pojo.BasePojo;
import com.wang.mvchain.common.util.DateUtil;

/**
 * 市场深度表,放的是最新的市场行情
 * 
 * @author wangsh
 * 
 */
public class AWebsite extends BasePojo<AWebsite>
{
	/*
	 * 数据库中存储的字段
	 */
	private int id;
	private String name;
	private String url;
	private byte status;
	private String moneystatus;
	private String authinfo;
	private String fee;
	private Date createtime;
	private Date updatetime;
	private Date pubtime;

	/* 字符串表示 */
	private String statusStr;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreatetime()
	{
		return createtime;
	}

	public void setCreatetime(Date createtime)
	{
		this.createtime = createtime;
	}

	public Date getUpdatetime()
	{
		return updatetime;
	}

	public void setUpdatetime(Date updatetime)
	{
		this.updatetime = updatetime;
	}

	public Date getPubtime()
	{
		return pubtime;
	}

	public void setPubtime(Date pubtime)
	{
		this.pubtime = pubtime;
	}

	public String getStatusStr()
	{
		/*
		 * 状态:1:启用,0:禁用
		 */
		AWebsiteEnum[] websiteEnums = AWebsiteEnum.values();
		for (int i = 0; i < websiteEnums.length; i++)
		{
			AWebsiteEnum websiteEnum = websiteEnums[i];
			if (websiteEnum.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (websiteEnum.getStatus() == this.getStatus())
				{
					this.statusStr = websiteEnum.getName();
					break;
				}
			}
		}
		return statusStr;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public String getMoneystatus()
	{
		return moneystatus;
	}

	public void setMoneystatus(String moneystatus)
	{
		this.moneystatus = moneystatus;
	}

	public String getAuthinfo()
	{
		return authinfo;
	}

	public void setAuthinfo(String authinfo)
	{
		this.authinfo = authinfo;
	}

	public String getFee()
	{
		return fee;
	}

	public void setFee(String fee)
	{
		this.fee = fee;
	}

	public JSONObject getMoneyStatusJSON()
	{
		try
		{
			JSONObject resultJSON = (JSONObject) JSON.parse(this.getMoneystatus());
			if (resultJSON != null)
			{
				return resultJSON;
			}
		} catch (Exception e)
		{
		}
		return new JSONObject();
	}

	public JSONObject getAuthinfoJSON()
	{
		try
		{
			return (JSONObject) JSON.parse(this.authinfo);
		} catch (Exception e)
		{
		}
		return new JSONObject();
	}

	public JSONObject getFeeJSON()
	{
		try
		{
			return (JSONObject) JSON.parse(this.fee);
		} catch (Exception e)
		{
		}
		return new JSONObject();
	}

	@Override
	public JSONObject toJSON()
	{
		DateUtil dateFormatUtil = new DateUtil();
		JSONObject resultJSON = new JSONObject();
		resultJSON.put("id", this.getId() + "");
		resultJSON.put("name", this.getName() + "");
		resultJSON.put("url", this.getUrl() + "");
		// resultJSON.put("moneystatus", this.getMoneystatus() + "");
		resultJSON.put("status", this.getStatus() + "");
		resultJSON.put("statusStr", this.getStatusStr() + "");
		// resultJSON.put("authinfo", this.getAuthinfoJSON());
		// resultJSON.put("fee", this.getFeeJSON());
		resultJSON.put("createtime", dateFormatUtil.formatDateTime(this.getCreatetime()) + "");
		resultJSON.put("updatetime", dateFormatUtil.formatDateTime(this.getUpdatetime()) + "");
		return resultJSON;
	}

	@Override
	public AWebsite parseJSON(JSONObject souJSON)
	{
		DateUtil dateFormatUtil = new DateUtil();
		if (souJSON.get("id") != null)
		{
			this.setId(Integer.valueOf(souJSON.get("id") + ""));
		}

		if (souJSON.get("name") != null)
		{
			this.setName(souJSON.get("name") + "");
		}

		if (souJSON.get("url") != null)
		{
			this.setUrl(souJSON.get("url") + "");
		}

		if (souJSON.get("status") != null)
		{
			this.setStatus(Byte.valueOf(souJSON.get("status") + ""));
		}

		if (souJSON.get("moneystatus") != null)
		{
			this.setMoneystatus(souJSON.get("moneystatus") + "");
		}
		if (souJSON.get("authinfo") != null)
		{
			this.setAuthinfo(souJSON.get("authinfo") + "");
		}

		if (souJSON.get("fee") != null)
		{
			this.setAuthinfo(souJSON.get("fee") + "");
		}

		if (souJSON.get("createtime") != null)
		{
			this.setCreatetime(dateFormatUtil.parseDateTime(souJSON.get("createtime") + ""));
		}

		if (souJSON.get("updatetime") != null)
		{
			this.setUpdatetime(dateFormatUtil.parseDateTime(souJSON.get("updatetime") + ""));
		}
		return this;
	}

	@Override
	public String toString()
	{
		return "AWebsite [id=" + id + ", name=" + name + ", url=" + url + ", status=" + status + ", moneystatus="
				+ moneystatus + ", authinfo=" + authinfo + ", fee=" + fee + ", createtime=" + createtime
				+ ", updatetime=" + updatetime + ", pubtime=" + pubtime + ", statusStr=" + statusStr + "]";
	}
}
