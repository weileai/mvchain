package com.wang.mvchain.users.pojo;

import java.math.RoundingMode;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.pojo.BasePojo;
import com.wang.mvchain.common.util.DoubleOperUtil;
import com.wang.mvchain.system.pojo.ACoinrate;
import com.wang.mvchain.system.pojo.ATradeType;
import com.wang.mvchain.system.pojo.AWebsite;

/**
 * 市场深度表,放的是最新的市场行情
 * 
 * 买与卖是站在网站的角度来看
 * 
 * @author wangsh
 * 
 */
public class AUsersTask extends BasePojo
{
	/*
	 * 数据库中存储的字段
	 */
	private int id;
	private int usersid;
	private int priftcoinid;
	//交易网站的买和卖单
	private int sellwbid ; 
	private int buywbid ; 
	private int buyctid;
	private int sellctid ;  
	private String name;
	private double prift;
	private double priftfee ; 
	private double souprift ; 
	private byte status;
	private double soubuyprice ;
	private double buyprice ;
	private double sousellprice ; 
	private double sellprice ; 
	private double taskbuyprice ; 
	private double tasksellprice ; 
	private double pricecha;
	private double buynum ;
	private double sellnum ; 
	private double feenum ; 
	private double totalnum ; 
	private double deptnum ; 
	private byte soutype;
	private String content ; 
	private Date createtime;
	private Date updatetime;
	private Date finishtime;

	/* 字符串表示 */
	private String statusStr;
	private String soutypeStr;
	
	private String buyctidTarStr = "0"; 
	private String sellctidTarStr = "0" ;

	// 对象表示
	private AUsers usersObj;
	private ACoinrate priftcoinObj;
	private AWebsite buyWbObj ; 
	private AWebsite sellWbObj ;
	private ATradeType sellTradeTypeObj;
	private ATradeType buyTradeTypeObj;
	
	/* 创建一个工具类 */
	private DoubleOperUtil doubleOperUtil = new DoubleOperUtil() ; 
	
	/* 存储所有状态的容器 */
	private Map<String,String> enumsMap = new TreeMap<String, String>();

	public Map<String, String> getEnumsMap()
	{
		// 根据状态值获取字符串描述
		AUsersTaskEnum[] enums = AUsersTaskEnum.values();
		for (int i = 0; i < enums.length; i++)
		{
			AUsersTaskEnum enumTemp = enums[i];
			String key = enumTemp.toString() ; 
			if(key.lastIndexOf("_") != -1)
			{
				key = key.substring(0,key.lastIndexOf("_")) ; 
			}
			enumsMap.put(key + "-" + enumTemp.getStatus() + "", enumTemp.getName());
		}
		return enumsMap;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getUsersid()
	{
		return usersid;
	}

	public void setUsersid(int usersid)
	{
		this.usersid = usersid;
	}

	public int getPriftcoinid()
	{
		return priftcoinid;
	}

	public void setPriftcoinid(int priftcoinid)
	{
		this.priftcoinid = priftcoinid;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public double getPrift()
	{
		return prift;
	}

	public void setPrift(double prift)
	{
		prift = doubleOperUtil.round(prift, 3, RoundingMode.HALF_EVEN.ordinal());
		this.prift = prift;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public double getPricecha()
	{
		return pricecha;
	}

	public void setPricecha(double pricecha)
	{
		this.pricecha = pricecha;
	}

	public byte getSoutype()
	{
		return soutype;
	}

	public void setSoutype(byte soutype)
	{
		this.soutype = soutype;
	}

	public Date getCreatetime()
	{
		return createtime;
	}

	public void setCreatetime(Date createtime)
	{
		this.createtime = createtime;
	}

	public Date getUpdatetime()
	{
		return updatetime;
	}

	public void setUpdatetime(Date updatetime)
	{
		this.updatetime = updatetime;
	}

	public Date getFinishtime()
	{
		return finishtime;
	}

	public void setFinishtime(Date finishtime)
	{
		this.finishtime = finishtime;
	}

	public AUsers getUsersObj()
	{
		return usersObj;
	}

	public void setUsersObj(AUsers usersObj)
	{
		this.usersObj = usersObj;
	}

	public ACoinrate getPriftcoinObj()
	{
		return priftcoinObj;
	}

	public void setPriftcoinObj(ACoinrate priftcoinObj)
	{
		this.priftcoinObj = priftcoinObj;
	}


	public String getStatusStr()
	{
		/*
		 * 状态0:未开始,1:进行中,2:完成,3:计算收益
		 */
		AUsersTaskEnum[] usersTaskEnums = AUsersTaskEnum.values();
		for (int i = 0; i < usersTaskEnums.length; i++)
		{
			AUsersTaskEnum usersTaskEnum = usersTaskEnums[i];
			if (usersTaskEnum.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (usersTaskEnum.getStatus() == this.getStatus())
				{
					this.statusStr = usersTaskEnum.getName();
					break;
				}
			}
		}
		return statusStr;
	}

	public String getSoutypeStr()
	{
		/*
		 * 来源类型:0:系统创建,1:用户创建
		 */
		AUsersTaskEnum[] usersTaskEnums = AUsersTaskEnum.values();
		for (int i = 0; i < usersTaskEnums.length; i++)
		{
			AUsersTaskEnum usersTaskEnum = usersTaskEnums[i];
			if (usersTaskEnum.toString().startsWith("SOUTYPE_"))
			{
				// 表示是状态的标识
				if (usersTaskEnum.getStatus() == this.getSoutype())
				{
					this.soutypeStr = usersTaskEnum.getName();
					break;
				}
			}
		}
		return soutypeStr;
	}

	public double getBuyprice()
	{
		return buyprice;
	}

	public void setBuyprice(double buyprice)
	{
		this.buyprice = buyprice;
	}

	public double getSellprice()
	{
		return sellprice;
	}

	public void setSellprice(double sellprice)
	{
		this.sellprice = sellprice;
	}

	public int getSellwbid()
	{
		return sellwbid;
	}

	public void setSellwbid(int sellwbid)
	{
		this.sellwbid = sellwbid;
	}

	public int getBuywbid()
	{
		return buywbid;
	}

	public void setBuywbid(int buywbid)
	{
		this.buywbid = buywbid;
	}

	public double getTotalnum()
	{
		return totalnum;
	}

	public void setTotalnum(double totalnum)
	{
		this.totalnum = totalnum;
	}

	public double getBuynum()
	{
		return buynum;
	}

	public void setBuynum(double buynum)
	{
		buynum = doubleOperUtil.round(buynum, 3, RoundingMode.HALF_EVEN.ordinal());
		this.buynum = buynum;
	}

	public double getSellnum()
	{
		return sellnum;
	}

	public void setSellnum(double sellnum)
	{
		sellnum = doubleOperUtil.round(sellnum, 3, RoundingMode.HALF_EVEN.ordinal());
		this.sellnum = sellnum;
	}

	public int getBuyctid()
	{
		return buyctid;
	}

	public void setBuyctid(int buyctid)
	{
		this.buyctid = buyctid;
	}

	public int getSellctid()
	{
		return sellctid;
	}

	public void setSellctid(int sellctid)
	{
		this.sellctid = sellctid;
	}

	public double getSoubuyprice()
	{
		return soubuyprice;
	}

	public void setSoubuyprice(double soubuyprice)
	{
		this.soubuyprice = soubuyprice;
	}

	public double getSousellprice()
	{
		return sousellprice;
	}

	public void setSousellprice(double sousellprice)
	{
		this.sousellprice = sousellprice;
	}

	public ATradeType getSellTradeTypeObj()
	{
		return sellTradeTypeObj;
	}

	public void setSellTradeTypeObj(ATradeType sellTradeTypeObj)
	{
		this.sellTradeTypeObj = sellTradeTypeObj;
	}

	public ATradeType getBuyTradeTypeObj()
	{
		return buyTradeTypeObj;
	}

	public void setBuyTradeTypeObj(ATradeType buyTradeTypeObj)
	{
		this.buyTradeTypeObj = buyTradeTypeObj;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public double getDeptnum()
	{
		return deptnum;
	}

	public void setDeptnum(double deptnum)
	{
		this.deptnum = deptnum;
	}

	public AWebsite getBuyWbObj()
	{
		return buyWbObj;
	}

	public void setBuyWbObj(AWebsite buyWbObj)
	{
		this.buyWbObj = buyWbObj;
	}

	public AWebsite getSellWbObj()
	{
		return sellWbObj;
	}

	public void setSellWbObj(AWebsite sellWbObj)
	{
		this.sellWbObj = sellWbObj;
	}

	public String getBuyctidTarStr()
	{
		return buyctidTarStr;
	}

	public void setBuyctidTarStr(String buyctidTarStr)
	{
		this.buyctidTarStr = buyctidTarStr;
	}

	public String getSellctidTarStr()
	{
		return sellctidTarStr;
	}

	public void setSellctidTarStr(String sellctidTarStr)
	{
		this.sellctidTarStr = sellctidTarStr;
	}

	public double getPriftfee()
	{
		return priftfee;
	}

	public void setPriftfee(double priftfee)
	{
		this.priftfee = priftfee;
	}

	public double getSouprift()
	{
		return souprift;
	}

	public void setSouprift(double souprift)
	{
		this.souprift = souprift;
	}

	public double getTaskbuyprice()
	{
		return taskbuyprice;
	}

	public void setTaskbuyprice(double taskbuyprice)
	{
		this.taskbuyprice = taskbuyprice;
	}

	public double getTasksellprice()
	{
		return tasksellprice;
	}

	public void setTasksellprice(double tasksellprice)
	{
		this.tasksellprice = tasksellprice;
	}

	public double getFeenum()
	{
		return feenum;
	}

	public void setFeenum(double feenum)
	{
		this.feenum = feenum;
	}

	@Override
	public JSONObject toJSON()
	{
		return null;
	}

	@Override
	protected Object parseJSON(JSONObject souJSON)
	{
		return null;
	}
}
