package com.wang.mvchain.finan.pojo;

import java.util.Date;

import com.wang.mvchain.users.pojo.AUsers;

/**
 * 提现账户列表
 * 
 * @author wangshMac
 */
public class AFinanAccount
{
	private int id;
	private int usersid;
	private String email;
	private String usersName;
	private String content;
	private byte accountType;
	private byte status;
	private Date createtime;
	private Date updatetime;
	private Date pubtime;

	/* 字符串描述 */
	private String statusStr;
	private String accountTypeStr;
	
	/* 关联对象 */
	private AUsers users ; 

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getUsersid()
	{
		return usersid;
	}

	public void setUsersid(int usersid)
	{
		this.usersid = usersid;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getUsersName()
	{
		return usersName;
	}

	public void setUsersName(String usersName)
	{
		this.usersName = usersName;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public byte getAccountType()
	{
		return accountType;
	}

	public void setAccountType(byte accountType)
	{
		this.accountType = accountType;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreatetime()
	{
		return createtime;
	}

	public void setCreatetime(Date createtime)
	{
		this.createtime = createtime;
	}

	public Date getUpdatetime()
	{
		return updatetime;
	}

	public void setUpdatetime(Date updatetime)
	{
		this.updatetime = updatetime;
	}

	public Date getPubtime()
	{
		return pubtime;
	}

	public void setPubtime(Date pubtime)
	{
		this.pubtime = pubtime;
	}

	public String getStatusStr()
	{
		// 根据状态值获取字符串描述
		AFinanAccountEnum[] finanAccountEnums = AFinanAccountEnum.values();
		for (int i = 0; i < finanAccountEnums.length; i++)
		{
			AFinanAccountEnum finanAccountEnum = finanAccountEnums[i];
			if (finanAccountEnum.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (finanAccountEnum.getStatus() == this.getStatus())
				{
					this.statusStr = finanAccountEnum.getName();
					break;
				}
			}
		}
		return statusStr;
	}

	public String getAccountTypeStr()
	{
		// 根据状态值获取字符串描述
		AFinanAccountEnum[] finanAccountEnums = AFinanAccountEnum.values();
		for (int i = 0; i < finanAccountEnums.length; i++)
		{
			AFinanAccountEnum finanAccountEnum = finanAccountEnums[i];
			if (finanAccountEnum.toString().startsWith("ACCOUNTTYPE_"))
			{
				// 表示是状态的标识
				if (finanAccountEnum.getStatus() == this.accountType)
				{
					this.accountTypeStr = finanAccountEnum.getName();
					break;
				}
			}
		}
		return accountTypeStr;
	}

	public AUsers getUsers()
	{
		return users;
	}

	public void setUsers(AUsers users)
	{
		this.users = users;
	}
}
