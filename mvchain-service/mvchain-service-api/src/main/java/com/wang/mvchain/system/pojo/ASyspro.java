package com.wang.mvchain.system.pojo;

import java.util.Date;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.pojo.BasePojo;
import com.wang.mvchain.common.util.DateUtil;

/**
 * 系统配置的pojo
 * 
 * @author wangshh
 * 
 */
public class ASyspro extends BasePojo<ASyspro>
{
	private int id;
	private String name;
	private String vals;
	private String content ; 
	private byte status;
	private Date createtime;
	private Date updatetime;
	private Date pubtime;
	
	//字符串表示
	private String statusStr;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Date getCreatetime()
	{
		return createtime;
	}

	public void setCreatetime(Date createtime)
	{
		this.createtime = createtime;
	}

	public Date getUpdatetime()
	{
		return updatetime;
	}

	public void setUpdatetime(Date updatetime)
	{
		this.updatetime = updatetime;
	}

	public Date getPubtime()
	{
		return pubtime;
	}

	public void setPubtime(Date pubtime)
	{
		this.pubtime = pubtime;
	}

	public String getStatusStr()
	{
		/*
		 * 状态，0禁用， 1启用
		 */
		ACoinrateEnum[] coinrateEnums = ACoinrateEnum.values();
		for (int i = 0; i < coinrateEnums.length; i++)
		{
			ACoinrateEnum coinrateEnum = coinrateEnums[i];
			if (coinrateEnum.toString().startsWith("STATUS_"))
			{
				// 表示是状态的标识
				if (coinrateEnum.getStatus() == this.getStatus())
				{
					this.statusStr = coinrateEnum.getName();
					break;
				}
			}
		}
		return statusStr;
	}

	public JSONObject toJSON()
	{
		DateUtil dateFormatUtil = new DateUtil();
		JSONObject resultJSON = new JSONObject();
		resultJSON.put("id", this.getId() + "");
		resultJSON.put("name", this.getName() + "");
		resultJSON.put("value", this.getVals() + "");
		resultJSON.put("status", this.getStatus() + "");
		resultJSON.put("statusStr", this.getStatusStr() + "");
		resultJSON.put("createtime", dateFormatUtil.formatDateTime(this.getCreatetime()) + "");
		resultJSON.put("updatetime", dateFormatUtil.formatDateTime(this.getUpdatetime()) + "");
		return resultJSON;
	}

	@Override
	protected ASyspro parseJSON(JSONObject souJSON)
	{
		return null;
	}

	public String getVals()
	{
		return vals;
	}

	public void setVals(String vals)
	{
		this.vals = vals;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}
}