package com.wang.mvchain.users.pojo;

/**
 * 管理员的枚举
 * 
 * @author wangsh
 *
 */
public enum AUsersTaskEnum
{
	STATUS_UNSTART(Byte.valueOf("0"), "未开始"), 
	STATUS_GOING(Byte.valueOf("1"), "进行中"),
	STATUS_MANUADJ(Byte.valueOf("2"), "人工调整"),
	STATUS_CALINCOME(Byte.valueOf("3"), "计算收益"),
	
	SOUTYPE_SYSTEM(Byte.valueOf("0"), "系统创建"),
	SOUTYPE_CNYUSD(Byte.valueOf("1"), "CNY_USD"),
	SOUTYPE_USDCNY(Byte.valueOf("2"), "USD_CNY"),
	SOUTYPE_LONG(Byte.valueOf("3"), "长期搬砖");

	private byte status;
	private String name;

	private AUsersTaskEnum(byte status, String name)
	{
		this.status = status;
		this.name = name;
	}

	public byte getStatus()
	{
		return status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public String toString()
	{
		return super.toString();
	}
}
