package com.wang.mvchain.outer;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.outer.service.ITradeService;
import com.wang.mvchain.users.pojo.AUsers;
import com.wang.mvchain.users.pojo.AUsersTask;
import com.wang.mvchain.users.pojo.AUsersTaskOrder;

/**
 * 搬砖的几步操作
 * 1,抓取行情
 * 2,判断行情创建任务;
 * 3,根据任务创建买单;(以买单成交量为准)
 * 4,根据买单的情况,创建卖单的情况;(以卖单的成交量为准)
 * 5,查询卖单的情况,更新收益,计算收益;(当收益大于0时,完成交易)
 * 
 * 有几种情况需要考虑
 * 1,第一次买卖刚好全部成交
 * 2,买全部成交,卖了一部分
 * -->第二次接着卖未成交的数量
 * -->一直到买卖数量相等(计算任务收益)
 * 3,买成交部分;
 * -->卖出的数量要与买的数量相等,更新任务的买数量与卖数量
 * -->卖出的数量<买入的数量,只有当价格大于当前价格的时候,才会修改价格.(继续卖)
 * -->买卖的数量相等,(相当于此任务还是未开始的状态,接着修改)
 * -->计算收益
 * @author wangsh
 *
 */
public interface IOutTimerService
{
	/**
	 * 开始抓取所有网站的线程
	 */
	void startDeptService();
	
	/**
	 * 批量抓取交易网站数据
	 * 
	 * @param type
	 *            1:btc_cny,2:ltc_cny
	 * @return
	 */
	boolean spliderDeptService(ITradeService tradeService, JSONObject typeJSON);

	/**
	 * 更新某个用户的所有账户信息
	 */
	void updateBatchUsersInfoService(AUsers users);
	
	/**
	 * 行情比较
	 */
	Map<String, Object> operCompareService(String type , Map<String, Object> condMap);
	
	/**
	 * 行情汇率比较
	 * @param type
	 * @param paramMap
	 * @return
	 */
	Map<String, Object> deptRateService(String type , Map<String, Object> paramMap) ;
	
	/**
	 * 执行任务
	 * @param flag 执行顺序的标识
	 */
	void operTaskExecuteService(String flag);
	
	/**
	 * 买卖单同时进行
	 */
	void operTaskTradeService(AUsersTask usersTask) ;
	
	/**
	 * 准备多线程买卖单同时进行
	 * @param usersTask
	 * @param type
	 */
	void taskOrdersTradeService(AUsersTask usersTask , String type);
	
	/**
	 * 取消订单
	 * @param tradeService
	 * @param condMap
	 * @return
	 */
	boolean cancelTaskOrdersService(AUsersTaskOrder usersTaskOrder);
	
	/**
	 * 查询订单的详情
	 * @param usersTaskOrder
	 * @return
	 */
	boolean findTaskOrdersService(AUsersTaskOrder usersTaskOrder);
	
	/**
	 * 从交易中扣除手续费
	 * @param num
	 * @param usersTask
	 * @return
	 */
	Map<String, Double> removeFee(double num , AUsersTask usersTask);
	
	/**
	 * 下单操作
	 * @param tradeService
	 * @param sleep
	 * @param condMap
	 * @return
	 */
	Map<String, Object> tradeOrdersService(ITradeService tradeService , int sleep , Map<String, String> condMap);
	
	/**
	 * 查看订单的详情
	 * @param tradeService
	 * @param tradeService	处理订单的接口
	 * 		order_id:订单id;
	 * 		moneytype:交易类型
	 * @param condMap	orderid:订单id
	 * @return
	 */
	Map<String, Object> findOrdersSingle(ITradeService tradeService , Map<String, String> condMap);
	
	/**
	 * 取消订单
	 * @param tradeService
	 * @param condMap
	 * @return
	 */
	Map<String, Object> cancelOrdersService(ITradeService tradeService , Map<String, String> condMap);
}
