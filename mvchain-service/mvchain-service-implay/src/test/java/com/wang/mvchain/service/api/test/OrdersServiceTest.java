package com.wang.mvchain.service.api.test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.test.BaseTest;
import com.wang.mvchain.common.util.PageInfoUtil;
import com.wang.mvchain.users.pojo.AUsersTask;
import com.wang.mvchain.users.service.IOrdersService;

/**
 * service的测试类
 * 
 * @author zjx
 *
 */
public class OrdersServiceTest extends BaseTest
{
	private IOrdersService ordersService;
	
	/**
	 * 初始化Spring,
	 */
	@Before
	public void initSpring()
	{
		this.ordersService = (IOrdersService) ctx.getBean("ordersService");
	}
	
	/**
	 * 查询一条管理员
	 */
	@Test
	public void findOneAdminsService()
	{
		PageInfoUtil pageInfoUtil = new PageInfoUtil();
		Map<String, Object> condMap = new HashMap<String, Object>();
		List<AUsersTask> usersTasksList = this.ordersService.findCondListUsersTaskService(pageInfoUtil,condMap);
		logger.info("分页信息:{}",pageInfoUtil);
		for (Iterator iterator = usersTasksList.iterator(); iterator.hasNext();)
		{
			AUsersTask usersTask = (AUsersTask) iterator.next();
			logger.info("信息:{}",usersTask);
		}
		logger.info("分页信息:{}",pageInfoUtil);
	}
	
	/**
	 * 查询一条管理员
	 */
	@Test
	public void findOneUsersTaskService()
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "1");
		AUsersTask usersTask = this.ordersService.findOneUsersTaskService(condMap);
		usersTask.setName("22");
		JSONObject resultJSON = this.ordersService.saveOneUsersTaskService(usersTask);
		this.logger.info("----" + resultJSON);
	}
}
