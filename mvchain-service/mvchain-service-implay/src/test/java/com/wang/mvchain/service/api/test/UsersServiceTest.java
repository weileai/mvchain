package com.wang.mvchain.service.api.test;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.test.BaseTest;
import com.wang.mvchain.common.util.DateUtil;
import com.wang.mvchain.common.util.PageInfoUtil;
import com.wang.mvchain.system.pojo.ACoinrate;
import com.wang.mvchain.system.pojo.ATradeType;
import com.wang.mvchain.system.pojo.AWebsite;
import com.wang.mvchain.users.pojo.AAdmins;
import com.wang.mvchain.users.pojo.AUsers;
import com.wang.mvchain.users.pojo.AUsersAccount;
import com.wang.mvchain.users.pojo.AUsersTask;
import com.wang.mvchain.users.pojo.AUsersTaskOrder;
import com.wang.mvchain.users.service.IOrdersService;
import com.wang.mvchain.users.service.IUsersService;

/**
 * service的测试类
 * 
 * @author zjx
 *
 */
public class UsersServiceTest extends BaseTest
{
	private IUsersService usersService;
	private IOrdersService ordersService;
	
	/**
	 * 初始化Spring,
	 */
	@Before
	public void initSpring()
	{
		this.usersService = (IUsersService) ctx.getBean("usersService");
		this.ordersService = (IOrdersService) ctx.getBean("ordersService");
	}
	
	/**
	 * 查询一条管理员
	 */
	@Test
	public void findOneAdminsService()
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "1");
		AAdmins admins = this.usersService.findOneAdminsService(condMap);
		logger.info("管理员信息:" + admins);
	}
	
	/**
	 * 查询多条管理员
	 */
	@Test
	public void findCondListAdminsService()
	{
		DateUtil dateUtil = new DateUtil();
		PageInfoUtil pageInfoUtil = new PageInfoUtil();
		pageInfoUtil.setCurrentPage(1);
		pageInfoUtil.setPageSize(1);
		Map<String, Object> condMap = new HashMap<String, Object>();
		/*condMap.put("keyword", "");
		condMap.put("st", dateUtil.formatDateTime(new Date()));
		condMap.put("ed", dateUtil.formatDateTime(new Date()));*/
		//List<AAdmins> adminsList = this.usersService.findCondListAdminsService(pageInfoUtil,condMap);
		List<AAdmins> adminsList = this.usersService.findCondListAdminsService(null,condMap);
		logger.info("分页信息:{}",pageInfoUtil);
		int count = 0 ; 
		for (Iterator iterator = adminsList.iterator(); iterator.hasNext();)
		{
			AAdmins admins = (AAdmins) iterator.next();
			logger.info(count + "---管理员信息:" + admins);
			count ++ ; 
		}
	}
	
	/**
	 * 添加一条管理员
	 */
	@Test
	public void saveOneAdminsService()
	{
		AAdmins admins = new AAdmins();
		admins.setEmail("22@22.com");
		admins.setCreatetime(new Date());
		admins.setUpdatetime(new Date());
		JSONObject resultJSON = this.usersService.saveOneAdminsService(admins);
		logger.info(resultJSON + ";管理员信息:" + admins);
	}
	
	/**
	 * 更新一条管理员
	 */
	@Test
	public void updateOneAdminsService()
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "1");
		AAdmins admins = this.usersService.findOneAdminsService(condMap);
		admins.setEmail(admins.getEmail() + "==");
		admins.setUpdatetime(new Date());
		JSONObject resultJSON = this.usersService.updateOneAdminsService(admins);
		logger.info(resultJSON + ";管理员信息:" + admins);
	}
	
	/**
	 * 查询多条管理员账号
	 */
	@Test
	public void findCondListUsersAccountService()
	{
		PageInfoUtil pageInfoUtil = new PageInfoUtil();
		Map<String, Object> condMap = new HashMap<String,Object>();
		condMap.put("groupby", "1");
		List<AUsersAccount> usersList = this.usersService.findCondListUsersAccountService(pageInfoUtil, condMap);
		logger.info(usersList.size());
	}
	
	/**
	 * 查询一个用户账户
	 */
	@Test
	public void findOneUsersAccountService()
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "1");
		AUsersAccount usersAccount = this.usersService.findOneUsersAccountService(condMap);
		logger.info("账户信息:id:{},用户名:{},余额:{}",usersAccount.getId(),usersAccount.getUsername(),usersAccount.getBalance());
		AUsers usersObj = usersAccount.getUsersObj() ; 
		logger.info("用户信息:id:{},邮箱:{}",usersObj.getId(),usersObj.getEmail());
		
		ACoinrate coinrateObj = usersAccount.getCoinrateObj() ; 
		logger.info("币种信息:id:{},名称:{}",coinrateObj.getId(),coinrateObj.getName());
		
		AWebsite websiteObj = usersAccount.getWebsiteObj() ; 
		logger.info("网站信息:id:{},名称:{}",websiteObj.getId(),websiteObj.getName());
	}
	
	/**
	 * 查询一个订单交易信息
	 */
	@Test
	public void findOneUsersTaskService()
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "1");
		AUsersTask usersTask = this.ordersService.findOneUsersTaskService(condMap);
		logger.info("订单任务信息:id:{},名称:{}",usersTask.getId(),usersTask.getName());
		AUsers usersObj = usersTask.getUsersObj() ; 
		logger.info("用户信息:id:{},邮箱:{}",usersObj.getId(),usersObj.getEmail());
		
		AWebsite buyWebsite = usersTask.getBuyWbObj();
		logger.info("买方网站信息:id:{},名称:{}",buyWebsite.getId(),buyWebsite.getName());
		
		AWebsite sellWebsite = usersTask.getSellWbObj();
		logger.info("卖方网站信息:id:{},名称:{}",sellWebsite.getId(),sellWebsite.getName());
		
		ATradeType buyTradeType = usersTask.getBuyTradeTypeObj();
		logger.info("买方交易信息:id:{},名称:{}",buyTradeType.getId(),buyTradeType.getName());
		
		ATradeType sellTradeType = usersTask.getSellTradeTypeObj();
		logger.info("卖方交易信息:id:{},名称:{}",sellTradeType.getId(),sellTradeType.getName());
	}
	
	/**
	 * 查询一个订单交易详细信息
	 */
	@Test
	public void findOneUsersTaskOrderService()
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "216093");
		AUsersTaskOrder usersTaskOrder = this.ordersService.findOneUsersTaskOrderService(condMap);
		
		logger.info("--------任务信息--------");
		/* 任务 */
		AUsersTask usersTask = usersTaskOrder.getUsersTaskObj() ; 
		
		logger.info("订单任务信息:id:{},名称:{}",usersTask.getId(),usersTask.getName());
		AUsers usersObj = usersTask.getUsersObj() ; 
		logger.info("用户信息:id:{},邮箱:{}",usersObj.getId(),usersObj.getEmail());
		
		AWebsite buyWebsite = usersTask.getBuyWbObj();
		logger.info("买方网站信息:id:{},名称:{}",buyWebsite.getId(),buyWebsite.getName());
		
		AWebsite sellWebsite = usersTask.getSellWbObj();
		logger.info("卖方网站信息:id:{},名称:{}",sellWebsite.getId(),sellWebsite.getName());
		
		ATradeType buyTradeType = usersTask.getBuyTradeTypeObj();
		logger.info("买方交易信息:id:{},名称:{}",buyTradeType.getId(),buyTradeType.getName());
		
		ATradeType sellTradeType = usersTask.getSellTradeTypeObj();
		logger.info("卖方交易信息:id:{},名称:{}",sellTradeType.getId(),sellTradeType.getName());
		
		logger.info("--------账户信息--------");
		/* 账户信息 */
		AUsersAccount usersAccount = usersTaskOrder.getUsersAccountObj();
		logger.info("账户信息:id:{},用户名:{},余额:{}",usersAccount.getId(),usersAccount.getUsername(),usersAccount.getBalance());
		usersObj = usersAccount.getUsersObj() ; 
		logger.info("用户信息:id:{},邮箱:{}",usersObj.getId(),usersObj.getEmail());
		
		ACoinrate coinrateObj = usersAccount.getCoinrateObj() ; 
		logger.info("币种信息:id:{},名称:{}",coinrateObj.getId(),coinrateObj.getName());
		
		AWebsite websiteObj = usersAccount.getWebsiteObj() ; 
		logger.info("网站信息:id:{},名称:{}",websiteObj.getId(),websiteObj.getName());
		
		logger.info("--------交易信息--------");
		ATradeType tradeTypeObj = usersTaskOrder.getTradeTypeObj();
		logger.info("卖方交易信息:id:{},名称:{}",tradeTypeObj.getId(),tradeTypeObj.getName());
	}
}
