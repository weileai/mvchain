package com.wang.mvchain.service.api.test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.wang.mvchain.common.test.BaseTest;
import com.wang.mvchain.common.util.DateUtil;
import com.wang.mvchain.stat.pojo.AStatTask;
import com.wang.mvchain.stat.service.IStatService;
import com.wang.mvchain.system.pojo.ACoinrate;
import com.wang.mvchain.users.pojo.AUsers;

/**
 * service的测试类
 * 
 * @author zjx
 *
 */
public class StatServiceTest extends BaseTest
{
	private IStatService statService;
	
	/**
	 * 初始化Spring,
	 */
	@Before
	public void initSpring()
	{
		this.statService = (IStatService) ctx.getBean("statService");
	}
	
	/**
	 * 更新任务统计
	 */
	@Test
	public void updateStatTaskService()
	{
		DateUtil dateUtil = new DateUtil();
		String dateStr = "2016-07-03 01:00:00" ; 
		Date currDate = dateUtil.parseDateTime(dateStr);
		this.statService.updateStatTaskService(currDate);
		logger.info("处理每天的job:{}",dateStr);
	}
	
	/**
	 * 查询一条信息
	 */
	@Test
	public void findOneStatTaskService()
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "1");
		AStatTask statTask = this.statService.findOneStatTaskService(condMap);
		
		logger.info("--------币种信息--------");
		AUsers usersObj = statTask.getUsersObj();
		logger.info("用户信息:id:{},名称:{}",usersObj.getId(),usersObj.getEmail());
		
		ACoinrate priftcoinObj = statTask.getPriftcoinObj();
		logger.info("币种信息:id:{},名称:{}",priftcoinObj.getId(),priftcoinObj.getName());
	}
}
