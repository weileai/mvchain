package com.wang.mvchain.service.api.test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.test.BaseTest;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.common.util.DateUtil;
import com.wang.mvchain.common.util.PageInfoUtil;
import com.wang.mvchain.system.pojo.ACoinrate;
import com.wang.mvchain.system.pojo.ADept;
import com.wang.mvchain.system.pojo.ADeptEnum;
import com.wang.mvchain.system.pojo.ATradeType;
import com.wang.mvchain.system.pojo.AWebsite;
import com.wang.mvchain.system.service.IWebsiteService;

/**
 * service的测试类
 * 
 * @author zjx
 *
 */
public class WebsiteServiceTest extends BaseTest
{
	private Logger logger = LogManager.getLogger(WebsiteServiceTest.class);
	private DateUtil dateUtil = new DateUtil();

	private IWebsiteService websiteService;
	
	/**
	 * 初始化Spring,
	 */
	@Before
	public void initSpring()
	{
		this.websiteService = (IWebsiteService) ctx.getBean("websiteService");
	}
	
	/**
	 * 查询一条网站信息
	 */
	@Test
	public void findOneWebsiteService()
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "1");
		AWebsite website = this.websiteService.findOneWebsiteService(condMap);
		logger.info("网站信息:" + website);
	}
	
	/**
	 * 批量更新信息
	 */
	@Test
	public void updateBatchDeptService()
	{
		List<ADept> deptList= new ArrayList<ADept>();
		ADept dept = new ADept();
		dept.setId(3);
		dept.setCreatetime(new Date());
		dept.setUpdatetime(new Date());
		deptList.add(dept);
		ADept dept2 = new ADept();
		dept2.setId(4);
		dept2.setCreatetime(new Date());
		dept2.setUpdatetime(new Date());
		deptList.add(dept2);
		JSONObject resultJSON = this.websiteService.updateBatchDeptService("2", deptList);
		logger.info("批量入库信息;返回信息:{}",resultJSON);
	}
	
	/**
	 * 网站分组统计
	 */
	@Test
	public void findCondListCoinrateService()
	{
		Map<String, Object> condMap = new HashMap<String,Object>();
		Calendar now = Calendar.getInstance();
		
		Date edDate = now.getTime() ; 
		now.add(Calendar.SECOND, -10);
		Date stDate = now.getTime() ; 
//ConstatFinalUtil.SYS_LOG.info(stDate.toLocaleString() + "--->" + edDate.toLocaleString());
		PageInfoUtil pageInfo = new PageInfoUtil();
		pageInfo.setPageSize(100);
		//先取取所有交易网站买入价最低的.
		condMap.put("st", this.dateUtil.formatDateTime(stDate));
		condMap.put("ed", this.dateUtil.formatDateTime(edDate));
		condMap.put("ptype", ADeptEnum.PTYPE_BUY.getStatus() + "");
		condMap.put("volnumdy", ConstatFinalUtil.SYSPRO_MAP.get("orders.trade.minnum"));
		condMap.put("orderby", "priceDesc");
		condMap.put("ctid", "1");
		condMap.put("groupby", "wbid");
		List<ADept> deptList = this.websiteService.findCondListDeptService(null, condMap);
		for (Iterator iterator = deptList.iterator(); iterator.hasNext();)
		{
			ADept dept = (ADept) iterator.next();
			logger.info("交易网站信息:{}",dept);
		}
	}
	
	/**
	 *  查询一条网站信息
	 */
	@Test
	public void findOneCoinrateService()
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "1");
		ACoinrate coinrate = this.websiteService.findOneCoinrateService(condMap);
		logger.info("币种信息:" + coinrate);
		
		JSONObject extendJSON = coinrate.getExtendJSON();
		JSONObject websiteJSON = (JSONObject) extendJSON.get("website");
		for (Iterator iterator2 = websiteJSON.entrySet().iterator(); iterator2.hasNext();)
		{
			Object obj =  iterator2.next();
			Entry entry = (Entry) obj ; 
			System.out.println(entry.getKey() + "====" + entry.getValue() + "===>" + entry.getClass().getSimpleName());
		}
	}
	
	
	/**
	 * 查询交易网站信息
	 */
	@Test
	public void findOneDeptService()
	{
		Map<String, Object> condMap = new HashMap<String,Object>();
		condMap.put("id", "1");
		ADept dept = this.websiteService.findOneDeptService(condMap);
		logger.info("交易网站信息:id:{},name:{}",dept.getId(),dept.getPrice());
		
		logger.info("--------网站信息--------");
		AWebsite websiteObj = dept.getWebsiteObj() ; 
		logger.info("网站信息:id:{},名称:{}",websiteObj.getId(),websiteObj.getName());
		
		logger.info("--------交易信息--------");
		ATradeType tradeTypeObj = dept.getTradeTypeObj();
		logger.info("卖方交易信息:id:{},名称:{}",tradeTypeObj.getId(),tradeTypeObj.getName());
	}
	
	/**
	 * 查询交易网站信息
	 */
	@Test
	public void findOneTradeTypeService()
	{
		Map<String, Object> condMap = new HashMap<String,Object>();
		condMap.put("id", "1");
		ATradeType tradeType = this.websiteService.findOneTradeTypeService(condMap);
		logger.info("卖方交易信息:id:{},名称:{}",tradeType.getId(),tradeType.getName());
		
		logger.info("--------币种信息--------");
		ACoinrate fromcoinCoinObj = tradeType.getFromcoinCoinObj();
		logger.info("从信息:id:{},名称:{}",fromcoinCoinObj.getId(),fromcoinCoinObj.getName());
		
		ACoinrate tocoinCoinObj = tradeType.getTocoinCoinObj();
		logger.info("到信息:id:{},名称:{}",tocoinCoinObj.getId(),tocoinCoinObj.getName());
	}
	
	/**
	 * 查询所有网站都有的交易对 
	 */
	@Test
	public void findCommonPairService()
	{
		Map<String, JSONArray> resMap = this.websiteService.operFindCommonPairService();
		for (Iterator iterator = resMap.entrySet().iterator(); iterator.hasNext();)
		{
			Entry me = (Entry) iterator.next();
			String key = me.getKey()  + ""; 
			JSONArray valMap = (JSONArray) me.getValue() ; 
			logger.info("{},{}",key,valMap);
		}
	}
	
	/**
	 * 查询所有网站都有的交易对 
	 */
	@Test
	public void operBatchCoinrateTradeTypeService()
	{
		JSONObject resJSON = this.websiteService.operBatchCoinrateTradeTypeService();
		logger.info("{}",resJSON);
	}
}
