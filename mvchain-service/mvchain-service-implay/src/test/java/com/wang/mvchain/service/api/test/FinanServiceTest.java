package com.wang.mvchain.service.api.test;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.test.BaseTest;
import com.wang.mvchain.common.util.PageInfoUtil;
import com.wang.mvchain.finan.pojo.AFinanAccount;
import com.wang.mvchain.finan.pojo.AFinanAccountEnum;
import com.wang.mvchain.finan.pojo.AFinanAsset;
import com.wang.mvchain.finan.pojo.AFinanAssetEnum;
import com.wang.mvchain.finan.pojo.AFinanHistory;
import com.wang.mvchain.finan.pojo.AFinanHistoryEnum;
import com.wang.mvchain.finan.service.IFinanService;
import com.wang.mvchain.stat.pojo.AStatTask;
import com.wang.mvchain.stat.service.IStatService;
import com.wang.mvchain.system.pojo.ACoinrate;
import com.wang.mvchain.system.pojo.AWebsite;
import com.wang.mvchain.users.pojo.AUsers;
import com.wang.mvchain.users.pojo.AUsersTask;

/**
 * service的测试类
 * 
 * @author zjx
 *
 */
public class FinanServiceTest extends BaseTest
{
	private IFinanService finanService;
	private IStatService statService;
	
	/**
	 * 初始化Spring,
	 */
	@Before
	public void initSpring()
	{
		this.finanService = (IFinanService) ctx.getBean("finanService");
		this.statService = (IStatService) ctx.getBean("statService");
	}
	
	/**
	 * 查询一条提现账户
	 */
	@Test
	public void findOneFinanAccountService()
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "1");
		AFinanAccount finanAccount = this.finanService.findOneFinanAccountService(condMap);
		this.logger.info("finanAccount:id:{},name:{}",finanAccount.getId(),finanAccount.getEmail());
		/* 查询用户信息 */
		AUsers users = finanAccount.getUsers() ; 
		this.logger.info("用户:id:{},name:{}",users.getId() , users.getEmail());
	}
	
	/**
	 * 添加一条提现账户
	 */
	@Test
	public void saveOneFinanAccountService()
	{
		AFinanAccount finanAccount = new AFinanAccount();
		finanAccount.setUsersid(2);
		finanAccount.setUsersName("22");
		finanAccount.setEmail("2");
		finanAccount.setContent("22");
		finanAccount.setAccountType(AFinanAccountEnum.ACCOUNTTYPE_ALIPAY.getStatus());
		finanAccount.setStatus(AFinanAccountEnum.STATUS_DISABLE.getStatus());
		finanAccount.setCreatetime(new Date());
		finanAccount.setUpdatetime(new Date());
		finanAccount.setPubtime(new Date());
		JSONObject resultJSON = this.finanService.saveOneFinanAccountService(finanAccount);
		this.logger.info("resultJSON:{}",resultJSON);
	}
	
	/**
	 * 更新一条提现账户
	 */
	@Test
	public void updateOneFinanAccountService()
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "2");
		AFinanAccount finanAccount = this.finanService.findOneFinanAccountService(condMap);
		this.logger.info("finanAccount:id:{},name:{}",finanAccount.getId(),finanAccount.getEmail());
		
		finanAccount.setUsersid(3);
		finanAccount.setUsersName("3333");
		finanAccount.setEmail("233");
		finanAccount.setContent("33");
		finanAccount.setAccountType(AFinanAccountEnum.ACCOUNTTYPE_WEIXIN.getStatus());
		finanAccount.setStatus(AFinanAccountEnum.STATUS_ENABLE.getStatus());
		finanAccount.setCreatetime(new Date());
		finanAccount.setUpdatetime(new Date());
		finanAccount.setPubtime(new Date());
		
		finanAccount.setUpdatetime(new Date());
		JSONObject resultJSON = this.finanService.updateOneFinanAccountService(finanAccount);
		this.logger.info("resultJSON:{}",resultJSON);
	}
	
	/**
	 * 查询多条提现账户
	 */
	@Test
	public void findCondListFinanAccountService()
	{
		Calendar calendar = Calendar.getInstance() ; 
		Date edDate = calendar.getTime() ; 
		calendar.set(Calendar.MONTH, -1);
		Date stDate = calendar.getTime() ; 
		
		PageInfoUtil pageInfoUtil = new PageInfoUtil() ; 
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		condMap.put("keyword", "2");
		condMap.put("status", "1");
		condMap.put("stDate", stDate);
		condMap.put("edDate", edDate);
		
		List<AFinanAccount> finanAccountList = this.finanService.findCondListFinanAccountService(pageInfoUtil, condMap);
		for (Iterator iterator = finanAccountList.iterator(); iterator.hasNext();)
		{
			AFinanAccount finanAccount = (AFinanAccount) iterator.next();
			this.logger.info("finanAccount:id:{},name:{}",finanAccount.getId(),finanAccount.getEmail());
		}
		this.logger.info("分页信息:总条数:{},总页数:{},当前页:{}",pageInfoUtil.getTotalRecord(),pageInfoUtil.getTotalPage(),pageInfoUtil.getCurrentPage());
	}
	
	/**
	 * 查询一条资产账户
	 */
	@Test
	public void findOneFinanAssetService()
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "1");
		AFinanAsset finanAsset = this.finanService.findOneFinanAssetService(condMap);
		this.logger.info("finanAsset:id:{},balance:{}",finanAsset.getId(),finanAsset.getBalance());
		/* 查询用户信息 */
		AUsers users = finanAsset.getUsers() ; 
		this.logger.info("用户:id:{},name:{}",users.getId() , users.getEmail());
		/* 查询用户信息 */
		ACoinrate coinrate = finanAsset.getCoinrate();
		this.logger.info("币种:id:{},name:{}",coinrate.getId() , coinrate.getName());
	}
	
	/**
	 * 添加一条资产账户
	 */
	@Test
	public void saveOneFinanAssetService()
	{
		AFinanAsset finanAsset = new AFinanAsset();
		finanAsset.setUsersid(2);
		finanAsset.setCoinid(2);
		finanAsset.setBalance(2);
		finanAsset.setCnyPrift(2);
		finanAsset.setTodayPrift(2);
		finanAsset.setTotalPrift(2);
		finanAsset.setStatus(AFinanAssetEnum.STATUS_DISABLE.getStatus());
		finanAsset.setCreatetime(new Date());
		finanAsset.setUpdatetime(new Date());
		finanAsset.setPubtime(new Date());
		JSONObject resultJSON = this.finanService.saveOneFinanAssetService(finanAsset);
		this.logger.info("resultJSON:{}",resultJSON);
	}
	
	/**
	 * 更新一条资产账户
	 */
	@Test
	public void updateOneFinanAssetService()
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "2");
		AFinanAsset finanAsset = this.finanService.findOneFinanAssetService(condMap);
		this.logger.info("finanAsset:id:{},balance:{}",finanAsset.getId(),finanAsset.getBalance());
		
		finanAsset.setUsersid(4);
		
		finanAsset.setUsersid(4);
		finanAsset.setCoinid(4);
		finanAsset.setBalance(4);
		finanAsset.setCnyPrift(4);
		finanAsset.setTodayPrift(4);
		finanAsset.setTotalPrift(4);
		
		finanAsset.setStatus(AFinanAssetEnum.STATUS_ENABLE.getStatus());
		finanAsset.setCreatetime(new Date());
		finanAsset.setUpdatetime(new Date());
		finanAsset.setPubtime(new Date());
		
		finanAsset.setUpdatetime(new Date());
		JSONObject resultJSON = this.finanService.updateOneFinanAssetService(finanAsset);
		this.logger.info("resultJSON:{}",resultJSON);
	}
	
	/**
	 * 查询多条资产账户
	 */
	@Test
	public void findCondListFinanAssetService()
	{
		Calendar calendar = Calendar.getInstance() ; 
		Date edDate = calendar.getTime() ; 
		calendar.set(Calendar.MONTH, -1);
		Date stDate = calendar.getTime() ; 
		
		PageInfoUtil pageInfoUtil = new PageInfoUtil() ; 
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		condMap.put("keyword", "2");
		condMap.put("status", "1");
		condMap.put("stDate", stDate);
		condMap.put("edDate", edDate);
		
		List<AFinanAsset> finanAssetList = this.finanService.findCondListFinanAssetService(pageInfoUtil, condMap);
		for (Iterator iterator = finanAssetList.iterator(); iterator.hasNext();)
		{
			AFinanAsset finanAsset = (AFinanAsset) iterator.next();
			this.logger.info("finanAsset:id:{},balance:{}",finanAsset.getId(),finanAsset.getBalance());
		}
		this.logger.info("分页信息:总条数:{},总页数:{},当前页:{}",pageInfoUtil.getTotalRecord(),pageInfoUtil.getTotalPage(),pageInfoUtil.getCurrentPage());
	}
	
	/**
	 * 查询一条交易历史
	 */
	@Test
	public void findOneFinanHistoryService()
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "1");
		AFinanHistory finanHistory = this.finanService.findOneFinanHistoryService(condMap);
		this.logger.info("finanHistory:id:{},name:{}",finanHistory.getId(),finanHistory.getUsersName());
		/* 资产账户信息 */
		AFinanAsset finanAsset = finanHistory.getFinanAsset();
		this.logger.info("finanAsset:id:{},balance:{}",finanAsset.getId(),finanAsset.getBalance());
		/* 交易网站信息 */
		AWebsite website = finanHistory.getWebsite();
		this.logger.info("website:id:{},name:{}",website.getId(),website.getName());
		/* 资产账户信息 */
		AFinanAccount finanAccount = finanHistory.getFinanAccount();
		this.logger.info("finanAccount:id:{},name:{}",finanAccount.getId(),finanAccount.getEmail());
		/* 资产账户信息 */
		AUsersTask usersTask = finanHistory.getUsersTask();
		this.logger.info("usersTask:id:{},name:{}",usersTask.getId(),usersTask.getName());
	}
	
	/**
	 * 添加一条交易历史
	 */
	@Test
	public void saveOneFinanHistoryService()
	{
		AFinanHistory finanHistory = new AFinanHistory();
		finanHistory.setAssetId(1);
		finanHistory.setAccountId(1);
		finanHistory.setWbId(1);
		finanHistory.setTaskId(1);
		finanHistory.setUsersName("22");
		finanHistory.setMoney(1);
		finanHistory.setPercent(0.1);
		finanHistory.setContent("1");
		finanHistory.setTradeType(AFinanHistoryEnum.TRADETYPE_IN.getStatus());
		finanHistory.setHistoryType(AFinanHistoryEnum.HISTORYTYPE_PRIFT.getStatus());
		finanHistory.setStatus(AFinanHistoryEnum.STATUS_PROCCED.getStatus());
		finanHistory.setCreatetime(new Date());
		finanHistory.setUpdatetime(new Date());
		finanHistory.setPubtime(new Date());
		JSONObject resultJSON = this.finanService.saveOneFinanHistoryService(finanHistory);
		this.logger.info("resultJSON:{}",resultJSON);
	}
	
	/**
	 * 更新一条交易历史
	 */
	@Test
	public void updateOneFinanHistoryService()
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "2");
		AFinanHistory finanHistory = this.finanService.findOneFinanHistoryService(condMap);
		this.logger.info("finanHistory:id:{},name:{}",finanHistory.getId(),finanHistory.getUsersName());
		
		finanHistory.setAssetId(2);
		finanHistory.setAccountId(2);
		finanHistory.setWbId(2);
		finanHistory.setTaskId(12);
		finanHistory.setUsersName("22");
		finanHistory.setMoney(2);
		finanHistory.setPercent(0.2);
		finanHistory.setContent("2");
		finanHistory.setTradeType(AFinanHistoryEnum.TRADETYPE_OUT.getStatus());
		finanHistory.setHistoryType(AFinanHistoryEnum.HISTORYTYPE_WITHDRAW.getStatus());
		finanHistory.setStatus(AFinanHistoryEnum.STATUS_PROCCED.getStatus());
		
		finanHistory.setCreatetime(new Date());
		finanHistory.setUpdatetime(new Date());
		finanHistory.setPubtime(new Date());
		
		finanHistory.setUpdatetime(new Date());
		JSONObject resultJSON = this.finanService.updateOneFinanHistoryService(finanHistory);
		this.logger.info("resultJSON:{}",resultJSON);
	}
	
	/**
	 * 查询多条交易历史
	 */
	@Test
	public void findCondListFinanHistoryService()
	{
		Calendar calendar = Calendar.getInstance() ; 
		Date edDate = calendar.getTime() ; 
		calendar.set(Calendar.MONTH, -1);
		Date stDate = calendar.getTime() ; 
		
		PageInfoUtil pageInfoUtil = new PageInfoUtil() ; 
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		condMap.put("keyword", "2");
		condMap.put("status", "1");
		condMap.put("stDate", stDate);
		condMap.put("edDate", edDate);
		
		List<AFinanHistory> finanHistoryList = this.finanService.findCondListFinanHistoryService(pageInfoUtil, condMap);
		for (Iterator iterator = finanHistoryList.iterator(); iterator.hasNext();)
		{
			AFinanHistory finanHistory = (AFinanHistory) iterator.next();
			this.logger.info("finanHistory:id:{},name:{}",finanHistory.getId(),finanHistory.getUsersName());
		}
		this.logger.info("分页信息:总条数:{},总页数:{},当前页:{}",pageInfoUtil.getTotalRecord(),pageInfoUtil.getTotalPage(),pageInfoUtil.getCurrentPage());
	}
	
	/**
	 * 重新计算百分比
	 */
	@Test
	public void operAssetPercentSyncService()
	{
		this.finanService.operAssetPercentSyncService();
	}
	
	/**
	 * 重新计算任务
	 */
	@Test
	public void operAssetPriftService()
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", "1");
		AStatTask statTask = this.statService.findOneStatTaskService(condMap);
		this.finanService.operAssetPriftService(statTask);
	}
	
	@Test
	public void operAssetBatchService()
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.clear();
		condMap.put("operType","todayprift");
		this.finanService.operAssetBatchService(condMap);
	}
}
