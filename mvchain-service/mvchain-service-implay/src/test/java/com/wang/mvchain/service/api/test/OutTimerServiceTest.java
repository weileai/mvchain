package com.wang.mvchain.service.api.test;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.wang.mvchain.common.test.BaseTest;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.outer.IOutTimerService;
import com.wang.mvchain.system.pojo.ACoinrate;
import com.wang.mvchain.system.service.IWebsiteService;

/**
 * service的测试类
 * 
 * @author zjx
 *
 */
public class OutTimerServiceTest extends BaseTest
{
	private IOutTimerService outTimerService;
	private IWebsiteService websiteService;
	
	/**
	 * 初始化Spring,
	 */
	@Before
	public void initSpring()
	{
		this.outTimerService = (IOutTimerService) ctx.getBean("outTimerService");
		this.websiteService = (IWebsiteService) ctx.getBean("websiteService");
	}
	
	/**
	 * 测试抓取行情
	 */
	@Test
	public void startDeptService()
	{
		logger.info("-- 测试抓取行情开始 --");
		this.outTimerService.startDeptService();
		logger.info("-- 测试抓取行情结束 --");
	}
	
	/**
	 * 获取一下行情
	 */
	@Test
	public void deptCompareService()
	{
		logger.info("-- 查询行情开始 --");
		//查询币种
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.clear();
		condMap.put("cid", "6");
		// 查询交易历史数据
		Map<String, Object> resultMap = this.outTimerService
				.operCompareService("1" , condMap);
		logger.info("-- 查询抓取行情结束 --" + resultMap);
	}
	
	/**
	 * 测试代码
	 */
	@Test
	public void operTaskExecuteService()
	{
		this.outTimerService.operTaskExecuteService("first");
	}
	
	/**
	 * 搬砖测试
	 * 看看是否可以根据币种创建任务
	 */
	@Test
	public void operCompareService()
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.clear();
		condMap.put("id", "21");
		ACoinrate coinrate = this.websiteService.findOneCoinrateService(condMap);
		ConstatFinalUtil.TIMER_LOG.info("----- 币种:{}搬砖任务开始 -----", coinrate.getName());
		condMap.clear();
		condMap.put("cid", coinrate.getId() + "");
		// 查询交易历史数据
		this.outTimerService.operCompareService("2" , condMap);
		ConstatFinalUtil.TIMER_LOG.info("----- 币种:{}搬砖任务结束 -----耗时(毫秒):{}", coinrate.getName() );
	}
}
