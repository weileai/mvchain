package com.wang.mvchain.system.service.implay;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wang.mvchain.common.service.BaseServiceImplay;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.common.util.PageInfoUtil;
import com.wang.mvchain.system.dao.IARegionDAO;
import com.wang.mvchain.system.dao.IASysproDAO;
import com.wang.mvchain.system.pojo.ARegion;
import com.wang.mvchain.system.pojo.ASyspro;
import com.wang.mvchain.system.pojo.ASysproEnum;
import com.wang.mvchain.system.service.ISystemService;

/**
 * 数据模块的实现类
 * 
 * @author wangshh
 * 
 */
@Service("systemService")
public class SystemServiceImplay extends BaseServiceImplay implements ISystemService
{
	@Resource
	private IARegionDAO regionDAO;
	@Resource
	private IASysproDAO sysproDAO;
	
	/**
	 * 初始化,先执行此方法
	 */
	@PostConstruct
	public void initMehtod()
	{
		Map<String, Object> condMap = new HashMap<String,Object>();
		//加载系统配置
		condMap.clear();
		condMap.put("status", ASysproEnum.STATUS_ENABLE.getStatus());
		condMap.put("reload", "reload");
		List<ASyspro> sysproList = this.findCondListSysproService(null, condMap);
		ConstatFinalUtil.SYS_LOG.info("初始化系统配置完成:总条数:{},详情:{}",
				ConstatFinalUtil.SYSPRO_MAP.size() , ConstatFinalUtil.SYSPRO_MAP);
	}
	
	@Override
	public JSONObject saveOneRegionService(ARegion region)
	{
		JSONObject resultJSON = new JSONObject();
		int res = this.regionDAO.save(region);
		if (res > 0)
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "添加成功");
		} else
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "添加失败");
		}
		return resultJSON;
	}

	@Override
	public JSONObject updateOneRegionService(ARegion region)
	{
		JSONObject resultJSON = new JSONObject();
		int res = this.regionDAO.update(region);
		if (res > 0)
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "更新成功");
		} else
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "更新失败");
		}
		return resultJSON ; 
	}

	@Override
	public JSONObject deleteOneRegionService(ARegion region)
	{
		JSONObject resultJSON = new JSONObject();
		int res = this.regionDAO.save(region);
		if (res > 0)
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "更新成功");
		} else
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "更新失败");
		}
		return resultJSON ; 
	}

	@Override
	public List<ARegion> findCondListRegionService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap)
	{
		List<ARegion> regionList = Collections.emptyList();
		if(pageInfoUtil != null)
		{
			Page<ARegion> pageInfo = PageHelper.startPage(pageInfoUtil.getCurrentPage(), pageInfoUtil.getPageSize());
			regionList = this.regionDAO.findList(condMap);
			//设置总记录数
			pageInfoUtil.setTotalRecord(Long.valueOf(pageInfo.getTotal()).intValue());
		}else
		{
			regionList = this.regionDAO.findList(condMap);
		}
		return regionList; 
	}

	@Override
	public ARegion findOneRegionService(Map<String, Object> condMap)
	{
		ARegion region = this.regionDAO.findOne(condMap);
		return region ; 
	}

	@Override
	public List<ARegion> findTreeRegionService(ARegion region)
	{
		List<ARegion> regionList = new ArrayList<ARegion>(); 
		if(region != null)
		{
			this.findTreeRegionServiceUtil(regionList,region);
			Collections.reverse(regionList);
			
			//获得字符串表示
			StringBuffer ressb = new StringBuffer();
			for (Iterator iterator = regionList.iterator(); iterator.hasNext();)
			{
				ARegion regTemp = (ARegion) iterator.next();
				ressb.append(regTemp.getName() + ConstatFinalUtil.SPLIT_STR);
			}
			
			if(!"".equalsIgnoreCase(ressb.toString()))
			{
				ressb.delete(ressb.lastIndexOf(ConstatFinalUtil.SPLIT_STR), ressb.length());
			}
			region.setTreeName(ressb.toString());
		}
		return regionList ; 
	}
	
	/**
	 * 计算树形结构的工具方法,一级节点-->二级节点-->三级节点
	 * @param ressb
	 * @param regionid
	 */
	private void findTreeRegionServiceUtil(List<ARegion> regList, ARegion region)
	{
		if(region == null)
		{
			return ;
		}
		regList.add(region);
		if(region.getParentid() != null)
		{
			Map<String, Object> condMap = new HashMap<String, Object>();
			condMap.put("id", region.getParentid() + "");
			ARegion regionRes = this.findOneRegionService(condMap);
			//ARegion regionRes = ConstatFinalUtil.regionMap.get(region.getParentid());
			findTreeRegionServiceUtil(regList, regionRes);
		}
	}
	
	@Override
	public JSONObject saveOneSysproService(ASyspro syspro)
	{
		JSONObject resultJSON = new JSONObject();
		// 查询用户的单点登陆id是否存在
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("name", syspro.getName());
		ASyspro sysproRes = this.findOneSysproService(condMap);
		if (sysproRes != null)
		{
			resultJSON.put("code", "-1");
			resultJSON.put("info", "系统配置名称已经存在;id:" + sysproRes.getId());
			return resultJSON;
		}
		int res = this.sysproDAO.save(syspro);
		if (res > 0)
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "添加成功");
		} else
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "添加失败");
		}
		return resultJSON;
	}

	@Override
	public JSONObject updateOneSysproService(ASyspro syspro)
	{
		JSONObject resultJSON = new JSONObject();
		// 查询用户的单点登陆id是否存在
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("name", syspro.getName());
		ASyspro sysproRes = this.findOneSysproService(condMap);
		if (sysproRes != null && sysproRes.getId() != syspro.getId())
		{
			resultJSON.put("code", "-1");
			resultJSON.put("info", "系统配置名称已经存在;id:" + sysproRes.getId());
			return resultJSON;
		}
		int res = this.sysproDAO.update(syspro);
		if (res > 0)
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "更新成功");
		} else
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "更新失败");
		}
		return resultJSON ; 
	}

	@Override
	public JSONObject deleteOneSysproService(ASyspro syspro)
	{
		JSONObject resultJSON = new JSONObject();
		int res = this.sysproDAO.save(syspro);
		if (res > 0)
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "更新成功");
		} else
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "更新失败");
		}
		return resultJSON ; 
	}

	@Override
	public List<ASyspro> findCondListSysproService(PageInfoUtil pageInfoUtil , Map<String, Object> condMap)
	{
		List<ASyspro> sysproList = Collections.emptyList();
		if(pageInfoUtil != null)
		{
			Page<ASyspro> pageInfo = PageHelper.startPage(pageInfoUtil.getCurrentPage(), pageInfoUtil.getPageSize());
			sysproList = this.sysproDAO.findList(condMap);
			//设置总记录数
			pageInfoUtil.setTotalRecord(Long.valueOf(pageInfo.getTotal()).intValue());
		}else
		{
			sysproList = this.sysproDAO.findList(condMap);
		}
		if("reload".equalsIgnoreCase(condMap.get("reload") + ""))
		{
			/*ConstatFinalUtil.SYSPRO_MAP.clear();*/
			for (Iterator iterator = sysproList.iterator(); iterator.hasNext();)
			{
				ASyspro syspro = (ASyspro) iterator.next();
				ConstatFinalUtil.SYSPRO_MAP.put(syspro.getName(), syspro.getVals());
			}
			ConstatFinalUtil.SYS_LOG.info("重新加载系统配置完成:总条数:{},详情:{}",
					ConstatFinalUtil.SYSPRO_MAP.size() , ConstatFinalUtil.SYSPRO_MAP);
		}
		return sysproList; 
	}

	@Override
	public ASyspro findOneSysproService(Map<String, Object> condMap)
	{
		ASyspro syspro = this.sysproDAO.findOne(condMap);
		return syspro ; 
	}
}
