package com.wang.mvchain.finan.dao;

import com.wang.mvchain.common.dao.IBaseDAO;
import com.wang.mvchain.finan.pojo.AFinanAccount;

/**
 * 提现账户
 * @author wangshMac
 *
 */
public interface IAFinanAccountDao extends IBaseDAO<AFinanAccount>
{
	
}
