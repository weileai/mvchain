package com.wang.mvchain.users.dao;

import com.wang.mvchain.common.dao.IBaseDAO;
import com.wang.mvchain.users.pojo.AAdmins;

/**
 * 管理员表的dao接口
 * 
 * @author zjx
 *
 */
public interface IAAdminsDAO extends IBaseDAO<AAdmins>
{
	
}
