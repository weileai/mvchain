package com.wang.mvchain.users.dao;

import java.util.List;
import java.util.Map;

import com.wang.mvchain.common.dao.IBaseDAO;
import com.wang.mvchain.users.pojo.AUsersAccount;

/**
 * 管理员表的dao接口
 * 
 * @author zjx
 *
 */
public interface IAUsersAccountDAO extends IBaseDAO<AUsersAccount>
{
	/**
	 * 更新收益
	 * @return
	 */
	int updatePrift(AUsersAccount usersAccount);
	
	/**
	 * 根据网站id,更新用户和认证信息
	 * @param usersAccount
	 * @return
	 */
	int updateAuditInfo(AUsersAccount usersAccount);
	
	/**
	 * 更新,更新时间
	 * @param usersAccount
	 * @return
	 */
	int updateUpdateTime(AUsersAccount usersAccount);
	
	/**
	 * 分组统计
	 * @param pageInfoUtil
	 * @param map
	 * @return
	 */
	List<Map> findStatList(Map<String, Object> map);
}
