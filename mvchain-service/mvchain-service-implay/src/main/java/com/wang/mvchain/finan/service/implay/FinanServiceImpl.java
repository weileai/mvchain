package com.wang.mvchain.finan.service.implay;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.wang.mvchain.common.service.BaseServiceImplay;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.common.util.HtmlParserUtil;
import com.wang.mvchain.common.util.PageInfoUtil;
import com.wang.mvchain.finan.dao.IAFinanAccountDao;
import com.wang.mvchain.finan.dao.IAFinanAssetDao;
import com.wang.mvchain.finan.dao.IAFinanHistoryDao;
import com.wang.mvchain.finan.pojo.AFinanAccount;
import com.wang.mvchain.finan.pojo.AFinanAsset;
import com.wang.mvchain.finan.pojo.AFinanHistory;
import com.wang.mvchain.finan.pojo.AFinanHistoryEnum;
import com.wang.mvchain.finan.service.IFinanService;
import com.wang.mvchain.outer.IOutTimerService;
import com.wang.mvchain.stat.pojo.AStatTask;
import com.wang.mvchain.system.pojo.ADept;
import com.wang.mvchain.users.pojo.AUsers;
import com.wang.mvchain.users.service.IUsersService;

/**
 * 资产账户列表
 * @author wangshMac
 *
 */
@Service("finanService")
public class FinanServiceImpl extends BaseServiceImplay implements IFinanService
{
	@Resource
	private IAFinanAccountDao finanAccountDao;
	@Resource
	private IAFinanAssetDao finanAssetDao;
	@Resource
	private IAFinanHistoryDao finanHistoryDao;
	
	@Resource
	private IUsersService usersService;
	@Resource
	private IOutTimerService outTimerService;
	
	@Override
	public JSONObject saveOneFinanAccountService(AFinanAccount finanAccounts)
	{
		JSONObject resultJSON = new JSONObject();
		int res = this.finanAccountDao.save(finanAccounts);
		if (res > 0)
		{
			/* 保存一条记录 */
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("id", finanAccounts.getId() + "");
			dataJSON.put("effect", res);
			resultJSON.put("data", dataJSON);
			
			/*操作成功*/
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
		} else
		{
			/*操作失败*/
			resultJSON.put("code", "-2");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-2"));
		}
		return resultJSON;
	}

	@Override
	public JSONObject updateOneFinanAccountService(AFinanAccount finanAccounts)
	{
		JSONObject resultJSON = new JSONObject();

		int res = this.finanAccountDao.update(finanAccounts);
		if (res > 0)
		{
			/* 保存一条记录 */
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("id", finanAccounts.getId() + "");
			dataJSON.put("effect", res);
			resultJSON.put("data", dataJSON);
			
			/*操作成功*/
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
		} else
		{
			/*操作失败*/
			resultJSON.put("code", "-2");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-2"));
		}
		return resultJSON;
	}

	@Override
	public JSONObject deleteOneFinanAccountService(AFinanAccount finanAccounts)
	{
		JSONObject resultJSON = new JSONObject();
		int res = this.finanAccountDao.delete(finanAccounts);
		if (res > 0)
		{
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("id", finanAccounts.getId() + "");
			dataJSON.put("effect", res);
			resultJSON.put("data", dataJSON);
			
			/*操作成功*/
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
		} else
		{
			/*操作失败*/
			resultJSON.put("code", "-2");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-2"));
		}
		return resultJSON;
	}

	@Override
	public List<AFinanAccount> findCondListFinanAccountService(PageInfoUtil pageInfoUtil,Map<String, Object> condMap)
	{
		List<AFinanAccount> finanAccountsList = Collections.emptyList() ; 
		
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") +"%");
		}
		
		if(pageInfoUtil != null)
		{
			//分页查询数据
			Page<AFinanAccount> pageInfo = PageHelper.startPage(pageInfoUtil.getCurrentPage(), pageInfoUtil.getPageSize());
			finanAccountsList = this.finanAccountDao.findList(condMap);
			//设置总记录数
			pageInfoUtil.setTotalRecord(Long.valueOf(pageInfo.getTotal()).intValue());
		}else
		{
			finanAccountsList = this.finanAccountDao.findList(condMap);
		}
		return finanAccountsList;
	}

	@Override
	public AFinanAccount findOneFinanAccountService(Map<String, Object> condMap)
	{
		return this.finanAccountDao.findOne(condMap);
	}
	
	@Override
	public JSONObject saveOneFinanAssetService(AFinanAsset finanAssets)
	{
		JSONObject resultJSON = new JSONObject();
		/* 按照usersId和coinId查询的时候不能重复 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.clear();
		condMap.put("usersid", finanAssets.getUsersid());
		condMap.put("coinid", finanAssets.getCoinid());
		
		PageInfoUtil pageInfoUtil = new PageInfoUtil() ;
		this.findCondListFinanAssetService(pageInfoUtil, condMap);
		if(pageInfoUtil.getTotalRecord() > 0 )
		{
			/*操作成功*/
			resultJSON.put("code", "13");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("13"));
			return resultJSON ; 
		}
		
		int res = this.finanAssetDao.save(finanAssets);
		if (res > 0)
		{
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("id", finanAssets.getId() + "");
			dataJSON.put("effect", res);
			resultJSON.put("data", dataJSON);
			
			/*操作成功*/
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
		} else
		{
			/*操作失败*/
			resultJSON.put("code", "-2");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-2"));
		}
		return resultJSON;
	}

	@Override
	public JSONObject updateOneFinanAssetService(AFinanAsset finanAssets)
	{
		JSONObject resultJSON = new JSONObject();
		/* 按照usersId和coinId查询的时候不能重复 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.clear();
		condMap.put("usersid", finanAssets.getUsersid());
		condMap.put("coinid", finanAssets.getCoinid());
		
		PageInfoUtil pageInfoUtil = new PageInfoUtil() ;
		List<AFinanAsset> assetList = this.findCondListFinanAssetService(pageInfoUtil, condMap);
		if(pageInfoUtil.getTotalRecord() > 0 )
		{
			AFinanAsset assetRes = assetList.get(0);
			if(assetRes.getId() != finanAssets.getId())
			{
				/*操作成功*/
				resultJSON.put("code", "13");
				resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("13"));
				return resultJSON ;
			}
		}

		int res = this.finanAssetDao.update(finanAssets);
		if (res > 0)
		{
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("id", finanAssets.getId() + "");
			dataJSON.put("effect", res);
			resultJSON.put("data", dataJSON);
			
			resultJSON.put("code", "0");
			resultJSON.put("info", "更新成功");
		} else
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "更新失败");
		}
		return resultJSON;
	}

	@Override
	public JSONObject deleteOneFinanAssetService(AFinanAsset finanAssets)
	{
		JSONObject resultJSON = new JSONObject();
		int res = this.finanAssetDao.delete(finanAssets);
		if (res > 0)
		{
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("id", finanAssets.getId() + "");
			dataJSON.put("effect", res);
			resultJSON.put("data", dataJSON);
			
			/*操作成功*/
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
		} else
		{
			/*操作失败*/
			resultJSON.put("code", "-2");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-2"));
		}
		return resultJSON;
	}

	@Override
	public List<AFinanAsset> findCondListFinanAssetService(PageInfoUtil pageInfoUtil,Map<String, Object> condMap)
	{
		List<AFinanAsset> finanAssetsList = Collections.emptyList() ; 
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") +"%");
		}
		if(pageInfoUtil != null)
		{
			//分页查询数据
			Page<AFinanAsset> pageInfo = PageHelper.startPage(pageInfoUtil.getCurrentPage(), pageInfoUtil.getPageSize());
			finanAssetsList = this.finanAssetDao.findList(condMap);
			//设置总记录数
			pageInfoUtil.setTotalRecord(Long.valueOf(pageInfo.getTotal()).intValue());
		}else
		{
			finanAssetsList = this.finanAssetDao.findList(condMap);
		}
		return finanAssetsList;
	}
	
	@Override
	public List<Map> findStatListService(Map<String, Object> condMap)
	{
		return this.finanAssetDao.findStatList(condMap);
	}

	@Override
	public AFinanAsset findOneFinanAssetService(Map<String, Object> condMap)
	{
		return this.finanAssetDao.findOne(condMap);
	}
	
	@Override
	public JSONObject saveOneFinanHistoryService(AFinanHistory finanHistory)
	{
		Map<String, Object> condMap = new HashMap<String,Object>();
		
		/*
		 * 更新一下价格
		 * 根据币种和交易网站查询价格
		 * */
		condMap.clear();
		condMap.put("id", finanHistory.getAssetId());
		AFinanAsset asset = this.findOneFinanAssetService(condMap);
		
		condMap.clear();
		condMap.put("cid", asset.getCoinid());
		// 查询交易历史数据
		Map<String, Object> resultMap = this.outTimerService.operCompareService("1" , condMap);
		ADept sellDept = (ADept) resultMap.get("sellDept");
		double price = 0 ; 
		if(ConstatFinalUtil.CONFIG_JSON.getString("USD_ID").equals(asset.getCoinid() + ""))
		{
			/* 如果是美金就是:1 */
			price = 1 ; 
		}else if(sellDept != null)
		{
			price = sellDept.getSouprice() ; 
		}
		/* 设置价格 */
		finanHistory.setPrice(price);
		finanHistory.setMoney(finanHistory.getPrice() * finanHistory.getNum());
		
		AUsers users = asset.getUsers() ; 
		finanHistory.setUsersName(users.getEmail());
		if(finanHistory.getContent() == null)
		{
			finanHistory.setContent(finanHistory.getContent() + ";" + finanHistory.getUsersName() + ";" + finanHistory.getTradeTypeStr() + 
					";" + finanHistory.getHistoryTypeStr());
		}
		JSONObject resultJSON = new JSONObject();
		int res = this.finanHistoryDao.save(finanHistory);
		if (res > 0)
		{
			/* 计算资产的统计 */
			operAssetPriftServiceUtil(finanHistory, condMap);
			
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("id", finanHistory.getId() + "");
			dataJSON.put("effect", res);
			resultJSON.put("data", dataJSON);
			
			/*操作成功*/
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
		} else
		{
			/*操作失败*/
			resultJSON.put("code", "-2");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-2"));
		}
		return resultJSON;
	}

	/**
	 * 根据历史来计算资产统计
	 * 
	 * @param finanHistory
	 * @param condMap
	 */
	private void operAssetPriftServiceUtil(AFinanHistory finanHistory, Map<String, Object> condMap)
	{
		if(finanHistory.getStatus() == AFinanHistoryEnum.STATUS_PROCCED.getStatus())
		{
			//查询美元汇率
			double usdRate = 1 ; 
			HtmlParserUtil htmlParserUtil = new HtmlParserUtil() ; 
			JSONObject resultUsdRateJSON = htmlParserUtil.findCoinrateRate(new JSONObject());
			/* 键为币种的id */
			JSONObject usdJSON = (JSONObject) resultUsdRateJSON.get("1");
			if(usdJSON != null)
			{
				usdRate = usdJSON.getDoubleValue("rate");
			}
			
			/* 要进行关联表的一些操作,先转钱,确定钱到账了,再进行系统增加 */
			/* 查询资产账户 */
			condMap.clear();
			condMap.put("id", finanHistory.getAssetId());
			AFinanAsset finanAssets = this.findOneFinanAssetService(condMap);
			
			/* 
			 * 计算一下总资产
			 *  */
			condMap.clear();
			condMap.put("usersid", ConstatFinalUtil.CONFIG_JSON.get("mvchain_id") + "") ;
			condMap.put("status", "1") ;
			condMap.put("groupby", "1");
			/* 指定币种 */
			condMap.put("coinid", finanAssets.getCoinid());
			/* 
			 * List中存储的是Map
			 * 币种,余额,冻结金额
			 *  */
			double money = 0 ; 
			List accountList = this.usersService.findUsersAccountStatListService(null, condMap);
			if(accountList.size() > 0 )
			{
				/* 存储的是map集合 */
				Map moneyMap = (Map) accountList.get(0);
				
				if(finanHistory.getTradeType() == AFinanHistoryEnum.TRADETYPE_IN.getStatus())
				{
					/* 更新其他信息 */
					finanAssets.setBalance(finanAssets.getBalance() + finanHistory.getNum());
					finanAssets.setUsdBalance(finanAssets.getUsdBalance() + finanHistory.getMoney());
					finanAssets.setTotalPrift(finanAssets.getTotalPrift() + finanHistory.getMoney());
					finanAssets.setTodayPrift(finanAssets.getTodayPrift() + finanHistory.getMoney());
				}else if(finanHistory.getTradeType() == AFinanHistoryEnum.TRADETYPE_OUT.getStatus())
				{
					/* 更新其他信息 */
					finanAssets.setBalance(finanAssets.getBalance() - finanHistory.getNum());
					finanAssets.setUsdBalance(finanAssets.getUsdBalance() - finanHistory.getMoney());
				}
				
				/* 该项币种的总金额 */
				money = Double.valueOf(moneyMap.get("balance") + "") + Double.valueOf(moneyMap.get("fundmoney") + "");
				if(money != 0)
				{
					double percent = finanAssets.getBalance() / money ;
					finanHistory.setPercent(percent);
				}
				
				finanAssets.setCnyPrift(finanAssets.getTotalPrift() * usdRate);
				finanAssets.setUpdatetime(new Date());
				/* 更新收益百分比 */
				this.updateOneFinanAssetService(finanAssets);
				
				/* 更新百分比 */
				this.finanHistoryDao.update(finanHistory);
			}
		}
	}

	@Override
	public JSONObject updateOneFinanHistoryService(AFinanHistory finanHistory)
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		JSONObject resultJSON = new JSONObject();
		int res = this.finanHistoryDao.update(finanHistory);
		if (res > 0)
		{
			operAssetPriftServiceUtil(finanHistory, condMap);
			
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("id", finanHistory.getId() + "");
			dataJSON.put("effect", res);
			resultJSON.put("data", dataJSON);
			
			resultJSON.put("code", "0");
			resultJSON.put("info", "更新成功");
		} else
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "更新失败");
		}
		return resultJSON;
	}

	@Override
	public JSONObject deleteOneFinanHistoryService(AFinanHistory finanHistory)
	{
		JSONObject resultJSON = new JSONObject();
		int res = this.finanHistoryDao.delete(finanHistory);
		if (res > 0)
		{
			JSONObject dataJSON = new JSONObject();
			dataJSON.put("id", finanHistory.getId() + "");
			dataJSON.put("effect", res);
			resultJSON.put("data", dataJSON);
			
			/*操作成功*/
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
		} else
		{
			/*操作失败*/
			resultJSON.put("code", "-2");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-2"));
		}
		return resultJSON;
	}

	@Override
	public List<AFinanHistory> findCondListFinanHistoryService(PageInfoUtil pageInfoUtil,Map<String, Object> condMap)
	{
		List<AFinanHistory> finanHistoryList = Collections.emptyList() ; 
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") +"%");
		}
		if(pageInfoUtil != null)
		{
			//分页查询数据
			Page<AFinanHistory> pageInfo = PageHelper.startPage(pageInfoUtil.getCurrentPage(), pageInfoUtil.getPageSize());
			finanHistoryList = this.finanHistoryDao.findList(condMap);
			//设置总记录数
			pageInfoUtil.setTotalRecord(Long.valueOf(pageInfo.getTotal()).intValue());
		}else
		{
			finanHistoryList = this.finanHistoryDao.findList(condMap);
		}
		return finanHistoryList;
	}

	@Override
	public AFinanHistory findOneFinanHistoryService(Map<String, Object> condMap)
	{
		return this.finanHistoryDao.findOne(condMap);
	}

	@Override
	public void operAssetPercentSyncService()
	{
		ConstatFinalUtil.SYS_LOG.info("====重新计算账户的百分比开始====");
		Map<String, Object> condMap = new HashMap<String, Object>();
		Map<String, Double> priceMap = new HashMap<String, Double>();
		
		//查询美元汇率
		double usdRate = 1 ; 
		HtmlParserUtil htmlParserUtil = new HtmlParserUtil() ; 
		JSONObject resultUsdRateJSON = htmlParserUtil.findCoinrateRate(new JSONObject());
		/* 键为币种的id */
		JSONObject usdJSON = (JSONObject) resultUsdRateJSON.get("1");
		if(usdJSON != null)
		{
			usdRate = usdJSON.getDoubleValue("rate");
		}
		
		/* 
		* 计算一下总资产
		*  */
		condMap.clear();
		condMap.put("usersid", ConstatFinalUtil.CONFIG_JSON.get("mvchain_id") + "") ;
		condMap.put("status", "1") ;
		condMap.put("groupby", "1");
		/* 指定币种 */
		//condMap.put("coinid", finanAssets.getCoinid());
		/* 
		 * List中存储的是Map
		 * 币种,余额,冻结金额
		 *  */
		List accountList = this.usersService.findUsersAccountStatListService(null, condMap);
		double totalMoney = 0 ; 
		/* 找到对应的总金额 */
		for (Iterator iterator2 = accountList.iterator(); iterator2.hasNext();)
		{
			Map moneyMap = (Map) iterator2.next() ;
			
			Double priceRes = priceMap.get(moneyMap.get("coinid") + "") ;
			if(priceRes == null)
			{
				/* 存储价格 */
				condMap.clear();
				condMap.put("cid", moneyMap.get("coinid"));
				double price = 0 ;
				/* 为了可以查询到金额,默认查询5次 */
				int count = 0 ; 
				while(count < 5)
				{
					// 查询交易历史数据
					Map<String, Object> resultMap = this.outTimerService.operCompareService("1" , condMap);
					ADept sellDept = (ADept) resultMap.get("sellDept");
					if(ConstatFinalUtil.CONFIG_JSON.getString("USD_ID").equals(moneyMap.get("coinid") + ""))
					{
						/* 如果是美金就是:1 */
						price = 1 ; 
					}else if(sellDept != null)
					{
						price = sellDept.getSouprice() ; 
					}
					/* 说明查询到金额了 */
					if(price > 0 )
					{
						break ; 
					}
					count ++ ; 
				}
				priceMap.put(moneyMap.get("coinid") + "",price);
				
				/* 价格的赋值 */
				priceRes = price ; 
			}
			
			/* 该项币种的总金额 */
			double money = Double.valueOf(moneyMap.get("balance") + "") + Double.valueOf(moneyMap.get("fundmoney") + "");
			/* 计算总金额 */
			totalMoney += money * priceRes ; 
		}
		
		/* 查询所有的账户资产 */
		condMap.clear();
		PageInfoUtil pageInfoUtil = new PageInfoUtil() ; 
		pageInfoUtil.setPageSize(100);
		this.findCondListFinanAssetService(pageInfoUtil, condMap);
		for(int i = 1 ; i <= pageInfoUtil.getTotalPage(); i ++)
		{
			pageInfoUtil.setCurrentPage(i);
			ConstatFinalUtil.SYS_LOG.info("====重新计算账户的百分比开始==正在进行页数:{}==",i);
			List<AFinanAsset> assetList = this.findCondListFinanAssetService(pageInfoUtil, condMap);
			for (Iterator iterator = assetList.iterator(); iterator.hasNext();)
			{
				AFinanAsset asset = (AFinanAsset) iterator.next();
				
				Double priceRes = priceMap.get(asset.getCoinid() + "") ;
				if(priceRes != 0)
				{
					/* 将价格重新按照再计算一次 */
					asset.setUsdBalance(asset.getBalance() * priceRes);
				}
				/* 该项币种的总金额 */
				if(totalMoney != 0)
				{
					double percent = asset.getUsdBalance() / totalMoney ;
					asset.setPercent(percent);
				}else
				{
					asset.setPercent(0);
				}
				
				/* 设置总收益的cny金额 */
				asset.setCnyPrift(asset.getTotalPrift() * usdRate);
				asset.setUpdatetime(new Date());
				this.updateOneFinanAssetService(asset);
			}
		}
		ConstatFinalUtil.SYS_LOG.info("====重新计算账户的百分比结束==总金额:{}==币种金额:{}",totalMoney,priceMap);
	}

	@Override
	public JSONObject operAssetPriftService(AStatTask statTask)
	{
		ConstatFinalUtil.SYS_LOG.info("====重新分配任务收益开始====");
		StringBuffer sb = new StringBuffer() ; 
		JSONObject resultJSON = new JSONObject() ; 
		/* 默认的结果 */
		resultJSON.put("code", "-2");
		resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-2"));
		
		PageInfoUtil pageInfoUtil = new PageInfoUtil() ; 
		Map<String, Object> condMap = new HashMap<String, Object>();
		/* 先查询一下,此任务是否已经计算过收益,如果计算过,直接跳过 */
		condMap.clear();
		condMap.put("taskId", statTask.getId());
		this.findCondListFinanHistoryService(pageInfoUtil, condMap);
		if(pageInfoUtil.getTotalRecord() > 0 )
		{
			ConstatFinalUtil.SYS_LOG.info("====此任务已经计算过收益了==任务id:{}==",statTask.getId());
			/* 已经存在,无法继续 */
			resultJSON.put("code", "13");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("13"));
			return resultJSON; 
		}
		
		/* 查询所有的账户资产 */
		condMap.clear();
		condMap.put("operType", "usersSum");
		List<Map> statList = this.findStatListService(condMap);
		/* 按照usersid和总的收益来计算的 */
		for (Iterator iterator = statList.iterator(); iterator.hasNext();)
		{
			Map map = (Map) iterator.next();
			String percent = map.get("percent") + "";
			/* 查询用户 */
			condMap.clear();
			condMap.put("id", map.get("usersid"));
			AUsers users = this.usersService.findOneUsersService(condMap);
			users.setPercent(Double.valueOf(percent));
			users.setUpdatetime(new Date());
			JSONObject resultDbJSON = this.usersService.updateOneUsersService(users);
			sb.append(users.getId() + "," + resultDbJSON.get("info") + ",percent:" + percent + ";");
			
			/* 查询用户资产 */
			condMap.clear();
			condMap.put("usersid", map.get("usersid"));
			condMap.put("coinid", ConstatFinalUtil.CONFIG_JSON.get("USD_ID"));
			List<AFinanAsset> assetList = this.findCondListFinanAssetService(new PageInfoUtil(), condMap);
			if(assetList.size() > 0 )
			{
				AFinanAsset asset = assetList.get(0);
				
				/* 纯收益 */
				double prift = statTask.getPrift() - statTask.getFee() ; 
				
				AFinanHistory history = new AFinanHistory() ;
				history.setAssetId(asset.getId());
				/* 任务id */
				history.setTaskId(statTask.getId());
				history.setUsersName(asset.getUsers().getEmail());
				history.setPrice(1);
				/* 收益分成 */
				history.setNum(prift * Double.valueOf(percent));
				history.setMoney(history.getPrice() * history.getNum());
				
				history.setTradeType(AFinanHistoryEnum.TRADETYPE_IN.getStatus());
				history.setHistoryType(AFinanHistoryEnum.HISTORYTYPE_PRIFT.getStatus());
				history.setStatus(AFinanHistoryEnum.STATUS_PROCCED.getStatus());
				
				history.setCreatetime(new Date());
				history.setUpdatetime(new Date());
				history.setPubtime(new Date());
				
				this.saveOneFinanHistoryService(history);
			}
		}
		ConstatFinalUtil.SYS_LOG.info("====重新分配任务收益结束====信息:{}",sb);
		resultJSON.put("code", "0");
		resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
		return resultJSON ; 
	}

	@Override
	public int operAssetBatchService(Map<String, Object> condMap)
	{
		return this.finanAssetDao.executeBatch(condMap);
	}
	
}
