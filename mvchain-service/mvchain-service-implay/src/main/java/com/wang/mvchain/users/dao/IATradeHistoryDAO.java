package com.wang.mvchain.users.dao;

import com.wang.mvchain.common.dao.IBaseDAO;
import com.wang.mvchain.users.pojo.ATradeHistory;

/**
 * 管理员表的dao接口
 * 
 * @author zjx
 *
 */
public interface IATradeHistoryDAO extends IBaseDAO<ATradeHistory>
{
	
}
