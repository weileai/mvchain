package com.wang.mvchain.users.service.implay;

import java.io.InputStream;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wang.mvchain.common.service.BaseServiceImplay;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.common.util.ExecutorServiceUtil;
import com.wang.mvchain.common.util.PageInfoUtil;
import com.wang.mvchain.outer.IOutTimerService;
import com.wang.mvchain.outer.service.ITradeService;
import com.wang.mvchain.outer.util.TradeUtil;
import com.wang.mvchain.system.pojo.ATradeType;
import com.wang.mvchain.system.pojo.AWebsite;
import com.wang.mvchain.system.service.IWebsiteService;
import com.wang.mvchain.users.dao.IATradeHistoryDAO;
import com.wang.mvchain.users.dao.IAUsersTaskDAO;
import com.wang.mvchain.users.dao.IAUsersTaskOrderDAO;
import com.wang.mvchain.users.pojo.ATradeHistory;
import com.wang.mvchain.users.pojo.ATradeHistoryEnum;
import com.wang.mvchain.users.pojo.AUsers;
import com.wang.mvchain.users.pojo.AUsersAccount;
import com.wang.mvchain.users.pojo.AUsersTask;
import com.wang.mvchain.users.pojo.AUsersTaskOrder;
import com.wang.mvchain.users.pojo.AUsersTaskOrderEnum;
import com.wang.mvchain.users.service.IOrdersService;
import com.wang.mvchain.users.service.IUsersService;
import com.wang.mvchain.util.ServiceCallableUtil;

@Service("ordersService")
public class OrdersServiceImplay extends BaseServiceImplay implements
		IOrdersService
{
	@Resource
	private IAUsersTaskDAO usersTaskDAO;
	@Resource
	private IAUsersTaskOrderDAO usersTaskOrderDAO;
	@Resource
	private IATradeHistoryDAO tradeHistoryDAO;
	
	@Resource
	private IUsersService usersService;
	@Resource
	private IWebsiteService websiteService;
	@Resource
	private IOutTimerService outTimerService;
	
	/**
	 * 添加一个用户任务表
	 */
	@Override
	public JSONObject saveOneUsersTaskService(AUsersTask usersTask)
	{
		JSONObject resultJSON = new JSONObject();
		if(usersTask.getTotalnum() <= 0)
		{
			resultJSON.put("code", "-2");
			resultJSON.put("info", "交易总量必须大于0,交易总量:" + usersTask.getTotalnum());
			return resultJSON ;
		}
		
		//第一个用户同时只能有一个任务,在运行,必须是系统创建的任务
		Map<String, Object> condMap = new HashMap<String, Object>();
		//查询用户
		condMap.clear();
		condMap.put("id", usersTask.getUsersid() + "");
		AUsers users = this.usersService.findOneUsersService(condMap);
		
		PageInfoUtil pageInfoUtil = new PageInfoUtil();
		condMap.clear();
		condMap.put("status", "1");
		condMap.put("usersid", usersTask.getUsersid() + "");
		List<AUsersTask> usersTaskPageList = this.findCondListUsersTaskService(pageInfoUtil,condMap);
		if(pageInfoUtil.getTotalRecord() >= users.getMaxtask() && usersTask.getSoutype() == 0)
		{
			resultJSON.put("code", "-1");
			resultJSON.put("info", "系统创建任务超出;当前执行任务数:" + pageInfoUtil.getTotalRecord() + ";用户最大任务数:" + users.getMaxtask());
			return resultJSON ; 
		}
		
		usersTask.setTasksellprice(usersTask.getSellprice());
		usersTask.setTaskbuyprice(usersTask.getBuyprice());
		
		int res = this.usersTaskDAO.save(usersTask);
		if (res > 0)
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "添加成功;网站深度:" + usersTask.getDeptnum() + ",交易量:" + usersTask.getTotalnum());
			
			if(usersTask.getSoutype() == 0)
			{
				//只有类型为系统创建的,才会发邮件
				//查询币种,查询交易类型
				condMap.clear();
				condMap.put("id", usersTask.getSellwbid() + "");
				AWebsite sellWebsite = this.websiteService.findOneWebsiteService(condMap);
				
				condMap.clear();
				condMap.put("id", usersTask.getBuywbid() + "");
				AWebsite buyWebsite = this.websiteService.findOneWebsiteService(condMap);
				
				//查询用户
				condMap.clear();
				condMap.put("id", usersTask.getUsersid() + "");
				AUsers usersObj = this.usersService.findOneUsersService(condMap);
				usersTask.setUsersObj(usersObj);
				
				//交易类型
				condMap.clear();
				condMap.put("id", usersTask.getSellctid() + "");
				ATradeType sellTradeType = this.websiteService.findOneTradeTypeService(condMap);
				condMap.clear();
				condMap.put("id", usersTask.getBuyctid() + "");
				ATradeType buyTradeType = this.websiteService.findOneTradeTypeService(condMap);
				
				//为用户发邮件,
				Map<String, String> emailMap = new HashMap<String, String>();
				emailMap.put("task_name", usersTask.getName());
				String webSell = "网站:" + sellWebsite.getName() + ";交易类型:" + sellTradeType.getName() + 
						";原始价格:" + usersTask.getSousellprice() + ";cny:" + usersTask.getSellprice();
				String webBuy = "网站:" + buyWebsite.getName() + ";交易类型:" + buyTradeType.getName() + 
						";原始价格:" + usersTask.getSoubuyprice() + ";cny:" + usersTask.getBuyprice() ; 
				
				emailMap.put("task_id", usersTask.getId() + "");
				emailMap.put("webSell", webSell);
				emailMap.put("webBuy", webBuy);
				emailMap.put("deptnum", usersTask.getDeptnum() + "");
				emailMap.put("totalnum", usersTask.getTotalnum() + "");
				emailMap.put("statusStr", usersTask.getStatusStr() + "");
				emailMap.put("createtime", this.dateUtil.formatDateTime(usersTask.getCreatetime()));
				emailMap.put("date", this.dateUtil.formatDateTime(new Date()));
				emailMap.put("websiteurl", ConstatFinalUtil.SYS_BUNDLE.getString("website.url"));
				
				String subject = "搬砖成功 "+ usersTask.getId() + "- btcchain " ; 
				emailMap.put("email", usersTask.getUsersObj().getEmail());
				emailMap.put("subject", subject);
				
				InputStream is = ConstatFinalUtil.class.getResourceAsStream("/template/email/usersTaskNotify.html");
				String content = this.fileUtil.readFile(is, ConstatFinalUtil.CHARSET, emailMap);
				//this.springEmailUtil.sendHTMLMail(emailMap.get("email"), subject, content);
				
				//发送邮件
				Map<String, Object> paramsMap = new HashMap<String, Object>();
				
				paramsMap.put("springEmailUtil", springEmailUtil);
				paramsMap.put("email", emailMap.get("email") + "");
				paramsMap.put("subject", subject);
				paramsMap.put("content", content);
				
				ServiceCallableUtil multiThreadUtil = new ServiceCallableUtil();
				multiThreadUtil.setParamsMap(paramsMap);
				multiThreadUtil.setOperType("sendEmailThread");
				
				ExecutorServiceUtil.submit(multiThreadUtil);
			}
		} else
		{
			resultJSON.put("code", res + "");
			resultJSON.put("info", "添加失败");
		}
		return resultJSON;
	}

	/**
	 * 更新一个用户任务表
	 */
	@Override
	public JSONObject updateOneUsersTaskService(AUsersTask usersTask)
	{
		JSONObject resultJSON = new JSONObject();

		int res = this.usersTaskDAO.update(usersTask);
		if (res > 0)
		{
			resultJSON.put("code", "1");
			resultJSON.put("info", "更新成功");
		} else
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "更新失败");
		}
		return resultJSON;
	}
	
	public JSONObject updateBatchUsersTaskService(String type,AUsersTask usersTask)
	{

		JSONObject resultJSON = new JSONObject();
		int res = 0 ; 
		if("updateWebsite".equalsIgnoreCase(type))
		{
			//仅更新收益
			res = this.usersTaskDAO.updateWebsite(usersTask);
		}else if("updateBuynum".equalsIgnoreCase(type))
		{
			res = this.usersTaskDAO.updateBuynum(usersTask);
		}else if("updateSellnum".equalsIgnoreCase(type))
		{
			res = this.usersTaskDAO.updateSellnum(usersTask);
		}
		if (res > 0)
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "更新成功,影响条数:" + res);
		} else
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "更新失败,影响条数:" + res);
		}
		return resultJSON;
	
	}

	/**
	 * 删除一个用户任务表
	 */
	@Override
	public JSONObject deleteOneUsersTaskService(AUsersTask usersTask)
	{
		JSONObject resultJSON = new JSONObject();
		int res = this.usersTaskDAO.delete(usersTask);
		if (res > 0)
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "删除成功");
		} else
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", "删除失败");
		}
		return resultJSON;
	}

	/**
	 * 查询一个用户任务表
	 */
	@Override
	public AUsersTask findOneUsersTaskService(Map<String, Object> condMap)
	{
		AUsersTask usersTask =  this.usersTaskDAO.findOne(condMap);
		return usersTask ; 
	}

	/**
	 * 查询多个用户任务表
	 */
	@Override
	public List<AUsersTask> findCondListUsersTaskService(PageInfoUtil pageInfoUtil,Map<String, Object> condMap)
	{
		List<AUsersTask> usersTaskList = Collections.emptyList() ; 
		if(pageInfoUtil != null)
		{
			//分页查询数据
			Page<AUsersTask> pageInfo = PageHelper.startPage(pageInfoUtil.getCurrentPage(), pageInfoUtil.getPageSize());
			usersTaskList = this.usersTaskDAO.findList(condMap);
			//设置总记录数
			pageInfoUtil.setTotalRecord(Long.valueOf(pageInfo.getTotal()).intValue());
		}else
		{
			usersTaskList = this.usersTaskDAO.findList(condMap);
		}
		return usersTaskList ; 
	}
	
	@Override
	public List findStatMapUsersTaskService(PageInfoUtil pageInfoUtil ,Map<String, Object> condMap)
	{
		List<AUsersTask> usersTaskList = Collections.emptyList() ; 
		if(pageInfoUtil != null)
		{
			//分页查询数据
			PageHelper.startPage(pageInfoUtil.getCurrentPage(), pageInfoUtil.getPageSize());
			usersTaskList = this.usersTaskDAO.findStatList(condMap);
			PageInfo<AUsersTask> pageInfo = new PageInfo<AUsersTask>(usersTaskList);
			usersTaskList = pageInfo.getList() ; 
			//设置总记录数
			pageInfoUtil.setTotalRecord(Long.valueOf(pageInfo.getTotal()).intValue());
		}else
		{
			usersTaskList = this.usersTaskDAO.findStatList(condMap);
		}
		return usersTaskList ; 
	}
	
	/**
	 * 添加一个用户任务订单
	 */
	@Override
	public JSONObject saveOneUsersTaskOrderService(AUsersTaskOrder usersTaskOrder)
	{
		JSONObject resultJSON = new JSONObject();
		usersTaskOrder.setCreatetime(new Date());
		usersTaskOrder.setUpdatetime(new Date());
		int res = this.usersTaskOrderDAO.save(usersTaskOrder);
		if (res > 0)
		{
			//usersTaskOrder.setId(res);
			resultJSON.put("code", "0");
			resultJSON.put("info", "添加成功,id:" + usersTaskOrder.getId());
			
			/* 挂单 */
			/*if(usersTaskOrder.getStatus() == AUsersTaskOrderEnum.STATUS_ORDER_ING.getStatus())*/
			/* 不再发送邮件了,就是让if里面的代码永远不执行 */
			if("".equalsIgnoreCase("1"))
			{
				Map<String, Object> condMap = new HashMap<String, Object>();
				//为用户发邮件,
				Map<String, String> emailMap = new HashMap<String, String>();
				
				//查询任务的详细信息
				condMap.clear();
				condMap.put("id", usersTaskOrder.getUtid() + "");
				AUsersTask usersTask = this.findOneUsersTaskService(condMap);
				
				emailMap.put("order_id", usersTaskOrder.getId() + "");
				emailMap.put("taskid", usersTask.getId() + "");
				emailMap.put("uaid", usersTaskOrder.getUaid() + "");
				emailMap.put("souprice", usersTaskOrder.getSouprice() + "");
				emailMap.put("price", usersTaskOrder.getPrice() + "");
				emailMap.put("tradeedval", usersTaskOrder.getTradeedval() + "");
				emailMap.put("totalval", usersTaskOrder.getTotalval() + "");
				emailMap.put("ordertype", usersTaskOrder.getOrdertypeStr() + "");
				emailMap.put("statusStr", usersTaskOrder.getStatusStr() + "");
				emailMap.put("createtime", this.dateUtil.formatDateTime(usersTaskOrder.getCreatetime()));
				emailMap.put("date", this.dateUtil.formatDateTime(new Date()));
				emailMap.put("websiteurl", ConstatFinalUtil.SYS_BUNDLE.getString("website.url"));
				
				String subject = "订单撤单失败;任务id:"+ usersTask.getId() +";订单id: "+ usersTaskOrder.getId() + "- btcchain " ; 
				emailMap.put("email", usersTask.getUsersObj().getEmail());
				emailMap.put("subject", subject);
				
				//File sourceFile = new File(ConstatFinalUtil.SYS_BUNDLE.getString("sys.truePath") + "/template/email/usersOrdersNotify.html");
				InputStream is = ConstatFinalUtil.class.getResourceAsStream("/template/email/usersOrdersNotify.html");
				String content = this.fileUtil.readFile(is, ConstatFinalUtil.CHARSET, emailMap);
				//this.springEmailUtil.sendHTMLMail(emailMap.get("email"), subject, content);
				
				//发送邮件
				Map<String, Object> paramsMap = new HashMap<String, Object>();
				
				paramsMap.put("springEmailUtil", springEmailUtil);
				paramsMap.put("email", emailMap.get("email") + "");
				paramsMap.put("subject", subject);
				paramsMap.put("content", content);
				
				ServiceCallableUtil multiThreadUtil = new ServiceCallableUtil();
				multiThreadUtil.setParamsMap(paramsMap);
				multiThreadUtil.setOperType("sendEmailThread");
				
				ExecutorServiceUtil.submit(multiThreadUtil);
			}
		} else
		{
			resultJSON.put("code", res + "");
			resultJSON.put("info", "添加失败");
		}
		return resultJSON;
	}

	/**
	 * 更新一个用户任务订单
	 */
	@Override
	public JSONObject updateOneUsersTaskOrderService(AUsersTaskOrder usersTaskOrder)
	{
		JSONObject resultJSON = new JSONObject();
		usersTaskOrder.setUpdatetime(new Date());
		/* 先查询一下订单 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		condMap.clear();
		condMap.put("id", usersTaskOrder.getId());
		AUsersTaskOrder usersTaskOrderRes = this.findOneUsersTaskOrderService(condMap);
		if(usersTaskOrderRes.getPriftstatus() == AUsersTaskOrderEnum.PRIFTSTATUS_CALED.getStatus())
		{
			/* 数据库中的订单已经订单过,不会再计算 */
			resultJSON.put("code", "2");
			resultJSON.put("info", "状态不正确,更新失败");
			return resultJSON ; 
		}
		int res = this.usersTaskOrderDAO.update(usersTaskOrder);
		if (res > 0)
		{
			resultJSON.put("code", res + "");
			resultJSON.put("info", "更新成功");
		} else
		{
			resultJSON.put("code", res + "");
			resultJSON.put("info", "更新失败");
		}
		return resultJSON;
	}

	/**
	 * 删除一个用户任务订单
	 */
	@Override
	public JSONObject deleteOneUsersTaskOrderService(AUsersTaskOrder usersTaskOrder)
	{
		JSONObject resultJSON = new JSONObject();
		int res = this.usersTaskOrderDAO.delete(usersTaskOrder);
		if (res > 0)
		{
			resultJSON.put("code", res + "");
			resultJSON.put("info", "删除成功");
		} else
		{
			resultJSON.put("code", res + "");
			resultJSON.put("info", "删除失败");
		}
		return resultJSON;
	}
	
	/**
	 * 删除一个用户任务订单
	 */
	@Override
	public JSONObject deleteBatchUsersTaskOrderService(Map<String, Object> condMap)
	{
		JSONObject resultJSON = new JSONObject();
		int res = this.usersTaskOrderDAO.delete(condMap);
		if (res > 0)
		{
			resultJSON.put("code", res + "");
			resultJSON.put("info", "删除成功");
		} else
		{
			resultJSON.put("code", res + "");
			resultJSON.put("info", "删除失败");
		}
		return resultJSON;
	}

	/**
	 * 查询一个用户任务订单
	 */
	@Override
	public AUsersTaskOrder findOneUsersTaskOrderService(Map<String, Object> condMap)
	{
		AUsersTaskOrder usersTaskOrder =  this.usersTaskOrderDAO.findOne(condMap);
		return usersTaskOrder ; 
	}

	/**
	 * 查询多个用户任务订单
	 */
	@Override
	public List<AUsersTaskOrder> findCondListUsersTaskOrderService(PageInfoUtil pageInfoUtil,Map<String, Object> condMap)
	{
		List<AUsersTaskOrder> usersTaskOrderList = Collections.emptyList();
		if(pageInfoUtil != null)
		{
			Page<AUsersTaskOrder> pageInfo = PageHelper.startPage(pageInfoUtil.getCurrentPage(), pageInfoUtil.getPageSize());
			usersTaskOrderList = this.usersTaskOrderDAO.findList(condMap);
			//设置总记录数
			pageInfoUtil.setTotalRecord(Long.valueOf(pageInfo.getTotal()).intValue());
		}else
		{
			usersTaskOrderList = this.usersTaskOrderDAO.findList(condMap);
		}
		return usersTaskOrderList; 
	}
	
	/**
	 * 添加一个交易历史
	 */
	@Override
	public JSONObject saveOneTradeHistoryService(ATradeHistory tradeHistory)
	{
		JSONObject resultJSON = new JSONObject();
		
		/* 查询一下:交易类型 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("name", tradeHistory.getCtName());
		ATradeType tradeType = this.websiteService.findOneTradeTypeService(condMap);
		if(tradeType != null)
		{
			tradeHistory.setCtid(tradeType.getId());
		}
		
		/* 更新一些属性 */
		tradeHistory.setTotalprice(tradeHistory.getDefprice() * tradeHistory.getTotalval());
		
		int res = this.tradeHistoryDAO.save(tradeHistory);
		if (res > 0)
		{
			//tradeHistory.setId(res);
			resultJSON.put("code", "0");
			resultJSON.put("info", "添加成功,id:" + tradeHistory.getId());
		} else
		{
			resultJSON.put("code", res + "");
			resultJSON.put("info", "添加失败");
		}
		return resultJSON;
	}

	/**
	 * 更新一个交易历史
	 */
	@Override
	public JSONObject updateOneTradeHistoryService(ATradeHistory tradeHistory)
	{
		JSONObject resultJSON = new JSONObject();
		
		/* 查询一下:交易类型 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("name", tradeHistory.getCtName());
		ATradeType tradeType = this.websiteService.findOneTradeTypeService(condMap);
		if(tradeType != null)
		{
			tradeHistory.setCtid(tradeType.getId());
		}
		
		/* 更新一些属性 */
		//tradeHistory.setTradePrice(tradeHistory.getDefprice());
		tradeHistory.setTotalprice(tradeHistory.getDefprice() * tradeHistory.getTotalval());
		
		int res = this.tradeHistoryDAO.update(tradeHistory);
		if (res > 0)
		{
			resultJSON.put("code", res + "");
			resultJSON.put("info", "更新成功");
		} else
		{
			resultJSON.put("code", res + "");
			resultJSON.put("info", "更新失败");
		}
		return resultJSON;
	}
	
	public JSONObject updateOneTradeHistoryService(ATradeHistory tradeHistory,Map<String, Object> condMap)
	{
		Map<String, String> paramsMap = new HashMap<String, String>();
		
		JSONObject resultJSON = this.updateOneTradeHistoryService(tradeHistory);
		String info = "" ; 
		/* 按照类型更新
		 * operType:更新的类型
		 * 其它的参数
		 *  */
		String operType = condMap.get("operType") + "";
		String status = condMap.get("status") + "";
		if("status".equalsIgnoreCase(operType) && status.equalsIgnoreCase(ATradeHistoryEnum.STATUS_ORDER_ING.getStatus() + ""))
		{
			/* 修改状态,修改为下单,下单之后变成了挂单 */
			/* 取到原来的状态 */
			String souStatus = condMap.get("souStatus") + "";
			if(souStatus.equalsIgnoreCase(ATradeHistoryEnum.STATUS_UNSTART.getStatus() + ""))
			{
				/* 必须是未下单
				 * ~下单
				 *  */
				paramsMap.clear();
				
				//设置交易类型
				paramsMap.put("moneytype", tradeHistory.getCtName());
				paramsMap.put("type", tradeHistory.getOrdertype() + "");
				paramsMap.put("price", tradeHistory.getDefprice() + "");
				paramsMap.put("amount", tradeHistory.getTotalval() + "");
				
				AUsersAccount usersAccount = tradeHistory.getUsersAccountObj() ; 
				//调试接口,获取当前所有的挂单
				ITradeService tradeService = TradeUtil.tradeServiceMap.get(usersAccount.getWbid() + "");
				tradeService.setAuthJSON(usersAccount.getAuthinfoJSON());
				/**
				 * 下单操作
				 */
				Map<String, Object> resultMap = 
						this.outTimerService.tradeOrdersService(tradeService, ConstatFinalUtil.SECOND * 1 , paramsMap);
				
				if(resultMap.get("order_id") != null)
				{
					/* 订单id */
					String order_id = resultMap.get("order_id") + "";
					/* 处理返回值信息 */
					tradeHistory.setOrdersn(order_id);
					tradeHistory.setStatus(ATradeHistoryEnum.STATUS_ORDER_ING.getStatus());
					/* 修改交易时间 */
					tradeHistory.setPubtime(new Date());
				}
				
			}
		}else if("status".equalsIgnoreCase(operType) && status.equalsIgnoreCase(ATradeHistoryEnum.STATUS_ORDER_KILL.getStatus() + ""))
		{
			/* 修改状态,修改为撤单,原来的状态必须为挂单 */
			/* 更新状态 */
			/* 取到原来的状态 */
			String souStatus = condMap.get("souStatus") + "";
			if(souStatus.equalsIgnoreCase(ATradeHistoryEnum.STATUS_ORDER_ING.getStatus() + ""))
			{
				/* 必须是挂单中
				 * ~取消订单
				 *  */
				paramsMap.clear();
				
				//设置交易类型
				paramsMap.put("moneytype", tradeHistory.getCtName());
				paramsMap.put("order_id", tradeHistory.getOrdersn());
				paramsMap.put("amount", tradeHistory.getTotalval() + "");
				
				AUsersAccount usersAccount = tradeHistory.getUsersAccountObj() ; 
				//调试接口,获取当前所有的挂单
				ITradeService tradeService = TradeUtil.tradeServiceMap.get(usersAccount.getWbid() + "");
				tradeService.setAuthJSON(usersAccount.getAuthinfoJSON());
				/* 取消订单 */
				Map<String, Object> resultMap = this.outTimerService.cancelOrdersService(tradeService, paramsMap);
				
				if("1".equalsIgnoreCase(resultMap.get("cancelFlag") + ""))
				{
					/* 已经撤单成功 */
					tradeHistory.setStatus(ATradeHistoryEnum.STATUS_ORDER_KILL.getStatus());
				}else if("2".equalsIgnoreCase(resultMap.get("cancelFlag") + ""))
				{
					/* 已经成交了 */
					tradeHistory.setStatus(ATradeHistoryEnum.STATUS_ORDER_FINISH.getStatus());
				}else if("equals".equalsIgnoreCase(resultMap.get("cancelFlag") + ""))
				{
					/* 已经成交了 */
					tradeHistory.setStatus(ATradeHistoryEnum.STATUS_ORDER_FINISH.getStatus());
				}
				/* 订单的成交量 */
				tradeHistory.setTradeedval(Double.valueOf(resultMap.get("deal_amount") + ""));
			}
		}else if("select".equalsIgnoreCase(operType))
		{
			/* 修改状态,修改为撤单,原来的状态必须为挂单 */
			/* 更新状态 */
			/* 取到原来的状态 */
			String souStatus = condMap.get("souStatus") + "";
			if(souStatus.equalsIgnoreCase(ATradeHistoryEnum.STATUS_ORDER_ING.getStatus() + ""))
			{
				/* 必须是挂单中
				 * ~取消订单
				 *  */
				paramsMap.clear();
				
				//设置交易类型
				paramsMap.put("moneytype", tradeHistory.getCtName());
				paramsMap.put("order_id", tradeHistory.getOrdersn());
				
				AUsersAccount usersAccount = tradeHistory.getUsersAccountObj() ; 
				//调试接口,获取当前所有的挂单
				ITradeService tradeService = TradeUtil.tradeServiceMap.get(usersAccount.getWbid() + "");
				tradeService.setAuthJSON(usersAccount.getAuthinfoJSON());
				Map<String, Object> resultMap = this.outTimerService.findOrdersSingle(tradeService, paramsMap);
				if("2".equalsIgnoreCase(resultMap.get("cancelFlag") + ""))
				{
					/* 已经成交了 */
					tradeHistory.setStatus(ATradeHistoryEnum.STATUS_ORDER_FINISH.getStatus());
				}else if("equals".equalsIgnoreCase(resultMap.get("cancelFlag") + ""))
				{
					/* 已经成交了 */
					tradeHistory.setStatus(ATradeHistoryEnum.STATUS_ORDER_FINISH.getStatus());
				}
				/* 订单订单的成交价格 */
				tradeHistory.setTradePrice(Double.valueOf(resultMap.get("tradePrice") + ""));
				/* 订单的成交量 */
				tradeHistory.setTradeedval(Double.valueOf(resultMap.get("deal_amount") + ""));
			}
		}
		/* 再更新一次 */
		resultJSON = this.updateOneTradeHistoryService(tradeHistory);
		resultJSON.put("info", resultJSON.get("info") + ";" + info);
		return resultJSON;
	}

	/**
	 * 删除一个交易历史
	 */
	@Override
	public JSONObject deleteOneTradeHistoryService(ATradeHistory tradeHistory)
	{
		JSONObject resultJSON = new JSONObject();
		int res = this.tradeHistoryDAO.delete(tradeHistory);
		if (res > 0)
		{
			resultJSON.put("code", res + "");
			resultJSON.put("info", "删除成功");
		} else
		{
			resultJSON.put("code", res + "");
			resultJSON.put("info", "删除失败");
		}
		return resultJSON;
	}
	
	/**
	 * 查询一个交易历史
	 */
	@Override
	public ATradeHistory findOneTradeHistoryService(Map<String, Object> condMap)
	{
		ATradeHistory tradeHistory =  this.tradeHistoryDAO.findOne(condMap);
		return tradeHistory ; 
	}

	/**
	 * 查询多个交易历史
	 */
	@Override
	public List<ATradeHistory> findCondListTradeHistoryService(PageInfoUtil pageInfoUtil,Map<String, Object> condMap)
	{
		if(condMap.get("keyword") != null && !"".equalsIgnoreCase(condMap.get("keyword") + ""))
		{
			condMap.put("keyword", "%" + condMap.get("keyword") + "%");
		}
		List<ATradeHistory> tradeHistoryList = Collections.emptyList();
		if(pageInfoUtil != null)
		{
			Page<ATradeHistory> pageInfo = PageHelper.startPage(pageInfoUtil.getCurrentPage(), pageInfoUtil.getPageSize());
			tradeHistoryList = this.tradeHistoryDAO.findList(condMap);
			//设置总记录数
			pageInfoUtil.setTotalRecord(Long.valueOf(pageInfo.getTotal()).intValue());
		}else
		{
			tradeHistoryList = this.tradeHistoryDAO.findList(condMap);
		}
		return tradeHistoryList; 
	}
}
