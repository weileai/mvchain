package com.wang.mvchain.finan.dao;

import java.util.List;
import java.util.Map;

import com.wang.mvchain.common.dao.IBaseDAO;
import com.wang.mvchain.finan.pojo.AFinanAsset;

/**
 * 资产账户
 * @author wangshMac
 *
 */
public interface IAFinanAssetDao extends IBaseDAO<AFinanAsset>
{
	/**
	 * 分组统计
	 * @param pageInfoUtil
	 * @param map
	 * @return
	 */
	List<Map> findStatList(Map<String, Object> map);
}
