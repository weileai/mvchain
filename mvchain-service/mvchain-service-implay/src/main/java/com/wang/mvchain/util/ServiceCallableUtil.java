package com.wang.mvchain.util;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Callable;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.common.util.SpringEmailUtil;
import com.wang.mvchain.outer.IOutTimerService;
import com.wang.mvchain.outer.service.ITradeService;
import com.wang.mvchain.stat.service.IStatService;
import com.wang.mvchain.system.pojo.ACoinrate;
import com.wang.mvchain.users.pojo.AUsers;
import com.wang.mvchain.users.pojo.AUsersTask;

/**
 * 多线程的工具类
 * 
 * @author wangsh
 *
 */
public class ServiceCallableUtil implements Callable<Object>
{
	private IOutTimerService outTimerService ;
	private IStatService statService ; 
	//操作类型
	private String operType ;
	// 存储参数的map
	private Map<String, Object> paramsMap;
	
	/**
	 * 多线程调用函数
	 */
	@Override
	public Object call() throws Exception
	{
		try
		{
			String thid = UUID.randomUUID().toString().replaceAll("-", "");
			Date createTime = new Date();
			paramsMap.put("createTime", createTime);
			paramsMap.put("thid", thid);
			//1是开始,0是停止
			paramsMap.put("status", "1");
			ConstatFinalUtil.THREAD_MAP.put(thid, paramsMap);
			
			ConstatFinalUtil.SYS_LOG.info(thid + "-----线程池开始-----");
			if("updateBatchUsersInfoService".equalsIgnoreCase(operType))
			{
				AUsers users = (AUsers) paramsMap.get("users");
				//btc批量核算
				this.outTimerService.updateBatchUsersInfoService(users);
			}else if("spiderDept".equalsIgnoreCase(operType))
			{
				//批量抓取行情
				spiderDept();
			}else if("updateStatTaskService".equalsIgnoreCase(operType))
			{
				//统计用户任务
				Date stDate = (Date) paramsMap.get("stDate");
				Date edDate = (Date) paramsMap.get("edDate");
				
				Calendar now = Calendar.getInstance();
				while(stDate.compareTo(edDate) != 1)
				{
					now.setTime(stDate);
					this.statService.updateStatTaskService(now.getTime());
					now.add(Calendar.DATE, 1);
					stDate = now.getTime() ; 
				}
			}else if("orderTrade".equalsIgnoreCase(operType))
			{
				//买卖单同时进行
				orderTrade();
			}else if("sendEmailThread".equalsIgnoreCase(operType))
			{
				//发线程发邮件
				sendEmailThread();
			}else if("mvchain".equalsIgnoreCase(operType))
			{
				//搬砖的操作
				this.mvchain();
			}
			//将线程移除掉
			ConstatFinalUtil.THREAD_MAP.remove(thid);
			ConstatFinalUtil.SYS_LOG.info(thid + "-----线程池结束-----");
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOG.error("启动多线程报错了;条件:{}", paramsMap + ";operType:" + this.operType,e);
		}
		return Long.valueOf("0");
	}
	
	/**
	 * 调用搬砖的job
	 */
	public void mvchain()
	{
		long st = System.currentTimeMillis() ; 
		ACoinrate coinrate = (ACoinrate) this.paramsMap.get("coinrate");
		ConstatFinalUtil.TIMER_LOG.info("----- 币种:{}搬砖任务开始 -----", coinrate.getName());
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.clear();
		condMap.put("cid", coinrate.getId() + "");
		// 查询交易历史数据
		this.outTimerService.operCompareService("2" , condMap);
		long ed = System.currentTimeMillis() ; 
		long cha = (ed - st ) ; 
		ConstatFinalUtil.TIMER_LOG.info("----- 币种:{}搬砖任务结束 -----耗时(毫秒):{}", coinrate.getName() , cha);
	}
	
	/**
	 * 采用多线程发邮件
	 */
	private void sendEmailThread()
	{
		SpringEmailUtil springEmailUtil = (SpringEmailUtil) paramsMap.get("springEmailUtil");
		String email = paramsMap.get("email") + "";
		String subject = paramsMap.get("subject") + "" ; 
		String content = paramsMap.get("content") + "" ; 
		springEmailUtil.sendHTMLMail(email, subject, content);
		ConstatFinalUtil.SYS_LOG.info(email + "-----发送邮件线程-----" + subject);
	}

	/**
	 * 买卖单同时进行
	 */
	private void orderTrade()
	{
		AUsersTask usersTask = (AUsersTask) paramsMap.get("usersTask");
		String type = paramsMap.get("type") + "";
		
		this.outTimerService.taskOrdersTradeService(usersTask, type);
	}

	/**
	 * 抓取行情数据
	 */
	private void spiderDept()
	{
		while(true)
		{
			long st = System.currentTimeMillis() ; 
			try
			{
				//交易的父类
				ITradeService tradeService = (ITradeService) paramsMap.get("tradeService");
				//交易网站中支持货币的json
				JSONObject val2JSON = (JSONObject) paramsMap.get("val2JSON");
				String key = val2JSON.get("key") + "";
				// btc_cny
				boolean flag = this.outTimerService.spliderDeptService(tradeService, val2JSON);
				long ed = System.currentTimeMillis() ; 
				long cha = (ed - st ) ; 
				/*if (flag)
				{
					ConstatFinalUtil.TIMER_LOG.info(key + "== "
							+ val2JSON.get("name") + "_成功抓取网站数据 =="
							+ tradeService.findid() + ";耗时(毫秒):{};网站:{}",cha , tradeService);
				} else
				{
					ConstatFinalUtil.TIMER_LOG.info(key + "== "
							+ val2JSON.get("name") + "_失败抓取网站数据 =="
							+ tradeService.findid() + ";耗时(毫秒):{};网站:{}",cha , tradeService);
				}*/
				String spiderTime = ConstatFinalUtil.SYSPRO_MAP.get("time.spider");
				if(spiderTime == null || "".equalsIgnoreCase(spiderTime))
				{
					spiderTime = "800";
				}
				Thread.sleep(Long.valueOf(spiderTime));
			} catch (Exception e)
			{
				ConstatFinalUtil.SYS_LOG.error("抓取行情线程休眠异常", e);
			}
		}
	}

	public void setParamsMap(Map<String, Object> paramsMap)
	{
		this.paramsMap = paramsMap;
	}

	public void setOutTimerService(IOutTimerService outTimerService)
	{
		this.outTimerService = outTimerService;
	}

	public void setOperType(String operType)
	{
		this.operType = operType;
	}

	public void setStatService(IStatService statService)
	{
		this.statService = statService;
	}
}
