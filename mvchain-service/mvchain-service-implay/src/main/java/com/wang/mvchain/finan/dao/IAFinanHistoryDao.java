package com.wang.mvchain.finan.dao;

import com.wang.mvchain.common.dao.IBaseDAO;
import com.wang.mvchain.finan.pojo.AFinanHistory;

/**
 * 资产账户交易历史
 * @author wangshMac
 *
 */
public interface IAFinanHistoryDao extends IBaseDAO<AFinanHistory>
{
	
}
