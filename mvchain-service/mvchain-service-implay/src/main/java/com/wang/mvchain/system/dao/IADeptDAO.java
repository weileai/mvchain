package com.wang.mvchain.system.dao;

import com.wang.mvchain.common.dao.IBaseDAO;
import com.wang.mvchain.system.pojo.ADept;

public interface IADeptDAO extends IBaseDAO<ADept>
{
}
