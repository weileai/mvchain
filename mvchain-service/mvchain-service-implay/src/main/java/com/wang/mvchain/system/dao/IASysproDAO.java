package com.wang.mvchain.system.dao;

import com.wang.mvchain.common.dao.IBaseDAO;
import com.wang.mvchain.system.pojo.ASyspro;

public interface IASysproDAO extends IBaseDAO<ASyspro>
{
}
