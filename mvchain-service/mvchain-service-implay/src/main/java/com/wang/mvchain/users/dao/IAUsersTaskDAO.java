package com.wang.mvchain.users.dao;

import java.util.List;
import java.util.Map;

import com.wang.mvchain.common.dao.IBaseDAO;
import com.wang.mvchain.users.pojo.AUsersTask;

/**
 * 管理员表的dao接口
 * 
 * @author zjx
 *
 */
public interface IAUsersTaskDAO extends IBaseDAO<AUsersTask>
{
	/**
	 * 更新网站信息
	 * @param usersTask
	 * @return
	 */
	int updateWebsite(AUsersTask usersTask);
	
	/**
	 * 更新网站信息
	 * @param usersTask
	 * @return
	 */
	int updateBuynum(AUsersTask usersTask);
	
	/**
	 * 更新网站信息
	 * @param usersTask
	 * @return
	 */
	int updateSellnum(AUsersTask usersTask);
	
	/**
	 * 统计的查询
	 * @param map
	 * @return
	 */
	List<AUsersTask> findStatList(Map<String, Object> map);
}
