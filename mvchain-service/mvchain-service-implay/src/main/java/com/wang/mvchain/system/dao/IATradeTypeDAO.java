package com.wang.mvchain.system.dao;

import com.wang.mvchain.common.dao.IBaseDAO;
import com.wang.mvchain.system.pojo.ATradeType;

public interface IATradeTypeDAO extends IBaseDAO<ATradeType>
{
	/**
	 * 更新某个币种的汇率
	 * @param tradeType
	 * @return
	 */
	int updateRate(ATradeType tradeType);
}
