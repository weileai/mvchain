package com.wang.mvchain.stat.dao;

import java.util.Map;

import com.wang.mvchain.common.dao.IBaseDAO;
import com.wang.mvchain.stat.pojo.AStatTask;

public interface IAStatTaskDAO extends IBaseDAO<AStatTask>
{
	/**
	 * 按照条件删除
	 * @param map
	 * @return
	 */
	int deleteCond(Map<String, Object> map);
}
