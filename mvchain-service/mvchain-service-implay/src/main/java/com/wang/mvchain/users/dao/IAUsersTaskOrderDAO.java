package com.wang.mvchain.users.dao;

import com.wang.mvchain.common.dao.IBaseDAO;
import com.wang.mvchain.users.pojo.AUsersTaskOrder;

/**
 * 管理员表的dao接口
 * 
 * @author zjx
 *
 */
public interface IAUsersTaskOrderDAO extends IBaseDAO<AUsersTaskOrder>
{
	
}
