<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%-- 引入自己的CSS --%>
<link href="${rootpath }/css/common.css" rel="stylesheet" type="text/css"/>
<!-- My97DatePicker日期插件 -->
<script type="text/javascript" src="${rootpath }/common/resource/My97DatePicker/WdatePicker.js"></script>
<%-- 引入自己的js --%>
<script type="text/javascript" src="${rootpath }/js/util.js"></script>