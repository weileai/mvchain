var path = "/mvchain-system";

/**
 * 处理分页操作的表单工具
 * @param {} formid 表单ID
 * @param {} currentPage	当前页值
 * @param {} currentPageid	当前页ID
 * @param {} pageSize	每页多少条值
 * @param {} pageSizeid	每页多少ID
 * @return {Boolean}
 */
function pageFormSubmit(formid,currentPage,currentPageid, pageSize, pageSizeid) {
	$("#" + formid + " input[name="+ currentPageid +"]").attr("value", currentPage);
	$("#" + formid + " input[name="+ pageSizeid +"]").attr("value", pageSize);
	
	if(typeof(navTabSearch) != 'undefined')
	{
		$("#" + formid ).bind("submit" , navTabSearch($("#" + formid )[0]));
	}else
	{
		$("#" + formid ).submit();
	}
	return false;
}