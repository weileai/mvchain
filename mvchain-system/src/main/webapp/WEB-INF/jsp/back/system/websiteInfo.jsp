<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<style>
	.pageFormContent label
	{
		float: none;
	}
</style>
<h2 class="contentTitle">网站查看</h2>
<div class="pageContent">
	<div class="pageFormContent nowrap" layoutH="97">
		<dl>
			<dt>名称：</dt>
			<dd>
				${requestScope.website.name }
			</dd>
		</dl>
		<dl>
			<dt>url：</dt>
			<dd>
				<a class="hreflink" target="_blank" href="${requestScope.website.url }">${website.url }</a>
			</dd>
		</dl>
		<dl>
			<dt>状态：</dt>
			<dd>
				${requestScope.website.statusStr}
			</dd>
		</dl>
		<c:forEach items="${requestScope.website.feeJSON }" var="me" varStatus="stat">
			<dl>
				<dt>${me.value.name }充值手续费：</dt>
				<dd>
					${me.value.recharge }
				</dd>
			</dl>
			<dl>
				<dt>${me.value.name }提现手续费：</dt>
				<dd>
					${me.value.withdraw }
				</dd>
			</dl>
		</c:forEach>
		<dl>
			<dt>发布时间：</dt>
			<dd>
				<fmt:formatDate value="${requestScope.website.pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
			</dd>
		</dl>
		<dl>
			<dt>创建时间：</dt>
			<dd>
				<fmt:formatDate value="${requestScope.website.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/> 
			</dd>
		</dl>
		<dl>
			<dt>更新时间：</dt>
			<dd>
				<fmt:formatDate value="${requestScope.website.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/>
			</dd>
		</dl>
		<dl>
			<dt>支持的交易类型：</dt>
			<dd>
				<%-- 判断选中的交易类型 --%>
				<c:forEach items="${requestScope.website.moneyStatusJSON }" var="me" varStatus="stat1">
					${stat1.count }、${me.value.name };交易手续费:${me.value.tradeFee }<br/>
				</c:forEach>
			</dd>
		</dl>
		<dl>
			<dt>接口认证模板：</dt>
			<dd>
				${requestScope.website.authinfo }
			</dd>
		</dl>
	</div>
</div>