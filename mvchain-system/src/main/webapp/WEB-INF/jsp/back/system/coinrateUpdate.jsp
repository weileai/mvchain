<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<style>
	.pageFormContent label
	{
		float: none;
	}
</style>
<h2 class="contentTitle">币种更新</h2>
<div class="pageContent">
	<form method="post" action="${rootpath }/back/system/coinrateUpdateSubmit.html" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone);">
		<input type="hidden" name="cid" value="${requestScope.coinrate.id }"/>
		
		<input type="hidden" name="callbackType" value="">
		<input type="hidden" name="navTabId" value="">
		<input type="hidden" name="forwardUrl" value="">
		
		<div class="pageFormContent nowrap" layoutH="97">
			<dl>
				<dt>名称：</dt>
				<dd>
					<input type="text" name="name" size="50" value="${requestScope.coinrate.name }" class="required"  alt="请输入您的名称"/>
					<span class="info"></span>
				</dd>
			</dl>
			<dl>
				<dt>英文名称：</dt>
				<dd>
					<input type="text" name="engname" size="50" value="${requestScope.coinrate.engname }" class="required"  alt="请输入您的英文名称"/>
					<span class="info"></span>
				</dd>
			</dl>
			<dl>
				<dt>发布时间：</dt>
				<dd>
					<c:set value="${sys_now }" var="pubtime"/>
					<c:if test="${requestScope.coinrate.pubtime != null }">
						<c:set value="${requestScope.coinrate.pubtime}" var="pubtime"/>
					</c:if>
					<input type="text" name="pubtimeCond" value="<fmt:formatDate value="${pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/>" 
						onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" />
					<span class="info">用来排序</span>
				</dd>
			</dl>
			<dl>
				<dt>搬砖状态：</dt>
				<dd>
					<input type="radio" id="prostatusCoinrateInsert1" name="prostatus" value="1" ${requestScope.coinrate.prostatus == '1' ? 'checked' : '' }>
					<label for="prostatusCoinrateInsert1">启用</label>
					<input type="radio" id="prostatusCoinrateInsert0" name="prostatus" value="0" ${requestScope.coinrate.prostatus == '0' ? 'checked' : '' }>
					<label for="prostatusCoinrateInsert0">禁用</label>
				</dd>
			</dl>
			<dl>
				<dt>状态：</dt>
				<dd>
					<input type="radio" id="statusCoinrateUpdate1" name="status" value="1" ${requestScope.coinrate.status == '1' ? 'checked' : '' }>
					<label for="statusCoinrateUpdate1">启用</label>
					<input type="radio" id="statusCoinrateUpdate0" name="status" value="0" ${requestScope.coinrate.status == '0' ? 'checked' : '' }>
					<label for="statusCoinrateUpdate0">禁用</label>
				</dd>
			</dl>
			<dl>
				<dt>扩展信息：</dt>
				<dd>
					<textarea name="extend" rows="10" cols="80">${requestScope.coinrate.extend }</textarea>
					<span class="info"></span>
				</dd>
			</dl>
			<dl>
				<dt>备注：</dt>
				<dd>
					<textarea name="content" rows="10" cols="60">${requestScope.coinrate.content }</textarea>
					<span class="info"></span>
				</dd>
			</dl>
		</div>
		<div class="formBar">
			<ul>
				<!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
				</li>
			</ul>
		</div>
	</form>
</div>