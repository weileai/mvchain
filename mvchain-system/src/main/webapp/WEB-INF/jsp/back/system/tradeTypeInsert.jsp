<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<style>
	.pageFormContent label
	{
		float: none;
	}
</style>
<h2 class="contentTitle">币种添加</h2>
<div class="pageContent">
	<form method="post" action="${rootpath }/back/system/tradeTypeInsertSubmit.html" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone);">
	
		<input type="hidden" name="callbackType" value="">
		<input type="hidden" name="navTabId" value="">
		<input type="hidden" name="forwardUrl" value="">
		
		<div class="pageFormContent nowrap" layoutH="97">
			<dl>
				<dt>名称：</dt>
				<dd>
					<input type="text" name="name" size="50" value="${requestScope.coinrate.name }" class="required"  alt="请输入您的名称"/>
					<span class="info"></span>
				</dd>
			</dl>
			<dl>
				<dt>币种(从)：</dt>
				<dd>
					<select name="fromcoinid" class="required">
						<option value="">请选择</option>
						<c:forEach items="${requestScope.coinrateList }" var="coinrate" varStatus="stat">
							<option value="${coinrate.id }" ${requestScope.tradeType.fromcoinid == coinrate.id ? 'selected' : '' }>${coinrate.engname }(${coinrate.name })</option>
						</c:forEach>
					</select>
				</dd>
			</dl>
			<dl>
				<dt>币种(到)：</dt>
				<dd>
					<select name="tocoinid" class="required">
						<option value="">请选择</option>
						<c:forEach items="${requestScope.coinrateList }" var="coinrate" varStatus="stat">
							<option value="${coinrate.id }" ${requestScope.tradeType.tocoinid == coinrate.id ? 'selected' : '' }>${coinrate.engname }(${coinrate.name })</option>
						</c:forEach>
					</select>
				</dd>
			</dl>
			<dl>
				<dt>汇率：</dt>
				<dd>
					<input type="text" name="rate" size="50" value="${requestScope.tradeType.rate }" class="required"  alt="请输入您的汇率"/>
					<span class="info"></span>
				</dd>
			</dl>
			<dl>
				<dt>发布时间：</dt>
				<dd>
					<c:set value="${sys_now }" var="pubtime"/>
					<c:if test="${requestScope.coinrate.pubtime != null }">
						<c:set value="${requestScope.coinrate.pubtime}" var="pubtime"/>
					</c:if>
					<input type="text" name="pubtimeCond" value="<fmt:formatDate value="${pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/>" 
						onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" />
					<span class="info">用来排序</span>
				</dd>
			</dl>
			<dl>
				<dt>状态：</dt>
				<dd>
					<input type="radio" id="statusCoinrateInsert1" name="status" value="1" checked="checked">
					<label for="statusCoinrateInsert1">启用</label>
					<input type="radio" id="statusCoinrateInsert0" name="status" value="0" >
					<label for="statusCoinrateInsert0">禁用</label>
				</dd>
			</dl>
			<dl>
				<dt>内容：</dt>
				<dd>
					<textarea name="content" rows="10" cols="80">${requestScope.coinrate.content }</textarea>
					<span class="info"></span>
				</dd>
			</dl>
		</div>
		<div class="formBar">
			<ul>
				<!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
				</li>
			</ul>
		</div>
	</form>
</div>