<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<div class="pageHeader">
	<form onsubmit="return navTabSearch(this);" action="${rootpath }/back/system/tradeTypeList.html"
		method="post">
		<div class="searchBar">
			<table class="searchContent">
				<tr>
					<td colspan="2">
						关键字：<input type="text" name="keyword" value="${requestScope.keyword }"/>
						状态：
						<select name="statusList" class="required">
							<option value="">请选择</option>
							<option value="1" ${requestScope.statusList == '1' ? 'selected' : '' }>启用</option>
							<option value="0" ${requestScope.statusList == '0' ? 'selected' : '' }>禁用</option>
						</select>
						币种(从)：
						<select name="fromcoinidList" class="required">
							<option value="">请选择</option>
							<c:forEach items="${requestScope.coinrateList }" var="coinrate" varStatus="stat">
								<option value="${coinrate.id }" ${requestScope.fromcoinidList == coinrate.id ? 'selected' : '' }>${coinrate.engname }(${coinrate.name })</option>
							</c:forEach>
						</select>
						币种(到)：
						<select name="tocoinidList" class="required">
							<option value="">请选择</option>
							<c:forEach items="${requestScope.coinrateList }" var="coinrate" varStatus="stat">
								<option value="${coinrate.id }" ${requestScope.tocoinidList == coinrate.id ? 'selected' : '' }>${coinrate.engname }(${coinrate.name })</option>
							</c:forEach>
						</select>
					</td>
				</tr>
			</table>
			<div class="subBar">
				<table class="searchContent">
					<td>
						发布时间：
						<input type="text" name="st" value="${requestScope.st }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" />
						<input type="text" name="ed" value="${requestScope.ed }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" />
					</td>
					<td align="right">
						<div class="buttonActive">
							<div class="buttonContent">
								<button type="submit">查询</button>
							</div>
						</div>
					</td>
				</table>
			</div>
		</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li>
				<a class="edit" href="${rootpath }/back/system/tradeTypeUpdate.html?tid={sid_user}"
					 target="navTab" ref="tradeTypeInfo"><span>查看</span></a>
			</li>
			<%--
			<li>
				<a class="delete" href="${rootpath }/back/system/tradeTypeDelete.html?rid={sid_user}" target="ajaxTodo"
					title="确定要删除吗?"><span>删除</span>
				</a>
			</li>
			 --%>
			<li>
				<a class="edit" href="${rootpath }/back/system/tradeTypeUpdate.html?operType=update&tid={sid_user}"
					target="navTab"><span>修改</span></a>
			</li>
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138">
		<thead>
			<tr>
				<th width="40">序号</th>
				<th width="40">id</th>
				<th width="100">名称</th>
				<th width="40">汇率</th>
				<th width="40">状态</th>
				<th width="100">创建时间</th>
				<th width="100">更新时间</th>
				<th width="100">发布时间(排序)</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${requestScope.tradeTypeList }" var="tradeType" varStatus="stat">
				<tr target="sid_user" rel="${tradeType.id }">
					<td>${stat.count }</td>
					<td>${tradeType.id }</td>
					<td>${tradeType.name }</td>
					<td>${tradeType.rate }</td>
					<td>${tradeType.statusStr }</td>
					<td><fmt:formatDate value="${tradeType.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					<td><fmt:formatDate value="${tradeType.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					<td><fmt:formatDate value="${tradeType.pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<form onsubmit="return navTabSearch(this);" id="pageFormTradeTypeList" method="post" action="${rootpath }/back/system/tradeTypeList.html">
		<input type="hidden" name="keyword" value="${requestScope.keyword }">
		<input type="hidden" name="statusList" value="${requestScope.statusList }">
		<input type="hidden" name="fromcoinidList" value="${requestScope.fromcoinidList }">
		<input type="hidden" name="tocoinidList" value="${requestScope.tocoinidList }">
		<input type="hidden" name="st" value="${requestScope.st }">
		<input type="hidden" name="ed" value="${requestScope.ed }">
		<div class="panelBar">
			<ul style="float: right;">
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormTradeTypeList','1','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>首页</span></a>
				</li>
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormTradeTypeList','${requestScope.pageInfo.prePage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>上一页</span></a>
				</li>
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormTradeTypeList','${requestScope.pageInfo.nextPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>下一页</span></a>
				</li>
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormTradeTypeList','${requestScope.pageInfo.totalPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>末页</span></a>
				</li>
				<li>
					第<input type="text" id="currentPage" name="currentPage" value="${requestScope.pageInfo.currentPage}" size="5" maxlength="5" class="textInput">页,
					每页<input type="text" id="pageSize" name="pageSize" value="${requestScope.pageInfo.pageSize}" size="5" maxlength="5" class="textInput">条
					共${requestScope.pageInfo.totalRecord}条,共${requestScope.pageInfo.totalPage}页
					<input type="submit" onclick="" value="GO">
				</li>
			</ul>
		</div>
	</form>
</div>