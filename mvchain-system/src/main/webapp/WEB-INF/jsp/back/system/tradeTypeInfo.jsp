<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<style>
	.pageFormContent label
	{
		float: none;
	}
</style>
<%@ include file="/common/include/page.jsp"%>
<h2 class="contentTitle">交易类型查看</h2>
<div class="pageContent">
	<div class="pageFormContent nowrap" layoutH="97">
		<dl>
			<dt>名称：</dt>
			<dd>
				${requestScope.tradeType.name }
			</dd>
		</dl>
		<dl>
			<dt>交易类型：</dt>
			<dd>
				${requestScope.tradeType.fromcoinCoinObj.name }-->${requestScope.tradeType.tocoinCoinObj.name };
				${requestScope.tradeType.fromcoinCoinObj.engname }-->${requestScope.tradeType.tocoinCoinObj.engname }
			</dd>
		</dl>
		<dl>
			<dt>汇率：</dt>
			<dd>
				${requestScope.tradeType.rate }(法币对rmb的汇率)
			</dd>
		</dl>
		<dl>
			<dt>状态：</dt>
			<dd>
				${requestScope.tradeType.statusStr}
			</dd>
		</dl>
		<dl>
			<dt>创建时间：</dt>
			<dd>
				<fmt:formatDate value="${requestScope.tradeType.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
			</dd>
		</dl>
		<dl>
			<dt>更新时间：</dt>
			<dd>
				<fmt:formatDate value="${requestScope.tradeType.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/>
			</dd>
		</dl>
		<dl>
			<dt>发布时间：</dt>
			<dd>
				<fmt:formatDate value="${requestScope.tradeType.pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
			</dd>
		</dl>
		<dl>
			<dt>备注：</dt>
			<dd>
				${requestScope.tradeType.content }
			</dd>
		</dl>
	</div>
</div>