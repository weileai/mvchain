<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<div class="pageHeader">
	<form onsubmit="return navTabSearch(this);" action="${rootpath }/back/system/coinrateList.html"
		method="post">
		<div class="searchBar">
			<table class="searchContent">
				<tr>
					<td>
						关键字：<input type="text" name="keyword" value="${requestScope.keyword }"/>
						搬砖状态：
						<select name="prostatusList" class="required">
							<option value="">请选择</option>
							<option value="1" ${requestScope.prostatusList == '1' ? 'selected' : '' }>启用</option>
							<option value="0" ${requestScope.prostatusList == '0' ? 'selected' : '' }>禁用</option>
						</select>
						状态：
						<select name="statusList" class="required">
							<option value="">请选择</option>
							<option value="1" ${requestScope.statusList == '1' ? 'selected' : '' }>启用</option>
							<option value="0" ${requestScope.statusList == '0' ? 'selected' : '' }>禁用</option>
						</select>
					</td>
				</tr>
			</table>
			<div class="subBar">
				<table class="searchContent">
					<td>
						发布时间：
						<input type="text" name="st" value="${requestScope.st }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" />
						<input type="text" name="ed" value="${requestScope.ed }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" />
					</td>
					<td align="right">
						<div class="buttonActive">
							<div class="buttonContent">
								<button type="submit">查询</button>
							</div>
						</div>
					</td>
				</table>
			</div>
		</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li>
				<a class="edit" href="${rootpath }/back/system/coinrateUpdate.html?cid={sid_user}"
					 target="navTab" ref="coinrateInfo"><span>查看</span></a>
			</li>
			<li>
				<a class="delete" href="${rootpath }/back/system/coinrateBatch.html?operType=coinBatch" target="ajaxTodo"
					title="确定同步币种?"><span>同步币种</span>
				</a>
			</li>
			<li>
				<a class="delete" href="${rootpath }/back/system/coinrateBatch.html?operType=coinTradeBatch" target="ajaxTodo"
					title="确定同步币种?"><span>同步交易对</span>
				</a>
			</li>
			<li>
				<a class="delete" href="${rootpath }/back/system/coinrateBatch.html?operType=websiteBatch" target="ajaxTodo"
					title="确定同步币种?"><span>同步网站</span>
				</a>
			</li>
			<%--
			<li>
				<a class="delete" href="${rootpath }/back/system/coinrateDelete.html?rid={sid_user}" target="ajaxTodo"
					title="确定要删除吗?"><span>删除</span>
				</a>
			</li>
			 --%>
			<li>
				<a class="edit" href="${rootpath }/back/system/coinrateUpdate.html?operType=update&cid={sid_user}"
					target="navTab" ref="coinrateUpdate"><span>修改</span></a>
			</li>
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138">
		<thead>
			<tr>
				<th width="40">序号</th>
				<th width="40">id</th>
				<th width="100">名称</th>
				<th width="100">英文名称</th>
				<th width="50">网站数量</th>
				<th width="150">网站</th>
				<th width="100">搬砖状态</th>
				<th width="100">状态</th>
				<th width="100">创建时间</th>
				<th width="100">更新时间</th>
				<th width="100">发布时间(排序)</th>
				<th width="100">操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${requestScope.coinrateList }" var="coinrate" varStatus="stat">
				<tr target="sid_user" rel="${coinrate.id }">
					<td>${stat.count }</td>
					<td>${coinrate.id }</td>
					<td>${coinrate.name }</td>
					<td>${coinrate.engname }</td>
					<td>${fn:length(coinrate.extendJSON.website)}</td>
					<td>
						<c:forEach items="${coinrate.extendJSON.website}" var="me" varStatus="stat">
							<c:set value="${me.value }" var="website"/>
							${website.id}->${website.name };
						</c:forEach>
					</td>
					<td>${coinrate.prostatusStr }</td>
					<td>${coinrate.statusStr }</td>
					<td><fmt:formatDate value="${coinrate.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					<td><fmt:formatDate value="${coinrate.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					<td><fmt:formatDate value="${coinrate.pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					<td>
						<a class="hreflink" href="${rootpath }/back/system/tradeTypeList.html?keyword=${coinrate.name }"
							target="navTab" ref="coinrateUpdate"><span>查询交易类型</span></a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<form onsubmit="return navTabSearch(this);" id="pageFormCoinrateList" method="post" action="${rootpath }/back/system/coinrateList.html">
		<input type="hidden" name="keyword" value="${requestScope.keyword }">
		<input type="hidden" name="statusList" value="${requestScope.statusList }">
		<input type="hidden" name="st" value="${requestScope.st }">
		<input type="hidden" name="ed" value="${requestScope.ed }">
		
		<input type="hidden" name="prostatusList" value="${requestScope.prostatusList }">
		<div class="panelBar">
			<ul style="float: right;">
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormCoinrateList','1','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>首页</span></a>
				</li>
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormCoinrateList','${requestScope.pageInfo.prePage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>上一页</span></a>
				</li>
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormCoinrateList','${requestScope.pageInfo.nextPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>下一页</span></a>
				</li>
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormCoinrateList','${requestScope.pageInfo.totalPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>末页</span></a>
				</li>
				<li>
					第<input type="text" id="currentPage" name="currentPage" value="${requestScope.pageInfo.currentPage}" size="5" maxlength="5" class="textInput">页,
					每页<input type="text" id="pageSize" name="pageSize" value="${requestScope.pageInfo.pageSize}" size="5" maxlength="5" class="textInput">条
					共${requestScope.pageInfo.totalRecord}条,共${requestScope.pageInfo.totalPage}页
					<input type="submit" onclick="" value="GO">
				</li>
			</ul>
		</div>
	</form>
</div>