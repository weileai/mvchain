<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<style>
	.pageFormContent label
	{
		float: none;
	}
</style>
<h2 class="contentTitle">系统配置查看</h2>
<div class="pageContent">
	<div class="pageFormContent nowrap" layoutH="97">
		<dl>
			<dt>名称：</dt>
			<dd>
				${requestScope.syspro.name }
			</dd>
		</dl>
		<dl>
			<dt>值：</dt>
			<dd>
				${syspro.vals }
			</dd>
		</dl>
		<dl>
			<dt>描述：</dt>
			<dd>
				${syspro.content }
			</dd>
		</dl>
		<dl>
			<dt>状态：</dt>
			<dd>
				${requestScope.syspro.statusStr}
			</dd>
		</dl>
		<dl>
			<dt>发布时间：</dt>
			<dd>
				<fmt:formatDate value="${requestScope.syspro.pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
			</dd>
		</dl>
		<dl>
			<dt>创建时间：</dt>
			<dd>
				<fmt:formatDate value="${requestScope.syspro.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/> 
			</dd>
		</dl>
		<dl>
			<dt>更新时间：</dt>
			<dd>
				<fmt:formatDate value="${requestScope.syspro.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/>
			</dd>
		</dl>
	</div>
</div>