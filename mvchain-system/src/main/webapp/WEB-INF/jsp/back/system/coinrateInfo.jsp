<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<h2 class="contentTitle">币种查看</h2>
<div class="pageContent">
	<div class="pageFormContent nowrap" layoutH="97">
		<dl>
			<dt>名称：</dt>
			<dd>
				${requestScope.coinrate.name }
			</dd>
		</dl>
		<dl>
			<dt>英文名称：</dt>
			<dd>
				${requestScope.coinrate.engname }
			</dd>
		</dl>
		<dl>
			<dt>搬砖状态：</dt>
			<dd>
				${requestScope.coinrate.prostatusStr}
			</dd>
		</dl>
		<dl>
			<dt>状态：</dt>
			<dd>
				${requestScope.coinrate.statusStr}
			</dd>
		</dl>
		<dl>
			<dt>扩展信息：</dt>
			<dd>
				${requestScope.coinrate.extend}
			</dd>
		</dl>
		<dl>
			<dt>创建时间：</dt>
			<dd>
				<fmt:formatDate value="${requestScope.coinrate.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
			</dd>
		</dl>
		<dl>
			<dt>更新时间：</dt>
			<dd>
				<fmt:formatDate value="${requestScope.coinrate.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/>
			</dd>
		</dl>
		<dl>
			<dt>发布时间：</dt>
			<dd>
				<fmt:formatDate value="${requestScope.coinrate.pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
			</dd>
		</dl>
		<dl>
			<dt>备注：</dt>
			<dd>
				${requestScope.coinrate.content }
			</dd>
		</dl>
	</div>
</div>