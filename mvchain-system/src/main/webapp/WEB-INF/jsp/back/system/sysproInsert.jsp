<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<style>
	.pageFormContent label
	{
		float: none;
	}
</style>
<h2 class="contentTitle">系统配置添加</h2>
<div class="pageContent">
	<form method="post" action="${rootpath }/back/system/sysproInsertSubmit.html" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone);">
		
		<input type="hidden" name="callbackType" value="">
		<input type="hidden" name="navTabId" value="">
		<input type="hidden" name="forwardUrl" value="">
		
		<div class="pageFormContent nowrap" layoutH="97">
			<dl>
				<dt>名称：</dt>
				<dd>
					<input type="text" name="name" size="50" value="${requestScope.syspro.name }" class="required"  alt="请输入您的名称"/>
					<span class="info"></span>
				</dd>
			</dl>
			<dl>
				<dt>发布时间：</dt>
				<dd>
					<c:set value="${sys_now }" var="pubtime"/>
					<c:if test="${requestScope.syspro.pubtime != null }">
						<c:set value="${requestScope.syspro.pubtime}" var="pubtime"/>
					</c:if>
					<input type="text" name="pubtimeCond" value="<fmt:formatDate value="${pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/>" 
						onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" />
					<span class="info">用来排序</span>
				</dd>
			</dl>
			<dl>
				<dt>状态：</dt>
				<dd>
					<input type="radio" id="statusWbInsert1" name="status" value="1" checked="checked">
					<label for="statusWbInsert1">启用</label>
					<input type="radio" id="statusWbInsert0" name="status" value="0" >
					<label for="statusWbInsert0">禁用</label>
				</dd>
			</dl>
			<dl>
				<dt>值：</dt>
				<dd>
					<textarea rows="5" cols="50" name="vals">${requestScope.syspro.vals }</textarea>
					<span class="info"></span>
				</dd>
			</dl>
			<dl>
				<dt>描述：</dt>
				<dd>
					<textarea rows="5" cols="50" name="content">${requestScope.syspro.content }</textarea>
					<span class="info"></span>
				</dd>
			</dl>
		</div>
		<div class="formBar">
			<ul>
				<!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
				</li>
			</ul>
		</div>
	</form>
</div>