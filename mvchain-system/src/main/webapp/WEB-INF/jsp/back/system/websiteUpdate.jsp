<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<style>
	.pageFormContent label
	{
		float: none;
	}
</style>
<h2 class="contentTitle">网站更新</h2>
<div class="pageContent">
	<form method="post" action="${rootpath }/back/system/websiteUpdateSubmit.html" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone);">
		
		<input type="hidden" name="callbackType" value="">
		<input type="hidden" name="navTabId" value="">
		<input type="hidden" name="forwardUrl" value="">
		
		<input type="hidden" name="wid" value="${requestScope.website.id }">
		
		<div class="pageFormContent nowrap" layoutH="97">
			<dl>
				<dt>名称：</dt>
				<dd>
					<input type="text" name="name" size="50" value="${requestScope.website.name }" class="required"  alt="请输入您的名称"/>
					<span class="info"></span>
				</dd>
			</dl>
			<dl>
				<dt>url：</dt>
				<dd>
					<input type="text" name="url" size="50" value="${requestScope.website.url }" class="required"  alt="请输入您的url"/>
					<span class="info"></span>
				</dd>
			</dl>
			<dl>
				<dt>发布时间：</dt>
				<dd>
					<c:set value="${sys_now }" var="pubtime"/>
					<c:if test="${requestScope.website.pubtime != null }">
						<c:set value="${requestScope.website.pubtime}" var="pubtime"/>
					</c:if>
					<input type="text" name="pubtimeCond" value="<fmt:formatDate value="${pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/>" 
						onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" />
					<span class="info">用来排序</span>
				</dd>
			</dl>
			<dl>
				<dt>状态：</dt>
				<dd>
					<input type="radio" id="statusWbUpdate1" name="status" value="1" ${requestScope.website.status == '1' ? 'checked' : ''}>
					<label for="statusWbUpdate1">启用</label>
					<input type="radio" id="statusWbUpdate0" name="status" value="0" ${requestScope.website.status == '0' ? 'checked' : ''}>
					<label for="statusWbUpdate0">禁用</label>
				</dd>
			</dl>
			<c:forEach items="${requestScope.website.feeJSON }" var="me" varStatus="stat">
				<dl>
					<dt>${me.value.name }充值手续费：</dt>
					<dd>
						<input type="text" name="recharge${me.key }" size="50" value="${me.value.recharge }" class="required"  alt="请输入${me.value.name }充值手续费"/>
						<span class="info"></span>
					</dd>
				</dl>
				<dl>
					<dt>${me.value.name }提现手续费：</dt>
					<dd>
						<input type="text" name="withdraw${me.key }" size="50" value="${me.value.withdraw }" class="required"  alt="请输入${me.value.name }提现手续费"/>
						<span class="info"></span>
					</dd>
				</dl>
			</c:forEach>
			<dl>
				<dt>支持的交易类型：</dt>
				<dd>
					<c:forEach items="${requestScope.tradeTypeList }" var="tradetype" varStatus="stat">
						<%-- 判断选中的交易类型 --%>
						<input type="checkbox" id="moneystatusWbUpdate${tradetype.id }" name="moneystatus" value="${tradetype.id }" 
						${fn:contains(requestScope.website.moneyStatusJSON,tradetype.name) ? 'checked' : ''}/>
						<label for="moneystatusWbUpdate${tradetype.id }">${stat.count }、${tradetype.name }</label>
						<c:forEach items="${requestScope.website.moneyStatusJSON }" var="me">
							<c:if test="${me.key == tradetype.id }">
								交易费:<input type="tradeFee" name="tradeFee${tradetype.id }" value="${me.value.tradeFee }"/>
								请输入0-1,按照百分比计算
							</c:if>
						</c:forEach>
						<br/>
					</c:forEach>
				</dd>
			</dl>
			<dl>
				<dt>接口认证模板：</dt>
				<dd>
					<textarea name="authinfo" rows="10" cols="80">${requestScope.website.authinfo }</textarea>
					<span class="info">与服务交易的认证模板</span>
				</dd>
			</dl>
		</div>
		<div class="formBar">
			<ul>
				<!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
				</li>
			</ul>
		</div>
	</form>
</div>