<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<style>
	.pageFormContent label
	{
		float: none;
	}
</style>
<h2 class="contentTitle">任务信息</h2>
<div class="pageContent">
	<div class="pageFormContent nowrap" layoutH="97">
		<dl>
			<dt>任务：</dt>
			<dd>
				<a class="hreflink"	height="400" width="800" target="dialog"
					href="${rootpath }/back/users/usersTaskUpdate.html?utid=${requestScope.usersTaskOrder.usersAccountObj.id }">
					${requestScope.usersTaskOrder.usersTaskObj.name }
				</a>
			</dd>
		</dl>
		<dl>
			<dt>用户网站账户：</dt>
			<dd>
				<a class="hreflink"	height="400" width="800" target="dialog"
					href="${rootpath }/back/users/usersAccountUpdate.html?uaid=${requestScope.usersTaskOrder.usersAccountObj.id }">
					${requestScope.usersTaskOrder.uaid }
				</a>
			</dd>
		</dl>
		<dl>
			<dt>订单id(第三方)：</dt>
			<dd>
				${requestScope.usersTaskOrder.ordersn }
			</dd>
		</dl>
		<dl>
			<dt>原始价格：</dt>
			<dd>
				${requestScope.usersTaskOrder.souprice }
			</dd>
		</dl>
		<dl>
			<dt>价格：</dt>
			<dd>
				${requestScope.usersTaskOrder.price}
			</dd>
		</dl>
		<dl>
			<dt>成交数量：</dt>
			<dd>
				${requestScope.usersTaskOrder.tradeedval }
			</dd>
		</dl>
		<dl>
			<dt>总数量：</dt>
			<dd>
				${requestScope.usersTaskOrder.totalval }
			</dd>
		</dl>
		<dl>
			<dt>总数量：</dt>
			<dd>
				${requestScope.usersTaskOrder.totalval }
			</dd>
		</dl>
		<dl>
			<dt>总价格：</dt>
			<dd>
				${requestScope.usersTaskOrder.totalprice }
			</dd>
		</dl>
		<dl>
			<dt>手续费：</dt>
			<dd>
				${requestScope.usersTaskOrder.fee }
			</dd>
		</dl>
		<dl>
			<dt>类型：</dt>
			<dd>
				${requestScope.usersTaskOrder.ordertypeStr }
			</dd>
		</dl>
		<dl>
			<dt>状态：</dt>
			<dd>
				${requestScope.usersTaskOrder.statusStr }
			</dd>
		</dl>
		<dl>
			<dt>创建时间：</dt>
			<dd>
				<fmt:formatDate value="${requestScope.usersTaskOrder.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
			</dd>
		</dl>
		<dl>
			<dt>更新时间：</dt>
			<dd>
				<fmt:formatDate value="${requestScope.usersTaskOrder.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/>
			</dd>
		</dl>
		<dl>
			<dt>完成时间：</dt>
			<dd>
				<fmt:formatDate value="${requestScope.usersTaskOrder.pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
			</dd>
		</dl>
	</div>
</div>