<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<style>
	.pageFormContent label
	{
		float: none;
	}
</style>
<h2 class="contentTitle">资产账户更新</h2>
<div class="pageContent">
	<form method="post" action="${rootpath }/back/finan/accountUpdateSubmit.html" 
		class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone);">
		<input type="hidden" name="accountId" value="${requestScope.account.id}"/>
		
		<input type="hidden" name="callbackType" value="">
		<input type="hidden" name="navTabId" value="">
		<input type="hidden" name="forwardUrl" value="">
		
		<div class="pageFormContent nowrap" layoutH="97">
			<dl>
				<dt>用户：</dt>
				<dd>
					${account.users.email}
				</dd>
			</dl>
			<dl>
				<dt>邮箱：</dt>
				<dd>
					${account.email}
				</dd>
			</dl>
			<dl>
				<dt>用户名：</dt>
				<dd>
					${account.usersName}
				</dd>
			</dl>
			<dl>
				<dt>账户类型：</dt>
				<dd>
					${account.accountTypeStr}
				</dd>
			</dl>
			<dl>
				<dt>状态：</dt>
				<dd>
					<input type="radio" id="statusaccountUpdate1" name="status" value="1" ${requestScope.account.status == '1' ? 'checked':''}>
					<label for="statusaccountUpdate1">启用</label>
					<input type="radio" id="statusaccountUpdate0" name="status" value="0" ${requestScope.account.status == '0' ? 'checked':''}>
					<label for="statusaccountUpdate0">禁用</label>
				</dd>
			</dl>
			<dl>
				<dt>创建时间：</dt>
				<dd>
					<fmt:formatDate value="${account.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
				</dd>
			</dl>
			<dl>
				<dt>更新时间：</dt>
				<dd>
					<fmt:formatDate value="${account.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/>
				</dd>
			</dl>
			<dl>
				<dt>发布时间：</dt>
				<dd>
					<fmt:formatDate value="${account.pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
				</dd>
			</dl>
		</div>
		<div class="formBar">
			<ul>
				<!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
				</li>
			</ul>
		</div>
	</form>
</div>