<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<style>
	.pageFormContent label
	{
		float: none;
	}
</style>
<h2 class="contentTitle">资产添加</h2>
<div class="pageContent">
	<form method="post" action="${rootpath }/back/finan/assetInsertSubmit.html" 
		class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone);">
		<input type="hidden" name="usersid" value="${param.usersid}"/>
		
		<input type="hidden" name="callbackType" value="">
		<input type="hidden" name="navTabId" value="">
		<input type="hidden" name="forwardUrl" value="">
		
		<div class="pageFormContent nowrap" layoutH="97">
			<dl>
				<dt>币种：</dt>
				<dd>
					<select name="coinid">
						<c:forEach items="${requestScope.coinrateList }" var="coinrate" varStatus="stat">
							<option value="${coinrate.id }">${coinrate.name }</option>
						</c:forEach>
					</select>
				</dd>
			</dl>
			<dl>
				<dt>发布时间：</dt>
				<dd>
					<input type="text" name="pubTimeStr" value="<fmt:formatDate value="${sys_now}" pattern="yyyy-MM-dd HH:mm:ss"/>" 
						onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" 
						readonly="true" />
				</dd>
			</dl>
			<dl>
				<dt>状态：</dt>
				<dd>
					<input type="radio" id="statusassetInsert1" name="status" value="1" checked="checked">
					<label for="statusassetInsert1">启用</label>
					<input type="radio" id="statusassetInsert0" name="status" value="0">
					<label for="statusassetInsert0">禁用</label>
				</dd>
			</dl>
		</div>
		<div class="formBar">
			<ul>
				<!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
				</li>
			</ul>
		</div>
	</form>
</div>