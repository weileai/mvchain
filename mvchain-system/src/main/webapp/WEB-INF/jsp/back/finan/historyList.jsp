<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<div class="pageHeader">
	<form onsubmit="return navTabSearch(this);" action="${rootpath }/back/finan/historyList.html"
		method="post">
		<input type="hidden" name="assetId" value="${requestScope.assetId }">
		<div class="searchBar">
			<table class="searchContent">
				<tr>
					<td>
						关键字：
						<input type="text" name="keyword" value="${requestScope.keyword }"/>
						状态:
						<select name="statusList" class="required">
							<option value="">请选择</option>
							<option value="0" ${requestScope.statusList == '0' ? 'selected' : '' }>取消</option>
							<option value="1" ${requestScope.statusList == '1' ? 'selected' : '' }>未处理</option>
							<option value="2" ${requestScope.statusList == '2' ? 'selected' : '' }>已处理</option>
						</select>
						交易网站：
						<select name="wbId" class="required">
							<option value="">请选择</option>
							<c:forEach items="${requestScope.websiteList }" var="website" varStatus="stat">
								<option value="${website.id }" ${requestScope.coinId == website.id ? 'selected' : '' }>${website.name }</option>
							</c:forEach>
						</select>
						交易类型:
						<select name="tradeType" class="required">
							<option value="">请选择</option>
							<option value="1" ${requestScope.statusList == '1' ? 'selected' : '' }>支出</option>
							<option value="0" ${requestScope.statusList == '0' ? 'selected' : '' }>收入</option>
						</select>
						历史类型:
						<select name="historyType" class="required">
							<option value="">请选择</option>
							<option value="0" ${requestScope.statusList == '0' ? 'selected' : '' }>充值</option>
							<option value="1" ${requestScope.statusList == '1' ? 'selected' : '' }>提现</option>
							<option value="2" ${requestScope.statusList == '2' ? 'selected' : '' }>收益</option>
						</select>
					</td>
				</tr>
			</table>
			<div class="subBar">
				<table class="searchContent">
					<td>
						更新时间：
						<input type="text" name="st" value="${requestScope.st }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" />
						<input type="text" name="ed" value="${requestScope.ed }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" />
					</td>
					<td align="right">
						<div class="buttonActive">
							<div class="buttonContent">
								<button type="submit">查询</button>
							</div>
						</div>
					</td>
				</table>
			</div>
		</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li>
				<a class="edit" href="${rootpath }/back/finan/historyUpdate.html?historyId={sid_user}&assetId=${param.assetId}"
					 target="navTab" ref="historyUpdate"><span>修改</span></a>
			</li>
			<li>
				<a class="edit" href="${rootpath }/back/finan/historyInsert.html?assetId=${param.assetId}"
					 target="navTab" ref="historyInsert"><span>添加</span></a>
			</li>
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138">
		<thead>
			<tr>
				<th width="40">序号</th>
				<th width="40">id</th>
				<th width="100">资产Id</th>
				<th width="60">网站</th>
				<th width="100">账户Id</th>
				<th width="100">任务Id</th>
				<th width="100">用户名</th>
				<th width="60">单价</th>
				<th width="60">数量</th>
				<th width="60">金额</th>
				<th width="60" title="当前币种的比例">比例</th>
				<th width="80">收入/支出</th>
				<th width="80">类型</th>
				<th width="60">状态</th>
				<th width="117">创建时间</th>
				<th width="117">更新时间</th>
				<th width="117">发布时间</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${requestScope.historyList }" var="history" varStatus="stat">
				<tr target="sid_user" rel="${history.id }">
					<td>${stat.count }</td>
					<td>${history.id }</td>
					<td>${history.assetId}</td>
					<td>${history.website.name}</td>
					<td>${history.accountId}</td>
					<td>
						<a class="hreflink" href="${rootpath }/back/stat/statTaskUpdate.html?stid=${history.taskId}"
					 		target="navTab" ref="taskInfo">
							${history.taskId}
						</a>
					</td>
					<td>${history.usersName }</td>
					<td>${history.price }</td>
					<td>${history.num }</td>
					<td>${history.money }</td>
					<td>${history.percent }</td>
					<td>${history.tradeTypeStr }</td>
					<td>${history.historyTypeStr }</td>
					<td>${history.statusStr }</td>
					<td><fmt:formatDate value="${history.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					<td><fmt:formatDate value="${history.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					<td><fmt:formatDate value="${history.pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<form onsubmit="return navTabSearch(this);" id="pageFormHistoryList" method="post" action="${rootpath }/back/finan/historyList.html">
		<input type="hidden" name="keyword" value="${requestScope.keyword }">
		<input type="hidden" name="statusList" value="${requestScope.statusList }">
		<input type="hidden" name="st" value="${requestScope.st }">
		<input type="hidden" name="ed" value="${requestScope.ed }">
		<input type="hidden" name="assetId" value="${requestScope.assetId }">
		<input type="hidden" name="wbId" value="${requestScope.wbId }">
		<input type="hidden" name="accountId" value="${requestScope.accountId }">
		<input type="hidden" name="tradeType" value="${requestScope.tradeType }">
		<input type="hidden" name="historyType" value="${requestScope.historyType }">
		<div class="panelBar">
			<ul style="float: right;">
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormHistoryList','1','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>首页</span></a>
				</li>
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormHistoryList','${requestScope.pageInfo.prePage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>上一页</span></a>
				</li>
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormHistoryList','${requestScope.pageInfo.nextPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>下一页</span></a>
				</li>
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormHistoryList','${requestScope.pageInfo.totalPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>末页</span></a>
				</li>
				<li>
					第<input type="text" id="currentPage" name="currentPage" value="${requestScope.pageInfo.currentPage}" size="5" maxlength="5" class="textInput">页,
					每页<input type="text" id="pageSize" name="pageSize" value="${requestScope.pageInfo.pageSize}" size="5" maxlength="5" class="textInput">条
					共${requestScope.pageInfo.totalRecord}条,共${requestScope.pageInfo.totalPage}页
					<input type="submit" onclick="" value="GO">
				</li>
			</ul>
		</div>
	</form>
</div>