<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<style>
	.pageFormContent label
	{
		float: none;
	}
</style>
<h2 class="contentTitle">历史更新</h2>
<div class="pageContent">
	<form method="post" action="${rootpath }/back/finan/historyUpdateSubmit.html" class="pageForm required-validate" 
		onsubmit="return validateCallback(this, navTabAjaxDone);">
		<input type="hidden" name="assetId" value="${param.assetId}"/>
		<input type="hidden" name="historyId" value="${requestScope.history.id}"/>
		
		<input type="hidden" name="callbackType" value="">
		<input type="hidden" name="navTabId" value="">
		<input type="hidden" name="forwardUrl" value="">
		
		<div class="pageFormContent nowrap" layoutH="97">
			<dl>
				<dt>资产：</dt>
				<dd>
					${requestScope.asset.coinrate.name }
				</dd>
			</dl>
			<dl>
				<dt>交易网站：</dt>
				<dd>
					<select name="wbId" class="required">
						<c:forEach items="${requestScope.websiteList }" var="website" varStatus="stat">
							<option value="${website.id }" ${requestScope.coinId == website.id ? 'selected' : '' }>${website.name }</option>
						</c:forEach>
					</select>
				</dd>
			</dl>
			<dl>
				<dt>价格：</dt>
				<dd>
					${requestScope.price }
				</dd>
			</dl>
			<dl>
				<dt>数量：</dt>
				<dd>
					<input type="text" name="num" value="${requestScope.history.num}" class="required">
				</dd>
			</dl>
			<dl>
				<dt>发布时间：</dt>
				<dd>
					<input type="text" name="pubTimeStr" value="<fmt:formatDate value="${requestScope.history.pubtime}" pattern="yyyy-MM-dd HH:mm:ss"/>" 
						onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" 
						readonly="true" />
				</dd>
			</dl>
			<dl>
				<dt>交易类型：</dt>
				<dd>
					<input type="radio" id="tradeHistoryUpdate1" name="tradeType" value="1" ${requestScope.history.tradeType == '1' ? 'checked' : '' }>
					<label for="tradeHistoryUpdate1">支出</label>
					<input type="radio" id="tradeHistoryUpdate0" name="tradeType" value="0" ${requestScope.history.tradeType == '0' ? 'checked' : '' }>
					<label for="tradeHistoryUpdate0">收入</label>
				</dd>
			</dl>
			<dl>
				<dt>历史类型：</dt>
				<dd>
					<input type="radio" id="typeHistoryUpdate1" name="historyType" value="1" ${requestScope.history.historyType == '1' ? 'checked' : '' }>
					<label for="typeHistoryUpdate1">提现</label>
					<input type="radio" id="typeHistoryUpdate0" name="historyType" value="0" ${requestScope.history.historyType == '0' ? 'checked' : '' }>
					<label for="typeHistoryUpdate0">充值</label>
					<input type="radio" id="typeHistoryUpdate2" name="historyType" value="2" ${requestScope.history.historyType == '2' ? 'checked' : '' }>
					<label for="typeHistoryUpdate2">收益</label>
				</dd>
			</dl>
			<dl>
				<dt>状态：</dt>
				<dd>
					<input type="radio" id="statusHistoryUpdate1" name="status" value="1" ${requestScope.history.status == '1' ? 'checked' : '' }>
					<label for="statusHistoryUpdate1">未处理</label>
					<input type="radio" id="statusHistoryUpdate0" name="status" value="0" ${requestScope.history.status == '0' ? 'checked' : '' }>
					<label for="statusHistoryUpdate0">取消</label>
					<input type="radio" id="statusHistoryUpdate2" name="status" value="2" ${requestScope.history.status == '2' ? 'checked' : '' }>
					<label for="statusHistoryUpdate2">已处理</label>
				</dd>
			</dl>
			<dl>
				<dt>内容：</dt>
				<dd>
					<textarea name="content" rows="10" cols="80">${requestScope.history.content}</textarea>
					<span class="info">内容</span>
				</dd>
			</dl>
		</div>
		<div class="formBar">
			<ul>
				<!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
				</li>
			</ul>
		</div>
	</form>
</div>