<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=7" />
<title>首页 --mvchain</title>
<link href="${rootpath }/jui_back/themes/default/style.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="${rootpath }/jui_back/themes/css/core.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="${rootpath }/jui_back/themes/css/print.css" rel="stylesheet" type="text/css" media="print"/>
<link href="${rootpath }/jui_back/uploadify/css/uploadify.css" rel="stylesheet" type="text/css" media="screen"/>
<!--[if IE]>
<link href="${rootpath }/jui_back/themes/css/ieHack.css" rel="stylesheet" type="text/css" media="screen"/>
<![endif]-->
<style type="text/css">
	#header{height:85px}
	#leftside, #container, #splitBar, #splitBarProxy{top:90px}
</style>

<!--[if lt IE 9]><script src="${rootpath }/jui_back/js/speedup.js" type="text/javascript"></script><script src="${rootpath }/jui_back/js/jquery-1.11.3.min.js" type="text/javascript"></script><![endif]-->
<!--[if gte IE 9]><!--><script src="${rootpath }/jui_back/js/jquery-2.1.4.min.js" type="text/javascript"></script><!--<![endif]-->

<script src="${rootpath }/jui_back/js/jquery.cookie.js" type="text/javascript"></script>
<script src="${rootpath }/jui_back/js/jquery.validate.js" type="text/javascript"></script>
<script src="${rootpath }/jui_back/js/jquery.bgiframe.js" type="text/javascript"></script>
<script src="${rootpath }/jui_back/xheditor/xheditor-1.2.2.min.js" type="text/javascript"></script>
<script src="${rootpath }/jui_back/xheditor/xheditor_lang/zh-cn.js" type="text/javascript"></script>
<script src="${rootpath }/jui_back/uploadify/scripts/jquery.uploadify.min.js" type="text/javascript"></script>

<script src="${rootpath }/jui_back/bin/dwz.min.js" type="text/javascript"></script>
<script src="${rootpath }/jui_back/js/dwz.regional.zh.js" type="text/javascript"></script>

<%@ include file="/common/include/title.jsp" %>

<script type="text/javascript">
$(function(){
	DWZ.init("${rootpath }/jui_back/dwz.frag.xml", {
		loginUrl:"login_dialog.html", loginTitle:"登录",	// 弹出登录对话框
//		loginUrl:"login.html",	// 跳到登录页面
		statusCode:{ok:200, error:300, timeout:301}, //【可选】
		keys: {statusCode:"statusCode", message:"message"}, //【可选】
		pageInfo:{pageNum:"pageNum", numPerPage:"numPerPage", orderField:"orderField", orderDirection:"orderDirection"}, //【可选】
		debug:false,	// 调试模式 【true|false】
		callback:function(){
			initEnv();
			$("#themeList").theme({themeBase:"themes"});
			//setTimeout(function() {$("#sidebar .toggleCollapse div").trigger("click");}, 10);
		}
	});
});
</script>
</head>

<body scroll="no">
	<div id="layout">
		<div id="header">
			<div class="headerNav">
				<a class="logo" href="http://j-ui.com">标志</a>
				<ul class="nav">
					<!-- <li id="switchEnvBox"><a href="javascript:">（<span>北京</span>）切换城市</a>
						<ul>
							<li><a href="sidebar_1.html">北京</a></li>
							<li><a href="sidebar_2.html">上海</a></li>
							<li><a href="sidebar_2.html">南京</a></li>
							<li><a href="sidebar_2.html">深圳</a></li>
							<li><a href="sidebar_2.html">广州</a></li>
							<li><a href="sidebar_2.html">天津</a></li>
							<li><a href="sidebar_2.html">杭州</a></li>
						</ul>
					</li> -->
					<li>
						<a target="_blank" href="${sso.admins.main}">${sessionScope.admins.email }</a>
					</li>
					<li><a href="${rootpath }/back/admin/threadList.html" target="navTab" rel="roleList">多线程列表</a></li>
					<li><a href="${rootpath }/back/admin/logout.html" onclick="return confirm('确认退出吗?')">退出</a></li>
				</ul>
				<ul class="themeList" id="themeList">
					<li theme="default"><div class="selected">蓝色</div></li>
					<li theme="green"><div>绿色</div></li>
					<!--<li theme="red"><div>红色</div></li>-->
					<li theme="purple"><div>紫色</div></li>
					<li theme="silver"><div>银色</div></li>
					<li theme="azure"><div>天蓝</div></li>
				</ul>
			</div>
		
			<div id="navMenu">
				<ul>
					<li><a href="${rootpath }/back/admin/leftmenu.html?operType=usersList"><span>用户模块</span></a></li>
					<li><a href="${rootpath }/back/admin/leftmenu.html?operType=finanList"><span>资产模块</span></a></li>
					<li><a href="${rootpath }/back/admin/leftmenu.html?operType=statList"><span>统计模块</span></a></li>
					<li><a href="${rootpath }/back/admin/leftmenu.html?operType=systemList"><span>系统模块</span></a></li>
				</ul>
			</div>
		</div>

		<div id="leftside">
			<div id="sidebar_s">
				<div class="collapse">
					<div class="toggleCollapse"><div></div></div>
				</div>
			</div>
			<div id="sidebar">
				<div class="toggleCollapse"><h2>主菜单</h2><div>收缩</div></div>

				<div class="accordion" fillSpace="sidebar">
					<div class="accordionHeader">
						<h2><span>Folder</span>界面组件</h2>
					</div>
					<div class="accordionContent">
						<ul class="tree treeFolder">
							<li>
								<a href="tabsPage.html" target="navTab">主框架面板</a>
								<ul>
									<li><a href="main.html" target="navTab" rel="main">我的主页</a></li>
								</ul>
							</li>
							<li>
								<a>表单组件</a>
								<ul>
									<li><a href="w_validation.html" target="navTab" rel="w_validation">表单验证</a></li>
									<li><a href="w_button.html" target="navTab" rel="w_button">按钮</a></li>
								</ul>
							</li>
							<li>
								<a>组合应用</a>
								<ul>
									<li><a href="demo/pagination/layout1.html" target="navTab" rel="pagination1">局部刷新分页1</a></li>
									<li><a href="demo/pagination/layout2.html" target="navTab" rel="pagination2">局部刷新分页2</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div id="container">
			<div id="navTab" class="tabsPage">
				<div class="tabsPageHeader">
					<div class="tabsPageHeaderContent"><!-- 显示左右控制时添加 class="tabsPageHeaderMargin" -->
						<ul class="navTab-tab">
							<li tabid="main" class="main"><a href="javascript:;"><span><span class="home_icon">我的主页</span></span></a></li>
						</ul>
					</div>
					<div class="tabsLeft">left</div><!-- 禁用只需要添加一个样式 class="tabsLeft tabsLeftDisabled" -->
					<div class="tabsRight">right</div><!-- 禁用只需要添加一个样式 class="tabsRight tabsRightDisabled" -->
					<div class="tabsMore">more</div>
				</div>
				<ul class="tabsMoreList">
					<li><a href="javascript:;">我的主页</a></li>
				</ul>
				<div class="navTab-panel tabsPageContent layoutBox">
					<div class="page unitBox">
						<div class="accountInfo">
							<!-- <div class="alertInfo">
								<h2><a href="doc/dwz-user-guide.pdf" target="_blank">DWZ框架使用手册(PDF)</a></h2>
								<a href="doc/dwz-user-guide.swf" target="_blank">DWZ框架演示视频</a>
							</div>
							<div class="right">
								<p><a href="doc/dwz-user-guide.zip" target="_blank" style="line-height:19px">DWZ框架使用手册(CHM)</a></p>
								<p><a href="doc/dwz-ajax-develop.swf" target="_blank" style="line-height:19px">DWZ框架Ajax开发视频教材</a></p>
							</div>
							<p><span>DWZ富客户端框架</span></p>
							<p>DWZ官方微博:<a href="http://weibo.com/dwzui" target="_blank">http://weibo.com/dwzui</a></p> -->
						</div>
						<div class="pageFormContent" layoutH="80">
							<!-- <iframe width="100%" height="430" class="share_self"  frameborder="0" scrolling="no" src="http://widget.weibo.com/weiboshow/index.php?width=0&height=430&fansRow=2&ptype=1&speed=300&skin=1&isTitle=0&noborder=1&isWeibo=1&isFans=0&uid=1739071261&verifier=c683dfe7"></iframe> -->
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	<div id="footer">Copyright &copy; 2010 mvchain后台</div>
</body>
</html>