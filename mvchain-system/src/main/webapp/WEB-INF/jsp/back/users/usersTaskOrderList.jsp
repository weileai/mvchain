<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<div class="pageHeader">
	<form onsubmit="return navTabSearch(this);" action="${rootpath }/back/users/usersTaskOrderList.html"
		method="post">
		<input type="hidden" name="utidList" value="${requestScope.utidList }">
		<div class="searchBar">
			<table class="searchContent">
				<tr>
					<td>
						关键字：
						<input type="text" name="keyword" value="${requestScope.keyword }"/>
						状态：
						<select name="statusList" class="required">
							<option value="">请选择</option>
							<option value="0" ${requestScope.statusList == '0' ? 'selected' : '' }>未开始</option>
							<option value="1" ${requestScope.statusList == '1' ? 'selected' : '' }>撤单</option>
							<option value="2" ${requestScope.statusList == '2' ? 'selected' : '' }>挂单</option>
							<option value="3" ${requestScope.statusList == '3' ? 'selected' : '' }>完成</option>
						</select>
						类型：
						<select name="ordertypeList" class="required">
							<option value="">请选择</option>
							<option value="0" ${requestScope.ordertypeList == '0' ? 'selected' : '' }>买</option>
							<option value="1" ${requestScope.ordertypeList == '1' ? 'selected' : '' }>卖</option>
						</select>
					</td>
				</tr>
			</table>
			<div class="subBar">
				<table class="searchContent">
					<td>
						更新时间：
						<input type="text" name="st" value="${requestScope.st }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" />
						<input type="text" name="ed" value="${requestScope.ed }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" />
					</td>
					<td align="right">
						<div class="buttonActive">
							<div class="buttonContent">
								<button type="submit">查询</button>
							</div>
						</div>
					</td>
				</table>
			</div>
		</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li>
				<a class="edit" href="${rootpath }/back/users/usersTaskOrderUpdate.html?utoid={sid_user}"
					 target="navTab" ref="usersTaskOrderInfo"><span>查看</span></a>
			</li>
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138">
		<thead>
			<tr>
				<th width="40">序号</th>
				<th width="40">id</th>
				<th width="100">任务</th>
				<th width="100">用户网站账户</th>
				<th width="100">交易类型</th>
				<th width="80">订单id(第三方)</th>
				<th width="60">原始价格</th>
				<th width="80">价格</th>
				<th width="40">成交数量</th>
				<th width="40">总数量</th>
				<th width="40">总价格</th>
				<th width="40">手续费</th>
				<th width="60">类型</th>
				<th width="60">状态</th>
				<th width="117">创建时间</th>
				<th width="117">更新时间</th>
				<th width="117">发布时间</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${requestScope.usersTaskOrderList }" var="usersTaskOrder" varStatus="stat">
				<tr target="sid_user" rel="${usersTaskOrder.id }">
					<td>${stat.count }</td>
					<td>${usersTaskOrder.id }</td>
					<td>
						<a class="hreflink"	height="400" width="800" target="dialog"
							href="${rootpath }/back/users/usersTaskUpdate.html?utid=${usersTaskOrder.usersAccountObj.id }">
							${usersTaskOrder.usersTaskObj.name }
						</a>
					</td>
					<td>
						<a class="hreflink"	height="400" width="800" target="dialog"
							href="${rootpath }/back/users/usersAccountUpdate.html?uaid=${usersTaskOrder.usersAccountObj.id }">
							${usersTaskOrder.uaid }
						</a>
					</td>
					<td>${usersTaskOrder.tradeTypeObj.name }</td>
					<td>${usersTaskOrder.ordersn }</td>
					<td>${usersTaskOrder.souprice }</td>
					<td>${usersTaskOrder.price }</td>
					<td>${usersTaskOrder.tradeedval }</td>
					<td>${usersTaskOrder.totalval }</td>
					<td>${usersTaskOrder.totalprice }</td>
					<td>${usersTaskOrder.fee }</td>
					<td>${usersTaskOrder.ordertypeStr }</td>
					<td>${usersTaskOrder.statusStr }</td>
					<td><fmt:formatDate value="${usersTaskOrder.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					<td><fmt:formatDate value="${usersTaskOrder.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					<td><fmt:formatDate value="${usersTaskOrder.pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<form onsubmit="return navTabSearch(this);" id="pageFormUsersTaskOrderList" method="post" action="${rootpath }/back/users/usersTaskOrderList.html">
		<input type="hidden" name="keyword" value="${requestScope.keyword }">
		<input type="hidden" name="statusList" value="${requestScope.statusList }">
		<input type="hidden" name="utidList" value="${requestScope.utidList }">
		<input type="hidden" name="ordertypeList" value="${requestScope.ordertypeList }">
		<input type="hidden" name="st" value="${requestScope.st }">
		<input type="hidden" name="ed" value="${requestScope.ed }">
		<div class="panelBar">
			<ul style="float: right;">
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormUsersTaskOrderList','1','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>首页</span></a>
				</li>
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormUsersTaskOrderList','${requestScope.pageInfo.prePage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>上一页</span></a>
				</li>
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormUsersTaskOrderList','${requestScope.pageInfo.nextPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>下一页</span></a>
				</li>
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormUsersTaskOrderList','${requestScope.pageInfo.totalPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>末页</span></a>
				</li>
				<li>
					第<input type="text" id="currentPage" name="currentPage" value="${requestScope.pageInfo.currentPage}" size="5" maxlength="5" class="textInput">页,
					每页<input type="text" id="pageSize" name="pageSize" value="${requestScope.pageInfo.pageSize}" size="5" maxlength="5" class="textInput">条
					共${requestScope.pageInfo.totalRecord}条,共${requestScope.pageInfo.totalPage}页
					<input type="submit" onclick="" value="GO">
				</li>
			</ul>
		</div>
	</form>
</div>