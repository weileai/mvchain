<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<%-- usersTask的枚举 --%>
<jsp:useBean id="usersTaskPojo" class="com.wang.mvchain.users.pojo.AUsersTask"/>
<div class="pageHeader">
	<form onsubmit="return navTabSearch(this);" action="${rootpath }/back/users/usersTaskList.html"
		method="post">
		<input type="hidden" name="usersidList" value="${requestScope.usersidList }">
		<div class="searchBar">
			<table class="searchContent">
				<tr>
					<td>
						关键字：
						<input type="text" name="keyword" value="${requestScope.keyword }"/>
						状态：
						<select name="statusList" class="required">
							<option value="">请选择</option>
							<c:set value="0" var="statCount"/>
							<c:forEach items="${usersTaskPojo.enumsMap }" var="me" varStatus="stat">
								<c:if test="${fn:startsWith(me.key,'STATUS-')}">
									<c:set value="${statCount + 1 }" var="statCount"/>
									<!-- 拆分,按照-拆分,取最后一个 -->
									<c:set value="${fn:split(me.key, '-')}" var="keys"/>
									<option value="${keys[1] }" ${requestScope.statusList == keys[1] ? 'selected' : '' }>${me.value }</option>
								</c:if>
							</c:forEach>
						</select>
						类型：
						<select name="soutypeList" class="required">
							<option value="">请选择</option>
							<c:set value="0" var="statCount"/>
							<c:forEach items="${usersTaskPojo.enumsMap }" var="me" varStatus="stat">
								<c:if test="${fn:startsWith(me.key,'SOUTYPE-')}">
									<c:set value="${statCount + 1 }" var="statCount"/>
									<!-- 拆分,按照-拆分,取最后一个 -->
									<c:set value="${fn:split(me.key, '-')}" var="keys"/>
									<option value="${keys[1] }" ${requestScope.soutypeList == keys[1] ? 'selected' : '' }>${me.value }</option>
								</c:if>
							</c:forEach>
						</select>
					</td>
				</tr>
			</table>
			<div class="subBar">
				<table class="searchContent">
					<td>
						更新时间：
						<input type="text" name="st" value="${requestScope.st }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" />
						<input type="text" name="ed" value="${requestScope.ed }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" />
					</td>
					<td align="right">
						<div class="buttonActive">
							<div class="buttonContent">
								<button type="submit">查询</button>
							</div>
						</div>
					</td>
				</table>
			</div>
		</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li>
				<a class="edit" href="${rootpath }/back/users/usersTaskUpdate.html?utid={sid_user}"
					 target="navTab" ref="usersTaskInfo"><span>查看</span></a>
			</li>
			<%--
			<li>
				<a class="delete" href="${rootpath }/back/users/usersDelete.html?rid={sid_user}" target="ajaxTodo"
					title="确定要删除吗?"><span>删除</span>
				</a>
			</li>
			--%>
			<li>
				<a class="edit" href="${rootpath }/back/users/usersTaskOrderList.html?utidList={sid_user}"
					target="navTab" ref="usersTaskOrderList"><span>查看订单</span></a>
			</li>
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138">
		<thead>
			<tr>
				<th width="40">序号</th>
				<th width="40">id</th>
				<th width="100">用户</th>
				<th width="80">收益币种</th>
				<th width="60">收益</th>
				<th width="60">手续费</th>
				<th width="80">名称</th>
				<th width="80">搬币种</th>
				<th width="40">买数量</th>
				<th width="40">卖数量</th>
				<th width="40">总数量</th>
				<th width="60">类型</th>
				<th width="60">状态</th>
				<th width="117">创建时间</th>
				<th width="117">更新时间</th>
				<th width="117">完成时间</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${requestScope.usersTaskList }" var="usersTask" varStatus="stat">
				<tr target="sid_user" rel="${usersTask.id }">
					<td>${stat.count }</td>
					<td>${usersTask.id }</td>
					<td>${usersTask.usersObj.email }</td>
					<td>${usersTask.priftcoinObj.name }</td>
					<td>${usersTask.prift }</td>
					<td>${usersTask.priftfee }</td>
					<td>${usersTask.name }</td>
					<td>${usersTask.sellTradeTypeObj.name }</td>
					<td>${usersTask.buynum }</td>
					<td>${usersTask.sellnum }</td>
					<td>${usersTask.totalnum }</td>
					<td>${usersTask.soutypeStr }</td>
					<td>${usersTask.statusStr }</td>
					<td><fmt:formatDate value="${usersTask.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					<td><fmt:formatDate value="${usersTask.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					<td><fmt:formatDate value="${usersTask.finishtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<form onsubmit="return navTabSearch(this);" id="pageFormUsersTaskList" method="post" action="${rootpath }/back/users/usersTaskList.html">
		<input type="hidden" name="keyword" value="${requestScope.keyword }">
		<input type="hidden" name="usersidList" value="${requestScope.usersidList }">
		<input type="hidden" name="coinidList" value="${requestScope.coinidList }">
		<input type="hidden" name="soutypeList" value="${requestScope.soutypeList }">
		<input type="hidden" name="statusList" value="${requestScope.statusList }">
		<input type="hidden" name="st" value="${requestScope.st }">
		<input type="hidden" name="ed" value="${requestScope.ed }">
		<div class="panelBar">
			<ul style="float: right;">
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormUsersTaskList','1','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>首页</span></a>
				</li>
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormUsersTaskList','${requestScope.pageInfo.prePage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>上一页</span></a>
				</li>
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormUsersTaskList','${requestScope.pageInfo.nextPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>下一页</span></a>
				</li>
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormUsersTaskList','${requestScope.pageInfo.totalPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>末页</span></a>
				</li>
				<li>
					第<input type="text" id="currentPage" name="currentPage" value="${requestScope.pageInfo.currentPage}" size="5" maxlength="5" class="textInput">页,
					每页<input type="text" id="pageSize" name="pageSize" value="${requestScope.pageInfo.pageSize}" size="5" maxlength="5" class="textInput">条
					共${requestScope.pageInfo.totalRecord}条,共${requestScope.pageInfo.totalPage}页
					<input type="submit" onclick="" value="GO">
				</li>
			</ul>
		</div>
	</form>
</div>