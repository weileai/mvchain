<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<div class="pageHeader">
	<form onsubmit="return navTabSearch(this);" action="${rootpath }/back/users/usersAccountList.html"
		method="post">
		<input type="hidden" name="usersidList" value="${requestScope.usersidList }">
		<div class="searchBar">
			<table class="searchContent">
				<tr>
					<td>
						关键字：
						<input type="text" name="keyword" value="${requestScope.keyword }"/>
						状态：
						<select name="statusList" class="required">
							<option value="">请选择</option>
							<option value="1" ${requestScope.statusList == '1' ? 'selected' : '' }>启用</option>
							<option value="0" ${requestScope.statusList == '0' ? 'selected' : '' }>禁用</option>
						</select>
						币种：
						<select name="coinidList" class="required">
							<option value="">请选择</option>
							<c:forEach items="${requestScope.coinrateList }" var="coinrate" varStatus="stat">
								<option value="${coinrate.id }" ${requestScope.coinidList == coinrate.id ? 'selected' : '' }>
									${coinrate.engname }(${coinrate.name })
								</option>
							</c:forEach>
						</select>
						网站：
						<select name="wbidList" class="required">
							<option value="">请选择</option>
							<c:forEach items="${requestScope.websiteList }" var="website" varStatus="stat">
								<option value="${website.id }" ${requestScope.wbidList == website.id ? 'selected' : '' }>
									${website.name }
								</option>
							</c:forEach>
						</select>
					</td>
				</tr>
			</table>
			<div class="subBar">
				<table class="searchContent">
					<td>
						发布时间：
						<input type="text" name="st" value="${requestScope.st }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" />
						<input type="text" name="ed" value="${requestScope.ed }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" />
					</td>
					<td align="right">
						<div class="buttonActive">
							<div class="buttonContent">
								<button type="submit">查询</button>
							</div>
						</div>
					</td>
				</table>
			</div>
		</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li>
				<a class="edit" href="${rootpath }/back/users/usersAccountUpdate.html?uaid={sid_user}"
					 target="navTab" ref="usersAccountInfo"><span>查看</span></a>
			</li>
			<%--
			<li>
				<a class="delete" href="${rootpath }/back/users/usersDelete.html?rid={sid_user}" target="ajaxTodo"
					title="确定要删除吗?"><span>删除</span>
				</a>
			</li>
			<li>
				<a class="edit" href="${rootpath }/back/users/usersUpdate.html?operType=update&uid={sid_user}"
					target="navTab" ref="usersUpdate"><span>修改</span></a>
			</li>
			--%>
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138">
		<thead>
			<tr>
				<th width="40">序号</th>
				<th width="40">id</th>
				<th width="100">用户</th>
				<th width="80">币种</th>
				<th width="80">网站</th>
				<th width="40">用户名</th>
				<th width="40">余额</th>
				<th width="40">冻结金额</th>
				<th width="40">最大交易数量</th>
				<th width="100">最大收益</th>
				<th width="40">状态</th>
				<th width="117">创建时间</th>
				<th width="117">更新时间</th>
				<th width="117">发布时间</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${requestScope.usersAccountList }" var="usersAccount" varStatus="stat">
				<tr target="sid_user" rel="${usersAccount.id }">
					<td>${stat.count }</td>
					<td>${usersAccount.id }</td>
					<td>${usersAccount.usersObj.email }</td>
					<td>${usersAccount.coinrateObj.name }</td>
					<td>${usersAccount.websiteObj.name }</td>
					<td>${usersAccount.username }</td>
					<td>${usersAccount.balance }</td>
					<td>${usersAccount.fundmoney }</td>
					<td>${usersAccount.trademaxnum }</td>
					<td>${usersAccount.prift }</td>
					<td>${usersAccount.statusStr }</td>
					<td><fmt:formatDate value="${usersAccount.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					<td><fmt:formatDate value="${usersAccount.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					<td><fmt:formatDate value="${usersAccount.pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<form onsubmit="return navTabSearch(this);" id="pageFormUsersAccountList" method="post" action="${rootpath }/back/users/usersAccountList.html">
		<input type="hidden" name="keyword" value="${requestScope.keyword }">
		<input type="hidden" name="usersidList" value="${requestScope.usersidList }">
		<input type="hidden" name="coinidList" value="${requestScope.coinidList }">
		<input type="hidden" name="wbidList" value="${requestScope.wbidList }">
		<input type="hidden" name="statusList" value="${requestScope.statusList }">
		<input type="hidden" name="prostatusList" value="${requestScope.prostatusList }">
		<input type="hidden" name="st" value="${requestScope.st }">
		<input type="hidden" name="ed" value="${requestScope.ed }">
		<div class="panelBar">
			<ul style="float: right;">
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormUsersAccountList','1','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>首页</span></a>
				</li>
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormUsersAccountList','${requestScope.pageInfo.prePage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>上一页</span></a>
				</li>
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormUsersAccountList','${requestScope.pageInfo.nextPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>下一页</span></a>
				</li>
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormUsersAccountList','${requestScope.pageInfo.totalPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>末页</span></a>
				</li>
				<li>
					第<input type="text" id="currentPage" name="currentPage" value="${requestScope.pageInfo.currentPage}" size="5" maxlength="5" class="textInput">页,
					每页<input type="text" id="pageSize" name="pageSize" value="${requestScope.pageInfo.pageSize}" size="5" maxlength="5" class="textInput">条
					共${requestScope.pageInfo.totalRecord}条,共${requestScope.pageInfo.totalPage}页
					<input type="submit" onclick="" value="GO">
				</li>
			</ul>
		</div>
	</form>
</div>