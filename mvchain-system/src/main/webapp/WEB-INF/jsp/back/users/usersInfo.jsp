<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<style>
	.pageFormContent label
	{
		float: none;
	}
</style>
<h2 class="contentTitle">用户信息</h2>
<div class="pageContent">
	<div class="pageFormContent nowrap" layoutH="97">
		<dl>
			<dt>邮箱：</dt>
			<dd>
				${requestScope.users.email }
			</dd>
		</dl>
		<dl>
			<dt>状态：</dt>
			<dd>
				${requestScope.users.statusStr}
			</dd>
		</dl>
		<dl>
			<dt>搬砖状态：</dt>
			<dd>
				${requestScope.users.prostatusStr}
			</dd>
		</dl>
		<dl>
			<dt>最大任务：</dt>
			<dd>
				${requestScope.users.maxtask}
			</dd>
		</dl>
		<dl>
			<dt>创建时间：</dt>
			<dd>
				<fmt:formatDate value="${requestScope.users.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
			</dd>
		</dl>
		<dl>
			<dt>更新时间：</dt>
			<dd>
				<fmt:formatDate value="${requestScope.users.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/>
			</dd>
		</dl>
	</div>
</div>