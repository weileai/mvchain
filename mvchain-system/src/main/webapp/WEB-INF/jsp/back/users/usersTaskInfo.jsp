<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<style>
	.pageFormContent label
	{
		float: none;
	}
</style>
<h2 class="contentTitle">任务信息</h2>
<div class="pageContent">
	<div class="pageFormContent nowrap" layoutH="97">
		<dl>
			<dt>用户：</dt>
			<dd>
				${requestScope.usersTask.usersObj.email }
			</dd>
		</dl>
		<dl>
			<dt>收益币种：</dt>
			<dd>
				${requestScope.usersTask.priftcoinObj.name }
			</dd>
		</dl>
		<dl>
			<dt>收益：</dt>
			<dd>
				${requestScope.usersTask.prift }
			</dd>
		</dl>
		<dl>
			<dt>手续费：</dt>
			<dd>
				${requestScope.usersTask.priftfee }
			</dd>
		</dl>
		<tr>
			<th>目标收益：</th>
			<td>
				${requestScope.usersTask.souprift }
			</td>
		</tr>
		<dl>
			<dt>名称：</dt>
			<dd>
				${requestScope.usersTask.name}
			</dd>
		</dl>
		<dl>
			<dt>卖数量：</dt>
			<dd>
				${requestScope.usersTask.sellnum}
			</dd>
		</dl>
		<dl>
			<dt>买数量：</dt>
			<dd>
				${requestScope.usersTask.buynum }
			</dd>
		</dl>
		<dl>
			<dt>交易深度：</dt>
			<dd>
				${requestScope.usersTask.deptnum }
			</dd>
		</dl>
		<dl>
			<dt>总数量：</dt>
			<dd>
				${requestScope.usersTask.totalnum }
			</dd>
		</dl>
		<dl>
			<dt>买(手续费)：</dt>
			<dd>
				${requestScope.usersTask.feenum }
			</dd>
		</dl>
		
		<dl>
			<dt>上次交易网站(卖)：</dt>
			<dd>
				网站:${requestScope.usersTask.sellWbObj.name};
				币种:${requestScope.usersTask.sellTradeTypeObj.name};
				原始价格:${requestScope.usersTask.sousellprice};
				价格:${requestScope.usersTask.sellprice};
				数量:${requestScope.usersTask.buynum};
			</dd>
		</dl>
		<dl>
			<dt>上次交易网站(买)：</dt>
			<dd>
				网站:${requestScope.usersTask.buyWbObj.name};
				币种:${requestScope.usersTask.buyTradeTypeObj.name};
				原始价格:${requestScope.usersTask.soubuyprice};
				价格:${requestScope.usersTask.buyprice};
				数量:${requestScope.usersTask.sellnum};
			</dd>
		</dl>
		<dl>
			<dt>任务原始价格：</dt>
			<dd>
				买:${requestScope.usersTask.taskbuyprice};
				卖:${requestScope.usersTask.tasksellprice};
			</dd>
		</dl>
		<dl>
			<dt>状态：</dt>
			<dd>
				${requestScope.usersTask.statusStr }
			</dd>
		</dl>
		<dl>
			<dt>类型：</dt>
			<dd>
				${requestScope.usersTask.soutypeStr }
			</dd>
		</dl>
		<dl>
			<dt>创建时间：</dt>
			<dd>
				<fmt:formatDate value="${requestScope.usersTask.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
			</dd>
		</dl>
		<dl>
			<dt>更新时间：</dt>
			<dd>
				<fmt:formatDate value="${requestScope.usersTask.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/>
			</dd>
		</dl>
		<dl>
			<dt>完成时间：</dt>
			<dd>
				<fmt:formatDate value="${requestScope.usersTask.finishtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
			</dd>
		</dl>
		<dl>
			<dt>内容：</dt>
			<dd>
				${requestScope.usersTask.content }
			</dd>
		</dl>
	</div>
</div>