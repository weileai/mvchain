<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<style>
	.pageFormContent label
	{
		float: none;
	}
</style>
<h2 class="contentTitle">用户更新</h2>
<div class="pageContent">
	<form method="post" action="${rootpath }/back/users/usersUpdateSubmit.html" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone);">
		<input type="hidden" name="uid" value="${requestScope.users.id }"/>
		
		<input type="hidden" name="callbackType" value="">
		<input type="hidden" name="navTabId" value="">
		<input type="hidden" name="forwardUrl" value="">
		
		<div class="pageFormContent nowrap" layoutH="97">
			<dl>
				<dt>邮箱：</dt>
				<dd>
					${requestScope.users.email }
				</dd>
			</dl>
			<dl>
				<dt>状态：</dt>
				<dd>
					<input type="radio" id="statusUsersUpdate1" name="status" value="1" ${requestScope.users.status == '1' ? 'checked' : '' }>
					<label for="statusUsersUpdate1">启用</label>
					<input type="radio" id="statusUsersUpdate0" name="status" value="0" ${requestScope.users.status == '0' ? 'checked' : '' }>
					<label for="statusUsersUpdate0">禁用</label>
				</dd>
			</dl>
			<dl>
				<dt>搬砖状态：</dt>
				<dd>
					<input type="radio" id="prostatusUsersUpdate1" name="prostatus" value="1" ${requestScope.users.prostatus == '1' ? 'checked' : '' }>
					<label for="prostatusUsersUpdate1">开启</label>
					<input type="radio" id="prostatusUsersUpdate0" name="prostatus" value="0" ${requestScope.users.prostatus == '0' ? 'checked' : '' }>
					<label for="prostatusUsersUpdate0">关闭</label>
				</dd>
			</dl>
			<dl>
				<dt>最大任务：</dt>
				<dd>
					<input type="text" name="maxtask" value="${requestScope.users.maxtask}">
				</dd>
			</dl>
			<dl>
				<dt>创建时间：</dt>
				<dd>
					<fmt:formatDate value="${requestScope.users.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
				</dd>
			</dl>
			<dl>
				<dt>更新时间：</dt>
				<dd>
					<fmt:formatDate value="${requestScope.users.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/>
				</dd>
			</dl>
		</div>
		<div class="formBar">
			<ul>
				<!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
				</li>
			</ul>
		</div>
	</form>
</div>