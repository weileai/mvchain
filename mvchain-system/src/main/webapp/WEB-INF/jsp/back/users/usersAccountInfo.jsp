<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<style>
	.pageFormContent label
	{
		float: none;
	}
</style>
<h2 class="contentTitle">用户账户信息</h2>
<div class="pageContent">
	<div class="pageFormContent nowrap" layoutH="97">
		<dl>
			<dt>用户：</dt>
			<dd>
				${requestScope.usersAccount.usersObj.email }
			</dd>
		</dl>
		<dl>
			<dt>币种：</dt>
			<dd>
				${requestScope.usersAccount.coinrateObj.name }
			</dd>
		</dl>
		<dl>
			<dt>网站：</dt>
			<dd>
				${requestScope.usersAccount.websiteObj.name }
			</dd>
		</dl>
		<dl>
			<dt>用户名：</dt>
			<dd>
				${requestScope.usersAccount.username }
			</dd>
		</dl>
		<dl>
			<dt>余额：</dt>
			<dd>
				${requestScope.usersAccount.balance}
			</dd>
		</dl>
		<dl>
			<dt>冻结金额：</dt>
			<dd>
				${requestScope.usersAccount.fundmoney}
			</dd>
		</dl>
		<dl>
			<dt>最大交易数量：</dt>
			<dd>
				${requestScope.usersAccount.trademaxnum }
			</dd>
		</dl>
		<dl>
			<dt>最大收益：</dt>
			<dd>
				${requestScope.usersAccount.prift }
			</dd>
		</dl>
		<dl>
			<dt>状态：</dt>
			<dd>
				${requestScope.usersAccount.statusStr }
			</dd>
		</dl>
		<dl>
			<dt>创建时间：</dt>
			<dd>
				<fmt:formatDate value="${requestScope.usersAccount.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
			</dd>
		</dl>
		<dl>
			<dt>更新时间：</dt>
			<dd>
				<fmt:formatDate value="${requestScope.usersAccount.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/>
			</dd>
		</dl>
		<dl>
			<dt>发布时间：</dt>
			<dd>
				<fmt:formatDate value="${requestScope.usersAccount.pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
			</dd>
		</dl>
	</div>
</div>