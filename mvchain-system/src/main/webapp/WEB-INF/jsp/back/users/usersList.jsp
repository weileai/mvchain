<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<div class="pageHeader">
	<form onsubmit="return navTabSearch(this);" action="${rootpath }/back/users/usersList.html"
		method="post">
		<div class="searchBar">
			<table class="searchContent">
				<tr>
					<td>
						关键字：<input type="text" name="keyword" value="${requestScope.keyword }"/>
						状态：
						<select name="statusList" class="required">
							<option value="">请选择</option>
							<option value="1" ${requestScope.statusList == '1' ? 'selected' : '' }>启用</option>
							<option value="0" ${requestScope.statusList == '0' ? 'selected' : '' }>禁用</option>
						</select>
						搬砖状态：
						<select name="prostatusList" class="required">
							<option value="">请选择</option>
							<option value="1" ${requestScope.prostatusList == '1' ? 'selected' : '' }>开启</option>
							<option value="0" ${requestScope.prostatusList == '0' ? 'selected' : '' }>关闭</option>
						</select>
					</td>
				</tr>
			</table>
			<div class="subBar">
				<table class="searchContent">
					<td>
						更新时间：
						<input type="text" name="st" value="${requestScope.st }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" />
						<input type="text" name="ed" value="${requestScope.ed }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" />
					</td>
					<td align="right">
						<div class="buttonActive">
							<div class="buttonContent">
								<button type="submit">查询</button>
							</div>
						</div>
					</td>
				</table>
			</div>
		</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li>
				<a class="edit" href="${rootpath }/back/users/usersUpdate.html?uid={sid_user}"
					 target="navTab" ref="usersInfo"><span>查看</span></a>
			</li>
			<li>
				<a class="edit" href="${rootpath }/back/users/usersUpdate.html?operType=update&uid={sid_user}"
					target="navTab" ref="usersUpdate"><span>修改</span></a>
			</li>
			<li>
				<a class="edit" href="${rootpath }/back/users/usersAccountList.html?usersidList={sid_user}"
					 target="navTab" ref="usersAccountList"><span>账户列表</span></a>
			</li>
			<li>
				<a class="edit" href="${rootpath }/back/finan/assetList.html?usersid={sid_user}"
					 target="navTab" ref="assetList"><span>资产列表</span></a>
			</li>
			<li>
				<a class="edit" href="${rootpath }/back/finan/accountList.html?usersid={sid_user}"
					 target="navTab" ref="accountList"><span>账户列表</span></a>
			</li>
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138">
		<thead>
			<tr>
				<th width="40">序号</th>
				<th width="40">id</th>
				<th width="100">邮箱</th>
				<th width="100">搬砖状态</th>
				<th width="100">状态</th>
				<th width="100">创建时间</th>
				<th width="100">更新时间</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${requestScope.usersList }" var="users" varStatus="stat">
				<tr target="sid_user" rel="${users.id }">
					<td>${stat.count }</td>
					<td>${users.id }</td>
					<td>${users.email }</td>
					<td>${users.prostatusStr }</td>
					<td>${users.statusStr }</td>
					<td><fmt:formatDate value="${users.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					<td><fmt:formatDate value="${users.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<form onsubmit="return navTabSearch(this);" id="pageFormUsersList" method="post" action="${rootpath }/back/users/usersList.html">
		<input type="hidden" name="keyword" value="${requestScope.keyword }">
		<input type="hidden" name="statusList" value="${requestScope.statusList }">
		<input type="hidden" name="prostatusList" value="${requestScope.prostatusList }">
		<input type="hidden" name="st" value="${requestScope.st }">
		<input type="hidden" name="ed" value="${requestScope.ed }">
		<div class="panelBar">
			<ul style="float: right;">
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormUsersList','1','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>首页</span></a>
				</li>
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormUsersList','${requestScope.pageInfo.prePage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>上一页</span></a>
				</li>
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormUsersList','${requestScope.pageInfo.nextPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>下一页</span></a>
				</li>
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormUsersList','${requestScope.pageInfo.totalPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>末页</span></a>
				</li>
				<li>
					第<input type="text" id="currentPage" name="currentPage" value="${requestScope.pageInfo.currentPage}" size="5" maxlength="5" class="textInput">页,
					每页<input type="text" id="pageSize" name="pageSize" value="${requestScope.pageInfo.pageSize}" size="5" maxlength="5" class="textInput">条
					共${requestScope.pageInfo.totalRecord}条,共${requestScope.pageInfo.totalPage}页
					<input type="submit" onclick="" value="GO">
				</li>
			</ul>
		</div>
	</form>
</div>