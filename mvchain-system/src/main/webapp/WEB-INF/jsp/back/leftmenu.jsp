<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>

<div class="accordion" fillSpace="sidebar">
	<div class="accordionContent">
		<c:choose>
			<c:when test="${param.operType == 'usersList' }">
				<ul class="tree treeFolder">
					<li>
						<a href="#">用户管理</a>
						<ul>
							<li><a href="${rootpath }/back/users/usersList.html" target="navTab" rel="usersList">用户列表</a></li>
							<li><a href="${rootpath }/back/users/usersAccountList.html" target="navTab" ref="usersAccountList">账户列表</a></li>
						</ul>
					</li>
					<li>
						<a href="#">任务管理</a>
						<ul>
							<li><a href="${rootpath }/back/users/usersTaskList.html" target="navTab" rel="usersTaskList">任务列表</a></li>
						</ul>
					</li>
				</ul>
			</c:when>
			<c:when test="${param.operType == 'finanList' }">
				<ul class="tree treeFolder">
					<li>
						<a href="#">资产列表</a>
						<ul>
							<li><a href="${rootpath }/back/finan/assetList.html" target="navTab" rel="assetList">资产列表</a></li>
							<li><a href="${rootpath }/back/finan/historyList.html" target="navTab" rel="historyList">历史列表</a></li>
							<li><a href="${rootpath }/back/finan/accountList.html" target="navTab" rel="historyList">账户列表</a></li>
						</ul>
					</li>
				</ul>
			</c:when>
			<c:when test="${param.operType == 'statList' }">
				<ul class="tree treeFolder">
					<li>
						<a href="#">任务统计</a>
						<ul>
							<li><a href="${rootpath }/back/stat/statTaskList.html" target="navTab" rel="usersList">任务统计列表</a></li>
							<li><a href="${rootpath }/back/stat/statTaskInsert.html" target="navTab" rel="usersAccountList">手工计算</a></li>
						</ul>
					</li>
				</ul>
			</c:when>
			<c:when test="${param.operType == 'systemList' }">
				<ul class="tree treeFolder">
					<li>
						<a href="#">配置管理</a>
						<ul>
							<li><a href="${rootpath }/back/system/sysproList.html" target="navTab" rel="sysproList">配置列表</a></li>
							<li><a href="${rootpath }/back/system/sysproInsert.html" target="navTab" rel="sysproInsert">添加配置</a></li>
						</ul>
					</li>
					<li>
						<a href="#">网站管理</a>
						<ul>
							<li><a href="${rootpath }/back/system/websiteList.html" target="navTab" rel="websiteList">网站列表</a></li>
							<li><a href="${rootpath }/back/system/websiteInsert.html" target="navTab" rel="websiteInsert">添加网站</a></li>
						</ul>
					</li>
					<li>
						<a href="#">币种管理</a>
						<ul>
							<li><a href="${rootpath }/back/system/coinrateList.html" target="navTab" rel="coinrateList">币种列表</a></li>
							<li><a href="${rootpath }/back/system/coinrateInsert.html" target="navTab" rel="coinrateInsert">添加币种</a></li>
							<li><a href="${rootpath }/back/system/tradeTypeList.html" target="navTab" rel="tradeTypeList">交易类型列表</a></li>
							<li><a href="${rootpath }/back/system/tradeTypeInsert.html" target="navTab" rel="tradeTypeInsert">添加交易类型</a></li>
						</ul>
					</li>
				</ul>
			</c:when>
		</c:choose>
	</div>
</div>