<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:useBean id="statTask" class="com.wang.mvchain.stat.pojo.AStatTask"/>
<%@ include file="/common/include/page.jsp"%>
<style>
	.pageFormContent label
	{
		float: none;
	}
</style>
<h2 class="contentTitle">修改任务</h2>
<div class="pageContent">
	<form method="post" action="${rootpath }/back/stat/statTaskUpdateSubmit.html" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone);">
		<input type="hidden" name="callbackType" value="">
		<input type="hidden" name="navTabId" value="">
		<input type="hidden" name="forwardUrl" value="">
		
		<input type="hidden" name="stid" value="${requestScope.statTask.id }">
		<input type="hidden" name="operType" value="update">
		
		<div class="pageFormContent nowrap" layoutH="97">
			<dl>
				<dt>标识：</dt>
				<dd>
					<c:forEach items="${statTask.flagMap }" var="map" varStatus="stat">
						<input type="radio" id="flagStatTaskUpdate${map.value }" name="flag" value="${map.value }" ${requestScope.statTask.flag == map.value ? 'checked' : '' }>
						<label for="flagStatTaskUpdate${map.value }">${map.key }</label>
					</c:forEach>
			</dl>
				</dd>
			</dl>
			<dl>
				<dt>状态：</dt>
				<dd>
					<c:forEach items="${statTask.statusMap }" var="map" varStatus="stat">
						<input type="radio" id="statusStatTaskUpdate${map.value }" name="status" value="${map.value }" ${requestScope.statTask.status == map.value ? 'checked' : '' }>
						<label for="statusStatTaskUpdate${map.value }">${map.key }</label>
					</c:forEach>
				</dd>
			</dl>
			<dl>
				<dt>id：</dt>
				<dd>
					${requestScope.statTask.id }
				</dd>
			</dl>
			<dl>
				<dt>用户名：</dt>
				<dd>
					${requestScope.statTask.usersObj.email }
				</dd>
			</dl>
			<dl>
				<dt>日期：</dt>
				<dd>
					<fmt:formatDate value="${requestScope.statTask.currdate }" pattern="yyyy-MM-dd"/>
				</dd>
			</dl>
			<dl>
				<dt>总收益：</dt>
				<dd>
					${requestScope.statTask.prift }
				</dd>
			</dl>
			<dl>
				<dt>手续费：</dt>
				<dd>
					${requestScope.statTask.fee }
				</dd>
			</dl>
			<dl>
				<dt>任务数：</dt>
				<dd>
					${requestScope.statTask.taskcount }
				</dd>
			</dl>
			<dl>
				<dt>卖最低价：</dt>
				<dd>
					${requestScope.statTask.sellpricemin }
				</dd>
			</dl>
			<dl>
				<dt>卖最高价：</dt>
				<dd>
					${requestScope.statTask.sellpricemax }
				</dd>
			</dl>
			<dl>
				<dt>买最低价：</dt>
				<dd>
					${requestScope.statTask.buypricemin }
				</dd>
			</dl>
			<dl>
				<dt>买最高价：</dt>
				<dd>
					${requestScope.statTask.buypricemax }
				</dd>
			</dl>
			<dl>
				<dt>创建时间：</dt>
				<dd>
					<fmt:formatDate value="${requestScope.statTask.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
				</dd>
			</dl>
			<dl>
				<dt>更新时间：</dt>
				<dd>
					<fmt:formatDate value="${requestScope.statTask.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/>
				</dd>
			</dl>
			
			<div class="divider"></div>
			<p>
				余额明细
			</p>
			<div class="divider"></div>
			<table class="table" width="100%">
				<thead>
					<tr>
						<th width="30">序号</th>
						<th width="100">币种</th>
						<th width="100">余额</th>
						<th width="100">冻结金额</th>
						<th width="100">折合cny</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${requestScope.statTask.balanceJSON }" var="me" varStatus="stat">
						<c:if test="${fn:startsWith(me.key,'coinid_')}">
							<tr target="sid_user" rel="${stat.count }">
								<td>${stat.count }</td>
								<td>${me.value.name }</td>
								<td>${me.value.balance}</td>
								<td>${me.value.fundmoney}</td>
								<td>${me.value.cny}</td>
							</tr>
						</c:if>
					</c:forEach>
					<tr target="sid_user" rel="${stat.count }">
						<td>合计:</td>
						<td></td>
						<td></td>
						<td></td>
						<td>${requestScope.statTask.balanceJSON.total_cny_balance }</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="formBar">
			<ul>
				<!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
				</li>
			</ul>
		</div>
	</form>
</div>