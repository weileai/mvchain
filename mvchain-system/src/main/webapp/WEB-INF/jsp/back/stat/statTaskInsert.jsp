<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<style>
	.pageFormContent label
	{
		float: none;
	}
</style>
<h2 class="contentTitle">手动任务统计</h2>
<div class="pageContent">
	<form method="post" action="${rootpath }/back/stat/statTaskInsertSubmit.html" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone);">
	
		<input type="hidden" name="callbackType" value="">
		<input type="hidden" name="navTabId" value="">
		<input type="hidden" name="forwardUrl" value="">
		
		<div class="pageFormContent nowrap" layoutH="97">
			<dl>
				<dt>开始时间：</dt>
				<dd>
					<input type="text" name="st" value="<fmt:formatDate value="${sys_now }" pattern="yyyy-MM-dd"/>" 
						onClick="WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly="true" />
					<span class="info">用来排序</span>
				</dd>
			</dl>
			<dl>
				<dt>结束时间：</dt>
				<dd>
					<input type="text" name="ed" value="<fmt:formatDate value="${sys_now }" pattern="yyyy-MM-dd"/>" 
						onClick="WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly="true" />
					<span class="info">用来排序</span>
				</dd>
			</dl>
		</div>
		<div class="formBar">
			<ul>
				<!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
				</li>
			</ul>
		</div>
	</form>
</div>