<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:useBean id="statTask" class="com.wang.mvchain.stat.pojo.AStatTask"/>
<%@ include file="/common/include/page.jsp"%>
<div class="pageHeader">
	<form onsubmit="return navTabSearch(this);" action="${rootpath }/back/stat/statTaskList.html"
		method="post">
		<div class="searchBar">
			<table class="searchContent">
				<tr>
					<td>
						标识:
						<select name="flag">
							<option value="">请选择</option>
							<c:forEach items="${statTask.flagMap }" var="map" varStatus="stat">
								<option value="${map.value}" ${requestScope.flag == map.value ? 'selected' : '' }>${map.key }</option>
							</c:forEach>
						</select>
						状态:
						<select name="status">
							<option value="">请选择</option>
							<c:forEach items="${statTask.statusMap }" var="map" varStatus="stat">
								<option value="${map.value}" ${requestScope.status == map.value ? 'selected' : '' }>${map.key }</option>
							</c:forEach>
						</select>
					</td>
				</tr>
			</table>
			<div class="subBar">
				<table class="searchContent">
					<td>
						更新时间：
						<input type="text" name="st" value="${requestScope.st }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" />
						<input type="text" name="ed" value="${requestScope.ed }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" />
					</td>
					<td align="right">
						<div class="buttonActive">
							<div class="buttonContent">
								<button type="submit">查询</button>
							</div>
						</div>
					</td>
				</table>
			</div>
		</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li>
				<a class="edit" href="${rootpath }/back/stat/statTaskUpdate.html?stid={sid_user}"
					 target="navTab" ref="statTaskInfo"><span>查看</span></a>
			</li>
			<li>
				<a class="edit" href="${rootpath }/back/stat/statTaskUpdate.html?stid={sid_user}&operType=update"
					 target="navTab" ref="statTaskInfo"><span>修改</span></a>
			</li>
			<li>
				<a class="edit" href="${rootpath }/back/stat/statTaskAssetPrift.html?stid={sid_user}"
					target="ajaxTodo" ref="statTaskAssetPrift" title="确认执行此操作吗?"><span>计算收益</span></a>
			</li>
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138">
		<thead>
			<tr>
				<th width="40">序号</th>
				<th width="40">id</th>
				<th width="100">用户名</th>
				<th width="100">日期</th>
				<th width="50">总收益</th>
				<th width="50">手续费</th>
				<th width="50">任务数</th>
				<th width="50">卖最低价</th>
				<th width="50">卖最高价</th>
				<th width="50">买最低价</th>
				<th width="50">买最高价</th>
				<th width="50">标识</th>
				<th width="50">状态</th>
				<th width="100">创建时间</th>
				<th width="100">更新时间</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${requestScope.statTaskList }" var="statTask" varStatus="stat">
				<tr target="sid_user" rel="${statTask.id }">
					<td>${stat.count }</td>
					<td>${statTask.id }</td>
					<td>${statTask.usersObj.email  }</td>
					<td><fmt:formatDate value="${statTask.currdate }" pattern="yyyy-MM-dd"/></td>
					<td>${statTask.prift }</td>
					<td>${statTask.fee }</td>
					<td>${statTask.taskcount }</td>
					<td>${statTask.sellpricemin }</td>
					<td>${statTask.sellpricemax }</td>
					<td>${statTask.buypricemin }</td>
					<td>${statTask.buypricemax }</td>
					<td>${statTask.flagStr }</td>
					<td>${statTask.statusStr }</td>
					<td><fmt:formatDate value="${statTask.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					<td><fmt:formatDate value="${statTask.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<form onsubmit="return navTabSearch(this);" id="pageFormStatTaskList" method="post" action="${rootpath }/back/stat/statTaskList.html">
		<input type="hidden" name="st" value="${requestScope.st }">
		<input type="hidden" name="ed" value="${requestScope.ed }">
		<input type="hidden" name="flag" value="${requestScope.flag }">
		<input type="hidden" name="status" value="${requestScope.status }">
		<div class="panelBar">
			<ul style="float: right;">
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormStatTaskList','1','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>首页</span></a>
				</li>
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormStatTaskList','${requestScope.pageInfo.prePage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>上一页</span></a>
				</li>
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormStatTaskList','${requestScope.pageInfo.nextPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>下一页</span></a>
				</li>
				<li>
					<a class="button" onclick="return pageFormSubmit('pageFormStatTaskList','${requestScope.pageInfo.totalPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#"><span>末页</span></a>
				</li>
				<li>
					第<input type="text" id="currentPage" name="currentPage" value="${requestScope.pageInfo.currentPage}" size="5" maxlength="5" class="textInput">页,
					每页<input type="text" id="pageSize" name="pageSize" value="${requestScope.pageInfo.pageSize}" size="5" maxlength="5" class="textInput">条
					共${requestScope.pageInfo.totalRecord}条,共${requestScope.pageInfo.totalPage}页
					<input type="submit" onclick="" value="GO">
				</li>
			</ul>
		</div>
	</form>
</div>