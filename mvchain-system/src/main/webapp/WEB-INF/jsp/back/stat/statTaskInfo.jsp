<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<h2 class="contentTitle">任务统计查看</h2>
<div class="pageContent">
	<div class="pageFormContent nowrap" layoutH="97">
		<dl>
			<dt>id：</dt>
			<dd>
				${requestScope.statTask.id }
			</dd>
		</dl>
		<dl>
			<dt>用户名：</dt>
			<dd>
				${requestScope.statTask.usersObj.email }
			</dd>
		</dl>
		<dl>
			<dt>日期：</dt>
			<dd>
				<fmt:formatDate value="${requestScope.statTask.currdate }" pattern="yyyy-MM-dd"/>
			</dd>
		</dl>
		<dl>
			<dt>总收益：</dt>
			<dd>
				${requestScope.statTask.prift }
			</dd>
		</dl>
		<dl>
			<dt>手续费：</dt>
			<dd>
				${requestScope.statTask.fee }
			</dd>
		</dl>
		<dl>
			<dt>任务数：</dt>
			<dd>
				${requestScope.statTask.taskcount }
			</dd>
		</dl>
		<dl>
			<dt>卖最低价：</dt>
			<dd>
				${requestScope.statTask.sellpricemin }
			</dd>
		</dl>
		<dl>
			<dt>卖最高价：</dt>
			<dd>
				${requestScope.statTask.sellpricemax }
			</dd>
		</dl>
		<dl>
			<dt>买最低价：</dt>
			<dd>
				${requestScope.statTask.buypricemin }
			</dd>
		</dl>
		<dl>
			<dt>买最高价：</dt>
			<dd>
				${requestScope.statTask.buypricemax }
			</dd>
		</dl>
		<dl>
			<dt>标识：</dt>
			<dd>
				${requestScope.statTask.flagStr }
			</dd>
		</dl>
		<dl>
			<dt>状态：</dt>
			<dd>
				${requestScope.statTask.statusStr }
			</dd>
		</dl>
		<dl>
			<dt>创建时间：</dt>
			<dd>
				<fmt:formatDate value="${requestScope.statTask.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
			</dd>
		</dl>
		<dl>
			<dt>更新时间：</dt>
			<dd>
				<fmt:formatDate value="${requestScope.statTask.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/>
			</dd>
		</dl>
		
		<div class="divider"></div>
		<p>
			余额明细
		</p>
		<div class="divider"></div>
		<table class="table" width="100%">
			<thead>
				<tr>
					<th width="30">序号</th>
					<th width="100">币种</th>
					<th width="100">余额</th>
					<th width="100">冻结金额</th>
					<th width="100">折合cny</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${requestScope.statTask.balanceJSON }" var="me" varStatus="stat">
					<c:if test="${fn:startsWith(me.key,'coinid_')}">
						<tr target="sid_user" rel="${stat.count }">
							<td>${stat.count }</td>
							<td>${me.value.name }</td>
							<td>${me.value.balance}</td>
							<td>${me.value.fundmoney}</td>
							<td>${me.value.cny}</td>
						</tr>
					</c:if>
				</c:forEach>
				<tr target="sid_user" rel="${stat.count }">
					<td>合计:</td>
					<td></td>
					<td></td>
					<td></td>
					<td>${requestScope.statTask.balanceJSON.total_cny_balance }</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>