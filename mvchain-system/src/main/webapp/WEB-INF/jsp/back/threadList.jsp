<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<div class="pageHeader">
	<form onsubmit="return navTabSearch(this);" action="${rootpath }/back/admin/threadList.html"
		method="post">
		<div class="searchBar">
			<table class="searchContent">
				<tr>
					<td>
						关键字：<input type="text" name="keyword" value="${requestScope.keyword }"/>
					</td>
				</tr>
			</table>
			<div class="subBar">
				<ul>
					<li>
						<div class="buttonActive">
							<div class="buttonContent">
								<button type="submit">查询</button>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</form>
</div>
<div class="pageContent">
	<table class="table" width="100%">
		<thead>
			<tr>
				<th width="40">序号</th>
				<th width="50">线程id</th>
				<th width="400">参数</th>
				<th width="100">创建时间</th>
				<%--
				<th width="150">操作</th>
				 --%>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${requestScope.threadMap }" var="me" varStatus="stat">
				<tr target="sid_user" rel="${me.key }">
					<td>${stat.count }</td>
					<td>${me.key }</td>
					<td>${me.value }</td>
					<td><fmt:formatDate value="${me.value.createTime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					<%--
					<td>
						<a class="hreflink" href="${rootpath }/back/admin/threadListSubmit.html?thid=${me.key }" target="ajaxTodo" title="确认要停止吗?">
							停止	
						</a>
					</td>
					 --%>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>