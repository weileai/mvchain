package com.wang.mvchain.system.springmvc.interceptor;

import java.net.URLEncoder;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.codec.digest.HmacAlgorithms;
import org.apache.commons.codec.digest.HmacUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.controller.BaseController;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.users.pojo.AAdmins;
import com.wang.mvchain.users.service.IUsersService;

@Component("adminsAuthInterceptor")
public class AdminsAuthInterceptor extends BaseController implements HandlerInterceptor
{
	@Resource
	private IUsersService usersService;
	
	/* 请求的Url */
	private String verifyURL = ConstatFinalUtil.SYS_BUNDLE.getString("sso.admins.verifyurl");
	/* 私钥 */
	private String usersCenterOuterPriKey = "wangshUsersCenter2019";
	
	/**
	 * 方法执行之后执行
	 */
	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object arg2, Exception exception)
			throws Exception
	{
		// ConstatFinalUtil.SYS_LOG.info("----3 afterCompletion(执行之后执行) ----");
	}

	/**
	 * 请求完成返回视图时调用
	 */
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object arg2, ModelAndView mv)
			throws Exception
	{
		// ConstatFinalUtil.SYS_LOG.info("----2 postHandle(请求完成返回视图时调用) ----");
	}

	/**
	 * 方法执行之前执行
	 */
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object obj) throws Exception
	{
		String info = "" ;
		
		Map<String, String> paramsMap = new HashMap<String, String>();
		HttpSession session = request.getSession();
		AAdmins admins = (AAdmins) session.getAttribute("admins");
		
		if(admins != null)
		{
			return true ; 
		}
		
		//ConstatFinalUtil.SYS_LOG.info("----1 preHandle(执行之后执行) ----");
		String[] tokens = request.getParameterValues("token");
		String token = "" ; 
		if(tokens != null && tokens.length > 0)
		{
			token = tokens[tokens.length - 1]; 
		}
		
		try
		{
			if(admins == null && !"".equalsIgnoreCase(token))
			{
				/*
				 * 拼装上行的json
				 */
				/*
				 * 拼装上行的json
				 */
				/* 拼装上行信息 */
				JSONObject reqJSON = new JSONObject() ; 
				reqJSON.put("version", "1");
				reqJSON.put("method", "verifyAdminsToken");
				reqJSON.put("pubKey", UUID.randomUUID().toString());
				reqJSON.put("time", System.currentTimeMillis() + "");
				
				/* 加密 */
				/* sha256,key加密 */
				HmacUtils hmacUtils = new HmacUtils(HmacAlgorithms.HMAC_SHA_256, usersCenterOuterPriKey);
				/* 原字符串 */
				String souStr = reqJSON.getString("version") + 
						reqJSON.getString("pubKey") + reqJSON.getString("time") ; 
				/* 加密 */
				String encrypt = hmacUtils.hmacHex(souStr);
				reqJSON.put("encrypt", encrypt);
				
				JSONObject dataJSON = new JSONObject();
				dataJSON.put("token", token);
				reqJSON.put("data", dataJSON);
				
				paramsMap.put("json", reqJSON.toJSONString());
				paramsMap.put("requestURL", verifyURL);
				/* 请求对方服务器 */
				String responseStr = httpUtil.methodPost(Collections.EMPTY_MAP , paramsMap);
				
				JSONObject responseJSON = (JSONObject) JSON.parse(responseStr);
				if(responseJSON != null && "0".equalsIgnoreCase(responseJSON.get("code") + ""))
				{
					//登陆成功
					dataJSON = (JSONObject) responseJSON.get("data");
					JSONObject adminsJSON = dataJSON.getJSONObject("admins");
					String id = adminsJSON.get("id") + "";
					String email = adminsJSON.get("email") + "";
					
					Map<String, Object> condMap = new HashMap<String, Object>();
					condMap.put("ssoid", id);
					admins = this.usersService.findOneAdminsService(condMap);
					
					if(admins != null)
					{
						admins.setEmail(email);
						admins.setSsoid(Integer.valueOf(id));
						admins.setUpdatetime(new Date());
						//将令牌放到对象之中
						admins.setTicketStr(token);
						
						this.usersService.updateOneAdminsService(admins);
					}else
					{
						admins = new AAdmins();
						admins.setEmail(email);
						admins.setSsoid(Integer.valueOf(id));
						admins.setStatus(Byte.valueOf("1"));
						admins.setCreatetime(new Date());
						admins.setUpdatetime(new Date());
						//将令牌放到对象之中
						admins.setTicketStr(token);
						
						JSONObject resultDbJSON = this.usersService.saveOneAdminsService(admins);
						/*if(Integer.valueOf(resultDbJSON.get("code") + "") > 0 )
						{
							admins.setId(Integer.valueOf(resultDbJSON.get("code") + ""));
						}*/
					}
					
					session.setAttribute("admins", admins);
					return true ; 
				}
			}
		} catch (Exception e)
		{
			ConstatFinalUtil.SYS_LOG.error("请求服务器报错了:{}",paramsMap,e);
		}
		
		//ConstatFinalUtil.SYS_LOG.info(request.getRequestURL() + "----->getRequestURI:" + request.getRequestURI());
		//拼装当前访问的url
		String queryStr = request.getQueryString() == null ? "" : request.getQueryString();
		String currUrl = request.getRequestURL() + "";
		if(!"".equalsIgnoreCase(queryStr))
		{
			currUrl += "?" + queryStr ;
		}
		currUrl = URLEncoder.encode(currUrl, "UTF-8");
		String url = ConstatFinalUtil.SYS_BUNDLE.getString("sso.admins.main");
		if(url.indexOf("&") == -1 && url.indexOf("?") == -1)
		{
			url += "?returnUrl=" +  currUrl ; 
		}else
		{
			url += "&returnUrl=" +  currUrl ; 
		}
		
		response.sendRedirect(url);
		//未登陆
		return false ; 
	}
}
