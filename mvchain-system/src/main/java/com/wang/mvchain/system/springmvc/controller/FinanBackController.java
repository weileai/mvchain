package com.wang.mvchain.system.springmvc.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.controller.BaseController;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.common.util.PageInfoUtil;
import com.wang.mvchain.finan.pojo.AFinanAccount;
import com.wang.mvchain.finan.pojo.AFinanAsset;
import com.wang.mvchain.finan.pojo.AFinanAssetEnum;
import com.wang.mvchain.finan.pojo.AFinanHistory;
import com.wang.mvchain.finan.pojo.AFinanHistoryEnum;
import com.wang.mvchain.finan.service.IFinanService;
import com.wang.mvchain.outer.IOutTimerService;
import com.wang.mvchain.system.pojo.ACoinrate;
import com.wang.mvchain.system.pojo.ACoinrateEnum;
import com.wang.mvchain.system.pojo.ADept;
import com.wang.mvchain.system.pojo.AWebsite;
import com.wang.mvchain.system.pojo.AWebsiteEnum;
import com.wang.mvchain.system.service.ISystemService;
import com.wang.mvchain.system.service.IWebsiteService;
import com.wang.mvchain.users.service.IUsersService;

/**
 * 资金账户的Controller
 * 
 * @author wangshMac
 */
@Controller
@RequestMapping("/back/finan/")
public class FinanBackController extends BaseController
{
	@Resource
	private IFinanService finanService;
	@Resource
	private ISystemService systemService;
	@Resource
	private IWebsiteService websiteService;
	@Resource
	private IUsersService usersService;
	@Resource
	private IOutTimerService outTimerService;
	
	/*------资产列表开始-----*/
	/**
	 * 资产同步
	 * @return
	 */
	@RequestMapping("/assetSync")
	public String assetSync(HttpServletRequest request,HttpServletResponse response)
	{
		String info = "同步失败" ; 
		
		Map<String, Object> condMap = new HashMap<String, Object>();
		/* 查询当前的数据中是否有数据 */
		condMap.clear();
		PageInfoUtil pageInfoUtil = new PageInfoUtil() ; 
		this.finanService.findCondListFinanAssetService(pageInfoUtil, condMap);
		if(pageInfoUtil.getTotalRecord() == 0 )
		{
			/* 查询目前所有的币种 */
			condMap.clear();
			condMap.put("usersid", ConstatFinalUtil.CONFIG_JSON.get("mvchain_id") + "") ;
			condMap.put("status", "1") ;
			condMap.put("groupby", "1");
			List accountList = this.usersService.findUsersAccountStatListService(null, condMap);
			request.setAttribute("accountList", accountList);
			
			for (Iterator iterator = accountList.iterator(); iterator.hasNext();)
			{
				Map map = (Map) iterator.next();
				/* 先插入资产币种
				 * 然后再增加历史 */
				AFinanAsset asset = new AFinanAsset() ; 
				
				asset.setUsersid(Integer.valueOf(ConstatFinalUtil.CONFIG_JSON.get("mvchain_id") + ""));
				asset.setCoinid(Integer.valueOf(map.get("coinid") + ""));
				asset.setStatus(AFinanAssetEnum.STATUS_ENABLE.getStatus());
				asset.setPubtime(new Date());
				
				asset.setCreatetime(new Date());
				asset.setUpdatetime(new Date());
				JSONObject resultDbJSON = this.finanService.saveOneFinanAssetService(asset);
				info = resultDbJSON.getString("info"); 
				
				/* 保存历史 */
				AFinanHistory history = new AFinanHistory() ; 
				history.setAssetId(asset.getId());
				
				history.setNum(Double.valueOf(map.get("balance") + "") + Double.valueOf(map.get("fundmoney") + ""));
				history.setTradeType(AFinanHistoryEnum.TRADETYPE_IN.getStatus());
				history.setHistoryType(AFinanHistoryEnum.HISTORYTYPE_RECHARGE.getStatus());
				history.setStatus(AFinanHistoryEnum.STATUS_PROCCED.getStatus());
				
				history.setCreatetime(new Date());
				history.setUpdatetime(new Date());
				history.setPubtime(new Date());
				resultDbJSON = this.finanService.saveOneFinanHistoryService(history);
				info = resultDbJSON.getString("info"); 
			}
		}else
		{
			/*记录已经存在*/
			info = ConstatFinalUtil.INFO_JSON.get("13") + ""; 
		}
		
		
		JSONObject resultJSON = this.returnJSON(request, info);
		this.printOut(request, response, resultJSON + "");
		return null;
	}
	
	/**
	 * 重新计算百分比
	 * @return
	 */
	@RequestMapping("/assetResetPercent")
	public String assetResetPercent(HttpServletRequest request,HttpServletResponse response)
	{
		String info = ConstatFinalUtil.INFO_JSON.getString("2") ; 
		
		this.finanService.operAssetPercentSyncService();
		info = ConstatFinalUtil.INFO_JSON.getString("0"); 
		
		JSONObject resultJSON = this.returnJSON(request, info);
		this.printOut(request, response, resultJSON + "");
		return null;
	}
	
	/**
	 * 资产列表
	 * @return
	 */
	@RequestMapping("/assetList")
	public String assetList(HttpServletRequest request)
	{
		/* 接收参数 */
		String keyword = request.getParameter("keyword");
		String statusList = request.getParameter("statusList");
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		String usersid = request.getParameter("usersid");
		String coinid = request.getParameter("coinid");
		//分页
		String currentPage = request.getParameter("currentPage");
		String pageSize = request.getParameter("pageSize");
		
		if(keyword == null)
		{
			keyword = "" ; 
		}
		keyword = keyword.trim();
		if(statusList == null)
		{
			statusList = "" ; 
		}
		if(st == null)
		{
			st = "" ; 
		}
		if(ed == null)
		{
			ed = "" ;
		}
		if(usersid == null)
		{
			usersid = "" ; 
		}
		if(coinid == null)
		{
			coinid = "" ; 
		}
		Date stDate = null ; 
		Date edDate = null ; 
		if(!"".equalsIgnoreCase(st) && !"".equalsIgnoreCase(ed))
		{
			stDate = this.dateUtil.parseDateTime(st);
			edDate = this.dateUtil.parseDateTime(ed);
		}
		
		//设置搜索条件
		PageInfoUtil pageInfo = this.setPageInfoCond(currentPage,pageSize);
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("keyword", keyword);
		if(!"".equalsIgnoreCase(statusList))
		{
			condMap.put("status", statusList);
		}
		if(!"".equalsIgnoreCase(coinid))
		{
			condMap.put("coinid", coinid);
		}
		if(!"".equalsIgnoreCase(usersid))
		{
			condMap.put("usersid", usersid);
		}
		condMap.put("stDate", stDate);
		condMap.put("edDate", edDate);
		List<AFinanAsset> assetList = this.finanService.findCondListFinanAssetService(pageInfo,  condMap);
		
		/*存放数据*/
		request.setAttribute("assetList", assetList);
		request.setAttribute("pageInfo", pageInfo);
		/* 查询所有启用的币种 */
		condMap.clear();
		condMap.put("status", ACoinrateEnum.STATUS_ENABLE.getStatus());
		List<ACoinrate> coinrateList = this.websiteService.findCondListCoinrateService(null, condMap);
		request.setAttribute("coinrateList", coinrateList);
		
		/*存储条件*/
		request.setAttribute("keyword", keyword);
		request.setAttribute("statusList", statusList);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		request.setAttribute("coinid", coinid);
		request.setAttribute("usersid", usersid);
		return "/back/finan/assetList";
	}
	
	/**
	 * 资产添加
	 * @return
	 */
	@RequestMapping("/assetInsert")
	public String assetInsert(HttpServletRequest request)
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		/* 查询所有启用的币种 */
		condMap.clear();
		condMap.put("status", ACoinrateEnum.STATUS_ENABLE.getStatus());
		List<ACoinrate> coinrateList = this.websiteService.findCondListCoinrateService(null, condMap);
		request.setAttribute("coinrateList", coinrateList);
		return "/back/finan/assetInsert";
	}
	
	/**
	 * 资产添加提交
	 * @return
	 */
	@RequestMapping("/assetInsertSubmit")
	public String assetInsertSubmit(AFinanAsset asset,HttpServletRequest request,HttpServletResponse response)
	{
		String pubTimeStr = request.getParameter("pubTimeStr");
		asset.setPubtime(this.dateUtil.parseDateTime(pubTimeStr));
		
		asset.setCreatetime(new Date());
		asset.setUpdatetime(new Date());
		JSONObject resultDbJSON = this.finanService.saveOneFinanAssetService(asset);
		String info = resultDbJSON.getString("info"); 
		JSONObject resultJSON = this.returnJSON(request, info);
		this.printOut(request, response, resultJSON + "");
		return null;
	}
	
	/**
	 * 资产更新
	 * @return
	 */
	@RequestMapping("/assetUpdate")
	public String assetUpdate(String assetId,HttpServletRequest request)
	{
		Map<String, Object> condMap = new HashMap<String,Object>();
		
		/* 根据id查询资产 */
		condMap.clear();
		condMap.put("id", assetId);
		AFinanAsset asset = this.finanService.findOneFinanAssetService(condMap);
		request.setAttribute("asset", asset);
		
		/* 查询所有启用的币种 */
		condMap.clear();
		condMap.put("status", ACoinrateEnum.STATUS_ENABLE.getStatus());
		List<ACoinrate> coinrateList = this.websiteService.findCondListCoinrateService(null, condMap);
		request.setAttribute("coinrateList", coinrateList);
		return "/back/finan/assetUpdate";
	}
	
	/**
	 * 资产更新提交
	 * @return
	 */
	@RequestMapping("/assetUpdateSubmit")
	public String assetUpdateSubmit(String assetId,HttpServletRequest request,HttpServletResponse response)
	{
		Map<String, Object> condMap = new HashMap<String,Object>();
		
		/* 根据id查询资产 */
		condMap.clear();
		condMap.put("id", assetId);
		AFinanAsset asset = this.finanService.findOneFinanAssetService(condMap);
		request.setAttribute("asset", asset);
		
		String pubTimeStr = request.getParameter("pubTimeStr");
		asset.setPubtime(this.dateUtil.parseDateTime(pubTimeStr));
		String status = request.getParameter("status");
		asset.setStatus(Byte.valueOf(status));
		
		asset.setCreatetime(new Date());
		asset.setUpdatetime(new Date());
		JSONObject resultDbJSON = this.finanService.updateOneFinanAssetService(asset);
		String info = resultDbJSON.getString("info"); 
		JSONObject resultJSON = this.returnJSON(request, info);
		this.printOut(request, response, resultJSON + "");
		return null;
	}
	/*------资产列表结束-----*/
	
	/*------历史列表开始-----*/
	/**
	 * 历史列表
	 * @return
	 */
	@RequestMapping("/historyList")
	public String historyList(HttpServletRequest request)
	{
		/* 接收参数 */
		String keyword = request.getParameter("keyword");
		String statusList = request.getParameter("statusList");
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		
		String assetId = request.getParameter("assetId");
		String wbId = request.getParameter("wbId");
		String accountId = request.getParameter("accountId");
		String tradeType = request.getParameter("tradeType");
		String historyType = request.getParameter("historyType");
		String taskId = request.getParameter("taskId");
		//分页
		String currentPage = request.getParameter("currentPage");
		String pageSize = request.getParameter("pageSize");
		
		if(keyword == null)
		{
			keyword = "" ; 
		}
		keyword = keyword.trim();
		if(statusList == null)
		{
			statusList = "" ; 
		}
		if(st == null)
		{
			st = "" ; 
		}
		if(ed == null)
		{
			ed = "" ;
		}
		if(assetId == null)
		{
			assetId = "" ; 
		}
		if(wbId == null)
		{
			wbId = "" ; 
		}
		if(accountId == null)
		{
			accountId = "" ; 
		}
		if(tradeType == null)
		{
			tradeType = "" ; 
		}
		if(historyType == null)
		{
			historyType = "" ; 
		}
		Date stDate = null ; 
		Date edDate = null ; 
		if(!"".equalsIgnoreCase(st) && !"".equalsIgnoreCase(ed))
		{
			stDate = this.dateUtil.parseDateTime(st);
			edDate = this.dateUtil.parseDateTime(ed);
		}
		
		//设置搜索条件
		PageInfoUtil pageInfo = this.setPageInfoCond(currentPage,pageSize);
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("keyword", keyword);
		if(!"".equalsIgnoreCase(statusList))
		{
			condMap.put("status", statusList);
		}
		if(!"".equalsIgnoreCase(assetId))
		{
			condMap.put("assetId", assetId);
		}
		if(!"".equalsIgnoreCase(wbId))
		{
			condMap.put("wbId", wbId);
		}
		if(!"".equalsIgnoreCase(accountId))
		{
			condMap.put("accountId", accountId);
		}
		if(!"".equalsIgnoreCase(taskId))
		{
			condMap.put("taskId", taskId);
		}
		if(!"".equalsIgnoreCase(tradeType))
		{
			condMap.put("tradeType", tradeType);
		}
		if(!"".equalsIgnoreCase(historyType))
		{
			condMap.put("historyType", historyType);
		}
		condMap.put("stDate", stDate);
		condMap.put("edDate", edDate);
		List<AFinanHistory> historyList = this.finanService.findCondListFinanHistoryService(pageInfo,  condMap);
		
		/*存放数据*/
		request.setAttribute("historyList", historyList);
		request.setAttribute("pageInfo", pageInfo);
		/* 查询所有启用的币种 */
		condMap.clear();
		condMap.put("status", AWebsiteEnum.STATUS_ENABLE.getStatus());
		List<AWebsite> websiteList = this.websiteService.findCondListWebsiteService(null, condMap);
		request.setAttribute("websiteList", websiteList);
		
		/*存储条件*/
		request.setAttribute("keyword", keyword);
		request.setAttribute("statusList", statusList);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		request.setAttribute("assetId", assetId);
		request.setAttribute("wbId", wbId);
		request.setAttribute("accountId", accountId);
		request.setAttribute("taskId", taskId);
		request.setAttribute("tradeType", tradeType);
		request.setAttribute("historyType", historyType);
		return "/back/finan/historyList";
	}
	
	/**
	 * 历史添加
	 * @return
	 */
	@RequestMapping("/historyInsert")
	public String historyInsert(HttpServletRequest request)
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		/* 查询所有启用的网站 */
		condMap.clear();
		condMap.put("status", AWebsiteEnum.STATUS_ENABLE.getStatus());
		List<AWebsite> websiteList = this.websiteService.findCondListWebsiteService(null, condMap);
		request.setAttribute("websiteList", websiteList);
		
		String assetId = request.getParameter("assetId");
		/* 根据id查询资产 */
		condMap.clear();
		condMap.put("id", assetId);
		AFinanAsset asset = this.finanService.findOneFinanAssetService(condMap);
		request.setAttribute("asset", asset);
		
		/* 币种id */
		String cid = asset.getCoinid() + "" ; 
		condMap.clear();
		condMap.put("cid", cid);
		// 查询交易历史数据
		Map<String, Object> resultMap = this.outTimerService.operCompareService("1" , condMap);
		ADept buyDept = (ADept) resultMap.get("buyDept");
		if(buyDept != null)
		{
			request.setAttribute("buyDept", buyDept.toJSON());
		}
		//卖一
		ADept sellDept = (ADept) resultMap.get("sellDept");
		if(sellDept != null)
		{
			request.setAttribute("sellDept", sellDept.toJSON());
		}
		double price = 0 ; 
		if(ConstatFinalUtil.CONFIG_JSON.getString("USD_ID").equals(asset.getCoinid() + ""))
		{
			/* 如果是美金就是:1 */
			price = 1 ; 
		}else if(sellDept != null)
		{
			price = sellDept.getSouprice() ; 
		}
		request.setAttribute("price", price);
		return "/back/finan/historyInsert";
	}
	
	/**
	 * 历史添加提交
	 * @return
	 */
	@RequestMapping("/historyInsertSubmit")
	public String historyInsertSubmit(AFinanHistory history , HttpServletRequest request,HttpServletResponse response)
	{
		String pubTimeStr = request.getParameter("pubTimeStr");
		history.setPubtime(this.dateUtil.parseDateTime(pubTimeStr));
		history.setCreatetime(new Date());
		history.setUpdatetime(new Date());
		JSONObject resultDbJSON = this.finanService.saveOneFinanHistoryService(history);
		String info = resultDbJSON.getString("info"); 
		JSONObject resultJSON = this.returnJSON(request, info);
		this.printOut(request, response, resultJSON + "");
		return null ; 
	}
	
	/**
	 * 历史更新
	 * @return
	 */
	@RequestMapping("/historyUpdate")
	public String historyUpdate(String historyId,HttpServletRequest request)
	{
		Map<String, Object> condMap = new HashMap<String,Object>();
		
		/* 根据id查询资产 */
		condMap.clear();
		condMap.put("id", historyId);
		AFinanHistory history = this.finanService.findOneFinanHistoryService(condMap);
		request.setAttribute("history", history);
		
		AFinanAsset asset = history.getFinanAsset() ; 
		
		/* 查询所有启用的网站 */
		condMap.clear();
		condMap.put("status", AWebsiteEnum.STATUS_ENABLE.getStatus());
		List<AWebsite> websiteList = this.websiteService.findCondListWebsiteService(null, condMap);
		request.setAttribute("websiteList", websiteList);
		/* 币种id */
		String cid = asset.getCoinid() + "" ; 
		condMap.clear();
		condMap.put("cid", cid);
		// 查询交易历史数据
		Map<String, Object> resultMap = this.outTimerService.operCompareService("1" , condMap);
		ADept buyDept = (ADept) resultMap.get("buyDept");
		if(buyDept != null)
		{
			request.setAttribute("buyDept", buyDept.toJSON());
		}
		//卖一
		ADept sellDept = (ADept) resultMap.get("sellDept");
		if(sellDept != null)
		{
			request.setAttribute("sellDept", sellDept.toJSON());
		}
		double price = 0 ; 
		if(ConstatFinalUtil.CONFIG_JSON.getString("USD_ID").equals(asset.getCoinid() + ""))
		{
			/* 如果是美金就是:1 */
			price = 1 ; 
		}else if(sellDept != null)
		{
			price = sellDept.getSouprice() ; 
		}
		request.setAttribute("price", price);
		return "/back/finan/historyUpdate";
	}
	
	/**
	 * 历史更新提交
	 * @return
	 */
	@RequestMapping("/historyUpdateSubmit")
	public String historyUpdateSubmit(String historyId,HttpServletRequest request,HttpServletResponse response)
	{
		Map<String, Object> condMap = new HashMap<String,Object>();
		
		/* 根据id查询资产 */
		condMap.clear();
		condMap.put("id", historyId);
		AFinanHistory finanHistory = this.finanService.findOneFinanHistoryService(condMap);
		
		String info = "" ; 
		/* 如果订单的状态已经变成了已处理,就无法操作了 */
		if(finanHistory.getStatus() != AFinanHistoryEnum.STATUS_PROCCED.getStatus())
		{
			String wbId = request.getParameter("wbId");
			String status = request.getParameter("status");
			String num = request.getParameter("num");
			String tradeType = request.getParameter("tradeType");
			String historyType = request.getParameter("historyType");
			String content = request.getParameter("content");
			
			finanHistory.setStatus(Byte.valueOf(status));
			finanHistory.setWbId(Integer.valueOf(wbId));
			finanHistory.setNum(Double.valueOf(num));
			finanHistory.setTradeType(Byte.valueOf(tradeType));
			finanHistory.setHistoryType(Byte.valueOf(historyType));
			finanHistory.setContent(content);
			
			String pubTimeStr = request.getParameter("pubTimeStr");
			finanHistory.setPubtime(this.dateUtil.parseDateTime(pubTimeStr));
			
			finanHistory.setUpdatetime(new Date());
			JSONObject resultDbJSON = this.finanService.updateOneFinanHistoryService(finanHistory);
			info = resultDbJSON.getString("info"); 
		}else
		{
			/* "14" : "记录已经锁定,无法操作", */
			info = ConstatFinalUtil.INFO_JSON.getString("14"); 
		}
		JSONObject resultJSON = this.returnJSON(request, info);
		this.printOut(request, response, resultJSON + "");
		return null ; 
	}
	/*------历史列表结束-----*/
	
	/*------资产账户开始-----*/
	/**
	 * 资产账户列表
	 * @return
	 */
	@RequestMapping("/accountList")
	public String accountList(HttpServletRequest request)
	{
		/* 接收参数 */
		String keyword = request.getParameter("keyword");
		String statusList = request.getParameter("statusList");
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		String usersid = request.getParameter("usersid");
		String accountType = request.getParameter("accountType");
		//分页
		String currentPage = request.getParameter("currentPage");
		String pageSize = request.getParameter("pageSize");
		
		if(keyword == null)
		{
			keyword = "" ; 
		}
		keyword = keyword.trim();
		if(statusList == null)
		{
			statusList = "" ; 
		}
		if(st == null)
		{
			st = "" ; 
		}
		if(ed == null)
		{
			ed = "" ;
		}
		if(usersid == null)
		{
			usersid = "" ; 
		}
		if(accountType == null)
		{
			accountType = "" ; 
		}
		Date stDate = null ; 
		Date edDate = null ; 
		if(!"".equalsIgnoreCase(st) && !"".equalsIgnoreCase(ed))
		{
			stDate = this.dateUtil.parseDateTime(st);
			edDate = this.dateUtil.parseDateTime(ed);
		}
		
		//设置搜索条件
		PageInfoUtil pageInfo = this.setPageInfoCond(currentPage,pageSize);
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("keyword", keyword);
		if(!"".equalsIgnoreCase(statusList))
		{
			condMap.put("status", statusList);
		}
		if(!"".equalsIgnoreCase(accountType))
		{
			condMap.put("accountType", accountType);
		}
		if(!"".equalsIgnoreCase(usersid))
		{
			condMap.put("usersid", usersid);
		}
		condMap.put("stDate", stDate);
		condMap.put("edDate", edDate);
		List<AFinanAccount> accountList = this.finanService.findCondListFinanAccountService(pageInfo,  condMap);
		
		/*存放数据*/
		request.setAttribute("accountList", accountList);
		request.setAttribute("pageInfo", pageInfo);
		
		/*存储条件*/
		request.setAttribute("keyword", keyword);
		request.setAttribute("statusList", statusList);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		request.setAttribute("accountType", accountType);
		request.setAttribute("usersid", usersid);
		return "/back/finan/accountList";
	}
	
	/**
	 * 资产账户更新
	 * @return
	 */
	@RequestMapping("/accountUpdate")
	public String accountUpdate(String accountId,HttpServletRequest request)
	{
		Map<String, Object> condMap = new HashMap<String,Object>();
		
		/* 根据id查询资产 */
		condMap.clear();
		condMap.put("id", accountId);
		AFinanAccount account = this.finanService.findOneFinanAccountService(condMap);
		request.setAttribute("account", account);
		return "/back/finan/accountUpdate";
	}
	
	/**
	 * 资产账户更新提交
	 * @return
	 */
	@RequestMapping("/accountUpdateSubmit")
	public String accountUpdateSubmit(String accountId,HttpServletRequest request,HttpServletResponse response)
	{
		Map<String, Object> condMap = new HashMap<String,Object>();
		
		/* 根据id查询资产 */
		condMap.clear();
		condMap.put("id", accountId);
		AFinanAccount account = this.finanService.findOneFinanAccountService(condMap);
		request.setAttribute("account", account);
		
		String info = "" ; 
		String status = request.getParameter("status");
		
		account.setStatus(Byte.valueOf(status));
		
		String pubTimeStr = request.getParameter("pubTimeStr");
		account.setPubtime(this.dateUtil.parseDateTime(pubTimeStr));
		
		JSONObject resultDbJSON = this.finanService.updateOneFinanAccountService(account);
		info = resultDbJSON.getString("info"); 
		JSONObject resultJSON = this.returnJSON(request, info);
		this.printOut(request, response, resultJSON + "");
		return null ; 
	}
	/*------资产账户结束-----*/
}
