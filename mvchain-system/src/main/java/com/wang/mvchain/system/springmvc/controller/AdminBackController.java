package com.wang.mvchain.system.springmvc.controller;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wang.mvchain.common.controller.BaseController;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.users.pojo.AAdmins;

/**
 * 管理员的主要界面controller
 * 
 * @author Administrator
 * 
 */
@Controller
@RequestMapping("/back/admin/")
public class AdminBackController extends BaseController
{
	/**
	 * 登陆后的首页
	 * 
	 * @return
	 */
	@RequestMapping("/main.html")
	public String main(HttpServletRequest request, HttpServletResponse response)
	{
		return "/back/main";
	}
	
	/**
	 * 左侧菜单列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/leftmenu.html")
	public String leftmenu(HttpServletRequest request , HttpServletResponse response)
	{
		return "/back/leftmenu" ; 
	}
	
	/**
	 * 多线程列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/threadList.html")
	public String threadList(HttpServletRequest request , HttpServletResponse response)
	{
		/*Map<String, Map<String, Object>> threadMap = ConstatFinalUtil.THREAD_MAP ; 
		request.setAttribute("threadMap", threadMap);*/
		return "/back/threadList" ; 
	}

	/**
	 * 登陆后的首页
	 * 
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping("/logout.html")
	public String logout(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		HttpSession session = request.getSession() ; 
		AAdmins admin = (AAdmins) session.getAttribute("admins");
		session.removeAttribute("admins");
		
		
		/* 用户中心注销以后,返回的页面 */
		String returnUrl = ConstatFinalUtil.SYS_BUNDLE.getString("website.back.url");
		returnUrl = URLEncoder.encode(returnUrl,"UTF-8");
		
		/* 用户中心退出的页面 */
		String logoutUrl = ConstatFinalUtil.SYS_BUNDLE.getString("sso.admins.logout") ; 
		if(returnUrl != null && !"".equalsIgnoreCase(logoutUrl))
		{
			logoutUrl += "&returnUrl=" + returnUrl ; 
		}
		response.sendRedirect(logoutUrl);
		return null;
	}
}
