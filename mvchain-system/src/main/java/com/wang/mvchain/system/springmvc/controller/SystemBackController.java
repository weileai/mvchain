package com.wang.mvchain.system.springmvc.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.controller.BaseController;
import com.wang.mvchain.common.util.PageInfoUtil;
import com.wang.mvchain.system.pojo.ACoinrate;
import com.wang.mvchain.system.pojo.ASyspro;
import com.wang.mvchain.system.pojo.ATradeType;
import com.wang.mvchain.system.pojo.AWebsite;
import com.wang.mvchain.system.service.ISystemService;
import com.wang.mvchain.system.service.IWebsiteService;

/**
 * 系统模块的controller
 * 
 * @author Administrator
 * 
 */
@Controller
@RequestMapping("/back/system/")
public class SystemBackController extends BaseController
{
	@Resource
	private IWebsiteService websiteService;
	@Resource
	private ISystemService systemService;

	/**
	 * 支持的网站列表
	 * 
	 * @return
	 */
	@RequestMapping("/websiteList.html")
	public String websiteList(HttpServletRequest request, HttpServletResponse response)
	{
		//参数
		String keyword = request.getParameter("keyword");
		String statusList = request.getParameter("statusList");
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		//分页
		String currentPage = request.getParameter("currentPage");
		String pageSize = request.getParameter("pageSize");
		
		if(keyword == null)
		{
			keyword = "" ; 
		}
		keyword = keyword.trim();
		//设置搜索条件
		PageInfoUtil pageInfo = this.setPageInfoCond(currentPage,pageSize);
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("keyword", keyword);
		condMap.put("status", statusList);
		condMap.put("st", st);
		condMap.put("ed", ed);
		List<AWebsite> websiteList = this.websiteService.findCondListWebsiteService(pageInfo,  condMap);
		
		//存放数据
		request.setAttribute("websiteList", websiteList);
		request.setAttribute("pageInfo", pageInfo);
		//存储条件
		request.setAttribute("keyword", keyword);
		request.setAttribute("statusList", statusList);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		return "/back/system/websiteList";
	}
	
	/**
	 * 添加支持网站
	 * 
	 * @return
	 */
	@RequestMapping("/websiteInsert.html")
	public String websiteInsert(HttpServletRequest request, HttpServletResponse response)
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		//查询所有的交易类型
		condMap.clear();
		condMap.put("status", "1");
		List<ATradeType> tradeTypeList = this.websiteService.findCondListTradeTypeService(null, condMap);
		request.setAttribute("tradeTypeList", tradeTypeList);
		
		//查询所有的交易类型
		condMap.clear();
		condMap.put("status", "1");
		List<ACoinrate> coinrateList = this.websiteService.findCondListCoinrateService(null, condMap);
		request.setAttribute("coinrateList", coinrateList);
		return "/back/system/websiteInsert";
	}
	
	/**
	 * 添加支持网站提交
	 * 
	 * @return
	 */
	@RequestMapping("/websiteInsertSubmit.html")
	public String websiteInsertSubmit(HttpServletRequest request, HttpServletResponse response , AWebsite website)
	{
		String info = "" ;
		
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		//查询所有的交易类型
		condMap.clear();
		condMap.put("status", "1");
		List<ATradeType> tradeTypeList = this.websiteService.findCondListTradeTypeService(null, condMap);
		request.setAttribute("tradeTypeList", tradeTypeList);
		
		//查询所有的交易类型
		condMap.clear();
		condMap.put("status", "1");
		List<ACoinrate> coinrateList = this.websiteService.findCondListCoinrateService(null, condMap);
		request.setAttribute("coinrateList", coinrateList);
		
		String pubtimeCond = request.getParameter("pubtimeCond");
		website.setPubtime(this.dateUtil.parseDateTime(pubtimeCond));
		String[] moneystatus = request.getParameterValues("moneystatus");
		JSONObject moneyStatusJSON = website.getMoneyStatusJSON() ; 
		//值为交易类型的id
		for (int i = 0; i < moneystatus.length; i++)
		{
			String moneysta = moneystatus[i];
			for (Iterator iterator = tradeTypeList.iterator(); iterator
					.hasNext();)
			{
				ATradeType tradeType = (ATradeType) iterator.next();
				if(moneysta.equalsIgnoreCase(tradeType.getId() + ""))
				{
					JSONObject tradeTypeJSON = new JSONObject();
					tradeTypeJSON.put("id", tradeType.getId() + "");
					tradeTypeJSON.put("name", tradeType.getName() + "");
					String tradeFee = request.getParameter("tradeFee" + tradeType.getId());
					tradeTypeJSON.put("tradeFee", tradeFee + "");
					tradeTypeJSON.put("status", "1");
					
					moneyStatusJSON.put(tradeType.getId() + "", tradeTypeJSON);
				}
			}
		}
		website.setMoneystatus(moneyStatusJSON + "");
		
		//手续费
		JSONObject feeJSON = new JSONObject();
		for (Iterator iterator = coinrateList.iterator(); iterator.hasNext();)
		{
			ACoinrate coinrate = (ACoinrate) iterator.next();
			String recharge = request.getParameter("recharge" + coinrate.getId());
			String withdraw = request.getParameter("withdraw" + coinrate.getId());
			if(recharge == null)
			{
				recharge = "" ; 
			}
			if("".equalsIgnoreCase(recharge))
			{
				recharge = "0";
			}
			if(withdraw == null)
			{
				withdraw = "" ; 
			}
			if("".equalsIgnoreCase(withdraw))
			{
				withdraw = "0";
			}
			JSONObject rwJSON = new JSONObject();
			rwJSON.put("name", coinrate.getEngname());
			rwJSON.put("recharge", recharge);
			rwJSON.put("withdraw", withdraw);
			feeJSON.put(coinrate.getId() + "", rwJSON);
		}
		website.setFee(feeJSON + "");
		
		//处理接口的认证模板
		website.setCreatetime(new Date());
		website.setUpdatetime(new Date());
		JSONObject resultDBJSON = this.websiteService.saveOneWebsiteService(website);
		info = resultDBJSON.get("info") + "" ;
		
		JSONObject resultJSON = this.returnJSON(request, info);
		this.printOut(request, response, resultJSON + "");
		return null;
	}
	
	/**
	 * 更新支持网站
	 * 
	 * @return
	 */
	@RequestMapping("/websiteUpdate.html")
	public String websiteUpdate(HttpServletRequest request, HttpServletResponse response)
	{
		String wid = request.getParameter("wid");
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		condMap.clear();
		condMap.put("id", wid);
		AWebsite website = this.websiteService.findOneWebsiteService( condMap);
		request.setAttribute("website", website);
		
		//查询所有的交易类型
		condMap.clear();
		condMap.put("status", "1");
		List<ATradeType> tradeTypeList = this.websiteService.findCondListTradeTypeService(null, condMap);
		request.setAttribute("tradeTypeList", tradeTypeList);
		
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			return "/back/system/websiteUpdate";
		}
		return "/back/system/websiteInfo";
	}
	
	/**
	 * 更新支持网站提交
	 * 
	 * @return
	 */
	@RequestMapping("/websiteUpdateSubmit.html")
	public String websiteUpdateSubmit(HttpServletRequest request, HttpServletResponse response)
	{
		String info = "" ; 
		String wid = request.getParameter("wid");
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", wid);
		AWebsite website = this.websiteService.findOneWebsiteService( condMap);
		
		//查询所有的交易类型
		condMap.clear();
		condMap.put("status", "1");
		List<ATradeType> tradeTypeList = this.websiteService.findCondListTradeTypeService(null, condMap);
		request.setAttribute("tradeTypeList", tradeTypeList);
		
		//查询所有的交易类型
		condMap.clear();
		condMap.put("status", "1");
		List<ACoinrate> coinrateList = this.websiteService.findCondListCoinrateService(null, condMap);
		request.setAttribute("coinrateList", coinrateList);
		
		//接收参数
		String name = request.getParameter("name");
		String url = request.getParameter("url");
		String status = request.getParameter("status");
		String authinfo = request.getParameter("authinfo");
		String pubtimeCond = request.getParameter("pubtimeCond");
		String[] moneystatus = request.getParameterValues("moneystatus");
		
		if(moneystatus != null)
		{
			JSONObject moneyStatusJSON = new JSONObject() ; 
			//值为交易类型的id
			for (int i = 0; i < moneystatus.length; i++)
			{
				String moneysta = moneystatus[i];
				for (Iterator iterator = tradeTypeList.iterator(); iterator
						.hasNext();)
				{
					ATradeType tradeType = (ATradeType) iterator.next();
					if(moneysta.equalsIgnoreCase(tradeType.getId() + ""))
					{
						JSONObject tradeTypeJSON = new JSONObject();
						tradeTypeJSON.put("id", tradeType.getId() + "");
						tradeTypeJSON.put("name", tradeType.getName() + "");
						String tradeFee = request.getParameter("tradeFee" + tradeType.getId());
						tradeTypeJSON.put("tradeFee", tradeFee + "");
						tradeTypeJSON.put("status", "1");
						
						moneyStatusJSON.put(tradeType.getId() + "", tradeTypeJSON);
					}
				}
			}
			
			website.setMoneystatus(moneyStatusJSON + "");
		}else
		{
			website.setMoneystatus(new JSONObject() + "");
		}
		
		//手续费
		JSONObject feeJSON = new JSONObject();
		for (Iterator iterator = coinrateList.iterator(); iterator.hasNext();)
		{
			ACoinrate coinrate = (ACoinrate) iterator.next();
			String recharge = request.getParameter("recharge" + coinrate.getId());
			String withdraw = request.getParameter("withdraw" + coinrate.getId());
			if(recharge == null)
			{
				recharge = "" ; 
			}
			if("".equalsIgnoreCase(recharge))
			{
				recharge = "0";
			}
			if(withdraw == null)
			{
				withdraw = "" ; 
			}
			if("".equalsIgnoreCase(withdraw))
			{
				withdraw = "0";
			}
			JSONObject rwJSON = new JSONObject();
			rwJSON.put("name", coinrate.getEngname());
			rwJSON.put("recharge", recharge);
			rwJSON.put("withdraw", withdraw);
			feeJSON.put(coinrate.getId() + "", rwJSON);
		}
		website.setFee(feeJSON + "");
		
		//参数赋值
		website.setName(name);
		website.setUrl(url);
		website.setStatus(Byte.valueOf(status));
		website.setAuthinfo(authinfo);
		website.setPubtime(this.dateUtil.parseDateTime(pubtimeCond));
		
		//更新数据
		website.setUpdatetime(new Date());
		JSONObject resultDBJSON = this.websiteService.updateOneWebsiteService( website);
		info = resultDBJSON.get("info") + "" ;
		
		JSONObject resultJSON = this.returnJSON(request, info);
		this.printOut(request, response, resultJSON + "");
		return null;
	}
	
	/**
	 * 货币列表
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping("/coinrateList.html")
	public String coinrateList(HttpServletRequest request , HttpServletResponse response) throws IOException
	{
		String keyword = request.getParameter("keyword");
		String statusList = request.getParameter("statusList");
		String prostatusList = request.getParameter("prostatusList");
		String currentPage = request.getParameter("currentPage");
		String pageSize = request.getParameter("pageSize");
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		if(keyword == null)
		{
			keyword = "" ; 
		}
		keyword = keyword.trim();
		
		PageInfoUtil pageInfo = this.setPageInfoCond(currentPage , pageSize);
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("keyword", keyword);
		condMap.put("status", statusList);
		condMap.put("prostatus", prostatusList);
		condMap.put("st", st);
		condMap.put("ed", ed);
		
		List<ACoinrate> coinrateList = this.websiteService.findCondListCoinrateService(pageInfo,  condMap);
		request.setAttribute("coinrateList", coinrateList);
		
		
		request.setAttribute("pageInfo", pageInfo);
		request.setAttribute("keyword", keyword);
		request.setAttribute("statusList", statusList);
		request.setAttribute("prostatusList", prostatusList);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		return "/back/system/coinrateList" ; 
	}
	
	/**
	 * 货币添加
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/coinrateInsert.html")
	public String coinrateInsert(HttpServletRequest request , HttpServletResponse response)
	{
		return "/back/system/coinrateInsert" ; 
	}
	
	/**
	 * 货币添加提交
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping("/coinrateInsertSubmit.html")
	public String coinrateInsertSubmit(HttpServletRequest request , HttpServletResponse response , ACoinrate coinrate) throws IOException
	{
		String info = "添加失败" ; 
		String pubtime = request.getParameter("pubtimeCond");
		coinrate.setPubtime(this.dateUtil.parseDateTime(pubtime));
		
		coinrate.setCreatetime(new Date());
		coinrate.setUpdatetime(new Date());
		JSONObject resDBJSON = this.websiteService.saveOneCoinrateService(coinrate);
		info = resDBJSON.get("info") + "";
		JSONObject resultJSON = this.returnJSON(request, info);
		
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter() ;
		out.println(resultJSON);
		out.flush();
		out.close();
		return null ;  
	}
	
	/**
	 * 货币更新
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping("/coinrateUpdate.html")
	public String coinrateUpdate(HttpServletRequest request , HttpServletResponse response) throws IOException
	{
		String operType = request.getParameter("operType");
		String cid = request.getParameter("cid");
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", cid);
		ACoinrate coinrate = this.websiteService.findOneCoinrateService( condMap);
		request.setAttribute("coinrate", coinrate);
		
		if("update".equalsIgnoreCase(operType))
		{
			return "/back/system/coinrateUpdate" ; 
		}
		
		return "/back/system/coinrateInfo" ; 
	}
	
	/**
	 * 货币更新提交
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping("/coinrateUpdateSubmit.html")
	public String coinrateUpdateSubmit(HttpServletRequest request , HttpServletResponse response) throws IOException
	{
		String info = "更新失败" ; 
		String cid = request.getParameter("cid");
		
		//查看用户名是否重复
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", cid);
		ACoinrate coinrate = this.websiteService.findOneCoinrateService( condMap);
		
		String name = request.getParameter("name");
		String engname = request.getParameter("engname");
		String status = request.getParameter("status");
		String prostatus = request.getParameter("prostatus");
		String content = request.getParameter("content");
		String extend = request.getParameter("extend");
		
		String pubtime = request.getParameter("pubtimeCond");
		coinrate.setPubtime(this.dateUtil.parseDateTime(pubtime));
		
		coinrate.setName(name);
		coinrate.setEngname(engname);
		coinrate.setProstatus(Byte.valueOf(prostatus));
		coinrate.setStatus(Byte.valueOf(status));
		coinrate.setExtend(extend);
		coinrate.setContent(content);
		
		coinrate.setUpdatetime(new Date());
		JSONObject resDBJSON = this.websiteService.updateOneCoinrateService( coinrate);
		info = resDBJSON.get("info") + "";
		
		JSONObject resultJSON = this.returnJSON(request, info);
		
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter() ;
		out.println(resultJSON);
		out.flush();
		out.close();
		return null ;
	}
	
	/**
	 * 货币更新提交
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping("/coinrateBatch.html")
	public String coinrateBatch(HttpServletRequest request , HttpServletResponse response) throws IOException
	{
		String info = "操作失败" ; 
		/* 批量更新此种交易币在哪个网站上有 */
		String operType = request.getParameter("operType");
		if("coinBatch".equalsIgnoreCase(operType))
		{
			Map<String, JSONArray> resultMap = this.websiteService.operFindCommonPairService();
			if(resultMap.size() > 0 )
			{
				info = "更新成功" ; 
			}
		}else if("coinTradeBatch".equalsIgnoreCase(operType))
		{
			JSONObject resDbJSON = this.websiteService.operBatchCoinrateTradeTypeService();
			info = resDbJSON.getString("info") ; 
		}else if("websiteBatch".equalsIgnoreCase(operType))
		{
			JSONObject resDbJSON = this.websiteService.operBatchWebsiteBatchService();
			info = resDbJSON.getString("info") ; 
		}
		
		JSONObject resultJSON = this.returnJSON(request, info);
		
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter() ;
		out.println(resultJSON);
		out.flush();
		out.close();
		return null ;
	}
	
	/**
	 * 交易类型列表
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping("/tradeTypeList.html")
	public String tradeTypeList(HttpServletRequest request , HttpServletResponse response) throws IOException
	{
		String keyword = request.getParameter("keyword");
		String fromcoinidList = request.getParameter("fromcoinidList");
		String tocoinidList = request.getParameter("tocoinidList");
		String statusList = request.getParameter("statusList");
		String currentPage = request.getParameter("currentPage");
		String pageSize = request.getParameter("pageSize");
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		if(keyword == null)
		{
			keyword = "" ; 
		}
		PageInfoUtil pageInfo = this.setPageInfoCond(currentPage , pageSize);
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("keyword", keyword);
		condMap.put("fromcoinid", fromcoinidList);
		condMap.put("tocoinid", tocoinidList);
		condMap.put("status", statusList);
		condMap.put("st", st);
		condMap.put("ed", ed);
		
		List<ATradeType> tradeTypeList = this.websiteService.findCondListTradeTypeService(pageInfo,  condMap);
		
		condMap.clear();
		condMap.put("status", "1");
		List<ACoinrate> coinrateList = this.websiteService.findCondListCoinrateService(null, condMap);
		
		request.setAttribute("tradeTypeList", tradeTypeList);
		request.setAttribute("coinrateList", coinrateList);
		request.setAttribute("pageInfo", pageInfo);
		
		request.setAttribute("keyword", keyword);
		request.setAttribute("statusList", statusList);
		request.setAttribute("fromcoinidList", fromcoinidList);
		request.setAttribute("tocoinidList", tocoinidList);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		return "/back/system/tradeTypeList" ; 
	}
	
	/**
	 * 交易类型添加
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/tradeTypeInsert.html")
	public String tradeTypeInsert(HttpServletRequest request , HttpServletResponse response)
	{
		//查询所有的币种
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		condMap.clear();
		condMap.put("status", "1");
		List<ACoinrate> coinrateList = this.websiteService.findCondListCoinrateService(null, condMap);
		request.setAttribute("coinrateList", coinrateList);
		
		return "/back/system/tradeTypeInsert" ; 
	}
	
	/**
	 * 交易类型添加提交
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping("/tradeTypeInsertSubmit.html")
	public String tradeTypeInsertSubmit(HttpServletRequest request , HttpServletResponse response , ATradeType tradeType) throws IOException
	{
		String info = "添加失败" ; 
		String pubtime = request.getParameter("pubtimeCond");
		tradeType.setPubtime(this.dateUtil.parseDateTime(pubtime));
		
		tradeType.setCreatetime(new Date());
		tradeType.setUpdatetime(new Date());
		JSONObject resDBJSON = this.websiteService.saveOneTradeTypeService(tradeType);
		info = resDBJSON.get("info") + "";
		JSONObject resultJSON = this.returnJSON(request, info);
		
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter() ;
		out.println(resultJSON);
		out.flush();
		out.close();
		return null ;  
	}
	
	/**
	 * 交易类型更新
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping("/tradeTypeUpdate.html")
	public String tradeTypeUpdate(HttpServletRequest request , HttpServletResponse response) throws IOException
	{
		String operType = request.getParameter("operType");
		String tid = request.getParameter("tid");
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", tid);
		ATradeType tradeType = this.websiteService.findOneTradeTypeService( condMap);
		request.setAttribute("tradeType", tradeType);
		
		if("update".equalsIgnoreCase(operType))
		{
			
			condMap.clear();
			condMap.put("status", "1");
			List<ACoinrate> coinrateList = this.websiteService.findCondListCoinrateService(null, condMap);
			request.setAttribute("coinrateList", coinrateList);
			
			return "/back/system/tradeTypeUpdate" ; 
		}
		
		return "/back/system/tradeTypeInfo" ; 
	}
	
	/**
	 * 交易类型更新提交
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping("/tradeTypeUpdateSubmit.html")
	public String tradeTypeUpdateSubmit(HttpServletRequest request , HttpServletResponse response) throws IOException
	{
		String info = "更新失败" ; 
		String tid = request.getParameter("tid");
		
		//查看用户名是否重复
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", tid);
		ATradeType tradeType = this.websiteService.findOneTradeTypeService( condMap);
		
		String fromcoinid = request.getParameter("fromcoinid");
		String tocoinid = request.getParameter("tocoinid");
		String name = request.getParameter("name");
		String rate = request.getParameter("rate");
		String status = request.getParameter("status");
		String content = request.getParameter("content");
		
		String pubtime = request.getParameter("pubtimeCond");
		tradeType.setPubtime(this.dateUtil.parseDateTime(pubtime));
		
		tradeType.setName(name);
		tradeType.setRate(Double.valueOf(rate));
		tradeType.setStatus(Byte.valueOf(status));
		tradeType.setContent(content);
		tradeType.setFromcoinid(Integer.valueOf(fromcoinid));
		tradeType.setTocoinid(Integer.valueOf(tocoinid));
		
		tradeType.setUpdatetime(new Date());
		JSONObject resDBJSON = this.websiteService.updateOneTradeTypeService( tradeType);
		info = resDBJSON.get("info") + "";
		
		JSONObject resultJSON = this.returnJSON(request, info);
		
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter() ;
		out.println(resultJSON);
		out.flush();
		out.close();
		return null ;
	}
	
	/**
	 * 支持的网站列表
	 * 
	 * @return
	 */
	@RequestMapping("/sysproList.html")
	public String sysproList(HttpServletRequest request, HttpServletResponse response)
	{
		//参数
		String keyword = request.getParameter("keyword");
		String statusList = request.getParameter("statusList");
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		//分页
		String currentPage = request.getParameter("currentPage");
		String pageSize = request.getParameter("pageSize");
		
		if(keyword == null)
		{
			keyword = "" ; 
		}
		keyword = keyword.trim();
		//设置搜索条件
		PageInfoUtil pageInfo = this.setPageInfoCond(currentPage,pageSize);
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("keyword", keyword);
		condMap.put("status", statusList);
		condMap.put("st", st);
		condMap.put("ed", ed);
		List<ASyspro> sysproList = this.systemService.findCondListSysproService(pageInfo,  condMap);
		
		//存放数据
		request.setAttribute("sysproList", sysproList);
		request.setAttribute("pageInfo", pageInfo);
		//存储条件
		request.setAttribute("keyword", keyword);
		request.setAttribute("statusList", statusList);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		return "/back/system/sysproList";
	}
	
	/**
	 * 添加支持网站
	 * 
	 * @return
	 */
	@RequestMapping("/sysproInsert.html")
	public String sysproInsert(HttpServletRequest request, HttpServletResponse response)
	{
		return "/back/system/sysproInsert";
	}
	
	/**
	 * 添加支持网站提交
	 * 
	 * @return
	 */
	@RequestMapping("/sysproInsertSubmit.html")
	public String sysproInsertSubmit(HttpServletRequest request, HttpServletResponse response , ASyspro syspro)
	{
		String info = "" ;
		
		String pubtimeCond = request.getParameter("pubtimeCond");
		syspro.setPubtime(this.dateUtil.parseDateTime(pubtimeCond));
		
		//处理接口的认证模板
		syspro.setCreatetime(new Date());
		syspro.setUpdatetime(new Date());
		JSONObject resultDBJSON = this.systemService.saveOneSysproService(syspro);
		info = resultDBJSON.get("info") + "" ;
		
		JSONObject resultJSON = this.returnJSON(request, info);
		this.printOut(request, response, resultJSON + "");
		return null;
	}
	
	/**
	 * 更新支持网站
	 * 
	 * @return
	 */
	@RequestMapping("/sysproUpdate.html")
	public String sysproUpdate(HttpServletRequest request, HttpServletResponse response)
	{
		String sid = request.getParameter("sid");
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		condMap.clear();
		condMap.put("id", sid);
		ASyspro syspro = this.systemService.findOneSysproService( condMap);
		request.setAttribute("syspro", syspro);
		
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			return "/back/system/sysproUpdate";
		}
		return "/back/system/sysproInfo";
	}
	
	/**
	 * 更新支持网站提交
	 * 
	 * @return
	 */
	@RequestMapping("/sysproUpdateSubmit.html")
	public String sysproUpdateSubmit(HttpServletRequest request, HttpServletResponse response)
	{
		String info = "" ; 
		String sid = request.getParameter("sid");
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", sid);
		ASyspro syspro = this.systemService.findOneSysproService( condMap);
		
		//接收参数
		String name = request.getParameter("name");
		String vals = request.getParameter("vals");
		String content = request.getParameter("content");
		String status = request.getParameter("status");
		
		//参数赋值
		syspro.setName(name);
		syspro.setVals(vals);
		syspro.setContent(content);
		syspro.setStatus(Byte.valueOf(status));
		
		String pubtimeCond = request.getParameter("pubtimeCond");
		syspro.setPubtime(this.dateUtil.parseDateTime(pubtimeCond));
		
		//更新数据
		syspro.setUpdatetime(new Date());
		JSONObject resultDBJSON = this.systemService.updateOneSysproService( syspro);
		info = resultDBJSON.get("info") + "" ;
		
		JSONObject resultJSON = this.returnJSON(request, info);
		this.printOut(request, response, resultJSON + "");
		return null;
	}
}
