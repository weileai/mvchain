package com.wang.mvchain.system.springmvc.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.controller.BaseController;
import com.wang.mvchain.common.util.PageInfoUtil;
import com.wang.mvchain.system.pojo.ACoinrate;
import com.wang.mvchain.system.pojo.AWebsite;
import com.wang.mvchain.system.service.IWebsiteService;
import com.wang.mvchain.users.pojo.AUsers;
import com.wang.mvchain.users.pojo.AUsersAccount;
import com.wang.mvchain.users.pojo.AUsersTask;
import com.wang.mvchain.users.pojo.AUsersTaskOrder;
import com.wang.mvchain.users.service.IOrdersService;
import com.wang.mvchain.users.service.IUsersService;

/**
 * 用户后台的Controller
 * 
 * @author Administrator
 * 
 */
@Controller
@RequestMapping("/back/users/")
public class UsersBackController extends BaseController
{
	@Resource
	private IUsersService usersService;
	@Resource
	private IWebsiteService websiteService;
	@Resource
	private IOrdersService ordersService;
	
	/**
	 * 用户列表
	 * 
	 * @return
	 */
	@RequestMapping("/usersList.html")
	public String usersList(HttpServletRequest request, HttpServletResponse response)
	{
		//参数
		String keyword = request.getParameter("keyword");
		String statusList = request.getParameter("statusList");
		String prostatusList = request.getParameter("prostatusList");
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		//分页
		String currentPage = request.getParameter("currentPage");
		String pageSize = request.getParameter("pageSize");
		
		if(keyword == null)
		{
			keyword = "" ; 
		}
		keyword = keyword.trim();
		//设置搜索条件
		PageInfoUtil pageInfo = this.setPageInfoCond(currentPage,pageSize);
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("keyword", keyword);
		condMap.put("status", statusList);
		condMap.put("prostatus", prostatusList);
		condMap.put("st", st);
		condMap.put("ed", ed);
		List<AUsers> usersList = this.usersService.findCondListUsersService(pageInfo,  condMap);
		
		//存放数据
		request.setAttribute("usersList", usersList);
		request.setAttribute("pageInfo", pageInfo);
		//存储条件
		request.setAttribute("keyword", keyword);
		request.setAttribute("statusList", statusList);
		request.setAttribute("prostatusList", prostatusList);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		return "/back/users/usersList";
	}
	
	/**
	 * 用户更新
	 * 
	 * @return
	 */
	@RequestMapping("/usersUpdate.html")
	public String usersUpdate(HttpServletRequest request, HttpServletResponse response)
	{
		String uid = request.getParameter("uid");
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		condMap.clear();
		condMap.put("id", uid);
		AUsers users = this.usersService.findOneUsersService( condMap);
		request.setAttribute("users", users);
		
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			return "/back/users/usersUpdate";
		}
		return "/back/users/usersInfo";
	}
	
	/**
	 * 用户更新提交
	 * 
	 * @return
	 */
	@RequestMapping("/usersUpdateSubmit.html")
	public String usersUpdateSubmit(HttpServletRequest request, HttpServletResponse response)
	{
		String info = "" ; 
		String uid = request.getParameter("uid");
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", uid);
		AUsers users = this.usersService.findOneUsersService( condMap);
		
		//接收参数
		String prostatus = request.getParameter("prostatus");
		String status = request.getParameter("status");
		String maxtask = request.getParameter("maxtask");
		
		users.setStatus(Byte.valueOf(status));
		users.setMaxtask(Integer.valueOf(maxtask));
		users.setProstatus(Byte.valueOf(prostatus));
		
		//更新数据
		users.setUpdatetime(new Date());
		JSONObject resultDBJSON = this.usersService.updateOneUsersService( users);
		info = resultDBJSON.get("info") + "" ;
		
		JSONObject resultJSON = this.returnJSON(request, info);
		this.printOut(request, response, resultJSON + "");
		return null;
	}
	
	/**
	 * 支持的网站列表
	 * 
	 * @return
	 */
	@RequestMapping("/usersAccountList.html")
	public String usersAccountList(HttpServletRequest request, HttpServletResponse response)
	{
		//参数
		String keyword = request.getParameter("keyword");
		String statusList = request.getParameter("statusList");
		String usersidList = request.getParameter("usersidList");
		String coinidList = request.getParameter("coinidList");
		String wbidList = request.getParameter("wbidList");
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		//分页
		String currentPage = request.getParameter("currentPage");
		String pageSize = request.getParameter("pageSize");
		
		if(keyword == null)
		{
			keyword = "" ; 
		}
		keyword = keyword.trim();
		//设置搜索条件
		PageInfoUtil pageInfo = this.setPageInfoCond(currentPage,pageSize);
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("keyword", keyword);
		condMap.put("status", statusList);
		condMap.put("usersid", usersidList);
		condMap.put("coinid", coinidList);
		condMap.put("wbid", wbidList);
		condMap.put("st", st);
		condMap.put("ed", ed);
		List<AUsersAccount> usersAccountList = this.usersService.findCondListUsersAccountService
				(pageInfo,  condMap);
		
		//存放数据
		request.setAttribute("usersAccountList", usersAccountList);
		request.setAttribute("pageInfo", pageInfo);
		
		condMap.clear();
		List<ACoinrate> coinrateList = this.websiteService.findCondListCoinrateService(null, condMap);
		request.setAttribute("coinrateList", coinrateList);
		
		condMap.clear();
		List<AWebsite> websiteList = this.websiteService.findCondListWebsiteService(null, condMap);
		request.setAttribute("websiteList", websiteList);
		
		//存储条件
		request.setAttribute("keyword", keyword);
		request.setAttribute("statusList", statusList);
		request.setAttribute("usersidList", usersidList);
		request.setAttribute("coinidList", coinidList);
		request.setAttribute("wbidList", wbidList);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		return "/back/users/usersAccountList";
	}
	
	/**
	 * 更新支持网站
	 * 
	 * @return
	 */
	@RequestMapping("/usersAccountUpdate.html")
	public String usersAccountUpdate(HttpServletRequest request, HttpServletResponse response)
	{
		String uaid = request.getParameter("uaid");
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		condMap.clear();
		condMap.put("id", uaid);
		AUsersAccount usersAccount = this.usersService.findOneUsersAccountService(condMap);
		request.setAttribute("usersAccount", usersAccount);
		
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			return "/back/users/usersAccountUpdate";
		}
		return "/back/users/usersAccountInfo";
	}
	
	/**
	 * 任务列表
	 * 
	 * @return
	 */
	@RequestMapping("/usersTaskList.html")
	public String usersTaskList(HttpServletRequest request, HttpServletResponse response)
	{
		//参数
		String keyword = request.getParameter("keyword");
		String statusList = request.getParameter("statusList");
		String usersidList = request.getParameter("usersidList");
		String soutypeList = request.getParameter("soutypeList");
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		//分页
		String currentPage = request.getParameter("currentPage");
		String pageSize = request.getParameter("pageSize");
		
		if(st == null)
		{
			st = "" ; 
		}
		if(ed == null)
		{
			ed = "" ; 
		}
		
		//处理日期
		Date stDate = null ; 
		Date edDate = null ; 
		if(!"".equalsIgnoreCase(st) && !"".equalsIgnoreCase(ed))
		{
			stDate = this.dateUtil.parseDateTime(st);
			edDate = this.dateUtil.parseDateTime(ed);
		}
		
		if(keyword == null)
		{
			keyword = "" ; 
		}
		keyword = keyword.trim();
		//设置搜索条件
		PageInfoUtil pageInfo = this.setPageInfoCond(currentPage,pageSize);
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("keyword", keyword);
		condMap.put("status", statusList);
		condMap.put("usersid", usersidList);
		condMap.put("soutype", soutypeList);
		condMap.put("stDate", stDate);
		condMap.put("edDate", edDate);
		List<AUsersTask> usersTaskList = this.ordersService.findCondListUsersTaskService(pageInfo,  condMap);
		
		//存放数据
		request.setAttribute("usersTaskList", usersTaskList);
		request.setAttribute("pageInfo", pageInfo);
		//存储条件
		request.setAttribute("keyword", keyword);
		request.setAttribute("statusList", statusList);
		request.setAttribute("usersidList", usersidList);
		request.setAttribute("soutypeList", soutypeList);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		return "/back/users/usersTaskList";
	}
	
	/**
	 * 任务更新
	 * 
	 * @return
	 */
	@RequestMapping("/usersTaskUpdate.html")
	public String usersTaskUpdate(HttpServletRequest request, HttpServletResponse response)
	{
		String utid = request.getParameter("utid");
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		condMap.clear();
		condMap.put("id", utid);
		AUsersTask usersTask = this.ordersService.findOneUsersTaskService( condMap);
		request.setAttribute("usersTask", usersTask);
		
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			return "/back/users/usersTaskUpdate";
		}
		return "/back/users/usersTaskInfo";
	}
	
	/**
	 * 任务订单列表
	 * 
	 * @return
	 */
	@RequestMapping("/usersTaskOrderList.html")
	public String usersTaskOrderList(HttpServletRequest request, HttpServletResponse response)
	{
		//参数
		String keyword = request.getParameter("keyword");
		String statusList = request.getParameter("statusList");
		String utidList = request.getParameter("utidList");
		String ordertypeList = request.getParameter("ordertypeList");
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		//分页
		String currentPage = request.getParameter("currentPage");
		String pageSize = request.getParameter("pageSize");
		
		if(keyword == null)
		{
			keyword = "" ; 
		}
		keyword = keyword.trim();
		//设置搜索条件
		PageInfoUtil pageInfo = this.setPageInfoCond(currentPage,pageSize);
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("keyword", keyword);
		condMap.put("status", statusList);
		condMap.put("utid", utidList);
		condMap.put("ordertype", ordertypeList);
		condMap.put("st", st);
		condMap.put("ed", ed);
		List<AUsersTaskOrder> usersTaskOrderList = this.ordersService.findCondListUsersTaskOrderService(pageInfo,  condMap);
		
		//存放数据
		request.setAttribute("usersTaskOrderList", usersTaskOrderList);
		request.setAttribute("pageInfo", pageInfo);
		//存储条件
		request.setAttribute("keyword", keyword);
		request.setAttribute("statusList", statusList);
		request.setAttribute("utidList", utidList);
		request.setAttribute("ordertypeList", ordertypeList);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		return "/back/users/usersTaskOrderList";
	}
	
	/**
	 * 任务订单更新
	 * 
	 * @return
	 */
	@RequestMapping("/usersTaskOrderUpdate.html")
	public String usersTaskOrderUpdate(HttpServletRequest request, HttpServletResponse response)
	{
		String utoid = request.getParameter("utoid");
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		condMap.clear();
		condMap.put("id", utoid);
		AUsersTaskOrder usersTaskOrder = this.ordersService.findOneUsersTaskOrderService( condMap);
		request.setAttribute("usersTaskOrder", usersTaskOrder);
		
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			return "/back/users/usersTaskOrderUpdate";
		}
		return "/back/users/usersTaskOrderInfo";
	}
}
