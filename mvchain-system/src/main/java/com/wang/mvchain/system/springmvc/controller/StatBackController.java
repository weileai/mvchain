package com.wang.mvchain.system.springmvc.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.controller.BaseController;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.common.util.ExecutorServiceUtil;
import com.wang.mvchain.common.util.PageInfoUtil;
import com.wang.mvchain.finan.service.IFinanService;
import com.wang.mvchain.stat.pojo.AStatTask;
import com.wang.mvchain.stat.service.IStatService;
import com.wang.mvchain.system.pojo.AWebsite;
import com.wang.mvchain.util.ServiceCallableUtil;

/**
 * 系统模块的controller
 * 
 * @author Administrator
 * 
 */
@Controller
@RequestMapping("/back/stat/")
public class StatBackController extends BaseController
{
	@Resource
	private IStatService statService;
	@Resource
	private IFinanService finanService;

	/**
	 * 用户任务
	 * 
	 * @return
	 */
	@RequestMapping("/statTaskList.html")
	public String statTaskList(HttpServletRequest request, HttpServletResponse response)
	{
		//参数
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		String flag = request.getParameter("flag");
		String status = request.getParameter("status");
		
		//分页
		String currentPage = request.getParameter("currentPage");
		String pageSize = request.getParameter("pageSize");
		
		if(flag == null)
		{
			flag = "" ; 
		}
		if(status == null)
		{
			status = "" ; 
		}
		
		//设置搜索条件
		PageInfoUtil pageInfo = this.setPageInfoCond(currentPage,pageSize);
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("st", st);
		condMap.put("ed", ed);
		condMap.put("flag", flag);
		condMap.put("status", status);
		List<AStatTask> statTaskList = this.statService.findCondListStatTaskService(pageInfo, condMap);
		
		//存放数据
		request.setAttribute("statTaskList", statTaskList);
		request.setAttribute("pageInfo", pageInfo);
		//存储条件
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		request.setAttribute("status", status);
		request.setAttribute("flag", flag);
		return "/back/stat/statTaskList";
	}
	
	/**
	 * 添加支持网站
	 * 
	 * @return
	 */
	@RequestMapping("/statTaskInsert.html")
	public String statTaskInsert(HttpServletRequest request, HttpServletResponse response)
	{
		return "/back/stat/statTaskInsert";
	}
	
	/**
	 * 添加支持网站提交
	 * 
	 * @return
	 */
	@RequestMapping("/statTaskInsertSubmit.html")
	public String statTaskInsertSubmit(HttpServletRequest request, HttpServletResponse response , AWebsite statTask)
	{
		String info = "" ;
		//参数
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		Map<String, Object> paramsMap = new HashMap<String, Object>();
		
		if(st == null)
		{
			st = ""; 
		}
		if(ed == null)
		{
			ed = ""; 
		}
		Date stDate = null ; 
		Date edDate = null ; 
		
		if(!"".equalsIgnoreCase(st) && !"".equalsIgnoreCase(ed))
		{
			stDate = this.dateUtil.parseDate(st);
			edDate = this.dateUtil.parseDate(ed);
			
			paramsMap.put("stDate", stDate);
			paramsMap.put("edDate", edDate);
			
			ServiceCallableUtil multiThreadUtil = new ServiceCallableUtil();
			multiThreadUtil.setParamsMap(paramsMap);
			multiThreadUtil.setStatService(statService);
			multiThreadUtil.setOperType("updateStatTaskService");
			
			ExecutorServiceUtil.submit(multiThreadUtil);
			info = "统计用户任务线程已经启动" ;
		}else
		{
			info = "开始时间或者时间不能为空" ;
		}
		
		JSONObject resultJSON = this.returnJSON(request, info);
		this.printOut(request, response, resultJSON + "");
		return null;
	}
	
	/**
	 * 添加支持网站
	 * 
	 * @return
	 */
	@RequestMapping("/statTaskUpdate.html")
	public String statTaskUpdate(HttpServletRequest request, HttpServletResponse response)
	{
		String stid = request.getParameter("stid");
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", stid);
		AStatTask statTask = this.statService.findOneStatTaskService(condMap);
		request.setAttribute("statTask", statTask);
		
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			return "/back/stat/statTaskUpdate";
		}
		return "/back/stat/statTaskInfo";
	}
	
	/**
	 * 添加支持网站
	 * 
	 * @return
	 */
	@RequestMapping("/statTaskUpdateSubmit.html")
	public String statTaskUpdateSubmit(HttpServletRequest request, HttpServletResponse response)
	{
		String stid = request.getParameter("stid");
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", stid);
		AStatTask statTask = this.statService.findOneStatTaskService(condMap);
		
		String info = ConstatFinalUtil.INFO_JSON.getString("-2") ; 
		
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			String status = request.getParameter("status");
			String flag = request.getParameter("flag");
			statTask.setFlag(Byte.valueOf(flag));
			statTask.setStatus(Byte.valueOf(status));
			/* 更新数据库 */
			JSONObject resultDBJSON = this.statService.updateOneStatTaskService(statTask);
			info = resultDBJSON.getString("info");
		}
		/* 处理返回结果 */
		JSONObject resultJSON = this.returnJSON(request, info);
		this.printOut(request, response, resultJSON + "");
		return null;
	}
	
	/**
	 * 重新计算收益
	 * 
	 * @return
	 */
	@RequestMapping("/statTaskAssetPrift.html")
	public String statTaskAssetPrift(HttpServletRequest request, HttpServletResponse response)
	{
		String info = ConstatFinalUtil.INFO_JSON.getString("-2") ;
		Map<String, Object> condMap = new HashMap<String, Object>(); 
		String stid = request.getParameter("stid");
		if(stid == null)
		{
			stid = "" ; 
		}
		if(!"".equalsIgnoreCase(stid))
		{
			/* 根据任务id查询任务 */
			condMap.clear();
			condMap.put("id", stid);
			AStatTask statTask = this.statService.findOneStatTaskService(condMap);
			/* 计算收益 */
			if(statTask != null)
			{
				JSONObject resultDbJSON = this.finanService.operAssetPriftService(statTask);
				info = resultDbJSON.getString("info") ;
			}
		}
		
		JSONObject resultJSON = this.returnJSON(request, info);
		this.printOut(request, response, resultJSON + "");
		return null;
	}
}
