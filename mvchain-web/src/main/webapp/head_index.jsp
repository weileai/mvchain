<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>项目首页 - mvchain</title>
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<div class="container main">
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<div class="row">
						<div class="col-xs-11 col-sm-12">
							<h2 class="sub-header">
								<small>
									<a href="${rootpath }/coinrateList.html?menuType=2" target="_blank">币种列表</a>
									<a href="${rootpath }/head/users/main.html" target="_blank">用户登陆</a>
								</small>
							</h2>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>日期</th>
									<th>升级明细</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>未上线功能</td>
									<td>
<pre>
-->将任务划分为长期购买
</pre>
									</td>
								</tr>
								<tr>
									<td>20190501</td>
									<td>
<pre>
-->买卖单同时(多线程)下单
-->每个用户同时进行的最大任务数(可设置)
-->行情中增加了合并交易量的功能

-->所有小数点统一精确到3位小数
-->增加了任务时时更新价格

-->撤单失败发送通知邮件
-->在订单列表中增加撤单的操作
-->更新任务价格重新发起一个线程(待定)
</pre>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>