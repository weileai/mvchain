<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html>
	<head>
		<title>${info } - mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<%@ include file="/common/include/title.jsp"%>
	</head>
	
	<body style="background: #f1f2f4">
		<%@ include file="/common/include/header.jsp"%>
		<div class="login-wrap">
			<div class="content">
				<div class="error_info">
					${info }
				</div>
			</div>
		</div>
		<!-- /container -->
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
<c:set value="" var="info" scope="session"/>