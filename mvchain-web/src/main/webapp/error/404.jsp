<%@page import="java.util.*"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.io.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>mvchain - 404</title>
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container">
			<img src="${rootpath }/images/500.gif" alt="哭泣,资源文件不存在" title="哭泣,资源文件不存在"/>
			<h1>亲,您访问的资源不存在.</h1>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>