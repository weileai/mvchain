<%@page import="java.util.*"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.io.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" isErrorPage="true"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>mvchain - 500</title>
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container">
			<img src="${rootpath }/images/500.gif" alt="哭泣,服务器出错了" title="哭泣,服务器出错了"/>
			<h1>亲,服务器鸭梨过大,请稍后再访问.</h1>
			<a href="#" onclick="$('#errdiv').toggle();return false;">展开(隐藏)错误信息:</a>
			<br/>
			<div id="errdiv" style="width:80%;display: none;">
				<%
					//先将错误信息写到内存
					ByteArrayOutputStream bos = new ByteArrayOutputStream();
					PrintStream ps = new PrintStream(bos);
					//先将请求头的信息写到里面
					ps.println("请示头信息:<br/>");
					Map reqmap = request.getParameterMap();
					for(Iterator iter = reqmap.entrySet().iterator() ; iter.hasNext() ; )
					{
						Map.Entry me = (Entry)iter.next();
						ps.println(me.getKey() + "--->" + me.getValue() + "<br/>");
					}
					ps.println("错误信息:<br/>");
					exception.printStackTrace(ps);
					out.println(bos);
					
					exception.printStackTrace();
				%>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>