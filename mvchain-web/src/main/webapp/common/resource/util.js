var path = "/mvchain-web";

/**
 * 处理分页操作的表单工具
 * @param {} formid 表单ID
 * @param {} currentPage	当前页值
 * @param {} currentPageid	当前页ID
 * @param {} pageSize	每页多少条值
 * @param {} pageSizeid	每页多少ID
 * @return {Boolean}
 */
function pageFormSubmit(formid,currentPage,currentPageid, pageSize, pageSizeid) {
	$("#" + formid + " input[name="+ currentPageid +"]").attr("value", currentPage);
	$("#" + formid + " input[name="+ pageSizeid +"]").attr("value", pageSize);
	
	if(typeof(navTabSearch) != 'undefined')
	{
		$("#" + formid ).bind("submit" , navTabSearch($("#" + formid )[0]));
	}else
	{
		$("#" + formid ).submit();
	}
	return false;
}

/**
 * 全选
 * @return 
 */
function checkAll(currid,tarname) {
	$("input[name=" + tarname + "]").each(function()
	{
		$(this).prop("checked",$("#" + currid).prop("checked"));
	});
	return false;
}

/**
 * 检查必须选中一个
 * @return 
 */
function checkOneSubmit(tarids,formid) {
	if(!window.confirm('确认执行此操作吗?'))
	{
		return false ; 
	}
	
	//设置标志位
	var flag ="0";
	$("input[name=" + tarids + "]").each(function()
	{
		if($(this).prop("checked") && flag == "0")
		{
			flag = '1';
		}
	});
	
	if(flag == '1')
	{
		return true ; 
	}
	alert("至少选择一个");
	return false;
}

/**
	计算金额
	
	单价,数量,总金额:其中的两个计算出第三个
	defprice
	totalval
	totalprice
*/
function compPrice(defprice,totalval,totalprice,operType)
{
	var defprice = $("#" + defprice).val();
	var totalval = $("#" + totalval).val();
	var totalprice = $("#" + totalprice).val();
	
	/* 单价要固定 */
	if(operType == 'totalPrice')
	{
		/* 已知单价和数量,计算总金额 */
		totalprice = defprice * totalval ; 
		totalprice = totalprice.toFixed(4);
	}else if(operType == 'totalval')
	{
		/* 已知单价和总金额,计算数量 */
		totalval = totalprice / defprice ; 
		totalval = totalval.toFixed(4);
	}
	
	/* 修改值 */
	$("#totalval").val(totalval);
	$("#totalprice").val(totalprice);
	return false ; 
}