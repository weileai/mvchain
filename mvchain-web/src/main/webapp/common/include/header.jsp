<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- 网页头部开始 -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">LOGO</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-left">
				<li class="${param.menuType == '1' ? 'active' : '' }"><a href="${rootpath }?menuType=1">首页</a></li>
				<li class="dropdown">
					<a target="_blank" href="${rootpath }/coinrateList.html?menuType=2"
						class="dropdown-toggle" type="button" 
						id="mvchainMenu" data-toggle="dropdown" aria-haspopup="true" 
						aria-expanded="true">
						系统信息
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu" aria-labelledby="mvchainMenu">
						<li><a target="_blank" href="${rootpath }/coinrateList.html?menuType=2">币种列表</a></li>
						<li><a target="_blank" href="${rootpath }/websiteList.html">网站列表</a></li>
						<li><a target="_blank" href="${rootpath }/tradeTypeList.html?menuType=2">交易对列表</a></li>
					</ul>
				</li>
				<c:choose>
					<c:when test="${sessionScope.users == null }">
						<li>
							<a href="${rootpath }/head/users/main.html">登陆</a>
						</li>
					</c:when>
					<c:otherwise>
						<li class="dropdown">
							<a target="_blank" href="${rootpath }/head/users/main.html" 
								class="dropdown-toggle" type="button" 
								id="mvchainMenu" data-toggle="dropdown" aria-haspopup="true" 
								aria-expanded="true">
								资产信息
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu" aria-labelledby="mvchainMenu">
								<li class="${param.menuType == '3' ? 'active' : '' }">
									<a target="_blank" href="${rootpath }/head/finan/assetList.html">资产列表</a>
								</li>
								<li><a target="_blank" href="${rootpath }/head/finan/accountList.html">账户列表</a></li>
								<li role="separator" class="divider"></li>
								<li><a target="_blank" href="${rootpath }/head/finan/historyList.html">历史列表</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a target="_blank" href="${rootpath }/head/users/main.html" 
								class="dropdown-toggle" type="button" 
								id="mvchainMenu" data-toggle="dropdown" aria-haspopup="true" 
								aria-expanded="true">
								搬砖信息
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu" aria-labelledby="mvchainMenu">
								<li class="${param.menuType == '3' ? 'active' : '' }">
									<a target="_blank" href="${rootpath }/head/stat/statTaskList.html?menuType=3">任务统计</a>
								</li>
								<li><a target="_blank" href="${rootpath }/head/users/main.html">搬砖信息</a></li>
								<li role="separator" class="divider"></li>
								<li><a target="_blank" href="${rootpath }/head/orders/ordersAllList.html">查询所有挂单</a></li>
								<li><a target="_blank" href="${rootpath }/head/users/usersAccountList.html">账户列表</a></li>
								<li><a target="_blank" href="${rootpath }/head/users/usersTaskList.html">任务列表</a></li>
								<li><a target="_blank" href="${rootpath }/head/orders/tradeHistoryList.html">交易历史列表</a></li>
							</ul>
						</li>
					</c:otherwise>
				</c:choose>
			</ul>
			<c:if test="${sessionScope.users != null }">
				<ul class="nav navbar-nav navbar-right">
					<li>
						<a href="${rootpath }/head/users/main.html">
							<img src="${rootpath}/images/user-pic.png" width="28" height="28">
						</a>
					</li>
					<li>
						<li class="dropdown">
							<a href="${rootpath }/head/users/main.html" class="dropdown-toggle" data-toggle="dropdown">
								${sessionScope.users.email }
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="${rootpath }/head/users/main.html">我的首页</a></li>
								<li><a target="_blank" href="${ucHead}">用户中心</a></li>
								<li><a onclick="return confirm('确认退出吗?')" href="${rootpath }/head/users/logout.html">退出</a></li>
							</ul>
						</li>
					</li>
					<li><a class="exit" onclick="return confirm('确认退出吗?')" href="${rootpath }/head/users/logout.html"> <i class="fa fa-sign-out"></i></a></li>
				</ul>
			</c:if>
			<form class="navbar-form navbar-right visible-xs">
				<input type="text" class="form-control" placeholder="Search...">
			</form>
		</div>
	</div>
</nav>

<!-- 提示信息 -->
<c:if test="${info != '' && info != null}">
	<div class="container">
		<div class="col-md-8 col-md-offset-1">
			<div class="alert alert-warning alert-dismissible fade in"
				role="alert">
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				${info }
			</div>
		</div>
	</div>
</c:if>