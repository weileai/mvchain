<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set value="${pageContext.request.contextPath }" var="rootpath"/>
<c:set value="${sessionScope.users.id == 1}" var="superAdminFlag"/>
<jsp:useBean id="sys_now" class="java.util.Date"/>

<%@ include file="/common/include/allUrl.jsp"%>