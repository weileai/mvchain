<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
<link rel="stylesheet" type="text/css" href="${rootpath }/common/resource/bootstrap/css/bootstrap.min.css" />
<link href="${rootpath }/css/main.css" rel="stylesheet" type="text/css" />
<link href="${rootpath }/css/common.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="${rootpath }/common/resource/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="${rootpath }/common/resource/bootstrap/js/bootstrap.min.js"></script>
<!-- My97DatePicker日期插件 -->
<script type="text/javascript" src="${rootpath }/common/resource/My97DatePicker/WdatePicker.js"></script>
<!-- 引用自己封闭的js常用文件 -->
<script type="text/javascript" src="${rootpath }/common/resource/util.js"></script>