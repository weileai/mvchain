<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html lang="zh-CN">
	<head>
		<title>货币列表 - mvchain</title>
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<!-- 网页头部结束 -->
		<div class="container main">
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<h2 class="sub-header">
						货币列表
						<small>
							<a href="${rootpath }/coinrateList.html">货币列表</a>
						</small>
					</h2>
					<form action="${rootpath }/coinrateList.html" method="post" class="form-inline">
						<div class="form-group">
							<label for="keyword">关键字:</label>
							<input type="text" name="keyword" value="${requestScope.keyword }"  class="form-control" placeholder="请输入关键字">
							<label for="keyword">搬砖状态:</label>
							<select name="prostatus" class="form-control">
								<option value="">请选择</option>
								<option value="1" ${requestScope.prostatus == '1' ? 'selected' : '' }>启用</option>
								<option value="0" ${requestScope.prostatus == '0' ? 'selected' : '' }>禁用</option>
							</select>
							<label for="email">发布时间:</label>
							<input type="text" name="st" value="${requestScope.st }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" class="form-control"/>
							-->
							<input type="text" name="ed" value="${requestScope.ed }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" class="form-control"/>
							<input type="submit" class="btn btn-primary" value="搜索">
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>序号</th>
									<th>名称</th>
									<th>英文名称</th>
									<th>网站数量</th>
									<th>网站</th>
									<th>搬砖状态</th>
									<th>创建时间</th>
									<th class="visible-lg">发布时间</th>
									<th>更新时间</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${requestScope.coinrateList}" var="coinrate" varStatus="stat">
									<tr>
										<td>${stat.count }</td>
										<td>${coinrate.name }</td>
										<td>${coinrate.engname }</td>
										<td>${fn:length(coinrate.extendJSON.website)}</td>
										<td>
											<c:forEach items="${coinrate.extendJSON.website}" var="me" varStatus="stat">
												<c:set value="${me.value }" var="website"/>
												${website.id}->${website.name };
											</c:forEach>
										</td>
										<td>${coinrate.prostatusStr }</td>
										<td><fmt:formatDate value="${coinrate.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td class="visible-lg"><fmt:formatDate value="${coinrate.pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td><fmt:formatDate value="${coinrate.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td>
											<a target="_blank" href="${rootpath }/dept.html?cid=${coinrate.id}">深度行情</a>
											<a target="_blank" href="${rootpath }/deptRate.html?cid=${coinrate.id}">价格汇总</a>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						
						<form id="pageFormCoinrateList" action="${rootpath }/coinrateList.html" method="post">
							<input type="hidden" name="keyword" value="${requestScope.keyword }">
							<input type="hidden" name="prostatus" value="${requestScope.prostatus }">
							<input type="hidden" name="st" value="${requestScope.st }">
							<input type="hidden" name="ed" value="${requestScope.ed }">
							
							<nav>
								<ul class="pager">
									<li>
										<a onclick="return pageFormSubmit('pageFormCoinrateList','1','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">首页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormCoinrateList','${requestScope.pageInfo.prePage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">上一页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormCoinrateList','${requestScope.pageInfo.nextPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">下一页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormCoinrateList','${requestScope.pageInfo.totalPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">末页</span></a>
									</li>
									<li>
										共${requestScope.pageInfo.totalPage }页
										共${requestScope.pageInfo.totalRecord }条
									</li>
									<li>第<input type="text" id="currentPage" name="currentPage" value="${requestScope.pageInfo.currentPage }" size="5" />页</li>
									<li>每页<input type="text" id="pageSize" name="pageSize" value="${requestScope.pageInfo.pageSize }" size="5"/>条</li>
									<li><input type="submit" value="GO" class="btn btn-primary"/></li>
								</ul>
							</nav>
						</form>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
