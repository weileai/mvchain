<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html>
	<head>
		<title>我的信息 - mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="sub-header">
						我的信息
						<small>
							<a href="${rootpath }/head/users/usersUpdate.html?operType=update">修改个人信息</a>
						</small>
					</h2>
					<form action="${rootpath }/head/users/usersAccountInsertSubmit.html" class="form-horizontal" method="post">
						<input type="hidden" name="operType" value="update">
						<div class="form-group">
							<label for="wbid" class="col-sm-2 control-label">邮箱:</label>
							<div class="col-sm-8">
								${requestScope.users.email }
							</div>
						</div>
						<div class="form-group">
							<label for="wbid" class="col-sm-2 control-label">最大任务:</label>
							<div class="col-sm-8">
								${requestScope.users.maxtask }(最多同时执行任务的数量)
							</div>
						</div>
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label">搬砖状态：</label>
							<div class="col-sm-8">
								${requestScope.users.prostatusStr }
							</div> 
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">创建时间：</label>
							<div class="col-sm-8">
								<fmt:formatDate value="${requestScope.users.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">更新时间：</label>
							<div class="col-sm-8">
								<fmt:formatDate value="${requestScope.users.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
