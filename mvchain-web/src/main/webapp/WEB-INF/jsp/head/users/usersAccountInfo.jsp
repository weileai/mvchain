<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html>
	<head>
		<title>账户详细信息 - mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="sub-header">
						账户详细信息
						<small>
							<a href="${rootpath}/head/users/usersAccountList.html">账户列表</a>
							<a href="${rootpath}/head/users/usersAccountInsert.html">添加账户</a>
						</small>
					</h2>
					<form action="${rootpath }/head/users/usersAccountInsertSubmit.html" class="form-horizontal" method="post">
						<input type="hidden" name="operType" value="update">
						<div class="form-group">
							<label for="wbid" class="col-sm-2 control-label">邮箱:</label>
							<div class="col-sm-8">
								${requestScope.usersAccount.usersObj.email }
							</div>
						</div>
						<div class="form-group">
							<label for="wbid" class="col-sm-2 control-label">币种:</label>
							<div class="col-sm-8">
								${requestScope.usersAccount.coinrateObj.name }
							</div>
						</div>
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label">网站:</label>
							<div class="col-sm-8">
								${requestScope.usersAccount.websiteObj.name }
							</div> 
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">用户名：</label>
							<div class="col-sm-8">
								${requestScope.usersAccount.username }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">余额：</label>
							<div class="col-sm-8">
								${requestScope.usersAccount.balance}
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">冻结金额：</label>
							<div class="col-sm-8">
								${requestScope.usersAccount.fundmoney}
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">最小交易量：</label>
							<div class="col-sm-8">
								${requestScope.usersAccount.trademinnum}(按照币种分)
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">最大交易量：</label>
							<div class="col-sm-8">
								${requestScope.usersAccount.trademaxnum}(按照币种分)
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">最大收益：</label>
							<div class="col-sm-8">
								${requestScope.usersAccount.prift}(按照币种分,目前仅支持rmb)
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">认证信息：</label>
							<div class="col-sm-8">
								<c:forEach items="${requestScope.usersAccount.authinfoJSON}" var="me" varStatus="stat">
									${stat.count }、${me.key }:${me.value }<br/>
								</c:forEach>
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">创建时间：</label>
							<div class="col-sm-8">
								<fmt:formatDate value="${requestScope.usersAccount.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">更新时间：</label>
							<div class="col-sm-8">
								<fmt:formatDate value="${requestScope.usersAccount.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
