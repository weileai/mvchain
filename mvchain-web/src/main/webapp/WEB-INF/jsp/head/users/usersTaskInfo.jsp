<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html>
	<head>
		<title>我的信息 - mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="sub-header">
						任务详情
						<small>
							<a href="${rootpath}/head/users/usersTaskList.html">我的任务列表</a>
							<a href="${rootpath}/head/users/usersTaskInsert.html">添加任务</a>
						</small>
					</h2>
					<form action="${rootpath }/head/users/usersAccountInsertSubmit.html" class="form-horizontal" method="post">
						<input type="hidden" name="operType" value="update">
						<div class="form-group">
							<label for="wbid" class="col-sm-2 control-label">用户:</label>
							<div class="col-sm-8">
								${requestScope.usersTask.usersObj.email }
							</div>
						</div>
						<div class="form-group">
							<label for="wbid" class="col-sm-2 control-label">收益币种:</label>
							<div class="col-sm-8">
								${requestScope.usersTask.priftcoinObj.name }
							</div>
						</div>
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label">收益:</label>
							<div class="col-sm-8">
								${requestScope.usersTask.prift }
							</div> 
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">手续费：</label>
							<div class="col-sm-8">
								${requestScope.usersTask.priftfee }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">目标收益：</label>
							<div class="col-sm-8">
								${requestScope.usersTask.souprift }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">名称：</label>
							<div class="col-sm-8">
								${requestScope.usersTask.name}
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">卖数量：</label>
							<div class="col-sm-8">
								${requestScope.usersTask.buynum }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">交易深度：</label>
							<div class="col-sm-8">
								${requestScope.usersTask.deptnum }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">总数量：</label>
							<div class="col-sm-8">
								${requestScope.usersTask.totalnum }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">买(手续费)：</label>
							<div class="col-sm-8">
								${requestScope.usersTask.feenum }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">上次交易网站(卖)：</label>
							<div class="col-sm-8">
								网站:${requestScope.usersTask.sellWbObj.name};
								币种:${requestScope.usersTask.sellTradeTypeObj.name};
								原始价格:${requestScope.usersTask.sousellprice};
								价格:${requestScope.usersTask.sellprice};
								数量:${requestScope.usersTask.buynum};
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">上次交易网站(买)：</label>
							<div class="col-sm-8">
								网站:${requestScope.usersTask.buyWbObj.name};
								币种:${requestScope.usersTask.buyTradeTypeObj.name};
								原始价格:${requestScope.usersTask.soubuyprice};
								价格:${requestScope.usersTask.buyprice};
								数量:${requestScope.usersTask.sellnum};
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">任务原始价格：</label>
							<div class="col-sm-8">
								买:${requestScope.usersTask.taskbuyprice};
								卖:${requestScope.usersTask.tasksellprice};
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">状态：</label>
							<div class="col-sm-8">
								${requestScope.usersTask.statusStr }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">类型：</label>
							<div class="col-sm-8">
								${requestScope.usersTask.soutypeStr }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">创建时间：</label>
							<div class="col-sm-8">
								<fmt:formatDate value="${requestScope.usersTask.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">更新时间：</label>
							<div class="col-sm-8">
								<fmt:formatDate value="${requestScope.usersTask.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/>
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">完成时间：</label>
							<div class="col-sm-8">
								<fmt:formatDate value="${requestScope.usersTask.finishtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
