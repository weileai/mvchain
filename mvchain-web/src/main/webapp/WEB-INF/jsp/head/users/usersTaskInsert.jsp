<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<%-- usersTask的枚举 --%>
<jsp:useBean id="usersTaskPojo" class="com.wang.mvchain.users.pojo.AUsersTask"/>
<!DOCTYPE HTML>
<html>
	<head>
		<title>添加任务- mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<%@ include file="/common/include/title.jsp"%>
		<%-- 表单验证 --%>
		<link rel="stylesheet" href="${rootpath }/common/resource/bootstrapvalidator/css/formValidation.css"/>
		<script type="text/javascript" src="${rootpath }/common/resource/bootstrapvalidator/js/formValidation.js"></script>
	    <script type="text/javascript" src="${rootpath }/common/resource/bootstrapvalidator/js/framework/bootstrap.js"></script>
	    
	    <script type="text/javascript">
			$(document).ready(function() {
			    $('#form').bootstrapValidator({
			    	container: 'tooltip',
			        feedbackIcons: {
			            valid: 'glyphicon glyphicon-ok',
			            invalid: 'glyphicon glyphicon-remove',
			            validating: 'glyphicon glyphicon-refresh'
			        },
			        fields: {
			        	name: {
			                validators: {
			                	notEmpty:
		                    	{
	                    			message: '名称不能为空'
		                    	}
			                }
			            },
			        	totalnum: {
			                validators: {
			                	notEmpty:
		                    	{
	                    			message: '交易总量不能为空'
		                    	},
		                    	regexp: {
		                            regexp: /^[0-9_\.]+$/,
		                            message: '请输入合法的数字'
		                        }
			                }
			            }
			        }
			    });
			    
			    $('#form').bootstrapValidator();
			});
		</script>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="sub-header">
						添加任务
						<small>
							<a href="${rootpath}/head/users/usersTaskList.html">任务列表</a>
						</small>
					</h2>
					<form id="form" action="${rootpath }/head/users/usersTaskInsertSubmit.html" class="form-horizontal" method="post">
						<input type="hidden" name="operType" value="${param.operType }">
						<input type="hidden" name="uaid" value="${requestScope.usersAccount.id }">
						<div class="form-group">
							<label for="tradeTypeId" class="col-sm-2 control-label">币种类型:</label>
							<div class="col-sm-8">
								<select name="tradeTypeId" class="form-control required">
									<c:forEach items="${requestScope.tradeTypeList }" var="tradeType" varStatus="stat">
										<option value="${tradeType.id }">${tradeType.name }</option>
									</c:forEach>
								</select>
							</div> 
						</div>
						<div class="form-group">
							<label for="totalnum" class="col-sm-2 control-label">交易数量:</label>
							<div class="col-sm-8">
								<input type="text" size="5" id="totalnum" name="totalnum" class="form-control" placeholder="交易数量" 
									value="${requestScope.usersTask.totalnum }" 
									autofocus="autofocus">
							</div> 
						</div>
						<div class="form-group">
							<label for="soutype1" class="col-sm-2 control-label">来源类型:</label>
							<div class="col-sm-8">
								<c:set value="0" var="statCount"/>
								<c:forEach items="${usersTaskPojo.enumsMap }" var="me" varStatus="stat">
									<c:if test="${fn:startsWith(me.key,'SOUTYPE-')}">
										<c:set value="${statCount + 1 }" var="statCount"/>
										<!-- 拆分,按照-拆分,取最后一个 -->
										<c:set value="${fn:split(me.key, '-')}" var="keys"/>
										<input type="radio" name="soutype" id="soutype${keys[1] }" value="${keys[1] }"
										${statCount == 1 ? 'checked' : '' }/>
										<label class="form-check-label" for="soutype${keys[1] }">
										    ${me.value }
										</label>
									</c:if>
								</c:forEach>
							</div> 
						</div>
						<div class="form-group">
							<label for="status0" class="col-sm-2 control-label">状态：</label>
							<div class="col-sm-8">
								<c:set value="0" var="statCount"/>
								<c:forEach items="${usersTaskPojo.enumsMap }" var="me" varStatus="stat">
									<c:if test="${fn:startsWith(me.key,'STATUS-')}">
										<c:set value="${statCount + 1 }" var="statCount"/>
										<!-- 拆分,按照-拆分,取最后一个 -->
										<c:set value="${fn:split(me.key, '-')}" var="keys"/>
										<input type="radio" name="status" id="status${keys[1] }" value="${keys[1] }"
										${statCount == 1 ? 'checked' : '' }/>
										<label class="form-check-label" for="status${keys[1] }">
										    ${me.value }
										</label>
									</c:if>
								</c:forEach>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-2">
								<button class="btn btn-lg btn-primary" type="submit">提交</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
<script type="text/javascript">
	websiteChange();
</script>
