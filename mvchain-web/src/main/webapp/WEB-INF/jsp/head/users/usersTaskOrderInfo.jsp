<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html>
	<head>
		<title>我的订单信息 - mvchain</title>
		<meta name="viewport" content="withh=device-withh, initial-scale=1.0" />
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="sub-header">
						任务详情
						<small>
							<a href="${rootpath}/head/users/usersTaskList.html">我的任务列表</a>
							<a href="${rootpath}/head/users/usersTaskInsert.html">添加任务</a>
						</small>
					</h2>
					<form action="${rootpath }/head/users/usersAccountInsertSubmit.html" class="form-horizontal" method="post">
						<input type="hidden" name="operType" value="update">
						<div class="form-group">
							<label for="wbid" class="col-sm-2 control-label">任务:</label>
							<div class="col-sm-8">
								<a class="hreflink"	height="400" withh="800" target="dialog"
											href="${rootpath }/head/users/usersTaskUpdate.html?utid=${requestScope.usersTaskOrder.usersTaskObj.id }">
											${requestScope.usersTaskOrder.usersTaskObj.name }
										</a>
							</div>
						</div>
						<div class="form-group">
							<label for="wbid" class="col-sm-2 control-label">用户网站账户:</label>
							<div class="col-sm-8">
								<a class="hreflink"	height="400" withh="800" target="dialog"
											href="${rootpath }/head/users/usersAccountUpdate.html?uaid=${requestScope.usersTaskOrder.usersAccountObj.id }">
											${requestScope.usersTaskOrder.uaid }
										</a>
							</div>
						</div>
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label">交易类型:</label>
							<div class="col-sm-8">
								${usersTaskOrder.tradeTypeObj.name }
							</div> 
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">订单id(第三方)：</label>
							<div class="col-sm-8">
								${requestScope.usersTaskOrder.ordersn }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">原始价格：</label>
							<div class="col-sm-8">
								${requestScope.usersTaskOrder.souprice }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">价格：</label>
							<div class="col-sm-8">
								${requestScope.usersTaskOrder.price}
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">成交价：</label>
							<div class="col-sm-8">
								${requestScope.usersTaskOrder.tradePrice}
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">成交数量：</label>
							<div class="col-sm-8">
								${requestScope.usersTaskOrder.tradeedval }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">总数量：</label>
							<div class="col-sm-8">
								${requestScope.usersTaskOrder.totalval }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">总价格：</label>
							<div class="col-sm-8">
								${requestScope.usersTaskOrder.totalprice }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">手续费：</label>
							<div class="col-sm-8">
								${requestScope.usersTaskOrder.fee }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">类型：</label>
							<div class="col-sm-8">
								${requestScope.usersTaskOrder.ordertypeStr }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">状态：</label>
							<div class="col-sm-8">
								${requestScope.usersTaskOrder.statusStr }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">收益状态：</label>
							<div class="col-sm-8">
								${requestScope.usersTaskOrder.priftstatusStr }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">创建时间：</label>
							<div class="col-sm-8">
								<fmt:formatDate value="${requestScope.usersTaskOrder.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">更新时间：</label>
							<div class="col-sm-8">
								<fmt:formatDate value="${requestScope.usersTaskOrder.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/>
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">完成时间：</label>
							<div class="col-sm-8">
								<fmt:formatDate value="${requestScope.usersTaskOrder.pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
							</div>
						</div>
					</form>
				</div>
			</div>
			
			<c:if test="${param.operType == 'tradeOrderInfo' }">
				<div class="row">
					<div class="col-md-offset-1 main">
						<h2 class="sub-header">交易网站明细</h2>
						<p>
							${requestScope.resultJSON.info }
							备注:状态,-1:已撤销  0:未成交  1:部分成交  2:完全成交 3 撤单处理中
						</p>
						<div class="table-responsive">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>序号</th>
										<th>订单号</th>
										<th>类型(买/卖)</th>
										<th>价格</th>
										<th>成交价</th>
										<th>数量</th>
										<th>成交量</th>
										<th>状态</th>
										<th>时间</th>
									</tr>
								</thead>
								<tbody>
									<c:set value="${requestScope.resultJSON.data.orders }" var="orders"/>
									<tr target="sid_user" rel="${orders.order_id}">
										<td>${stat.count }</td>
										<td>${orders.order_id}</td>
										<td>
											<c:choose>
												<c:when test="${orders.type == '0'}">
													买
												</c:when>
												<c:when test="${orders.type == '1'}">
													卖
												</c:when>
											</c:choose>
										</td>
										<td>${orders.price }</td>
										<td>${orders.avg_price }</td>
										<td>${orders.amount}</td>
										<td>${orders.deal_amount}</td>
										<td>${orders.status }</td>
										<td>${orders.create_date }</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</c:if>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
