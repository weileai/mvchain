<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<%-- usersTask的枚举 --%>
<jsp:useBean id="usersTaskOrderPojo" class="com.wang.mvchain.users.pojo.AUsersTaskOrder"/>
<!DOCTYPE HTML>
<html>
	<head>
		<title>更新任务订单- mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<%@ include file="/common/include/title.jsp"%>
		<%-- 表单验证 --%>
		<link rel="stylesheet" href="${rootpath }/common/resource/bootstrapvalidator/css/formValidation.css"/>
		<script type="text/javascript" src="${rootpath }/common/resource/bootstrapvalidator/js/formValidation.js"></script>
	    <script type="text/javascript" src="${rootpath }/common/resource/bootstrapvalidator/js/framework/bootstrap.js"></script>
	    
	    <script type="text/javascript">
			$(document).ready(function() {
			    $('#form').bootstrapValidator({
			    	container: 'tooltip',
			        feedbackIcons: {
			            valid: 'glyphicon glyphicon-ok',
			            invalid: 'glyphicon glyphicon-remove',
			            validating: 'glyphicon glyphicon-refresh'
			        },
			        fields: {
			        	ordersn: {
			                validators: {
			                	notEmpty:
		                    	{
	                    			message: '第三方订单号不能为空'
		                    	}
			                }
			            },
			        	totalnum: {
			                validators: {
			                	notEmpty:
		                    	{
	                    			message: '交易总量不能为空'
		                    	},
		                    	regexp: {
		                            regexp: /^[0-9_\.]+$/,
		                            message: '请输入合法的数字'
		                        }
			                }
			            }
			        }
			    });
			    
			    $('#form').bootstrapValidator();
			});
		</script>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="sub-header">
						更新用户订单任务
						<small>
							<a href="${rootpath}/head/users/usersTaskOrderList.html">任务列表</a>
						</small>
					</h2>
					<form id="form" action="${rootpath }/head/users/usersTaskOrderUpdateSubmit.html" class="form-horizontal" method="post">
						<input type="hidden" name="operType" value="${param.operType }">
						<input type="hidden" name="utoid" value="${requestScope.usersTaskOrder.id }">
						<div class="form-group">
							<label for="totalnum" class="col-sm-2 control-label">用户账户id:</label>
							<div class="col-sm-8">
								<input type="text" size="5" id="uaid" name="uaid" class="form-control" placeholder="用户账户id" 
									value="${requestScope.usersTaskOrder.uaid}" 
									autofocus="autofocus">
							</div> 
						</div>
						<div class="form-group">
							<label for="totalnum" class="col-sm-2 control-label">交易类型id:</label>
							<div class="col-sm-8">
								<select name="ctid" class="form-control required">
									<c:forEach items="${requestScope.tradeTypeList }" var="tradeType" varStatus="stat">
										<option value="${tradeType.id }" ${requestScope.usersTaskOrder.ctid == tradeType.id ? 'selected' : ''}>${tradeType.name }</option>
									</c:forEach>
								</select>
							</div> 
						</div>
						<div class="form-group">
							<label for="totalnum" class="col-sm-2 control-label">订单号(第三方):</label>
							<div class="col-sm-8">
								<input type="text" size="5" id="ordersn" name="ordersn" class="form-control" placeholder="订单号(第三方)" 
									value="${requestScope.usersTaskOrder.ordersn}" 
									autofocus="autofocus">
							</div> 
						</div>
						<div class="form-group">
							<label for="totalnum" class="col-sm-2 control-label">原始价格:</label>
							<div class="col-sm-8">
								<input type="text" size="5" id="souprice" name="souprice" class="form-control" placeholder="原始价格" 
									value="${requestScope.usersTaskOrder.souprice}" 
									autofocus="autofocus">
							</div> 
						</div>
						<div class="form-group">
							<label for="totalnum" class="col-sm-2 control-label">价格:</label>
							<div class="col-sm-8">
								<input type="text" size="5" id="price" name="price" class="form-control" placeholder="价格" 
									value="${requestScope.usersTaskOrder.price}" 
									autofocus="autofocus">
							</div> 
						</div>
						<div class="form-group">
							<label for="totalnum" class="col-sm-2 control-label">成交价格:</label>
							<div class="col-sm-8">
								<input type="text" size="5" id="tradePrice" name="tradePrice" class="form-control" placeholder="成交价格" 
									value="${requestScope.usersTaskOrder.tradePrice}" 
									autofocus="autofocus">
							</div> 
						</div>
						<div class="form-group">
							<label for="totalnum" class="col-sm-2 control-label">成交数量:</label>
							<div class="col-sm-8">
								<input type="text" size="5" id="tradeedval" name="tradeedval" class="form-control" placeholder="成交数量" 
									value="${requestScope.usersTaskOrder.tradeedval}" 
									autofocus="autofocus">
							</div> 
						</div>
						<div class="form-group">
							<label for="totalnum" class="col-sm-2 control-label">总数量:</label>
							<div class="col-sm-8">
								<input type="text" size="5" id="totalval" name="totalval" class="form-control" placeholder="总数量" 
									value="${requestScope.usersTaskOrder.totalval}" 
									autofocus="autofocus">
							</div> 
						</div>
						<div class="form-group">
							<label for="totalnum" class="col-sm-2 control-label">手续费:</label>
							<div class="col-sm-8">
								<input type="text" size="5" id="fee" name="fee" class="form-control" placeholder="手续费" 
									value="${requestScope.usersTaskOrder.fee}" 
									autofocus="autofocus">
							</div> 
						</div>
						<div class="form-group">
							<label for="totalnum" class="col-sm-2 control-label">发布时间:</label>
							<div class="col-sm-8">
								<input type="text" name="pubTimeStr" value="<fmt:formatDate value="${requestScope.usersTaskOrder.pubtime}" pattern="yyyy-MM-dd HH:mm:ss"/>" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" 
								readonly="true" class="form-control"/>
							</div> 
						</div>
						<div class="form-group">
							<label for="soutype1" class="col-sm-2 control-label">类型:</label>
							<div class="col-sm-8">
								<c:set value="0" var="statCount"/>
								<c:forEach items="${usersTaskOrderPojo.enumsMap }" var="me" varStatus="stat">
									<c:if test="${fn:startsWith(me.key,'ORDERTYPE-')}">
										<c:set value="${statCount + 1 }" var="statCount"/>
										<!-- 拆分,按照-拆分,取最后一个 -->
										<c:set value="${fn:split(me.key, '-')}" var="keys"/>
										<input type="radio" name="ordertype" id="ordertype${keys[1] }" value="${keys[1] }"
										${requestScope.usersTaskOrder.ordertype == keys[1]  ? 'checked' : '' }/>
										<label class="form-check-label" for="ordertype${keys[1] }">
										    ${me.value }
										</label>
									</c:if>
								</c:forEach>
							</div> 
						</div>
						<div class="form-group">
							<label for="status0" class="col-sm-2 control-label">状态：</label>
							<div class="col-sm-8">
								<c:set value="0" var="statCount"/>
								<c:forEach items="${usersTaskOrderPojo.enumsMap }" var="me" varStatus="stat">
									<c:if test="${fn:startsWith(me.key,'STATUS-')}">
										<c:set value="${statCount + 1 }" var="statCount"/>
										<!-- 拆分,按照-拆分,取最后一个 -->
										<c:set value="${fn:split(me.key, '-')}" var="keys"/>
										<input type="radio" name="status" id="status${keys[1] }" value="${keys[1] }"
										${requestScope.usersTaskOrder.status == keys[1] ? 'checked' : '' }/>
										<label class="form-check-label" for="status${keys[1] }">
										    ${me.value }
										</label>
									</c:if>
								</c:forEach>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-2">
								<button class="btn btn-lg btn-primary" type="submit">提交</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
<script type="text/javascript">
	websiteChange();
</script>
