<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html>
	<head>
		<title>修改个人信息 - mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="sub-header">
						修改我的个人信息
						<small>
						</small>
					</h2>
					<form action="${rootpath }/head/users/usersUpdateSubmit.html" class="form-horizontal" 
						method="post">
						<input type="hidden" name="operType" value="${requestScope.operType }">
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label">邮箱:</label>
							<div class="col-sm-8">
								${requestScope.users.email }
							</div> 
						</div>
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label">搬砖状态:</label>
							<div class="col-sm-8">
								<input type="radio" id="prostatus0" name="prostatus" value="0" ${requestScope.users.prostatus == '0' ? 'checked' : '' }>
								<label for="prostatus0">关闭&nbsp;&nbsp;&nbsp;</label>
								<input type="radio" id="prostatus1" name="prostatus" value="1" ${requestScope.users.prostatus == '1' ? 'checked' : '' }>
								<label for="prostatus1">开启&nbsp;&nbsp;&nbsp;</label>
							</div> 
						</div>
						<div class="form-group">
							<label for="maxtask" class="col-sm-2 control-label">最大任务:</label>
							<div class="col-sm-8">
									<input type="text" id="maxtask" name="maxtask" class="form-control" value="${requestScope.users.maxtask}">
									(最多同时执行任务的数量)
							</div>
						</div>
						<div class="form-group">
							<label for="wbid" class="col-sm-2 control-label">交易配置(币种):</label>
							<div class="col-sm-8">
								<select name="coinid" class="form-control required">
									<option value="">请选择</option>
									<c:forEach items="${requestScope.coinrateList }" var="coinrate" varStatus="stat">
										<option value="${coinrate.id }">
											${coinrate.engname }(${coinrate.name })
										</option>
									</c:forEach>
								</select>
								当选择币种时,最大交易量和最小交易量才会起作用
							</div>
						</div>
						<div class="form-group">
							<label for="trademinnum" class="col-sm-2 control-label">最小交易量:</label>
							<div class="col-sm-8">
								<input type="text" id="trademinnum" name="trademinnum" class="form-control" value="1">
							</div>
						</div>
						<div class="form-group">
							<label for="trademaxnum" class="col-sm-2 control-label">最大交易量:</label>
							<div class="col-sm-8">
								<input type="text" id="trademaxnum" name="trademaxnum" class="form-control" value="5">
							</div>
						</div>
						<div class="form-group">
							<label for="prift" class="col-sm-2 control-label">最小收益(价格百分比):</label>
							<div class="col-sm-8">
								<input type="text" id="prift" name="prift" value="20" class="form-control">
								只对cny起作用(差价/两个价格的和),大于此值就搬砖
							</div>
						</div>
						<div class="form-group">
							<label for="wbid" class="col-sm-2 control-label">注册时间:</label>
							<div class="col-sm-8">
								<fmt:formatDate value="${requestScope.users.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
							</div>
						</div>
						<div class="form-group">
							<label for="wbid" class="col-sm-2 control-label">更新时间:</label>
							<div class="col-sm-8">
								<fmt:formatDate value="${requestScope.users.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-2">
								<button class="btn btn-lg btn-primary" type="submit">提交</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
