<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html>
	<head>
		<title>修改账户详细信息 - mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="sub-header">
						修改账户详细信息
						<small>
							<a href="${rootpath}/head/users/usersAccountList.html">账户列表</a>
							<a href="${rootpath}/head/users/usersAccountInsert.html">添加账户</a>
						</small>
					</h2>
					<form action="${rootpath }/head/users/usersAccountUpdateSubmit.html" class="form-horizontal" method="post">
						<input type="hidden" name="operType" value="${param.operType }">
						<input type="hidden" name="uaid" value="${requestScope.usersAccount.id }">
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label">邮箱:</label>
							<div class="col-sm-8">
								${requestScope.usersAccount.usersObj.email }
							</div> 
						</div>
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label">币种:</label>
							<div class="col-sm-8">
								${requestScope.usersAccount.coinrateObj.name}
							</div> 
						</div>
						<div class="form-group">
							<label for="wbid" class="col-sm-2 control-label">网站:</label>
							<div class="col-sm-8">
								${requestScope.usersAccount.websiteObj.name}
							</div>
						</div>
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label">用户名:</label>
							<div class="col-sm-8">
								<input type="text" id="username" name="username" class="form-control" readonly="readonly" value="${requestScope.usersAccount.username }" size="50"/>
							</div> 
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">认证信息:</label>
							<div class="col-sm-8" id="authinfo">
								<c:forEach items="${requestScope.usersAccount.authinfoJSON}" var="me" varStatus="stat">
									${stat.count }、${me.key }:<input type="text" class="form-control" name="${me.key }" value="${me.value }" size="50"/><br/>
								</c:forEach>
							</div>
						</div>
						<div class="form-group">
							<label for="trademinnum" class="col-sm-2 control-label">最小交易量：</label>
							<div class="col-sm-8">
								<input type="text" id="trademinnum" name="trademinnum" class="form-control" value="${requestScope.usersAccount.trademinnum}">
								(按照币种分)
							</div>
						</div>
						<div class="form-group">
							<label for="trademaxnum" class="col-sm-2 control-label">最大交易量：</label>
							<div class="col-sm-8">
								<input type="text" id="trademaxnum" name="trademaxnum" class="form-control" value="${requestScope.usersAccount.trademaxnum}">
								(按照币种分)
							</div>
						</div>
						<div class="form-group">
							<label for="prift" class="col-sm-2 control-label">最大收益：</label>
							<div class="col-sm-8">
								<input type="text" id="prift" name="prift" class="form-control" value="${requestScope.usersAccount.prift}">
								(按照币种分,目前仅支持rmb)
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-2 control-label">余额：</label>
							<div class="col-sm-8">
								${requestScope.usersAccount.balance}
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-2 control-label">冻结金额：</label>
							<div class="col-sm-8">
								${requestScope.usersAccount.fundmoney}
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-2">
								<button class="btn btn-lg btn-primary" type="submit">提交</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
