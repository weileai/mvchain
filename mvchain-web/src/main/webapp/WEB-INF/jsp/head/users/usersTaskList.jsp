<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<%-- usersTask的枚举 --%>
<jsp:useBean id="usersTaskPojo" class="com.wang.mvchain.users.pojo.AUsersTask"/>
<!DOCTYPE HTML>
<html lang="zh-CN">
	<head>
		<title>我的任务列表 - mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container main">
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<h2 class="sub-header">
						任务列表
						<small>
							<a href="${rootpath}/head/users/usersTaskList.html">我的任务列表</a>
							<a href="${rootpath}/head/users/usersTaskInsert.html" target="_blank">添加任务</a>
							<a href="${rootpath}/head/users/usersTaskOrderDelete.html" onclick="return confirm('确定要操作吗?')">清除撤单任务</a>
						</small>
					</h2>
					<form action="${rootpath}/head/users/usersTaskList.html" method="post" class="form-inline">
						<div class="form-group">
							<!-- <label for="keyword">关键字:</label> -->
							<input type="text" name="keyword" value="${requestScope.keyword }"  class="form-control" placeholder="请输入关键字">
							<label for="keyword">状态：</label>
							<select name="statusList" class="form-control required">
								<option value="">请选择</option>
								<c:set value="0" var="statCount"/>
								<c:forEach items="${usersTaskPojo.enumsMap }" var="me" varStatus="stat">
									<c:if test="${fn:startsWith(me.key,'STATUS-')}">
										<c:set value="${statCount + 1 }" var="statCount"/>
										<!-- 拆分,按照-拆分,取最后一个 -->
										<c:set value="${fn:split(me.key, '-')}" var="keys"/>
										<option value="${keys[1] }" ${requestScope.statusList == keys[1] ? 'selected' : '' }>${me.value }</option>
									</c:if>
								</c:forEach>
							</select>
							<label for="keyword">类型：</label>
							<select name="soutypeList" class="form-control required">
								<option value="">请选择</option>
								<c:set value="0" var="statCount"/>
								<c:forEach items="${usersTaskPojo.enumsMap }" var="me" varStatus="stat">
									<c:if test="${fn:startsWith(me.key,'SOUTYPE-')}">
										<c:set value="${statCount + 1 }" var="statCount"/>
										<!-- 拆分,按照-拆分,取最后一个 -->
										<c:set value="${fn:split(me.key, '-')}" var="keys"/>
										<option value="${keys[1] }" ${requestScope.soutypeList == keys[1] ? 'selected' : '' }>${me.value }</option>
									</c:if>
								</c:forEach>
							</select>
							<label for="email">更新时间:</label>
							<input type="text" name="st" value="${requestScope.st }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" class="form-control"/>
							-->
							<input type="text" name="ed" value="${requestScope.ed }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" class="form-control"/>
							<input type="submit" class="btn btn-primary" value="搜索">
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>序号</th>
									<th>收益币</th>
									<th>收益</th>
									<th>手续费</th>
									<th>名称</th>
									<th>搬币种</th>
									<th>买数量</th>
									<th>卖数量</th>
									<th>总数量</th>
									<th>类型</th>
									<th>状态</th>
									<th>更新时间</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody>
								<c:set value="0" var="totalprift"/>
								<c:set value="0" var="totalfee"/>
								<c:forEach items="${requestScope.usersTaskList }" var="usersTask" varStatus="stat">
									<tr target="sid_user" rel="${usersTask.id }">
										<td>${stat.count }</td>
										<td>${usersTask.priftcoinObj.name }</td>
										<td>${usersTask.prift }</td>
										<td>${usersTask.priftfee }</td>
										<td title="${usersTask.name }">${fn:substring(usersTask.name,0,5) }</td>
										<td>${usersTask.sellTradeTypeObj.name }</td>
										<td>${usersTask.buynum }</td>
										<td>${usersTask.sellnum }</td>
										<td>${usersTask.totalnum }</td>
										<td>${usersTask.soutypeStr }</td>
										<td>${usersTask.statusStr }</td>
										<td><fmt:formatDate value="${usersTask.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td>
											<a target="_blank" href="${rootpath }/head/users/usersTaskUpdate.html?utid=${usersTask.id}">查看</a>
											<c:if test="${usersTask.soutype == '1' || usersTask.status < 2 }">
												<a target="_blank" href="${rootpath }/head/users/usersTaskUpdate.html?utid=${usersTask.id}&operType=update">修改</a><br/>
											</c:if>
											<br/>
											<a target="_blank" href="${rootpath }/head/users/usersTaskOrderList.html?utidList=${usersTask.id}&operType=update">查看订单</a>
											<c:if test="${superAdminFlag && usersTask.status < 2}">
												<a target="_blank" href="${rootpath }/head/users/usersAccountList.html?utid=${usersTask.id}&coinidList=${usersTask.sellTradeTypeObj.fromcoinid}">交易</a>
											</c:if>
										</td>
									</tr>
									
									<c:set value="${totalprift + usersTask.prift }" var="totalprift"/>
									<c:set value="${totalfee + usersTask.priftfee }" var="totalfee"/>
								</c:forEach>
								
								<tr>
									<td>合计:</td>
									<td></td>
									<td><fmt:formatNumber value="${totalprift }" pattern="#,####.####"/></td>
									<td><fmt:formatNumber value="${totalfee}" pattern="#,####.####"/></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td>
									</td>
								</tr>
							</tbody>
						</table>
						
						<form id="pageFormUsersAccountList" method="post" action="${rootpath }/head/users/usersTaskList.html">
							<input type="hidden" name="keyword" value="${requestScope.keyword }">
							<input type="hidden" name="usersidList" value="${requestScope.usersidList }">
							<input type="hidden" name="coinidList" value="${requestScope.coinidList }">
							<input type="hidden" name="soutypeList" value="${requestScope.soutypeList }">
							<input type="hidden" name="statusList" value="${requestScope.statusList }">
							<input type="hidden" name="st" value="${requestScope.st }">
							<input type="hidden" name="ed" value="${requestScope.ed }">
						
							<nav>
								<ul class="pager">
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','1','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">首页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','${requestScope.pageInfo.prePage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">上一页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','${requestScope.pageInfo.nextPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">下一页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','${requestScope.pageInfo.totalPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">末页</span></a>
									</li>
									<li>
										共${requestScope.pageInfo.totalPage }页
										共${requestScope.pageInfo.totalRecord }条
									</li>
									<li>第<input type="text" id="currentPage" name="currentPage" value="${requestScope.pageInfo.currentPage }" size="5" />页</li>
									<li>每页<input type="text" id="pageSize" name="pageSize" value="${requestScope.pageInfo.pageSize }" size="5"/>条</li>
									<li><input type="submit" value="GO" class="btn btn-primary"/></li>
								</ul>
							</nav>
						</form>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
