<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
	<head>
		<title>个人中心 - 用户中心</title>
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container">
			<div class="row">
				<div class="col-xs-11 col-sm-12 main">
					<h1 class="page-header">个人中心</h1>
					<!-- <div class="row placeholders">
						<div class="col-xs-6 col-sm-3 placeholder">
							<img alt="Generic placeholder thumbnail" class="img-responsive"
								data-src="holder.js/200x200/auto/sky">
							<h4>Label</h4>
							<span class="text-muted">Something else</span>
						</div>
						<div class="col-xs-6 col-sm-3 placeholder">
							<img alt="Generic placeholder thumbnail" class="img-responsive"
								data-src="holder.js/200x200/auto/vine">
							<h4>Label</h4>
							<span class="text-muted">Something else</span>
						</div>
						<div class="col-xs-6 col-sm-3 placeholder">
							<img alt="Generic placeholder thumbnail" class="img-responsive"
								data-src="holder.js/200x200/auto/sky">
							<h4>Label</h4>
							<span class="text-muted">Something else</span>
						</div>
						<div class="col-xs-6 col-sm-3 placeholder">
							<img alt="Generic placeholder thumbnail" class="img-responsive"
								data-src="holder.js/200x200/auto/vine">
							<h4>Label</h4>
							<span class="text-muted">Something else</span>
						</div>
					</div> -->
	
					<h2 class="sub-header">我的总资产</h2>
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<th colspan="3">我的总资产</th>
								<th><a href="${rootpath }/head/users/usersUpdate.html">查看</a></th>
							</thead>
							<tbody>
								<tr>
									<td>序号</td>
									<td>币种</td>
									<td>余额</td>
									<td>冻结金额</td>
									<td>折合cny</td>
								</tr>
								<c:set value="0" var="totalPrice"/>
								<c:forEach items="${requestScope.accountList }" var="map" varStatus="stat">
									<tr>
										<td>${stat.count}</td>
										<td>${map.coinrate.name}</td>
										<td><fmt:formatNumber value="${map.balance}" pattern="#,####.####"/></td>
										<td><fmt:formatNumber value="${map.fundmoney}" pattern="#,####.####"/></td>
										<td>
											<c:choose>
												<c:when test="${map.coinid == '2'}">
													<%-- 美元 --%>
													<c:set value="${totalPrice + map.balance * requestScope.usdRate }" var="totalPrice"/>
													<fmt:formatNumber value="${map.balance * requestScope.usdRate}" pattern="#,####.####"
													var="usdprice"/>
													${usdprice}
												</c:when>
												<c:when test="${map.coinid == '3'}">
													<fmt:formatNumber value="${map.balance}" pattern="#,####.####"/>
													<c:set value="${totalPrice + map.balance }" var="totalPrice"/>
												</c:when>
												<c:otherwise>
													0
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
								</c:forEach>
								<tr>
									<td>合计:</td>
									<td></td>
									<td></td>
									<td></td>
									<td>
										<fmt:formatNumber value="${totalPrice }" pattern="#,####.####"/>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
