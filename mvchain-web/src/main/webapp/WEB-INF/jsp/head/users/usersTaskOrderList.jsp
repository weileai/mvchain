<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html lang="zh-CN">
	<head>
		<title>我的任务订单列表 - mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<!-- 网页头部结束 -->
		<div class="container main">
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<h2 class="sub-header">
						任务订单列表
						<small>
							<a href="${rootpath}/head/users/usersTaskOrderList.html?utidList=${requestScope.utidList}&uaid=${requestScope.uaid}&websiteId=${requestScope.websiteId}">我的任务订单列表</a>
							<c:if test="${requestScope.usersTask.status == 1 }">
								<a href="${rootpath}/head/users/usersAccountList.html?utid=${requestScope.utidList}&coinidList=${requestScope.usersTask.sellTradeTypeObj.fromcoinid}" target="_blank">下订单</a>
							</c:if>
							<a href="${rootpath}/deptRate.html?cid=${requestScope.usersTask.sellTradeTypeObj.fromcoinid}" target="_blank">查看行情</a>
						</small>
					</h2>
					<form target="_blank" class="form-inline" action="${rootpath}/head/users/usersTaskOrderInsert.html" method="get">
						<input type="hidden" name="taskId" value="${requestScope.utidList}">
						交易历史id:<input type="text" class="form-control" name="thId" value="0" placeholder="请输入交易历史id">
						<input type="submit" class="btn btn-primary" value="添加订单">
					</form>
					<form class="form-inline" action="${rootpath}/head/users/usersTaskOrderList.html" method="post">
						<input type="hidden" name="utidList" value="${requestScope.utidList }">
						<input type="hidden" name="uaid" value="${requestScope.uaid }">
						<input type="hidden" name="websiteId" value="${requestScope.websiteId }">
						<div class="form-group">
							<!-- <label for="keyword">关键字:</label> -->
							<input type="text" name="keyword" value="${requestScope.keyword }"  class="form-control" placeholder="请输入关键字">
							<label for="keyword">状态：</label>
							<select name="statusList" class="form-control required">
								<option value="">请选择</option>
								<option value="0" ${requestScope.statusList == '0' ? 'selected' : '' }>未开始</option>
								<option value="1" ${requestScope.statusList == '1' ? 'selected' : '' }>撤单</option>
								<option value="2" ${requestScope.statusList == '2' ? 'selected' : '' }>挂单</option>
								<option value="3" ${requestScope.statusList == '3' ? 'selected' : '' }>完成</option>
							</select>
							<label for="keyword">类型：</label>
							<select name="ordertypeList" class="form-control required">
								<option value="">请选择</option>
								<option value="0" ${requestScope.ordertypeList == '0' ? 'selected' : '' }>买</option>
								<option value="1" ${requestScope.ordertypeList == '1' ? 'selected' : '' }>卖</option>
							</select>
							<input type="submit" class="btn btn-primary" value="搜索">
							<label for="realPrice">实时价格:</label>
							<span class="lead texted-success">
								<c:set value="${requestScope.resultJSON.data.bids[0].rate }" var="realPrice"/>
								${realPrice}
							</span>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>序号</th>
									<th>用户</th>
									<th>交易类型</th>
									<th>订单id</th>
									<th>原价</th>
									<th>价格</th>
									<th>成交价</th>
									<th>成交量</th>
									<th>总数量</th>
									<th>总价格</th>
									<th>手续费</th>
									<th>类型</th>
									<th>状态</th>
									<th>收益状态</th>
									<th>创建时间</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${requestScope.usersTaskOrderList }" var="usersTaskOrder" varStatus="stat">
									<tr target="sid_user" rel="${usersTaskOrder.id }">
										<td>${stat.count }</td>
										<td>
											<a class="hreflink"	height="400" width="800" target="dialog"
												href="${rootpath }/head/users/usersAccountUpdate.html?uaid=${usersTaskOrder.usersAccountObj.id }">
												${usersTaskOrder.usersAccountObj.websiteObj.name }(${usersTaskOrder.uaid })
											</a>
										</td>
										<td>${usersTaskOrder.tradeTypeObj.name }</td>
										<td>${usersTaskOrder.ordersn }</td>
										<td>${usersTaskOrder.souprice }</td>
										<td>${usersTaskOrder.price }</td>
										<td>${usersTaskOrder.tradePrice }</td>
										<td>${usersTaskOrder.tradeedval }</td>
										<td>${usersTaskOrder.totalval }</td>
										<td>${usersTaskOrder.totalprice }</td>
										<td>${usersTaskOrder.fee }</td>
										<td>${usersTaskOrder.ordertypeStr }</td>
										<td>${usersTaskOrder.statusStr }</td>
										<td>${usersTaskOrder.priftstatusStr }</td>
										<td><fmt:formatDate value="${usersTaskOrder.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td>
											<a target="_blank" href="${rootpath }/head/users/usersTaskOrderUpdate.html?utoid=${usersTaskOrder.id}">查看</a>
											<a target="_blank" href="${rootpath }/head/users/usersTaskOrderUpdate.html?operType=update&utoid=${usersTaskOrder.id}">修改</a>
											<br/>
											<c:if test="${usersTaskOrder.status == 2}">
												<a onclick="return confirm('确认提交吗?')" 
													href="${rootpath }/head/users/usersTaskOrderUpdateSubmit.html?operType=cancel&utoid=${usersTaskOrder.id}&utidList=${usersTaskOrder.utid}">
													撤单
												</a>
											</c:if>
											<a target="_blank" href="${rootpath }/head/users/usersTaskOrderUpdate.html?utoid=${usersTaskOrder.id}&operType=tradeOrderInfo" title="查看(所有)">查所</a>
											<br/>
											<c:if test="${superAdminFlag && usersTaskOrder.usersTaskObj.status < 2}">
												<a target="_blank" href="${rootpath }/head/orders/ordersList.html?uaid=${usersTaskOrder.uaid}&utid=${usersTaskOrder.utid}">交易</a>
											</c:if>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						
						<form id="pageFormUsersAccountList" method="post" action="${rootpath }/head/users/usersTaskOrderList.html">
							<input type="hidden" name="keyword" value="${requestScope.keyword }">
							<input type="hidden" name="statusList" value="${requestScope.statusList }">
							<input type="hidden" name="utidList" value="${requestScope.utidList }">
							<input type="hidden" name="ordertypeList" value="${requestScope.ordertypeList }">
							<input type="hidden" name="st" value="${requestScope.st }">
							<input type="hidden" name="ed" value="${requestScope.ed }">
							<input type="hidden" name="uaid" value="${requestScope.uaid }">
							<input type="hidden" name="websiteId" value="${requestScope.websiteId }">
							
							<nav>
								<ul class="pager">
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','1','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">首页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','${requestScope.pageInfo.prePage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">上一页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','${requestScope.pageInfo.nextPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">下一页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','${requestScope.pageInfo.totalPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">末页</span></a>
									</li>
									<li>
										共${requestScope.pageInfo.totalPage }页
										共${requestScope.pageInfo.totalRecord }条
									</li>
									<li>第<input type="text" id="currentPage" name="currentPage" value="${requestScope.pageInfo.currentPage }" size="5" />页</li>
									<li>每页<input type="text" id="pageSize" name="pageSize" value="${requestScope.pageInfo.pageSize }" size="5"/>条</li>
									<li><input type="submit" value="GO" class="btn btn-primary"/></li>
								</ul>
							</nav>
						</form>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
