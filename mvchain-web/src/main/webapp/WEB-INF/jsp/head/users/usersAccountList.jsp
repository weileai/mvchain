<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html lang="zh-CN">
	<head>
		<title>账户列表 - mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<%@ include file="/common/include/title.jsp"%>
		<script type="text/javascript">
			function batchSel()
			{
				var operTypeBatch = $("#operTypeBatch").val(); 
				if('1' == operTypeBatch)
				{
					$("#statusBatch").show();
					$("#pubtimeBatch").hide();
				}else if('2' == operTypeBatch)
				{
					$("#statusBatch").hide();
					$("#pubtimeBatch").show();
				}
			}
		</script>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<!-- 网页头部结束 -->
		<div class="container main">
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<h2 class="sub-header">
						账户列表
						<small>
							<a href="${rootpath}/head/users/usersAccountList.html">账户列表</a>
							<a href="${rootpath}/head/users/usersAccountInsert.html" target="_blank">添加账户</a>
						</small>
					</h2>
					<form action="${rootpath}/head/users/usersAccountList.html" method="post" class="form-inline">
						<div class="form-group">
							<!-- <label for="keyword">关键字:</label> -->
							<input type="text" name="keyword" value="${requestScope.keyword }"  class="form-control" placeholder="请输入关键字">
							<label for="keyword">状态：</label>
							<select name="statusList" class="form-control required">
								<option value="">请选择</option>
								<option value="1" ${requestScope.statusList == '1' ? 'selected' : '' }>启用</option>
								<option value="0" ${requestScope.statusList == '0' ? 'selected' : '' }>禁用</option>
							</select>
							<label for="keyword">币种：</label>
							<select name="coinidList" class="form-control required">
								<option value="">请选择</option>
								<c:forEach items="${requestScope.coinrateList }" var="coinrate" varStatus="stat">
									<option value="${coinrate.id }" ${requestScope.coinidList == coinrate.id ? 'selected' : '' }>
										${coinrate.engname }(${coinrate.name })
									</option>
								</c:forEach>
							</select>
							<label for="keyword">网站：</label>
							<select name="wbidList" class="form-control required">
								<option value="">请选择</option>
								<c:forEach items="${requestScope.websiteList }" var="website" varStatus="stat">
									<option value="${website.id }" ${requestScope.wbidList == website.id ? 'selected' : '' }>
										${website.name }
									</option>
								</c:forEach>
							</select>
							<label for="email">发布时间:</label>
							<input type="text" name="st" value="${requestScope.st }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" class="form-control"/>
							-->
							<input type="text" name="ed" value="${requestScope.ed }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" class="form-control"/>
							<input type="submit" class="btn btn-primary" value="搜索">
							<label for="realPrice">任务id:</label>
							<span class="lead texted-success">
								${param.utid }
							</span>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th><input type="checkbox" name="selall" id="selall" onclick="checkAll('selall' ,'uaids' )"></th>
									<th>序号</th>
									<th>币种</th>
									<th>网站</th>
									<th>用户名</th>
									<th>余额</th>
									<th>冻结金额</th>
									<th title="最小交易量">最小</th>
									<th title="最大交易量">最大</th>
									<th title="最大收益">收益</th>
									<th>状态</th>
									<th>更新时间</th>
									<th>发布时间</th>
									<th>操作</th>
								</tr>
							</thead>
							<form id="batchForm" method="post" action="${rootpath }/head/users/usersAccountBatch.html" onsubmit="return checkOneSubmit('uaids','batchForm')">
								<c:set value="0" var="totalBalance"/>
								<c:set value="0" var="totalFund"/>
								<tbody>
									<c:forEach items="${requestScope.usersAccountList }" var="usersAccount" varStatus="stat">
										<tr target="sid_user" rel="${usersAccount.id }">
											<td><input type="checkbox" name="uaids" value="${usersAccount.id }"></td>
											<td>${stat.count }</td>
											<td>
												<a href="${rootpath}/deptRate.html?cid=${usersAccount.coinid}" target="_blank">
													${usersAccount.coinrateObj.name }
												</a>
											</td>
											<td>${usersAccount.websiteObj.name }</td>
											<td title="${usersAccount.username }">${fn:substring(usersAccount.username,0,10) }</td>
											<td>${usersAccount.balance }</td>
											<td>${usersAccount.fundmoney }</td>
											<td title="最小交易量">${usersAccount.trademinnum }</td>
											<td title="最大交易量">${usersAccount.trademaxnum }</td>
											<td title="最大收益">${usersAccount.prift }</td>
											<td>${usersAccount.statusStr }</td>
											<td><fmt:formatDate value="${usersAccount.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
											<td><fmt:formatDate value="${usersAccount.pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
											<td>
												<a target="_blank" href="${rootpath }/head/users/usersAccountUpdate.html?uaid=${usersAccount.id}">查看</a>
												<a target="_blank" href="${rootpath }/head/users/usersAccountUpdate.html?uaid=${usersAccount.id}&operType=update">修改</a>
												<a target="_blank" href="${rootpath }/head/orders/ordersList.html?uaid=${usersAccount.id}&utid=${param.utid}">交易挂单</a>
												<br/>
												<a target="_blank" href="${rootpath }/head/orders/tradeHistoryInsert.html?uaid=${usersAccount.id}">交易历史</a>
												<a target="_blank" href="${rootpath }/head/users/usersTaskOrderList.html?uaid=${usersAccount.id}">查看订单</a>
											</td>
										</tr>
										
										<c:set value="${totalBalance + usersAccount.balance }" var="totalBalance"/>
										<c:set value="${totalFund + usersAccount.fundmoney }" var="totalFund"/>
									</c:forEach>
									<tr target="sid_user" rel="${usersAccount.id }">
											<td colspan="2">本页总计</td>
											<td>${usersAccount.coinrateObj.name }</td>
											<td>${usersAccount.websiteObj.name }</td>
											<td>${usersAccount.username }</td>
											<td><fmt:formatNumber value="${totalBalance }" pattern="#,####.####"/></td>
											<td><fmt:formatNumber value="${totalFund }" pattern="#,####.####"/></td>
											<td>${usersAccount.statusStr }</td>
											<td><fmt:formatDate value="${usersAccount.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
											<td><fmt:formatDate value="${usersAccount.pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
											<td>
											</td>
										</tr>
								</tbody>
							</table>
							操作类型:
							<select name="operType" id="operTypeBatch" onchange="return batchSel()">
								<option value="1">修改状态</option>
								<option value="2">修改发布时间</option>
							</select>
							<select name="status" id="statusBatch" style="display: none;">
								<option value="0">禁用</option>
								<option value="1">启用</option>
							</select>
							<span id="pubtimeBatch" style="display: none;">
								<c:set value="${sys_now }" var="pubtime"/>
								<input type="text" name="pubtimeCond" value="<fmt:formatDate value="${pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/>" 
									onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" />
								</span>
							<input type="submit" value="提交">
						</form>
						注:选择网站下面的任意账户可修改信息(用户名和认证信息)
						
						<form id="pageFormUsersAccountList" method="post" action="${rootpath }/head/users/usersAccountList.html">
							<input type="hidden" name="keyword" value="${requestScope.keyword }">
							<input type="hidden" name="usersidList" value="${requestScope.usersidList }">
							<input type="hidden" name="coinidList" value="${requestScope.coinidList }">
							<input type="hidden" name="wbidList" value="${requestScope.wbidList }">
							<input type="hidden" name="statusList" value="${requestScope.statusList }">
							<input type="hidden" name="prostatusList" value="${requestScope.prostatusList }">
							<input type="hidden" name="st" value="${requestScope.st }">
							<input type="hidden" name="ed" value="${requestScope.ed }">
							
							<nav>
								<ul class="pager">
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','1','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">首页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','${requestScope.pageInfo.prePage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">上一页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','${requestScope.pageInfo.nextPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">下一页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','${requestScope.pageInfo.totalPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">末页</span></a>
									</li>
									<li>
										共${requestScope.pageInfo.totalPage }页
										共${requestScope.pageInfo.totalRecord }条
									</li>
									<li>第<input type="text" id="currentPage" name="currentPage" value="${requestScope.pageInfo.currentPage }" size="5" />页</li>
									<li>每页<input type="text" id="pageSize" name="pageSize" value="${requestScope.pageInfo.pageSize }" size="5"/>条</li>
									<li><input type="submit" value="GO" class="btn btn-primary"/></li>
								</ul>
							</nav>
						</form>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
<script type="text/javascript">
batchSel();
</script>
