<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html>
	<head>
		<title>添加账户- mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<%@ include file="/common/include/title.jsp"%>
		<script type="text/javascript">
			function websiteChange()
			{
				var authinfo = "" ; 
				var wbid = "" ;
				var wbtext = "" ;
				$("#wbid").children("option").each(function()
				{
					if($(this).prop("selected"))
					{
						authinfo = $(this).attr("authinfo") ; 
						wbid = $(this).attr("value");
						wbtext = $(this).html();
					}
				});
				
				//alert(wbid + "--" + wbtext + '--' + authinfo);
				eval("var json = " + authinfo)
				var sb = "" ; 
				for(var temp in json )
				{
					sb += temp + ":<input type='text' name='"+temp+"' class='form-control' value='' size='50'/><br/>" ;
				}
				//alert(sb);
				$("#authinfo").html(sb);				
			}
		</script>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="sub-header">
						添加账户
						<small>
							<a href="${rootpath}/head/users/usersAccountList.html">账户列表</a>
						</small>
					</h2>
					<form action="${rootpath }/head/users/usersAccountInsertSubmit.html" class="form-horizontal" method="post">
						<input type="hidden" name="operType" value="update">
						<div class="form-group">
							<label for="wbid" class="col-sm-2 control-label">网站:</label>
							<div class="col-sm-8">
								<select id="wbid" name="wbid" class="form-control required" onchange="return websiteChange()">
									<c:forEach items="${requestScope.websiteList }" var="website" varStatus="stat">
										<option value="${website.id }" authinfo=${website.authinfo }>
											${website.name }
										</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label">用户名:</label>
							<div class="col-sm-8">
								<input type="text" id="username" name="username" class="form-control" value="${requestScope.usersAccount.username }" size="50"/>(仅用来标识)
							</div> 
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">认证信息:</label>
							<div class="col-sm-8" id="authinfo">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-2">
								<button class="btn btn-lg btn-primary" type="submit">提交</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
<script type="text/javascript">
	websiteChange();
</script>
