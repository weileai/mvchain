<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<%-- usersTask的枚举 --%>
<jsp:useBean id="pojo" class="com.wang.mvchain.system.pojo.ATradeType"/>
<!DOCTYPE HTML>
<html lang="zh-CN">
	<head>
		<title>交易对列表 - mvchain</title>
		<%@ include file="/common/include/title.jsp"%>
		<script type="text/javascript">
			/**
				提交行情
			*/
			function deptSubmit(hrefId,deptSelId)
			{
				var href = $("#" + hrefId).attr("souHref") + "&wbid=" + $("#" + deptSelId).val() ;
				$("#" + hrefId).attr("href",href);
				return true ; 
			}
		</script>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<!-- 网页头部结束 -->
		<div class="container main">
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<h2 class="sub-header">
						交易对列表
						<small>
							<a href="${rootpath }/tradeTypeList.html">交易对列表</a>
						</small>
					</h2>
					<form action="${rootpath }/tradeTypeList.html" method="post" class="form-inline">
						<div class="form-group">
							<label for="keyword">关键字:</label>
							<input type="text" name="keyword" value="${requestScope.keyword }"  class="form-control" placeholder="请输入关键字">
							<label for="keyword">状态:</label>
							<select name="status" class="form-control">
								<option value="">请选择</option>
								<c:set value="0" var="statCount"/>
								<c:forEach items="${pojo.enumsMap }" var="me" varStatus="stat">
									<c:if test="${fn:startsWith(me.key,'STATUS-')}">
										<c:set value="${statCount + 1 }" var="statCount"/>
										<!-- 拆分,按照-拆分,取最后一个 -->
										<c:set value="${fn:split(me.key, '-')}" var="keys"/>
										<option value="${keys[1] }" ${requestScope.status == keys[1] ? 'selected' : '' }>${me.value }</option>
									</c:if>
								</c:forEach>
							</select>
							<label for="email">发布时间:</label>
							<input type="text" name="st" value="${requestScope.st }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" class="form-control"/>
							-->
							<input type="text" name="ed" value="${requestScope.ed }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" class="form-control"/>
							<input type="submit" class="btn btn-primary" value="搜索">
						</div>
					</form>
					<form method="post" class="form-inline">
						<div class="form-group">
							<label for="keyword">网站:</label>
							<select name="websiteId" id="websiteId"  class="form-control">
								<c:forEach items="${requestScope.websiteList}" var="website" varStatus="stat">
									<option value="${website.id}">${website.name }</option>
								</c:forEach>
							</select>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>序号</th>
									<th>名称</th>
									<th>从</th>
									<th>到</th>
									<th>汇率</th>
									<th>状态</th>
									<th>创建时间</th>
									<th class="visible-lg">发布时间</th>
									<th>更新时间</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${requestScope.tradeTypeList}" var="tradeType" varStatus="stat">
									<tr>
										<td>${stat.count }</td>
										<td>${tradeType.name }</td>
										<td>${tradeType.fromcoinCoinObj.name }</td>
										<td>${tradeType.tocoinCoinObj.name }</td>
										<td>${tradeType.rate }</td>
										<td>${tradeType.statusStr }</td>
										<td><fmt:formatDate value="${tradeType.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td class="visible-lg"><fmt:formatDate value="${tradeType.pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td><fmt:formatDate value="${tradeType.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td>
											<a target="_blank" id="dept_${tradeType.id }" href="" souHref="${rootpath }/tradeTypeDept.html?tradeTyepId=${tradeType.id}"
												onclick="return deptSubmit('dept_${tradeType.id }','websiteId')">查看行情</a>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						
						<form id="pageFormTradeTypeList" action="${rootpath }/tradeTypeList.html" method="post">
							<input type="hidden" name="keyword" value="${requestScope.keyword }">
							<input type="hidden" name="status" value="${requestScope.status }">
							<input type="hidden" name="st" value="${requestScope.st }">
							<input type="hidden" name="ed" value="${requestScope.ed }">
							
							<nav>
								<ul class="pager">
									<li>
										<a onclick="return pageFormSubmit('pageFormTradeTypeList','1','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">首页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormTradeTypeList','${requestScope.pageInfo.prePage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">上一页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormTradeTypeList','${requestScope.pageInfo.nextPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">下一页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormTradeTypeList','${requestScope.pageInfo.totalPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">末页</span></a>
									</li>
									<li>
										共${requestScope.pageInfo.totalPage }页
										共${requestScope.pageInfo.totalRecord }条
									</li>
									<li>第<input type="text" id="currentPage" name="currentPage" value="${requestScope.pageInfo.currentPage }" size="5" />页</li>
									<li>每页<input type="text" id="pageSize" name="pageSize" value="${requestScope.pageInfo.pageSize }" size="5"/>条</li>
									<li><input type="submit" value="GO" class="btn btn-primary"/></li>
								</ul>
							</nav>
						</form>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
