<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html lang="zh-CN">
	<head>
		<title>${requestScope.tradeType.name }(${requestScope.website.name })交易对列表 - mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<%@ include file="/common/include/title.jsp"%>
		<script type="text/javascript">
			/*
				网站切换
			*/
			function toggWebsite(webid)
			{
				var trs = $("#priceRateList > tbody > tr") ; 
				if('showAll' == webid)
				{
					trs.show();
				}else
				{
					trs.hide();
					trs.each(function()
					{
						if($(this).attr("webSellid") == webid || $(this).attr("webBuyid") == webid
						 || $(this).attr("chainFlag") == webid)	
						{
							$(this).show();
						}
					});
				}
				return false ; 
			}
		</script>
	</head>
	
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<!-- 网页头部结束 -->
		<div class="container main">
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<h2 class="sub-header">网站过滤
						<small>
							<a href="${rootpath }/tradeTypeDept.html?tradeTyepId=${requestScope.tradeType.id }&wbid=${requestScope.website.id }">
								${requestScope.tradeType.name }(${requestScope.website.name })交易对列表
							</a>
						</small>
					</h2>
					<div class="oninfo">
						<div>
							网站:
							<a href="${requestScope.website.url }" target="_blank">
								${requestScope.website.name }
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<div class="table-responsive">
						<table class="table table-striped" id="priceRateList">
							<thead>
								<tr>
									<th>序号</th>
									<th>价格(原卖)</th>
									<th>数量</th>
									<th>价格(原买)</th>
									<th>数量</th>
									<th>差价(价格)</th>
									<th>数量(价格)</th>
								</tr>
							</thead>
							<tbody>
								<c:set value="${requestScope.resultJSON.data.asks }" var="asks"/>
								<c:set value="${requestScope.resultJSON.data.bids }" var="bids"/>
								<c:forEach items="${asks}" var="map" varStatus="stat">
									<c:set value="${bids[stat.count] }" var="bidsTemp" />
									
									<fmt:formatNumber value="${map.rate - bidsTemp.rate }" pattern="#.###" var="priceCha"/>
									<fmt:formatNumber value="${map.vol - bidsTemp.vol }" pattern="#.###" var="valCha"/>
									<tr webSellid="${map.sellWebsite.id}" chainFlag="${cha > 0 ? 'chaTrue':'chaFalse'}" 
										webBuyid="${map.buyWebsite.id}" class="${cha > 0 ? 'hreflink' : ''}">
										<td>${stat.count }</td>
										<td>${map.rate }</td>
										<td>${map.vol }</td>
										<td>${bidsTemp.rate }</td>
										<td>${bidsTemp.vol }</td>
										<td>${priceCha }</td>
										<td>${valCha }</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>