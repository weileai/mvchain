<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html lang="zh-CN">
	<head>
		<title>${requestScope.coinrate.name }(${requestScope.coinrate.engname })货币网站汇总列表 - mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<%@ include file="/common/include/title.jsp"%>
		<script type="text/javascript">
			/*
				网站切换
			*/
			function toggWebsite(webid)
			{
				var trs = $("#priceRateList > tbody > tr") ; 
				if('showAll' == webid)
				{
					trs.show();
				}else
				{
					trs.hide();
					trs.each(function()
					{
						if($(this).attr("webSellid") == webid || $(this).attr("webBuyid") == webid
						 || $(this).attr("chainFlag") == webid)	
						{
							$(this).show();
						}
					});
				}
				return false ; 
			}
		</script>
	</head>
	
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<!-- 网页头部结束 -->
		<div class="container main">
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<h2 class="sub-header">网站过滤
						<small>
							<a href="${rootpath }/deptRate.html?cid=${requestScope.coinrate.id}">
								${requestScope.coinrate.name }(${requestScope.coinrate.engname })货币网站列表
							</a>
						</small>
					</h2>
					<div class="oninfo">
						<div>
							刷新时间:<fmt:formatDate value="${sys_now }" pattern="yyyy-MM-dd HH:mm:ss"/>
							离下次刷新剩余秒数:<span id="remSec" class="error_info"></span>
							<input type="checkbox" name="autoflush" id="autoflush" value="1" checked="checked"/>
							<label for="autoflush">自动刷新</label>
						</div>
						网站过滤:
						<a href="#" onclick="return toggWebsite('showAll')">全部</a>
						<c:forEach items="${requestScope.resultMap.deptBuyList }" var="dept" varStatus="stat">
							<a href="#" onclick="return toggWebsite('${dept.websiteObj.id}')">${dept.websiteObj.name }</a>
						</c:forEach>
						收益:
						<a href="#" onclick="return toggWebsite('chaTrue')">
							收益正
						</a>
						<a href="#" onclick="return toggWebsite('chaFalse')">
							收益负
						</a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<div class="table-responsive">
						<table class="table table-striped" id="priceRateList">
							<thead>
								<tr>
									<th>序号</th>
									<th>名称</th>
									<th>价格(原卖)</th>
									<th>价格(卖)</th>
									<th>时间(卖)</th>
									<th>名称</th>
									<th>价格(原买)</th>
									<th>价格(买)</th>
									<th>时间(买)</th>
									<th>差价</th>
									<th>总价格</th>
									<th>差价率</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${requestScope.resultResList}" var="map" varStatus="stat">
									<c:set value="${map.buyDept.price - map.sellDept.price }" var="cha"/>
									<c:set value="${map.buyDept.souprice - map.sellDept.souprice }" var="soucha"/>
									<c:set value="${map.buyDept.price + map.sellDept.price }" var="he"/>
									<tr webSellid="${map.sellWebsite.id}" chainFlag="${cha > 0 ? 'chaTrue':'chaFalse'}" 
										webBuyid="${map.buyWebsite.id}" class="${cha > 0 ? 'hreflink' : ''}">
										<td>${stat.count }</td>
										<td><a href="${map.sellWebsite.url }" target="_blank">${map.sellWebsite.name }</a></td>
										<td>${map.sellDept.souprice }*${map.sellDept.rate }</td>
										<td>${map.sellDept.price }</td>
										<td><fmt:formatDate value="${map.sellDept.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td><a href="${map.buyWebsite.url }" target="_blank">${map.buyWebsite.name }</a></td>
										<td>${map.buyDept.souprice }*${map.buyDept.rate }</td>
										<td>${map.buyDept.price }</td>
										<td><fmt:formatDate value="${map.buyDept.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td>
											<fmt:formatNumber value="${soucha}" pattern="#.##"/>
											-->
											<fmt:formatNumber value="${cha}" pattern="#.##"/>
										</td>
										<td><fmt:formatNumber value="${he}" pattern="#.##"/></td>
										<td><fmt:formatNumber value="${cha / he}" pattern="#.##%"/></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
<script type="text/javascript">
	var timer = 5000 ;
	var remTimer = 5000 ; 
	var timerefreshFlag = "" ; 
	var now = new Date();
	
	/**
		定时刷新网页
	*/
	function timerefresh()
	{
		var currdate = new Date();
		if((currdate - now) > timer)
		{
			var url = window.location.href ;
			var ed = url.indexOf("&date="); 
			if(ed != -1)
			{
				//截字符串
				url = url.substring(0 , ed);
			}
			url += "&date=" + new Date().getTime();
			window.location.href = url ;		
		}else
		{
			//window.alert("差价:${requestScope.resultMap.buyDept.price - requestScope.resultMap.sellDept.price },请进行操作");
			document.title.status = "页面刷新" ;
			//window.clearInterval(timerefreshFlag);
		}
	}
	
	timerefreshFlag = window.setTimeout(function()
	{
		//必须选中自动刷新
		if($("#autoflush").prop("checked"))
		{
			timerefresh();
		}
	}, timer);
	
	window.setInterval(function ()
	{
		$("#remSec").html(remTimer / 1000);
		remTimer -= 1000 ; 
	}, 1000)
</script>