<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<jsp:useBean id="tradeHistoryCond" class="com.wang.mvchain.users.pojo.ATradeHistory"/>
<!DOCTYPE HTML>
<html>
	<head>
		<title>添加交易历史- mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<%@ include file="/common/include/title.jsp"%>
		<%-- 表单验证 --%>
		<link rel="stylesheet" href="${rootpath }/common/resource/bootstrapvalidator/css/formValidation.css"/>
		<script type="text/javascript" src="${rootpath }/common/resource/bootstrapvalidator/js/formValidation.js"></script>
	    <script type="text/javascript" src="${rootpath }/common/resource/bootstrapvalidator/js/framework/bootstrap.js"></script>
	    
	    <script type="text/javascript">	    
			$(document).ready(function() {
			    $('#form').bootstrapValidator({
			    	container: 'tooltip',
			        feedbackIcons: {
			            valid: 'glyphicon glyphicon-ok',
			            invalid: 'glyphicon glyphicon-remove',
			            validating: 'glyphicon glyphicon-refresh'
			        },
			        fields: {
			        	ctName: {
			                validators: {
			                	notEmpty:
		                    	{
	                    			message: '名称不能为空'
		                    	}
			                }
			            },
			        	defprice: {
			                validators: {
			                	notEmpty:
		                    	{
	                    			message: '价格不能为空'
		                    	},
		                    	regexp: {
		                            regexp: /^[0-9_\.]+$/,
		                            message: '请输入合法的数字'
		                        }
			                }
			            },
			        	totalval: {
			                validators: {
			                	notEmpty:
		                    	{
	                    			message: '交易总量不能为空'
		                    	},
		                    	regexp: {
		                            regexp: /^[0-9_\.]+$/,
		                            message: '请输入合法的数字'
		                        }
			                }
			            }
			        }
			    });
			    
			    $('#form').bootstrapValidator();
			    
			    tradeTypeChange('tradeType','ctName');
			});
		</script>
		
		<script type="text/javascript">
			/*
				下拉框切换
			*/
			function tradeTypeChange(currSelect,tarSelect)
			{
				var currSelect = $('#' + currSelect).find('option:selected').html() ; 
				$('#'+tarSelect).val(currSelect);
			}
		</script>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="sub-header">
						添加交易历史
						<small>
							<a href="${rootpath}/head/orders/tradeHistoryList.html">我的任务列表</a>
						</small>
					</h2>
					<form id="form" action="${rootpath }/head/orders/tradeHistoryInsertSubmit.html" class="form-horizontal" method="post">
						<input type="hidden" name="uaid" value="${param.uaid }">
						<div class="form-group">
							<label for="name" class="col-sm-2 control-label">交易名称:</label>
							<div class="col-sm-8">
								<select id="tradeType" class="form-control" onchange="tradeTypeChange('tradeType','ctName')">
									<c:forEach items="${requestScope.tradeTypeList }" var="tradeType" varStatus="stat">
										<option value="${tradeType.id }">${tradeType.name }</option>
									</c:forEach>
								</select>
							</div> 
						</div>
						<div class="form-group">
							<label for="name" class="col-sm-2 control-label">交易名称:</label>
							<div class="col-sm-8">
								<input type="text" id="ctName" name="ctName" class="form-control" placeholder="交易名称" 
									value="${requestScope.tradeHistory.ctName }" 
									autofocus="autofocus">
							</div> 
						</div>
						<div class="form-group">
							<label for="totalnum" class="col-sm-2 control-label">价格:</label>
							<div class="col-sm-8">
								<input type="text" size="5" id="defprice" name="defprice" class="form-control" placeholder="价格" 
									value="${requestScope.tradeHistory.defprice }" 
									autofocus="autofocus">
							</div> 
						</div>
						<div class="form-group">
							<label for="totalnum" class="col-sm-2 control-label">交易数量:</label>
							<div class="col-sm-8">
								<input type="text" size="5" id="totalval" name="totalval" class="form-control" placeholder="交易数量" 
									value="${requestScope.tradeHistory.totalval }" 
									autofocus="autofocus" onblur="compPrice('defprice','totalval','totalprice','totalPrice')">
							</div> 
						</div>
						<div class="form-group">
							<label for="totalnum" class="col-sm-2 control-label">总金额:</label>
							<div class="col-sm-8">
								<input type="text" size="5" id="totalprice" name="totalprice" class="form-control" placeholder="总金额" 
									value="0" 
									autofocus="autofocus" onblur="compPrice('defprice','totalval','totalprice','totalval')">
							</div> 
						</div>
						<div class="form-group">
							<label for="totalnum" class="col-sm-2 control-label">交易时间:</label>
							<div class="col-sm-8">
								<c:set value="${sys_now }" var="pubTimeStr"/>
								<c:if test="${requestScope.tradeHistory.pubtime != null}">
									<c:set value="${requestScope.tradeHistory.pubtime}" var="pubTimeStr"/>
								</c:if>
								<input type="text" name="pubTimeStr" value="<fmt:formatDate value="${pubTimeStr }" pattern="yyyy-MM-dd HH:mm:ss"/>" 
									onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" class="form-control"/>
							</div> 
						</div>
						<div class="form-group">
							<label for="totalnum" class="col-sm-2 control-label">订单号:</label>
							<div class="col-sm-8">
								<input type="text" size="5" id="ordersn" name="ordersn" class="form-control" placeholder="订单号" 
									value="${requestScope.tradeHistory.ordersn }" 
									autofocus="autofocus">
							</div> 
						</div>
						<div class="form-group">
							<label for="soutype1" class="col-sm-2 control-label">交易类型:</label>
							<div class="col-sm-8">
								<c:forEach items="${tradeHistoryCond.ordertypeMap }" var="me" varStatus="stat">
									<!-- checkbox -->
									<c:set value="${requestScope.tradeHistory.ordertype == me.key || stat.count == 1}" var="checkFlag"/>
									
									<input type="radio" id="ordertype${me.key }" name="ordertype" value="${me.key }" ${checkFlag  ? 'checked' : '' }>
									<label for="ordertype${me.key }">${me.value }&nbsp;&nbsp;&nbsp;</label>
								</c:forEach>
							</div> 
						</div>
						<%-- <div class="form-group">
							<label for="status0" class="col-sm-2 control-label">状态：</label>
							<div class="col-sm-8">
								<c:forEach items="${tradeHistoryCond.statusMap }" var="me" varStatus="stat">
									<!-- checkbox -->
									<c:set value="${requestScope.tradeHistory.status == me.key || stat.count == 1}" var="checkFlag"/>
									
									<input type="radio" id="status${me.key }" name="status" value="${me.key }" ${checkFlag  ? 'checked' : '' }>
									<label for="status${me.key }">${me.value }&nbsp;&nbsp;&nbsp;</label>
								</c:forEach>
							</div>
						</div> --%>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-2">
								<button class="btn btn-lg btn-primary" type="submit">提交</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>