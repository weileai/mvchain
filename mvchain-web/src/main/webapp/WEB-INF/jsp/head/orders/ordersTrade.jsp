<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html>
	<head>
		<title>我的${requestScope.usersAccount.websiteObj.name}交易- mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<%@ include file="/common/include/title.jsp"%>
		<%-- 表单验证 --%>
		<link rel="stylesheet" href="${rootpath }/common/resource/bootstrapvalidator/css/formValidation.css"/>
		<script type="text/javascript" src="${rootpath }/common/resource/bootstrapvalidator/js/formValidation.js"></script>
	    <script type="text/javascript" src="${rootpath }/common/resource/bootstrapvalidator/js/framework/bootstrap.js"></script>
	    
	    <script type="text/javascript">	    
			$(document).ready(function() {
			    $('#form').bootstrapValidator({
			    	container: 'tooltip',
			        feedbackIcons: {
			            valid: 'glyphicon glyphicon-ok',
			            invalid: 'glyphicon glyphicon-remove',
			            validating: 'glyphicon glyphicon-refresh'
			        },
			        fields: {
			        	price: {
			                validators: {
			                	notEmpty:
		                    	{
	                    			message: '价格不能为空'
		                    	},
		                    	regexp: {
		                            regexp: /^[0-9_\.]+$/,
		                            message: '请输入合法的数字'
		                        }
			                }
			            }
			        },
			        fields: {
			        	price: {
			                validators: {
			                	notEmpty:
		                    	{
	                    			message: '数量不能为空'
		                    	},
		                    	regexp: {
		                            regexp: /^[0-9_\.]+$/,
		                            message: '请输入合法的数字'
		                        }
			                }
			            }
			        }
			    });
			    
			    $('#form').bootstrapValidator();
			    
			    /*获取一下价格*/
			    getPrice()
			});
			
			/* 默认值 */
			var cid = ${requestScope.usersAccount.coinid};
			var wbid = ${requestScope.usersAccount.wbid};
			/* 获取价格 */
			function getPrice()
			{
				/*http://localhost:8080/mvchain-web/outer/system/client.html?json= 
				{"code":"0","method":"deptList","operType":"cancel","data":{"cid":"1","wbid":"1"}} */
				/* console.info("====" + $("#moneytype > option:selected").attr("tradeTypeId")) */
				var tradeTypeId = $("#moneytype > option:selected").attr("tradeTypeId");
				$.post(
					"${rootpath}/outer/system/client.html",
					"json={'code':'0','method':'deptList','operType':'cancel','data':{'cid':'"+ cid +"','wbid':'"+ wbid +"','tradeTypeId':'"+ tradeTypeId +"'}}",
					function(data)
					{
						if(data.code == "0")
						{
							/* 判断买卖价格 */
							if($("#type0").prop("checked"))
							{
								/* 买 */
								$("#price").val(data.data.deptSellArr[0].souprice);
							}else
							{
								/* 卖 */
								$("#price").val(data.data.deptBuyArr[0].souprice);
							}
							$("#buyPriceDiv").html(data.data.deptBuyArr[0].souprice);
							$("#sellPriceDiv").html(data.data.deptSellArr[0].souprice);
						}else
						{
							alert(data.info);
						}
					},
					"json"
				);
				return true ;
			}
		</script>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="sub-header">
						交易
						<small>
							<a href="${rootpath}/head/orders/ordersList.html?uaid=${requestScope.usersAccount.id}">我的交易挂单列表</a>
						</small>
					</h2>
					<form id="form" action="${rootpath }/head/orders/ordersTradeSubmit.html" class="form-horizontal" method="post">
						<input type="hidden" name="uaid" value="${requestScope.usersAccount.id}">
						<input type="hidden" name="utid" value="${requestScope.usersTask.id }">
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label">任务Id:</label>
							<div class="col-sm-8">
								${param.utid}
							</div> 
						</div>
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label">交易网站:</label>
							<div class="col-sm-8">
								${requestScope.usersAccount.websiteObj.name}
							</div> 
						</div>
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label">币种类型:</label>
							<div class="col-sm-8">
								<c:set value="${requestScope.usersAccount.coinrateObj.name}" var="coinrateName"/>
								${coinrateName }
								<select name="moneytype" id="moneytype" class="required" onchange="return getPrice()">
									<c:forEach items="${requestScope.usersAccount.websiteObj.moneyStatusJSON }" var="moneyJSON" varStatus="stat">
										<option value="${moneyJSON.value.name }" tradeTypeId="${moneyJSON.value.id }" 
										${requestScope.moneytype == moneyJSON.value.name || fn:indexOf(moneyJSON.value.name,fn:toLowerCase(coinrateName)) != -1 ? 'selected' : '' }>
										${moneyJSON.value.name }
										</option>
									</c:forEach>
								</select>
							</div> 
						</div>
						<div class="form-group">
							<label for="wbid" class="col-sm-2 control-label">交易类型:</label>
							<div class="col-sm-8">
								<input type="radio" id="type0" name="type" value="0" checked="checked" onclick="return getPrice()">
								<label for="type0">买&nbsp;&nbsp;&nbsp;</label>
								<input type="radio" id="type1" name="type" value="1" onclick="return getPrice()">
								<label for="type1">卖&nbsp;&nbsp;&nbsp;</label>
							</div>
						</div>
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label">价格:</label>
							<div class="col-sm-8">
								<input type="text" size="5" id="price" name="price" class="form-control" placeholder="价格" 
								value="${requestScope.paramsMap.price}" autofocus="autofocus">
								买入价:<span id="buyPriceDiv">${requestScope.resultMap.buyDept.price }</span>;
								卖出价:<span id="sellPriceDiv">${requestScope.resultMap.sellDept.price }</span>;
							</div> 
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">数量:</label>
							<div class="col-sm-8" id="authinfo">
								<input type="text" size="5" id="amount" name="amount" class="form-control" placeholder="数量" 
								value="${requestScope.amount}" autofocus="autofocus">
							</div>
						</div>
						<c:if test="${requestScope.amount != '0'}">
							<div class="form-group">
								<div class="col-sm-8 col-sm-offset-2">
									<button class="btn btn-lg btn-primary" type="submit">提交</button>
								</div>
							</div>
						</c:if>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
<!-- <script type="text/javascript">
	websiteChange();
</script> -->
