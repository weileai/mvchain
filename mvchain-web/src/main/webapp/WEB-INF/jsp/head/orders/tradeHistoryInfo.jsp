<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<jsp:useBean id="tradeHistoryCond" class="com.wang.mvchain.users.pojo.ATradeHistory"/>
<!DOCTYPE HTML>
<html>
	<head>
		<title>查看交易历史- mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="sub-header">
						查看交易历史
						<small>
							<a href="${rootpath}/head/orders/tradeHistoryList.html">我的任务列表</a>
						</small>
					</h2>
					<form id="form" action="${rootpath }/head/orders/tradeHistoryInsertSubmit.html" class="form-horizontal" method="post">
						<div class="form-group">
							<label for="name" class="col-sm-2 control-label">id:</label>
							<div class="col-sm-8">
								${requestScope.tradeHistory.id }
							</div> 
						</div>
						<div class="form-group">
							<label for="name" class="col-sm-2 control-label">账户信息:</label>
							<div class="col-sm-8">
								<a class="hreflink"	height="400" width="800" target="dialog"
									href="${rootpath }/head/orders/usersAccountUpdate.html?uaid=${tradeHistory.usersAccountObj.id }">
									${tradeHistory.usersAccountObj.websiteObj.name }(${tradeHistory.uaid })
								</a>
							</div> 
						</div>
						<div class="form-group">
							<label for="name" class="col-sm-2 control-label">交易名称:</label>
							<div class="col-sm-8">
								${requestScope.tradeHistory.ctName }
							</div> 
						</div>
						<div class="form-group">
							<label for="name" class="col-sm-2 control-label">订单号:</label>
							<div class="col-sm-8">
								${requestScope.tradeHistory.ordersn }
							</div> 
						</div>
						<div class="form-group">
							<label for="totalnum" class="col-sm-2 control-label">实时价格:</label>
							<div class="col-sm-8">
								<c:set value="${requestScope.resultJSON.data.bids[0].rate }" var="realPrice"/>
								<p class="text-success lead">
									${realPrice } 
									差价:<fmt:formatNumber value="${realPrice - requestScope.tradeHistory.tradePrice}" pattern="#.#####"/>
									涨幅:<fmt:formatNumber value="${(realPrice - requestScope.tradeHistory.tradePrice) / requestScope.tradeHistory.tradePrice}" pattern="#.##%"/>
								</p>
							</div> 
						</div>
						<div class="form-group">
							<label for="totalnum" class="col-sm-2 control-label">价格:</label>
							<div class="col-sm-8">
								<fmt:formatNumber value="${requestScope.tradeHistory.defprice }" pattern="#.#####"/>
							</div> 
						</div>
						<div class="form-group">
							<label for="totalnum" class="col-sm-2 control-label">成交价:</label>
							<div class="col-sm-8">
								<fmt:formatNumber value="${requestScope.tradeHistory.tradePrice }" pattern="#.#####"/>
							</div> 
						</div>
						<div class="form-group">
							<label for="totalnum" class="col-sm-2 control-label">交易数量:</label>
							<div class="col-sm-8">
								${requestScope.tradeHistory.totalval }
							</div> 
						</div>
						<div class="form-group">
							<label for="totalnum" class="col-sm-2 control-label">成交量:</label>
							<div class="col-sm-8">
								${requestScope.tradeHistory.tradeedval }
							</div> 
						</div>
						<div class="form-group">
							<label for="totalnum" class="col-sm-2 control-label">总价格:</label>
							<div class="col-sm-8">
								${requestScope.tradeHistory.totalprice }
							</div> 
						</div>
						<div class="form-group">
							<label for="soutype1" class="col-sm-2 control-label">交易类型:</label>
							<div class="col-sm-8">
								${requestScope.tradeHistory.ordertypeStr}
							</div> 
						</div>
						<div class="form-group">
							<label for="soutype1" class="col-sm-2 control-label">状态:</label>
							<div class="col-sm-8">
								${requestScope.tradeHistory.statusStr}
							</div> 
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">创建时间：</label>
							<div class="col-sm-8">
								<fmt:formatDate value="${requestScope.tradeHistory.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">更新时间：</label>
							<div class="col-sm-8">
								<fmt:formatDate value="${requestScope.tradeHistory.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/>
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">交易时间：</label>
							<div class="col-sm-8">
								<fmt:formatDate value="${requestScope.tradeHistory.pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
							</div>
						</div>
						<%-- <div class="form-group">
							<label for="status0" class="col-sm-2 control-label">状态：</label>
							<div class="col-sm-8">
								<c:forEach items="${tradeHistoryCond.statusMap }" var="me" varStatus="stat">
									<!-- checkbox -->
									<c:set value="${requestScope.tradeHistory.status == me.key || stat.count == 1}" var="checkFlag"/>
									
									<input type="radio" id="status${me.key }" name="status" value="${me.key }" ${checkFlag  ? 'checked' : '' }>
									<label for="status${me.key }">${me.value }&nbsp;&nbsp;&nbsp;</label>
								</c:forEach>
							</div>
						</div> --%>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>