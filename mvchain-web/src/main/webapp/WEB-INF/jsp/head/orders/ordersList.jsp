<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html lang="zh-CN">
	<head>
		<title>我的${requestScope.usersAccount.websiteObj.name}交易挂单 - mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<!-- 网页头部结束 -->
		<div class="container main">
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<h2 class="sub-header">
						交易网站:${requestScope.usersAccount.websiteObj.name}
						<small>
							<a href="${rootpath}/head/orders/ordersList.html?uaid=${requestScope.usersAccount.id}">我的交易挂单列表</a>
							<a target="_blank" href="${rootpath}/head/orders/ordersTrade.html?uaid=${requestScope.usersAccount.id}&utid=${param.utid}">下单交易</a>
						</small>
					</h2>
					<form action="${rootpath}/head/orders/ordersList.html" method="post" class="form-inline">
						<input type="hidden" name="uaid" value="${requestScope.usersAccount.id}">
						<div class="form-group">
							<label for="keyword">币种类型：</label>
							<select name="moneytype" class="form-control required">
								<option value="">请选择</option>
								<c:forEach items="${requestScope.usersAccount.websiteObj.moneyStatusJSON }" var="moneyJSON" varStatus="stat">
									<option value="${moneyJSON.value.name }" ${requestScope.moneytype == moneyJSON.value.name ? 'selected' : '' }>${moneyJSON.value.name }</option>
								</c:forEach>
							</select>
							<input type="submit" class="btn btn-primary" value="搜索">
							<label for="realPrice">任务id:</label>
							<span class="lead texted-success">
								${param.utid }
							</span>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<td><input type="checkbox" name="selAll" id="selAll" onclick="checkAll('selAll','ids')"></td>
									<th>序号</th>
									<th>订单号</th>
									<th>类型(买/卖)</th>
									<th>价格</th>
									<th>数量</th>
									<th>成交量</th>
									<th>时间</th>
									<th>操作</th>
								</tr>
							</thead>
							<form id="batchForm" method="post" action="${rootpath}/head/orders/ordersCancel.html" onsubmit="return checkOneSubmit('ids','batchForm')">
								<input type="hidden" name="uaid" value="${requestScope.usersAccount.id}">
								<input type="hidden" name="moneytype" value="${requestScope.moneytype}">
								
								<c:set value="0" var="totalBalance"/>
								<c:set value="0" var="totalFund"/>
								<tbody>
									<c:set value="0" var="totalNum"/>
									<c:set value="0" var="totalDealNum"/>
									<c:forEach items="${requestScope.resultJSON.data.ordersList }" var="orders" varStatus="stat">
										<tr target="sid_user" rel="${orders.order_id}">
											<td><input type="checkbox" name="ids" value="${orders.order_id}"></td>
											<td>${stat.count }</td>
											<td>${orders.order_id}</td>
											<td>
												<c:choose>
													<c:when test="${orders.type == '0'}">
														买
													</c:when>
													<c:when test="${orders.type == '1'}">
														卖
													</c:when>
												</c:choose>
											</td>
											<td>${orders.price }</td>
											<td>${orders.amount}</td>
											<td>${orders.deal_amount}</td>
											<td>${orders.create_date }</td>
											<td>
												<a onclick="return confirm('确认执行此操作吗?')"
													href="${rootpath}/head/orders/ordersCancel.html?ids=${orders.order_id}&uaid=${requestScope.usersAccount.id}&moneytype=${requestScope.moneytype}">撤单</a>
											</td>
										</tr>
									</c:forEach>
									
									<tr>
										<td>合计:</td>
										<td></td>
										<td></td>
										<td></td>
										<td><fmt:formatNumber value="${totalNum }" pattern="#,####.####"/></td>
										<td><fmt:formatNumber value="${totalDealNum}" pattern="#,####.####"/></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
								</tbody>
							</table>
							操作类型:
							<select name="operType" id="operTypeBatch" onchange="return batchSel()">
								<option value="1">撤单</option>
							</select>
							<input type="submit" value="提交">
						</form>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
