<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<jsp:useBean id="tradeHistoryCond" class="com.wang.mvchain.users.pojo.ATradeHistory"/>
<!DOCTYPE HTML>
<html lang="zh-CN">
	<head>
		<title>我的交易历史列表 - mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<%@ include file="/common/include/title.jsp"%>
		<script type="text/javascript">
			/**
				计算价格
			*/
			function priceRelaPrice()
			{
				/* 基本价格 */
				var priceRela = $("#priceRela").val();
				/* 每一列要赋值 */
				/* 涨:0.85%;
				     跌:0.17%;
				*/
				$("td[price=priceCond]").each(function()
				{
					/* .children("span").html(text); */
					var souPrice = $.trim($(this).children("a").html());
					/* 计算比例 */
					var text = (souPrice - priceRela ) / priceRela * 100; 
					/* 赋值
						toFixed:保留小数点以后数位;
					*/
					$(this).children("span").html(text.toFixed(2) + "%");
				});
				return false ; 
			}
		</script>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<!-- 网页头部结束 -->
		<div class="container main">
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<h2 class="sub-header">
						交易历史列表
						<small>
							<a href="${rootpath}/head/orders/tradeHistoryList.html?utidList=${requestScope.utidList}&uaid=${requestScope.uaid}&websiteId=${requestScope.websiteId}">我的交易历史列表</a>
							<c:if test="${requestScope.usersTask.status == 1 }">
								<a href="${rootpath}/head/orders/usersAccountList.html?utid=${requestScope.utidList}&coinidList=${requestScope.usersTask.sellTradeTypeObj.fromcoinid}" target="_blank">下订单</a>
							</c:if>
						</small>
					</h2>
					<form class="form-inline" action="${rootpath}/head/orders/tradeHistoryList.html" method="post">
						<input type="hidden" name="uaid" value="${requestScope.uaid }">
						<div class="form-group">
							<!-- <label for="keyword">关键字:</label> -->
							<input type="text" name="keyword" value="${requestScope.keyword }"  class="form-control" placeholder="请输入关键字">
							<label for="keyword">状态：</label>
							<select name="statusList" class="form-control required">
								<option value="" >请选择</option>
								<c:forEach items="${tradeHistoryCond.statusMap }" var="me" varStatus="stat">
									<!-- checkbox -->
									<option value="${me.key }" ${requestScope.statusList == me.key ? 'selected' : ''}>${me.value }</option>
								</c:forEach>
							</select>
							<label for="keyword">类型：</label>
							<select name="ordertypeList" class="form-control required">
								<option value="">请选择</option>
								<c:forEach items="${tradeHistoryCond.ordertypeMap }" var="me" varStatus="stat">
									<option value="${me.key }" ${requestScope.ordertypeList == me.key ? 'selected' : ''}>${me.value }</option>
								</c:forEach>
							</select>
							<label for="email">交易时间:</label>
							<input type="text" name="st" value="${requestScope.st }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" class="form-control"/>
							-->
							<input type="text" name="ed" value="${requestScope.ed }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" class="form-control"/>
							<input type="submit" class="btn btn-primary" value="搜索">
						</div>
					</form>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<h2 class="sub-header"></h2>
					<div class="table-responsive">
						<form class="form-inline" id="batchForm" method="post" action="${rootpath }/head/orders/tradeHistoryBatch.html" onsubmit="return checkOneSubmit('thids','batchForm')">
							<input type="hidden" name="keyword" value="${requestScope.keyword }">
							<input type="hidden" name="statusList" value="${requestScope.statusList }">
							<input type="hidden" name="ordertypeList" value="${requestScope.ordertypeList }">
							<input type="hidden" name="st" value="${requestScope.st }">
							<input type="hidden" name="ed" value="${requestScope.ed }">
							<input type="hidden" name="uaid" value="${requestScope.uaid }">
							
							操作类型:
							<select name="operType" id="operTypeBatch" class="form-control">
								<option value="status">修改状态</option>
								<option value="select">查询状态</option>
							</select>
							状态:
							<select name="status" id="statusBatch" class="form-control">
								<c:forEach items="${tradeHistoryCond.statusMap }" var="me" varStatus="stat">
									<!-- checkbox -->
									<option value="${me.key }" ${requestScope.statusList == me.key ? 'selected' : ''}>${me.value }</option>
								</c:forEach>
							</select>
							<input type="submit" value="提交" class="btn btn-primary">
							<br/>
							<input type="text" class="form-control" name="priceRela" id="priceRela" value="0">
							<input type="button" value="计算价格" class="btn btn-primary" onclick="return priceRelaPrice()">
							注:只有修改状态:(挂单,撤单)可以使用;查询状态也可以使用
							<table class="table table-striped">
								<thead>
									<tr>
										<th><input type="checkbox" name="selall" id="selall" onclick="checkAll('selall' ,'thids')"></th>
										<th>序号</th>
										<th>用户</th>
										<th>交易类型</th>
										<th>订单id</th>
										<th>原价</th>
										<th>成交价</th>
										<th>成交量</th>
										<th>总数量</th>
										<th>总价格</th>
										<th>类型</th>
										<th>状态</th>
										<th>交易时间</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${requestScope.tradeHistoryList }" var="tradeHistory" varStatus="stat">
										<tr target="sid_user" rel="${tradeHistory.id }">
											<td><input type="checkbox" name="thids" value="${tradeHistory.id }"></td>
											<td>${stat.count }</td>
											<td>
												<a class="hreflink"	height="400" width="800" target="dialog"
													href="${rootpath }/head/users/usersAccountUpdate.html?uaid=${tradeHistory.usersAccountObj.id }">
													${tradeHistory.usersAccountObj.websiteObj.name }(${tradeHistory.uaid })
												</a>
											</td>
											<td>${tradeHistory.ctName }</td>
											<td>${tradeHistory.ordersn }</td>
											<td price="priceCond">
												<a href="" onclick="$('#priceRela').val($.trim($(this).html()));return false;">
													${tradeHistory.defprice }
												</a>
												<span class="text-warning"></span>
											</td>
											<td>${tradeHistory.tradePrice }</td>
											<td>${tradeHistory.tradeedval }</td>
											<td>${tradeHistory.totalval }</td>
											<td>${tradeHistory.totalprice }</td>
											<td>${tradeHistory.ordertypeStr }</td>
											<td width="60">${tradeHistory.statusStr }</td>
											<td><fmt:formatDate value="${tradeHistory.pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
											<td>
												<!-- 未下单时才能查看 -->
												<c:if test="${tradeHistory.status == '0' }">
													<a target="_blank" href="${rootpath }/head/orders/tradeHistoryUpdate.html?thid=${tradeHistory.id}&operType=update">修改</a>
												</c:if>
												<a target="_blank" href="${rootpath }/head/orders/tradeHistoryUpdate.html?thid=${tradeHistory.id}">查看</a>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</form>
						<form id="pageFormUsersAccountList" method="post" action="${rootpath }/head/orders/tradeHistoryList.html">
							<input type="hidden" name="keyword" value="${requestScope.keyword }">
							<input type="hidden" name="statusList" value="${requestScope.statusList }">
							<input type="hidden" name="ordertypeList" value="${requestScope.ordertypeList }">
							<input type="hidden" name="st" value="${requestScope.st }">
							<input type="hidden" name="ed" value="${requestScope.ed }">
							<input type="hidden" name="uaid" value="${requestScope.uaid }">
							
							<nav>
								<ul class="pager">
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','1','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">首页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','${requestScope.pageInfo.prePage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">上一页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','${requestScope.pageInfo.nextPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">下一页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','${requestScope.pageInfo.totalPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">末页</span></a>
									</li>
									<li>
										共${requestScope.pageInfo.totalPage }页
										共${requestScope.pageInfo.totalRecord }条
									</li>
									<li>第<input type="text" id="currentPage" name="currentPage" value="${requestScope.pageInfo.currentPage }" size="5" />页</li>
									<li>每页<input type="text" id="pageSize" name="pageSize" value="${requestScope.pageInfo.pageSize }" size="5"/>条</li>
									<li><input type="submit" value="GO" class="btn btn-primary"/></li>
								</ul>
							</nav>
						</form>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
