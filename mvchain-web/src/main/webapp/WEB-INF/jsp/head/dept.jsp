<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html lang="zh-CN">
	<head>
		<title>${requestScope.coinrate.name }(${requestScope.coinrate.engname })深度列表 - mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<%@ include file="/common/include/title.jsp"%>
		<style>
			.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td
			{
				padding: 8px 1px;
				font-size: 12px;
			}
		</style>
	</head>
	<body>
		<embed src="${rootpath }/error/btc_cny_music.mp3" autostart="true" hidden="true" loop="true">
		<%@ include file="/common/include/header.jsp"%>
		<!-- 网页头部结束 -->
		<div class="container main">
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<h2 class="sub-header">
						${requestScope.coinrate.name }(${requestScope.coinrate.engname })深度列表
					</h2>
					<c:choose>
						<c:when test="${requestScope.cid == '1' }">
							btc_cny
							<c:if test="${requestScope.resultMap.buyDept.price - requestScope.resultMap.sellDept.price > param.chprice}">
								<audio src="${rootpath }/error/btc_cny_music.mp3" autoplay="autoplay" loop="loop"></audio>  
								<span class="error_info">搬砖开始</span>
							</c:if>
							<c:if test="${requestScope.resultMap.buyDept.price < param.buyprice}">
								<audio src="${rootpath }/error/btc_cny_music.mp3" autoplay="autoplay" loop="loop"></audio>
								<span class="error_info">搬砖开始</span>
							</c:if>
							<c:if test="${requestScope.resultMap.sellDept.price > param.sellprice}">
								<audio src="${rootpath }/error/btc_cny_music.mp3" autoplay="autoplay" loop="loop"></audio>
								<span class="error_info">搬砖开始</span>
							</c:if>
						</c:when>
						<c:when test="${requestScope.cid == '4' }">
							ltc_cny
							<c:if test="${requestScope.resultMap.buyDept.price - requestScope.resultMap.sellDept.price > param.chprice}">
								<audio src="${rootpath }/error/ltc_cny_music.mp3" autoplay="autoplay" loop="loop"></audio>
								<span class="error_info">搬砖开始</span>
							</c:if>
							<c:if test="${requestScope.resultMap.buyDept.price < param.buyprice}">
								<audio src="${rootpath }/error/ltc_cny_music.mp3" autoplay="autoplay" loop="loop"></audio>
								<span class="error_info">搬砖开始</span>
							</c:if>
							<c:if test="${requestScope.resultMap.sellDept.price > param.sellprice}">
								<audio src="${rootpath }/error/ltc_cny_music.mp3" autoplay="autoplay" loop="loop"></audio>
								<span class="error_info">搬砖开始</span>
							</c:if>
						</c:when>
					</c:choose>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<div class="oninfo">
						<div>
							刷新时间:<fmt:formatDate value="${sys_now }" pattern="yyyy-MM-dd HH:mm:ss"/>
							离下次刷新剩余秒数:<span id="remSec" class="error_info"></span>
						</div>
						条数:${fn:length(requestScope.resultMap.deptSellList)};卖出价: ${requestScope.resultMap.sellDept.price},交易量:${requestScope.resultMap.sellDept.volnum},网站id: <a href="${requestScope.resultMap.sellDept.websiteObj.url}" target="_blank">${requestScope.resultMap.sellDept.websiteObj.name}</a><br/>   
						条数:${fn:length(requestScope.resultMap.deptBuyList)};买入价: ${requestScope.resultMap.buyDept.price},交易量:${requestScope.resultMap.buyDept.volnum},网站id: <a href="${requestScope.resultMap.buyDept.websiteObj.url}" target="_blank">${requestScope.resultMap.buyDept.websiteObj.name}</a><br/>
						单价差价:<fmt:formatNumber value="${requestScope.resultMap.buyDept.price - requestScope.resultMap.sellDept.price }" pattern="#.####"/>,
						交易量差:<fmt:formatNumber value="${requestScope.resultMap.buyDept.volnum - requestScope.resultMap.sellDept.volnum}" pattern="#.####"/>
						总差价:
						<fmt:formatNumber value="${requestScope.resultMap.buyDept.price * requestScope.resultMap.sellDept.volnum - requestScope.resultMap.sellDept.price *requestScope.resultMap.buyDept.volnum }" pattern="#.#####"/>
						<form action="${rootpath }/dept.html" method="get">
							<p style="padding: 5px;">
								<input type="hidden" name="cid" value="${param.cid }" size="5"/>
								<input type="checkbox" name="autoflush" id="autoflush" value="1" checked="checked"/>
								<label for="autoflush">自动刷新</label>
								价格差价提醒:
								<input type="text" name="chprice" value="${param.chprice }"/>
								卖的最高价:<input type="text" name="sellprice" value="${param.sellprice }"/>
								买的最低价:<input type="text" name="buyprice" value="${param.buyprice }"/>
								<input type="submit" value="设置"/>
								注:价格差价大于此值时提醒
							</p>
						</form>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<p class="leader">卖列表(要从别人那买,价格从低到高排序)</p>
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>&nbsp;</th>
									<th>币种</th>
									<th>原价&nbsp;*&nbsp;汇率</th>
									<th>价格</th>
									<th>交易量</th>
									<th>合并量</th>
									<th>金额</th>
									<th>网站</th>
									<th>更新时间</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${requestScope.resultMap.deptSellList }" var="dept" varStatus="stat">
									<tr>
										<td>${stat.count }</td>
										<td>${dept.tradeTypeObj.name}</td>
										<td>${dept.souprice}&nbsp;*&nbsp;${dept.rate}</td>
										<td price="sellprice">${dept.price}</td>
										<td>${dept.volnum}</td>
										<td>${dept.mergevol}</td>
										<td>
											<fmt:formatNumber value="${dept.volnum * dept.price}" pattern="##.####"/><br/>
										</td>
										<td><a href="${dept.websiteObj.url}" target="_blank">${dept.websiteObj.name}</a></td>
										<td>
											<fmt:formatDate value="${dept.updatetime}" pattern="yyyy-MM-dd HH:mm:ss"/>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<div class="col-xs-6">
					<p class="leader">买列表(要卖给别人,价格从高到低排序)</p>
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>&nbsp;</th>
									<th>币种</th>
									<th>原价&nbsp;*&nbsp;汇率</th>
									<th>价格</th>
									<th>交易量</th>
									<th>合并量</th>
									<th>金额</th>
									<th>网站</th>
									<th>更新时间</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${requestScope.resultMap.deptBuyList }" var="dept" varStatus="stat">
									<tr>
										<td>${stat.count }</td>
										<td>${dept.tradeTypeObj.name}</td>
										<td>${dept.souprice}&nbsp;*&nbsp;${dept.rate}</td>
										<td price="buyprice">${dept.price}</td>
										<td>${dept.volnum}</td>
										<td>${dept.mergevol}</td>
										<td>
											<fmt:formatNumber value="${dept.volnum * dept.price}" pattern="##.####"/><br/>
										</td>
										<td><a href="${dept.websiteObj.url}" target="_blank">${dept.websiteObj.name}</a></td>
										<td>
											<fmt:formatDate value="${dept.updatetime}" pattern="yyyy-MM-dd HH:mm:ss"/>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
<script type="text/javascript">
	//将买价和卖价相待的时候使用背景色标识
	var buypriceCount = 0 ;
	$("td[price=sellprice]").each(function()
	{
		var sellprice = $(this).html();
		var buypriceObj = $("td[price=buyprice]").eq(buypriceCount) ; 
		var buyprice = $("td[price=buyprice]").eq(buypriceCount).html();
		if(parseFloat(buyprice) > parseFloat(sellprice))
		{
			//alert(buypriceCount + "-->" + sellprice + "-->" + buyprice);
			$(this).parent().attr("class","hreflink");
			buypriceObj.parent().attr("class","hreflink");
		}
		buypriceCount ++ ; 
	});

	var chaPrice = ${requestScope.resultMap.buyDept.price - requestScope.resultMap.sellDept.price } ;
	var timer = 5000 ;
	var remTimer = 5000 ; 
	var timerefreshFlag = "" ; 
	var now = new Date();
	
	/**
		定时刷新网页
	*/
	function timerefresh()
	{
		var currdate = new Date();
		if(chaPrice < 0 || (currdate - now) > timer)
		{
			var url = window.location.href ;
			var ed = url.indexOf("&date="); 
			if(ed != -1)
			{
				//截字符串
				url = url.substring(0 , ed);
			}
			url += "&date=" + new Date().getTime();
			window.location.href = url ;		
		}else
		{
			//window.alert("差价:${requestScope.resultMap.buyDept.price - requestScope.resultMap.sellDept.price },请进行操作");
			document.title.status = "差价:${requestScope.resultMap.buyDept.price - requestScope.resultMap.sellDept.price },请进行操作" ;
			//window.clearInterval(timerefreshFlag);
		}
	}
	
	timerefreshFlag = window.setTimeout(function()
	{
		//必须选中自动刷新
		if($("#autoflush").prop("checked"))
		{
			timerefresh();
		}
	}, timer);
	
	window.setInterval(function ()
	{
		$("#remSec").html(remTimer / 1000);
		remTimer -= 1000 ; 
	}, 1000)
</script>