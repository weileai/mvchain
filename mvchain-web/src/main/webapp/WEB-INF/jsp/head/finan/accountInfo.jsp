<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html>
	<head>
		<title>我的账户信息 - mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="sub-header">
						我的账户信息
						<small>
							<a href="${rootpath }/head/finan/accountList.html">我的账户列表</a>
						</small>
					</h2>
					<form action="${rootpath }/head/finan/accountUpdateSubmit.html" class="form-horizontal" method="post">
						<input type="hidden" name="operType" value="update">
						<input type="hidden" name="accoutId" value="${requestScope.account.id }">
						<div class="form-group">
							<label for="wbid" class="col-sm-2 control-label">邮箱:</label>
							<div class="col-sm-8">
								${requestScope.account.email }
							</div>
						</div>
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label">用户名:</label>
							<div class="col-sm-8">
								${requestScope.account.usersName }
							</div> 
						</div>
						<div class="form-group">
							<label for="pubtimeStr" class="col-sm-2 control-label">发布时间:</label>
							<div class="col-sm-8">
								<fmt:formatDate value="${requestScope.account.pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
							</div> 
						</div>
						<div class="form-group">
							<label for="status0" class="col-sm-2 control-label">账户类型：</label>
							<div class="col-sm-8">
								${requestScope.account.accountTypeStr }
							</div>
						</div>
						<div class="form-group">
							<label for="status0" class="col-sm-2 control-label">状态：</label>
							<div class="col-sm-8">
								${requestScope.account.statusStr }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">描述:</label>
							<div class="col-sm-8" id="authinfo">
								${requestScope.account.content}
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
