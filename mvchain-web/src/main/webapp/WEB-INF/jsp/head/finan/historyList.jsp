<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html lang="zh-CN">
	<head>
		<title>我的历史列表 - mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container main">
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<h2 class="sub-header">
						历史列表
						<small>
							<a href="${rootpath }/head/finan/historyList.html">我的历史列表</a>
						</small>
					</h2>
					<form action="${rootpath}/head/finan/historyList.html" method="post" class="form-inline">
						<div class="form-group">
							<input type="text" name="keyword" value="${requestScope.keyword }"  class="form-control" placeholder="请输入关键字">
							<label for="status">状态:</label>
							<select name="status" class="form-control required">
								<option value="">请选择</option>
								<option value="0" ${requestScope.status == '0' ? 'selected' : '' }>取消</option>
								<option value="1" ${requestScope.status == '1' ? 'selected' : '' }>未处理</option>
								<option value="2" ${requestScope.status == '2' ? 'selected' : '' }>已处理</option>
							</select>
							<label for="wbId">交易网站：</label>
							<select name="wbId" class="form-control required">
								<option value="">请选择</option>
								<c:forEach items="${requestScope.websiteList }" var="website" varStatus="stat">
									<option value="${website.id }" ${requestScope.wbId == website.id ? 'selected' : '' }>${website.name }</option>
								</c:forEach>
							</select>
							<label for="tradeType">交易类型:</label>
							<select name="tradeType" class="form-control required">
								<option value="">请选择</option>
								<option value="1" ${requestScope.tradeType == '1' ? 'selected' : '' }>支出</option>
								<option value="0" ${requestScope.tradeType == '0' ? 'selected' : '' }>收入</option>
							</select>
							<label for="historyType">历史类型:</label>
							<select name="historyType" class="form-control required">
								<option value="">请选择</option>
								<option value="0" ${requestScope.historyType == '0' ? 'selected' : '' }>充值</option>
								<option value="1" ${requestScope.historyType == '1' ? 'selected' : '' }>提现</option>
								<option value="2" ${requestScope.historyType == '2' ? 'selected' : '' }>收益</option>
							</select>
							<label for="email">更新时间:</label>
							<input type="text" name="st" value="${requestScope.st }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" class="form-control"/>
							-->
							<input type="text" name="ed" value="${requestScope.ed }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" class="form-control"/>
							<input type="submit" class="btn btn-primary" value="搜索">
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>序号</th>
									<th>资产id</th>
									<th>网站</th>
									<th>账户id</th>
									<th>任务id</th>
									<th>用户名</th>
									<th>价格</th>
									<th>数量</th>
									<th>金额</th>
									<th title="当前币种的比例">百分比</th>
									<th>交易类型</th>
									<th>历史类型</th>
									<th>状态</th>
									<th>创建时间</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${requestScope.historyList }" var="history" varStatus="stat">
									<tr target="sid_user" rel="${history.id }">
										<td>${stat.count }</td>
										<td>
											<a href="${rootpath}/head/finan/assetUpdate.html?assetId=${history.assetId}" target="_blank">
												${history.finanAsset.coinrate.name }(${history.assetId})
											</a>
										</td>
										<td>${history.website.name}</td>
										<td title="${history.finanAccount.email }">
											<a href="${rootpath}/head/finan/assetUpdate.html?assetId=${history.assetId}" target="_blank">
												${history.accountId}
											</a>
										</td>
										<td>
											<a href="${rootpath }/head/stat/statTaskUpdate.html?stid=${history.taskId}"
									 		target="navTab" ref="taskInfo">
												${history.taskId}
											</a>
										</td>
										<td title="${history.usersName}">${fn:substring(history.usersName,0,5)}</td>
										<td>${history.price}</td>
										<td>${history.num}</td>
										<td>${history.money}</td>
										<td>${history.percent}</td>
										<td>${history.tradeTypeStr}</td>
										<td>${history.historyTypeStr}</td>
										<td>${history.statusStr}</td>
										<td><fmt:formatDate value="${history.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td>
											<a target="_blank" href="${rootpath }/head/finan/historyUpdate.html?historyId=${history.id}&operType=update">修改</a>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						
						<form id="pageFormUsersAccountList" method="post" action="${rootpath }/head/finan/historyList.html">
							<input type="hidden" name="keyword" value="${requestScope.keyword }">
							<input type="hidden" name="status" value="${requestScope.status }">
							<input type="hidden" name="wbId" value="${requestScope.wbId }">
							<input type="hidden" name="tradeType" value="${requestScope.tradeType }">
							<input type="hidden" name="historyType" value="${requestScope.historyType }">
							<input type="hidden" name="st" value="${requestScope.st }">
							<input type="hidden" name="ed" value="${requestScope.ed }">
						
							<nav>
								<ul class="pager">
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','1','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">首页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','${requestScope.pageInfo.prePage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">上一页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','${requestScope.pageInfo.nextPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">下一页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','${requestScope.pageInfo.totalPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">末页</span></a>
									</li>
									<li>
										共${requestScope.pageInfo.totalPage }页
										共${requestScope.pageInfo.totalRecord }条
									</li>
									<li>第<input type="text" id="currentPage" name="currentPage" value="${requestScope.pageInfo.currentPage }" size="5" />页</li>
									<li>每页<input type="text" id="pageSize" name="pageSize" value="${requestScope.pageInfo.pageSize }" size="5"/>条</li>
									<li><input type="submit" value="GO" class="btn btn-primary"/></li>
								</ul>
							</nav>
						</form>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
