<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html lang="zh-CN">
	<head>
		<title>我的账户列表 - mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container main">
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<h2 class="sub-header">
						账户列表
						<small>
							<a href="${rootpath }/head/finan/accountList.html">我的账户列表</a>
							<a href="${rootpath }/head/finan/accountInsert.html" target="_blank">添加</a>
						</small>
					</h2>
					<form action="${rootpath}/head/finan/accountList.html" method="post" class="form-inline">
						<div class="form-group">
							<!-- <label for="keyword">关键字:</label> -->
							<input type="text" name="keyword" value="${requestScope.keyword }"  class="form-control" placeholder="请输入关键字">
							<label for="status">状态:</label>
							<select name="status" class="form-control required">
								<option value="">请选择</option>
								<option value="1" ${requestScope.status == '1' ? 'selected' : '' }>启用</option>
								<option value="0" ${requestScope.status == '0' ? 'selected' : '' }>禁用</option>
							</select>
							<label for="accountType">账户类型：</label>
							<select name="accountType" class="form-control required">
								<option value="">请选择</option>
								<option value="0" ${requestScope.accountType == '0' ? 'selected' : '' }>支付宝</option>
								<option value="1" ${requestScope.accountType == '1' ? 'selected' : '' }>微信</option>
							</select>
							<label for="email">更新时间:</label>
							<input type="text" name="st" value="${requestScope.st }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" class="form-control"/>
							-->
							<input type="text" name="ed" value="${requestScope.ed }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" class="form-control"/>
							<input type="submit" class="btn btn-primary" value="搜索">
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>序号</th>
									<th>邮箱</th>
									<th>用户名</th>
									<th>内容</th>
									<th>账户类型</th>
									<th>状态</th>
									<th>创建时间</th>
									<th>更新时间</th>
									<th>发布时间</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${requestScope.accountList }" var="account" varStatus="stat">
									<tr target="sid_user" rel="${account.id }">
										<td>${stat.count }</td>
										<td>${account.email}</td>
										<td>${account.usersName}</td>
										<td>${account.content}</td>
										<td>${account.accountTypeStr}</td>
										<td>${account.statusStr}</td>
										<td><fmt:formatDate value="${account.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td><fmt:formatDate value="${account.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td><fmt:formatDate value="${account.pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td>
											<a target="_blank" href="${rootpath }/head/finan/accountUpdate.html?accountId=${account.id}">查看</a>
											<a target="_blank" href="${rootpath }/head/finan/accountUpdate.html?accountId=${account.id}&operType=update">修改</a>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						
						<form id="pageFormUsersAccountList" method="post" action="${rootpath }/head/finan/accountList.html">
							<input type="hidden" name="keyword" value="${requestScope.keyword }">
							<input type="hidden" name="status" value="${requestScope.status }">
							<input type="hidden" name="accountType" value="${requestScope.accountType }">
							<input type="hidden" name="st" value="${requestScope.st }">
							<input type="hidden" name="ed" value="${requestScope.ed }">
						
							<nav>
								<ul class="pager">
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','1','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">首页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','${requestScope.pageInfo.prePage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">上一页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','${requestScope.pageInfo.nextPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">下一页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','${requestScope.pageInfo.totalPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">末页</span></a>
									</li>
									<li>
										共${requestScope.pageInfo.totalPage }页
										共${requestScope.pageInfo.totalRecord }条
									</li>
									<li>第<input type="text" id="currentPage" name="currentPage" value="${requestScope.pageInfo.currentPage }" size="5" />页</li>
									<li>每页<input type="text" id="pageSize" name="pageSize" value="${requestScope.pageInfo.pageSize }" size="5"/>条</li>
									<li><input type="submit" value="GO" class="btn btn-primary"/></li>
								</ul>
							</nav>
						</form>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
