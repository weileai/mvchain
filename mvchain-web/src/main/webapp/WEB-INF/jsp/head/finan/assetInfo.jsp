<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html>
	<head>
		<title>我的信息 - mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="sub-header">
						我的资产
						<small>
							<a href="${rootpath }/head/finan/assetUpdate.html?assetId=${requestScope.asset.id}">我的资产</a>
						</small>
					</h2>
					<form action="${rootpath }/head/users/usersAccountInsertSubmit.html" class="form-horizontal" method="post">
						<input type="hidden" name="operType" value="update">
						<div class="form-group">
							<label for="wbid" class="col-sm-2 control-label">邮箱:</label>
							<div class="col-sm-8">
								${requestScope.asset.users.email}
							</div>
						</div>
						<div class="form-group">
							<label for="wbid" class="col-sm-2 control-label">币种:</label>
							<div class="col-sm-8">
								${requestScope.asset.coinrate.name}
							</div>
						</div>
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label">余额：</label>
							<div class="col-sm-8">
								<fmt:formatNumber value="${requestScope.asset.balance }" pattern="#.###"/>
							</div> 
						</div>
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label">usd余额：</label>
							<div class="col-sm-8">
								<fmt:formatNumber value="${requestScope.asset.usdBalance }" pattern="#.###"/>
							</div> 
						</div>
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label">百分比：</label>
							<div class="col-sm-8">
								<fmt:formatNumber value="${requestScope.asset.percent }" pattern="#.##%"/>
							</div> 
						</div>
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label">今日收益：</label>
							<div class="col-sm-8">
								<fmt:formatNumber value="${requestScope.asset.todayPrift }" pattern="#.###"/>
							</div> 
						</div>
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label">总收益：</label>
							<div class="col-sm-8">
								<fmt:formatNumber value="${requestScope.asset.totalPrift }" pattern="#.###"/>
							</div> 
						</div>
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label">总收益(CNY)：</label>
							<div class="col-sm-8">
								<fmt:formatNumber value="${requestScope.asset.cnyPrift }" pattern="#.###"/>
							</div> 
						</div>
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label">状态：</label>
							<div class="col-sm-8">
								${requestScope.asset.statusStr }
							</div> 
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">创建时间：</label>
							<div class="col-sm-8">
								<fmt:formatDate value="${requestScope.asset.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">更新时间：</label>
							<div class="col-sm-8">
								<fmt:formatDate value="${requestScope.asset.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/>
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">发布时间：</label>
							<div class="col-sm-8">
								<fmt:formatDate value="${requestScope.asset.pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
