<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html lang="zh-CN">
	<head>
		<title>我的资产列表 - mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container main">
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<h2 class="sub-header">
						资产列表
						<small>
							<a href="${rootpath }/head/finan/assetList.html">我的资产列表</a>
						</small>
					</h2>
					<form action="${rootpath}/head/finan/assetList.html" method="post" class="form-inline">
						<div class="form-group">
							<!-- <label for="keyword">关键字:</label> -->
							<input type="text" name="keyword" value="${requestScope.keyword }"  class="form-control" placeholder="请输入关键字">
							<label for="coinid">币种：</label>
							<select name="coinid" class="form-control required">
								<option value="">请选择</option>
								<c:forEach items="${requestScope.coinrateList }" var="coinrate" varStatus="stat">
									<option value="${coinrate.id }" ${requestScope.coinid == coinrate.id ? 'selected' : '' }>${coinrate.name }</option>
								</c:forEach>
							</select>
							<label for="email">更新时间:</label>
							<input type="text" name="st" value="${requestScope.st }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" class="form-control"/>
							-->
							<input type="text" name="ed" value="${requestScope.ed }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" class="form-control"/>
							<input type="submit" class="btn btn-primary" value="搜索">
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>序号</th>
									<th>币种</th>
									<th>余额</th>
									<th>usd金额</th>
									<th>百分比</th>
									<th>总收益</th>
									<th>今天收益</th>
									<th>RMB收益</th>
									<th>创建时间</th>
									<th>更新时间</th>
									<th>发布时间</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody>
								<c:set value="0" var="totalPercent"/>
								<c:set value="0" var="totalUsdBalance"/>
								<c:forEach items="${requestScope.assetList }" var="asset" varStatus="stat">
									<c:set value="${totalPercent + asset.percent }" var="totalPercent"/>
									<c:set value="${totalUsdBalance + asset.usdBalance }" var="totalUsdBalance"/>
									<tr target="sid_user" rel="${asset.id }">
										<td>${stat.count }</td>
										<td>${asset.coinrate.name }</td>
										<td><fmt:formatNumber value="${asset.balance }" pattern="#.###"/></td>
										<td><fmt:formatNumber value="${asset.usdBalance }" pattern="#.###"/></td>
										<td><fmt:formatNumber value="${asset.percent }" pattern="#.###%"/></td>
										<td><fmt:formatNumber value="${asset.totalPrift }" pattern="#.###"/></td>
										<td><fmt:formatNumber value="${asset.todayPrift }" pattern="#.###"/></td>
										<td><fmt:formatNumber value="${asset.cnyPrift }" pattern="#.###"/></td>
										<td><fmt:formatDate value="${asset.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td><fmt:formatDate value="${asset.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td><fmt:formatDate value="${asset.pubtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td>
											<a target="_blank" href="${rootpath }/head/finan/assetUpdate.html?assetId=${asset.id}">查看</a>
											<a target="_blank" href="${rootpath }/head/finan/historyList.html?assetId=${asset.id}">历史列表</a>
											<br/>
											<a target="_blank" href="${rootpath }/head/finan/historyInsert.html?assetId=${asset.id}&">充值/提现</a>
										</td>
									</tr>
								</c:forEach>
								<tr target="sid_user" rel="${asset.id }">
									<td></td>
									<td></td>
									<td></td>
									<td><fmt:formatNumber value="${totalUsdBalance }" pattern="#.###"/></td>
									<td><fmt:formatNumber value="${totalPercent }" pattern="#.###%"/></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td>
									</td>
								</tr>
							</tbody>
						</table>
						
						<form id="pageFormUsersAccountList" method="post" action="${rootpath }/head/finan/assetList.html">
							<input type="hidden" name="keyword" value="${requestScope.keyword }">
							<input type="hidden" name="coinid" value="${requestScope.coinid }">
							<input type="hidden" name="st" value="${requestScope.st }">
							<input type="hidden" name="ed" value="${requestScope.ed }">
						
							<nav>
								<ul class="pager">
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','1','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">首页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','${requestScope.pageInfo.prePage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">上一页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','${requestScope.pageInfo.nextPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">下一页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormUsersAccountList','${requestScope.pageInfo.totalPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">末页</span></a>
									</li>
									<li>
										共${requestScope.pageInfo.totalPage }页
										共${requestScope.pageInfo.totalRecord }条
									</li>
									<li>第<input type="text" id="currentPage" name="currentPage" value="${requestScope.pageInfo.currentPage }" size="5" />页</li>
									<li>每页<input type="text" id="pageSize" name="pageSize" value="${requestScope.pageInfo.pageSize }" size="5"/>条</li>
									<li><input type="submit" value="GO" class="btn btn-primary"/></li>
								</ul>
							</nav>
						</form>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
