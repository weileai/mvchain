<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html>
	<head>
		<title>修改历史- mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="sub-header">
						修改历史
						<small>
							<a href="${rootpath}/head/finan/historyList.html">历史列表</a>
						</small>
					</h2>
					<form action="${rootpath }/head/finan/historyUpdateSubmit.html" class="form-horizontal" method="post">
						<input type="hidden" name="operType" value="update">
						<input type="hidden" name="historyId" value="${requestScope.history.id }">
						<div class="form-group">
							<label for="wbid" class="col-sm-2 control-label">资产id:</label>
							<div class="col-sm-8">
								<a target="_blank" href="${rootpath }/head/finan/assetUpdate.html?assetId=${requestScope.history.assetId }">查看(${requestScope.history.assetId })</a>
							</div>
						</div>
						<div class="form-group">
							<label for="wbId" class="col-sm-2 control-label">交易网站:</label>
							<div class="col-sm-8">
								<select name="wbId">
									<option value="0">请选择</option>
									<c:forEach items="${requestScope.websiteList }" var="website" varStatus="stat">
										<option value="${website.id }" ${requestScope.history.wbId == website.id ? 'selected':'' }>${website.name }</option>
									</c:forEach>
								</select>
							</div> 
						</div>
						<div class="form-group">
							<label for="accountId" class="col-sm-2 control-label">账户:</label>
							<div class="col-sm-8">
								<select name="accountId">
									<c:forEach items="${requestScope.accountList }" var="account" varStatus="stat">
										<option value="${account.id }" ${requestScope.history.accountId == account.id ? 'selected':'' }>${account.email }</option>
									</c:forEach>
								</select>
							</div> 
						</div>
						<div class="form-group">
							<label for="usersName" class="col-sm-2 control-label">用户名:</label>
							<div class="col-sm-8">
								<input type="text" id="usersName" name="usersName" class="form-control" value="${requestScope.history.usersName }" 
									size="50"/>
							</div> 
						</div>
						<div class="form-group">
							<label for="price" class="col-sm-2 control-label">价格:</label>
							<div class="col-sm-8">
								${requestScope.price }
							</div> 
						</div>
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label">数量:</label>
							<div class="col-sm-8">
								<input type="text" id="num" name="num" class="form-control" value="${requestScope.history.num }" 
									size="50"/>
							</div> 
						</div>
						<div class="form-group">
							<label for="pubtimeStr" class="col-sm-2 control-label">发布时间:</label>
							<div class="col-sm-8">
								<input type="text" name="pubtimeStr" value="<fmt:formatDate value="${sys_now }" pattern="yyyy-MM-dd HH:mm:ss"/>" 
								onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" 
								readonly="true" class="form-control"/>
							</div> 
						</div>
						<div class="form-group">
							<label for="status0" class="col-sm-2 control-label">状态：</label>
							<div class="col-sm-8">
								<input type="radio" id="tradeType0" name="tradeType" value="0" checked="checked">
								<label for="tradeType0">收入(充值)&nbsp;&nbsp;&nbsp;</label>
								<input type="radio" id="tradeType1" name="tradeType" value="1">
								<label for="tradeType1">支出(提现)&nbsp;&nbsp;&nbsp;</label>
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-sm-2 control-label">备注:</label>
							<div class="col-sm-8" id="content">
								<textarea rows="5" cols="40" class="form-control" name="content">${requestScope.history.content}</textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-2">
								<button class="btn btn-lg btn-primary" type="submit">提交</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
