<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html>
	<head>
		<title>添加账户- mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-1 main">
					<h2 class="sub-header">
						添加账户
						<small>
							<a href="${rootpath}/head/finan/accountList.html">账户列表</a>
						</small>
					</h2>
					<form action="${rootpath }/head/finan/accountInsertSubmit.html" class="form-horizontal" method="post">
						<input type="hidden" name="operType" value="update">
						<div class="form-group">
							<label for="wbid" class="col-sm-2 control-label">邮箱:</label>
							<div class="col-sm-8">
								<input type="text" id="email" name="email" class="form-control" value="${requestScope.account.email }" 
								size="50"/>
							</div>
						</div>
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label">用户名:</label>
							<div class="col-sm-8">
								<input type="text" id="usersName" name="usersName" class="form-control" value="${requestScope.account.usersName }" 
									size="50"/>
							</div> 
						</div>
						<div class="form-group">
							<label for="pubtimeStr" class="col-sm-2 control-label">发布时间:</label>
							<div class="col-sm-8">
								<input type="text" name="pubtimeStr" value="<fmt:formatDate value="${sys_now }" pattern="yyyy-MM-dd HH:mm:ss"/>" 
								onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" 
								readonly="true" class="form-control"/>
							</div> 
						</div>
						<div class="form-group">
							<label for="status0" class="col-sm-2 control-label">账户类型：</label>
							<div class="col-sm-8">
								<input type="radio" id="accountType1" name="accountType" value="1" checked="checked">
								<label for="accountType1">微信&nbsp;&nbsp;&nbsp;</label>
								<input type="radio" id="accountType0" name="accountType" value="0">
								<label for="accountType0">支付宝&nbsp;&nbsp;&nbsp;</label>
								<%--
								<input type="radio" id="status1" name="status" value="3" ${requestScope.usersTask.status == '3' ? 'checked' : '' }>
								<label for="status1">计算收益&nbsp;&nbsp;&nbsp;</label>
								 --%>
							</div>
						</div>
						<div class="form-group">
							<label for="status0" class="col-sm-2 control-label">状态：</label>
							<div class="col-sm-8">
								<input type="radio" id="status1" name="status" value="1" checked="checked">
								<label for="status1">启用&nbsp;&nbsp;&nbsp;</label>
								<input type="radio" id="status0" name="status" value="0">
								<label for="status0">禁用&nbsp;&nbsp;&nbsp;</label>
								<%--
								<input type="radio" id="status1" name="status" value="3" ${requestScope.usersTask.status == '3' ? 'checked' : '' }>
								<label for="status1">计算收益&nbsp;&nbsp;&nbsp;</label>
								 --%>
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">描述:</label>
							<div class="col-sm-8" id="authinfo">
								<textarea rows="5" cols="40" class="form-control" name="content">${requestScope.account.content}</textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-2">
								<button class="btn btn-lg btn-primary" type="submit">提交</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
