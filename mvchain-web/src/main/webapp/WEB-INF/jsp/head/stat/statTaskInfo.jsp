<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html>
	<head>
		<title>任务统计详情 - mvchain</title>
		<%@ include file="/common/include/title.jsp"%>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container">
			<div class="row">
				<div class="col-md-offset-1 main">
					<h2 class="sub-header">
						任务统计详情
						<small>
							<a href="${rootpath }/head/stat/statTaskList.html">任务统计列表</a>
						</small>
					</h2>
					<form class="form-horizontal">
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">用户名:</label>
							<div class="col-sm-8">
								${requestScope.statTask.usersObj.email }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">日期:</label>
							<div class="col-sm-8">
								<fmt:formatDate value="${requestScope.statTask.currdate }" pattern="yyyy-MM-dd"/>
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">总收益:</label>
							<div class="col-sm-8">
								${requestScope.statTask.prift }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">手续费</label>
							<div class="col-sm-8">
								${requestScope.statTask.fee }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">任务数:</label>
							<div class="col-sm-8">
								${requestScope.statTask.taskcount }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">卖最低价:</label>
							<div class="col-sm-8">
								${requestScope.statTask.sellpricemin }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">卖最高价:</label>
							<div class="col-sm-8">
								${requestScope.statTask.sellpricemax }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">买最低价:</label>
							<div class="col-sm-8">
								${requestScope.statTask.buypricemin }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">买最高价:</label>
							<div class="col-sm-8">
								${requestScope.statTask.buypricemax }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">快照:</label>
							<div class="col-sm-8">
								${requestScope.statTask.flag }
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">创建时间:</label>
							<div class="col-sm-8">
								<fmt:formatDate value="${requestScope.statTask.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/>
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">更新时间:</label>
							<div class="col-sm-8">
								<fmt:formatDate value="${requestScope.statTask.updatetime }" pattern="yyyy-MM-dd HH:mm:ss"/>
							</div>
						</div>
					</form>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-offset-1 main">
					<h2 class="sub-header">余额明细</h2>
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>序号</th>
									<th>币种</th>
									<th>今天余额</th>
									<th>今天冻结金额</th>
									<th>今天折合cny</th>
									<th>余额</th>
									<th>冻结金额</th>
									<th>折合cny</th>
									<th>余额差价(今-当)</th>
									<th>冻结差价(今-当)</th>
								</tr>
							</thead>
							<tbody>
								<c:set value="0" var="totalPrice"/>
								<c:forEach items="${requestScope.accountList }" var="map" varStatus="stat">
									<c:set value="coinid_${map.coinid}" var="statKey"/>
									<tr>
										<td>${stat.count}</td>
										<td>${map.coinrate.name}</td>
										<td><fmt:formatNumber value="${map.balance}" pattern="#,####.####"/></td>
										<td><fmt:formatNumber value="${map.fundmoney}" pattern="#,####.####"/></td>
										<td>
											<c:choose>
												<c:when test="${map.coinid == '2'}">
													<%-- 美元 --%>
													<c:set value="${totalPrice + map.balance * requestScope.usdRate }" var="totalPrice"/>
													<fmt:formatNumber value="${map.balance * requestScope.usdRate}" pattern="#,####.####"
													var="usdprice"/>
													${usdprice}
												</c:when>
												<c:when test="${map.coinid == '3'}">
													<fmt:formatNumber value="${map.balance}" pattern="#,####.####"/>
													<c:set value="${totalPrice + map.balance }" var="totalPrice"/>
												</c:when>
												<c:otherwise>
													0
												</c:otherwise>
											</c:choose>
										</td>
										<td><fmt:formatNumber value="${requestScope.statTask.balanceJSON[statKey].balance}" pattern="#,####.####"/></td>
										<td><fmt:formatNumber value="${requestScope.statTask.balanceJSON[statKey].fundmoney}" pattern="#,####.####"/></td>
										<td>
											<c:choose>
												<c:when test="${map.coinid == '2'}">
													<%-- 美元 --%>
													<fmt:formatNumber value="${requestScope.statTask.balanceJSON[statKey].balance * requestScope.usdRate}" pattern="#,####.####"
													var="usdprice"/>
													${usdprice}
												</c:when>
												<c:when test="${map.coinid == '3'}">
													<fmt:formatNumber value="${requestScope.statTask.balanceJSON[statKey].balance}" pattern="#,####.####"/>
												</c:when>
												<c:otherwise>
													0
												</c:otherwise>
											</c:choose>
										</td>
										<td><fmt:formatNumber value="${map.balance - requestScope.statTask.balanceJSON[statKey].balance}" pattern="#,####.####"/></td>
										<td><fmt:formatNumber value="${map.fundmoney - requestScope.statTask.balanceJSON[statKey].fundmoney}" pattern="#,####.####"/></td>
									</tr>
								</c:forEach>
								<%-- <c:forEach items="${requestScope.statTask.balanceJSON }" var="me" varStatus="stat">
									<c:if test="${fn:startsWith(me.key,'coinid_')}">
										<tr target="sid_user" rel="${stat.count }">
											<td>${stat.count }</td>
											<td>${me.value.name }</td>
											<td>${me.value.balance}</td>
											<td>${me.value.fundmoney}</td>
											<td>${me.value.cny}</td>
										</tr>
									</c:if>
								</c:forEach>
								<tr target="sid_user" rel="${stat.count }">
									<td>合计:</td>
									<td></td>
									<td></td>
									<td></td>
									<td>${requestScope.statTask.balanceJSON.total_cny_balance }</td>
								</tr> --%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
