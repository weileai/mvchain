<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:useBean id="statTaskPage" class="com.wang.mvchain.stat.pojo.AStatTask"/>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html lang="zh-CN">
	<head>
		<title>我的任务统计列表 - mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<%@ include file="/common/include/title.jsp"%>
		<script type="text/javascript">
			$(function () {
			    $('#chart_container_profit').highcharts({
			        chart: {
			            type: 'spline'
			        },
			        title: {
			            text: '任务统计'
			        },
			        subtitle: {
			            text: '条件:${requestScope.st }-->${requestScope.ed }'
			        },
			        xAxis: {
			            categories: [
						<c:forEach items="${requestScope.statTasksReverList }" var="statTask" varStatus="stat">
							'<fmt:formatDate value="${statTask.currdate }" pattern="yyyy-MM-dd"/>'
							${stat.last ? '' : ','}
						</c:forEach>
			           	]
			        },
			        yAxis: {
			            title: {
			                text: '金额'
			            },
			            labels: {
			                formatter: function () {
			                    return this.value + '元';
			                }
			            }
			        },
			        tooltip: {
			            crosshairs: true,
			            shared: true
			        },
			        plotOptions: {
			            spline: {
			                marker: {
			                    radius: 4,
			                    lineColor: '#666666',
			                    lineWidth: 1
			                }
			            }
			        },
			        series: [{
			            name: '总金额',
			            marker: {
			                symbol: 'square'
			            },
			            data: [
						<c:forEach items="${requestScope.statTasksReverList }" var="statTask" varStatus="stat">
							${statTask.prift }
							${stat.last ? '' : ','}
						</c:forEach>
			            ]
			            <%-- 
			            data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, {
			                y: 26.5,
			                marker: {
			                    symbol: 'url(${rootpath}/images/sun.png)'
			                }
			            }, 23.3, 18.3, 13.9, 9.6]
			           --%>
			
			        }, {
			            name: '手续费',
			            marker: {
			                symbol: 'diamond'
			            },
			            data: [
						<c:forEach items="${requestScope.statTasksReverList }" var="statTask" varStatus="stat">
							${statTask.fee }
							${stat.last ? '' : ','}
						</c:forEach>
			            ]
			            <%-- 
			            data: [{
			                y: 3.9,
			                marker: {
			                    symbol: 'url(${rootpath}/images/snow.png)'
			                }
			            }, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
			            --%>
			        }]
			    });
			});
		</script>
		<script src="${rootpath }/common/resource/Highcharts/js/highcharts.js"></script>
		<script src="${rootpath }/common/resource/Highcharts/js/modules/exporting.js"></script>
	</head>
	<body>
		<%@ include file="/common/include/header.jsp"%>
		<div class="container main">
			<div class="row">
				<div class="list-top">
					<h2 class="sub-header user-h2 fl">
						<i class="fa fa-align-justify list"></i>&nbsp;&nbsp;
						<a href="${rootpath }/head/stat/statTaskList.html?menuType=3">任务统计列表</a>
					</h2>
				</div>
				<div class="table-responsive" style="padding: 0 20px">
					<a href="#" onclick="$('#chart_container_profit').show();return false;">显示图表</a>
					<a href="#" onclick="$('#chart_container_profit').hide();return false;">隐藏图表</a>
					<div id="chart_container_profit" style="width: 98%;height: 430px; padding: 0px; margin: 10px;"></div>
				</div>	
			</div>
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<h2 class="sub-header">
						<small>
							<a href="${rootpath }/head/stat/statTaskList.html?menuType=3">任务统计列表</a>
						</small>
					</h2>
					<form action="${rootpath}/head/stat/statTaskList.html?menuType=3" method="post" class="form-inline">
						<div class="form-group">
							<label for="email">标识:</label>
							<select name="flag" class="form-control">
								<option value="">请选择</option>
								<c:forEach items="${statTaskPage.flagMap }" var="map" varStatus="stat">
									<option value="${map.value}" ${requestScope.flag == map.value ? 'selected' : '' }>${map.key }</option>
								</c:forEach>
							</select>
							<label for="email">时间:</label>
							<input type="text" name="st" value="${requestScope.st }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" class="form-control"/>
							-->
							<input type="text" name="ed" value="${requestScope.ed }" onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" readonly="true" class="form-control"/>
							<input type="submit" class="btn btn-primary" value="搜索">
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-11 col-sm-12">
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>序号</th>
									<th>用户名</th>
									<th>日期</th>
									<th>总收益</th>
									<th>手续费</th>
									<th>任务数</th>
									<th>卖最低价</th>
									<th>卖最高价</th>
									<th>买最低价</th>
									<th>买最高价</th>
									<th>标识</th>
									<th>创建时间</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody>
									<c:set value="0" var="totalprift"/>
									<c:set value="0" var="totalfee"/>
									<c:set value="0" var="totaltaskcount"/>
									<c:forEach items="${requestScope.statTaskList }" var="statTask" varStatus="stat">
										<tr target="sid_user" rel="${statTask.id }">
											<td>${stat.count }</td>
											<td title="${statTask.usersObj.email }">${fn:substring(statTask.usersObj.email,0,5) }</td>
											<td><fmt:formatDate value="${statTask.currdate }" pattern="yyyy-MM-dd"/></td>
											<td>${statTask.prift }</td>
											<td>${statTask.fee}</td>
											<td>${statTask.taskcount }</td>
											<td>${statTask.sellpricemin }</td>
											<td>${statTask.sellpricemax }</td>
											<td>${statTask.buypricemin }</td>
											<td>${statTask.buypricemax }</td>
											<td>${statTask.flagStr }</td>
											<td><fmt:formatDate value="${statTask.createtime }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
											<td><a href="${rootpath }/head/stat/statTaskUpdate.html?stid=${statTask.id}" target="_blank">查看</a></td>
										</tr>
										
										<c:set value="${totalprift + statTask.prift }" var="totalprift"/>
										<c:set value="${totalfee + statTask.fee }" var="totalfee"/>
										<c:set value="${totaltaskcount + statTask.taskcount  }" var="totaltaskcount"/>
									</c:forEach>
									<tr>
										<td colspan="2">合计</td>
										<td></td>
										<td><fmt:formatNumber value="${totalprift }" pattern="#,####.####"/></td>
										<td><fmt:formatNumber value="${totalfee}" pattern="#,####.####"/></td>
										<td>${totaltaskcount }</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
								</tbody>
						</table>
						
						<form id="pageFormStatTaskList" action="${rootpath }/head/stat/statTaskList.html?menuType=3" method="post">
							<input type="hidden" name="st" value="${requestScope.st }">
							<input type="hidden" name="ed" value="${requestScope.ed }">
							<input type="hidden" name="flag" value="${requestScope.flag }">
						
							<nav>
								<ul class="pager">
									<li>
										<a onclick="return pageFormSubmit('pageFormStatTaskList','1','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">首页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormStatTaskList','${requestScope.pageInfo.prePage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">上一页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormStatTaskList','${requestScope.pageInfo.nextPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">下一页</span></a>
									</li>
									<li>
										<a onclick="return pageFormSubmit('pageFormStatTaskList','${requestScope.pageInfo.totalPage}','currentPage','${requestScope.pageInfo.pageSize}','pageSize')" href="#">末页</span></a>
									</li>
									<li>
										共${requestScope.pageInfo.totalPage }页
										共${requestScope.pageInfo.totalRecord }条
									</li>
									<li>第<input type="text" id="currentPage" name="currentPage" value="${requestScope.pageInfo.currentPage }" size="5" />页</li>
									<li>每页<input type="text" id="pageSize" name="pageSize" value="${requestScope.pageInfo.pageSize }" size="5"/>条</li>
									<li><input type="submit" value="GO" class="btn btn-primary"/></li>
								</ul>
							</nav>
						</form>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
