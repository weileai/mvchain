<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/include/page.jsp"%>
<!DOCTYPE HTML>
<html lang="zh-CN">
	<head>
		<title>我的任务统计列表 - mvchain</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<%@ include file="/common/include/title.jsp"%>
		<script src="${rootpath }/lib/charts/highcharts.js" type="text/javascript"></script>
		<script type="text/javascript">
		var profitData = ${requestScope.profitArray };
		var feeData = ${requestScope.feeArray };
		var g_chart_profit;
		jQuery(function(){
			if (g_chart_profit != undefined) {
				delete g_chart_profit;
			}
			Highcharts.setOptions({
				global : {
					timezoneOffset : 8,
				}
			});
			g_chart_profit = new Highcharts.Chart({
				chart : {
					renderTo : 'chart_container_profit',
					type : 'spline',
					zoomType : 'x',
					animation : false,
				},
				credits : {
					enabled : false
				},
				tooltip : {
					formatter : function() {
						return '<b>' + this.series.name + '</b><br/>'
								+ Highcharts.dateFormat('%b %d, %I:%M %p', this.x) + ': '
								+ this.y + '%';
					},
					positioner : function() {
						return {
							x : 720,
							y : 10
						};
					}
				},
				title : {
					text : 'Profit/Fee 图表'
				},
				xAxis : {
					title : {
						text : "Date/Time"
					},
					type : 'datetime',
					tickPixelInterval : 150
				},
				yAxis : {
					title : {
						text : ''
					},
					plotLines : []
				},
				plotOptions : {
					spline : {
						marker : {
							enabled : true,
							symbol : 'circle',
							radius : 2
						},
						animation : false,
						states : {
							hover : {
								marker : {
									enabled : true,
								},
								lineWidth : 2,
							}
						}
					}
				},
				series : [ {
					name : 'Profit',
					turboThreshold : 100,
					data:profitData
				}, {
					name : 'Fee',
					turboThreshold : 100,
					data:feeData
				} ]
			});			
		});
		</script>
	</head>
	
	<body style="background: #f1f2f4">
		<%@ include file="/common/include/header.jsp"%>
		<div class="container" style="padding-top: 62px;">
			<div class="row main">
				<div class="" style="padding: 0">
					<div class="list-top">
						<h2 class="sub-header user-h2 fl">
							<i class="fa fa-align-justify list"></i>&nbsp;&nbsp;
							<a href="${rootpath }/head/stat/statTaskListCharts.html?menuType=3">任务统计列表</a>
						</h2>
					</div>
					<div class="table-responsive" style="padding: 0 20px">
						<div id="chart_container_profit" style="width: 98%;height: 430px; padding: 0px; margin: 10px;"></div>
					</div>
				</div>
			</div>
		</div>
		<%@ include file="/common/include/footer.jsp"%>
	</body>
</html>
