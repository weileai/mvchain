package com.wang.mvchain.web.springmvc.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.controller.BaseController;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.common.util.PageInfoUtil;
import com.wang.mvchain.outer.IOutTimerService;
import com.wang.mvchain.outer.service.ITradeService;
import com.wang.mvchain.outer.util.TradeUtil;
import com.wang.mvchain.system.pojo.ACoinrate;
import com.wang.mvchain.system.pojo.ATradeType;
import com.wang.mvchain.system.pojo.ATradeTypeEnum;
import com.wang.mvchain.system.pojo.AWebsite;
import com.wang.mvchain.system.service.IWebsiteService;
import com.wang.mvchain.users.pojo.ATradeHistory;
import com.wang.mvchain.users.pojo.ATradeHistoryEnum;
import com.wang.mvchain.users.pojo.AUsersAccount;
import com.wang.mvchain.users.pojo.AUsersAccountEnum;
import com.wang.mvchain.users.pojo.AUsersTask;
import com.wang.mvchain.users.pojo.AUsersTaskOrder;
import com.wang.mvchain.users.pojo.AUsersTaskOrderEnum;
import com.wang.mvchain.users.service.IOrdersService;
import com.wang.mvchain.users.service.IUsersService;

/**
 * 对外交易订单的controller
 * 
 * @author Administrator
 * 
 */
@Controller
@RequestMapping("/head/orders/")
public class TradeOrdersHeadController extends BaseController
{
	@Resource
	private IUsersService usersService;
	@Resource
	private IOrdersService ordersService;
	@Resource
	private IWebsiteService websiteService;
	@Resource
	private IOutTimerService outTimerService;

	/**
	 * 查询某网站的挂单列表
	 * 
	 * @return
	 */
	@RequestMapping("/ordersList.html")
	public String ordersList(HttpServletRequest request, HttpServletResponse response)
	{
		//交易账户id
		String uaid = request.getParameter("uaid");
		Map<String, Object> condMap = new HashMap<String,Object>();
		condMap.put("id", uaid);
		AUsersAccount usersAccount = this.usersService.findOneUsersAccountService(condMap);
		
		String moneytype = request.getParameter("moneytype");
		if(moneytype == null)
		{
			moneytype = "" ; 
		}
		
		if(!"".equalsIgnoreCase(moneytype))
		{
			//调试接口,获取当前所有的挂单
			ITradeService tradeService = TradeUtil.tradeServiceMap.get(usersAccount.getWbid() + "");
			tradeService.setAuthJSON(usersAccount.getAuthinfoJSON());
			Map<String, String> paramsMap = new HashMap<String,String>(); 
			//设置交易类型
			paramsMap.put("moneytype", moneytype);
			JSONObject resultJSON = tradeService.getOrders(paramsMap);
			//存储条件
			request.setAttribute("resultJSON", resultJSON);
		}
		request.setAttribute("usersAccount", usersAccount);
		request.setAttribute("moneytype", moneytype);
		return "/head/orders/ordersList";
	}
	
	/**
	 * 查询所有网站的挂单列表
	 * @return
	 */
	@RequestMapping("/ordersAllList.html")
	public String ordersAllList(HttpServletRequest request, HttpServletResponse response)
	{
		/* 最终存储的结果 */
		JSONObject resultJSON = new JSONObject(); 
		JSONObject dataRes = new JSONObject() ; 
		dataRes.put("ordersList", new JSONArray());
		resultJSON.put("data", dataRes);
		
		/* 查询所有的状态为启用的交易类型 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("status", AUsersAccountEnum.STATUS_ENABLE.getStatus());
		List<AUsersAccount> usersAccountList = this.usersService.findCondListUsersAccountService(null, condMap);
		for (Iterator iterator = usersAccountList.iterator(); iterator.hasNext();)
		{
			AUsersAccount usersAccount = (AUsersAccount) iterator.next();
			/* 币种为rmb的忽略处理 */
			if(usersAccount.getCoinid() != Integer.valueOf(ConstatFinalUtil.CONFIG_JSON.get("CNY_ID") + "") 
					&& 
				usersAccount.getCoinid() != Integer.valueOf(ConstatFinalUtil.CONFIG_JSON.get("USD_ID") + "")
					)
			{
				/* 查询所有的挂单信息 */
				AWebsite website = usersAccount.getWebsiteObj();
				ACoinrate coinrate = usersAccount.getCoinrateObj() ;
				for (Iterator iterator2 = website.getMoneyStatusJSON().entrySet().iterator(); iterator2.hasNext();)
				{
					Entry me = (Entry) iterator2.next() ;
					JSONObject moneyJSON = (JSONObject) me.getValue();
					/* 取得moneyType */
					String moneytype = moneyJSON.getString("name");
					/* 必须币种一样 */
					if(moneytype.toLowerCase().indexOf(coinrate.getEngname().toLowerCase()) == -1)
					{
						/* 必须网站中包含的名称和账户对应的币种一致 */
						continue ; 
					}
					
					//调试接口,获取当前所有的挂单
					ITradeService tradeService = TradeUtil.tradeServiceMap.get(usersAccount.getWbid() + "");
					tradeService.setAuthJSON(usersAccount.getAuthinfoJSON());
					Map<String, String> paramsMap = new HashMap<String,String>(); 
					//设置交易类型
					paramsMap.put("moneytype", moneytype);
					JSONObject resultTempJSON = tradeService.getOrders(paramsMap);
					
					/* 将第二次查询的结果放到第一次查询的结果中 */
					JSONObject dataJSON = resultJSON.getJSONObject("data");
					JSONObject dataTempJSON = resultTempJSON.getJSONObject("data");
					if(dataTempJSON != null)
					{
						JSONArray ordersList = dataJSON.getJSONArray("ordersList");
						JSONArray ordersTempList = dataTempJSON.getJSONArray("ordersList");
						/* 为订单结果增加其它的一些类型 */
						for (Iterator iterator3 = ordersTempList.iterator(); iterator3.hasNext();)
						{
							JSONObject ordersJSON = (JSONObject) iterator3.next();
							ordersJSON.put("uaid", usersAccount.getId() + "");
							ordersJSON.put("websiteName", website.getName() + "");
							ordersJSON.put("moneytype", moneytype + "");
						}
						/* 将最新查询的结果存储到目标结果中 */
						ordersList.addAll(ordersTempList);
					}
					ConstatFinalUtil.SYS_LOG.info("正在查询挂单;交易账户类型id:{},网站id:{},类型:{}",usersAccount.getId() ,website.getName(), moneytype);
				}
			}
		}
		//存储条件
		request.setAttribute("resultJSON", resultJSON);
		return "/head/orders/ordersAllList";
	}
	
	/**
	 * 下单交易
	 * 
	 * @return
	 */
	@RequestMapping("/ordersTrade.html")
	public String ordersTrade(HttpServletRequest request, HttpServletResponse response,
			String utid)
	{
		//交易账户id
		String uaid = request.getParameter("uaid");
		Map<String, Object> condMap = new HashMap<String,Object>();
		condMap.put("id", uaid);
		AUsersAccount usersAccount = this.usersService.findOneUsersAccountService(condMap);
		request.setAttribute("usersAccount", usersAccount);
		
		/* 处理订单 */
		if(utid == null)
		{
			utid = "" ; 
		}
		if(!"".equalsIgnoreCase(utid))
		{
			condMap.clear();
			condMap.put("id", utid);
			AUsersTask usersTask = this.ordersService.findOneUsersTaskService(condMap);
			if(usersTask != null)
			{
				/* 买 */
				double totalBuyVal = this.doubleOperUtil.sum(usersTask.getTotalnum(), usersTask.getFeenum());
				totalBuyVal = this.doubleOperUtil.sub(totalBuyVal, usersTask.getBuynum());
				//卖
				double totalSellVal = usersTask.getTotalnum() - usersTask.getSellnum();
				/* 买的数量和卖的数量,谁大就取谁 */
				double amount = totalBuyVal ; 
				if(totalBuyVal < totalSellVal)
				{
					amount = totalSellVal ; 
				}
				request.setAttribute("amount", amount);
			}
			request.setAttribute("usersTask", usersTask);
		}
		return "/head/orders/ordersTrade";
	}
	
	/**
	 * 下单交易提交
	 * 
	 * @param utid 用户订单的任务id
	 * @return
	 */
	@RequestMapping("/ordersTradeSubmit.html")
	public String ordersTradeSubmit(HttpServletRequest request, HttpServletResponse response,
			String utid)
	{
		String info = "" ; 
		//交易账户id
		String uaid = request.getParameter("uaid");
		Map<String, Object> condMap = new HashMap<String,Object>();
		condMap.put("id", uaid);
		AUsersAccount usersAccount = this.usersService.findOneUsersAccountService(condMap);
		
		String moneytype = request.getParameter("moneytype");
		String type = request.getParameter("type");
		String price = request.getParameter("price");
		String amount = request.getParameter("amount");
		
		if(moneytype == null)
		{
			moneytype = "" ; 
		}
		
		Map<String, String> paramsMap = new HashMap<String,String>(); 
		//设置交易类型
		paramsMap.put("moneytype", moneytype);
		paramsMap.put("type", type);
		paramsMap.put("price", price);
		paramsMap.put("amount", amount);
		
		//请选择交易类型
		if(!"".equalsIgnoreCase(moneytype))
		{
			//调试接口,获取当前所有的挂单
			ITradeService tradeService = TradeUtil.tradeServiceMap.get(usersAccount.getWbid() + "");
			tradeService.setAuthJSON(usersAccount.getAuthinfoJSON());
			JSONObject resultJSON = tradeService.trade(paramsMap);
			info = resultJSON.get("info") + "";
			//存储条件
			request.setAttribute("resultJSON", resultJSON);
			
			/* 处理订单 */
			if(utid == null)
			{
				utid = "" ; 
			}
			if(!"".equalsIgnoreCase(utid) && "0".equalsIgnoreCase(resultJSON.getString("code")))
			{
				JSONObject resultDataJSON = resultJSON.getJSONObject("data");
				
				condMap.clear();
				condMap.put("id", utid);
				AUsersTask usersTask = this.ordersService.findOneUsersTaskService(condMap);
				if(usersTask != null)
				{
					/* 更新一下usersTask */
					if(usersTask.getBuywbid() == 0 )
					{
						/* 买 */
						usersTask.setBuywbid(usersAccount.getWbid());
						this.ordersService.updateOneUsersTaskService(usersTask);
					}
					if(usersTask.getSellwbid() == 0 )
					{
						/* 卖 */
						usersTask.setSellwbid(usersAccount.getWbid());
						this.ordersService.updateOneUsersTaskService(usersTask);
					}
					
					
					/* 下订单,只下订单,不管是否成交 */
					AUsersTaskOrder usersTaskOrder = new AUsersTaskOrder();
					/* 设置关联关系 */
					usersTaskOrder.setUtid(usersTask.getId());
					usersTaskOrder.setUaid(usersAccount.getId());
					
					/* 设置订单id */
					usersTaskOrder.setOrdersn(resultDataJSON.getString("order_id"));
					usersTaskOrder.setTradeedval(0);
					usersTaskOrder.setTotalval(Double.valueOf(amount));
					
					//设置交易类型
					condMap.clear();
					condMap.put("name", moneytype);
					ATradeType tradeType = this.websiteService.findOneTradeTypeService(condMap);
					usersTaskOrder.setCtid(tradeType.getId());
					
					usersTaskOrder.setPrice(Double.valueOf(price));
					usersTaskOrder.setSouprice(Double.valueOf(price));
					
					/* 设置订单的交易类型 */
					usersTaskOrder.setOrdertype(Byte.valueOf(type));
					
					usersTaskOrder.setStatus(AUsersTaskOrderEnum.STATUS_ORDERING.getStatus());
					usersTaskOrder.setPriftstatus(AUsersTaskOrderEnum.PRIFTSTATUS_UNCALED.getStatus());
					
					usersTaskOrder.setCreatetime(new Date());
					usersTaskOrder.setUpdatetime(new Date());
					usersTaskOrder.setPubtime(new Date());
					
					//计算交易手续费
					Map<String, Double> feeMap = this.outTimerService.removeFee(usersTaskOrder.getTradeedval(), usersTask);
					/*
					resultMap.put("sellfee", sellfee);
					resultMap.put("buyfee", buyfee);
					*/
					if(type.equalsIgnoreCase(AUsersTaskOrderEnum.ORDERTYPE_BUY.getStatus() + ""))
					{
						//买的交易量,已经完成的交易量
						usersTask.setBuynum(usersTask.getBuynum() + usersTaskOrder.getTradeedval());
						if(feeMap.get("sellfee") != null)
						{
							usersTaskOrder.setFee(feeMap.get("sellfee"));
						}
						
						//更新任务的买数量与卖数量
						usersTask.setUpdatetime(new Date());
						this.ordersService.updateBatchUsersTaskService("updateBuynum",usersTask);
					}else if(type.equalsIgnoreCase(AUsersTaskOrderEnum.ORDERTYPE_SELL.getStatus() + ""))
					{
						//卖的交易量,已经完成的交易量
						usersTask.setSellnum(usersTask.getSellnum() + usersTaskOrder.getTradeedval());
						if(feeMap.get("buyfee") != null)
						{
							usersTaskOrder.setFee(feeMap.get("buyfee"));
						}
						
						//更新任务的买数量与卖数量
						usersTask.setUpdatetime(new Date());
						this.ordersService.updateBatchUsersTaskService("updateSellnum",usersTask);
					}
					
					//更新买单信息
					usersTaskOrder.setUpdatetime(new Date());
					JSONObject resDBJSON = this.ordersService.saveOneUsersTaskOrderService(usersTaskOrder);
					/* 执行的结果 */
					info = info + resDBJSON.get("info") + "";
				}
			}
		}else
		{
			info = "选择交易类型" ; 
		}
		request.setAttribute("moneytype", moneytype);
		request.setAttribute("info", info);
		request.setAttribute("paramsMap", paramsMap);
		return this.ordersTrade(request, response,utid);
	}
	
	/**
	 * 取消交易
	 * 
	 * @return
	 */
	@RequestMapping("/ordersCancel.html")
	public String ordersCancel(HttpServletRequest request, HttpServletResponse response)
	{
		String info = "" ; 
		//交易账户id
		String uaid = request.getParameter("uaid");
		Map<String, Object> condMap = new HashMap<String,Object>();
		condMap.put("id", uaid);
		AUsersAccount usersAccount = this.usersService.findOneUsersAccountService(condMap);
		
		String moneytype = request.getParameter("moneytype");
		if(moneytype == null)
		{
			moneytype = "" ; 
		}
		
		//请选择交易类型
		if(!"".equalsIgnoreCase(moneytype))
		{
			String[] ids = request.getParameterValues("ids");
			
			//调试接口,获取当前所有的挂单
			ITradeService tradeService = TradeUtil.tradeServiceMap.get(usersAccount.getWbid() + "");
			tradeService.setAuthJSON(usersAccount.getAuthinfoJSON());
			Map<String, String> paramsMap = new HashMap<String,String>();
			
			int succedCount = 0 ; 
			for (int i = 0; i < ids.length; i++)
			{
				String id = ids[i];
				
				//设置交易类型
				paramsMap.clear();
				paramsMap.put("moneytype", moneytype);
				paramsMap.put("order_id", id);
				JSONObject resultJSON = tradeService.cancelOrder(paramsMap);
				if("0".equalsIgnoreCase(resultJSON.get("code") + ""))
				{
					succedCount ++ ; 
				}
				info = resultJSON.get("info") + "";
			}
			info = "总条数:" + ids.length + ";成功条数:" + succedCount; 
		}else
		{
			info = "选择交易类型" ; 
		}
		request.setAttribute("moneytype", moneytype);
		request.setAttribute("info", info);
		return this.ordersList(request, response);
	}
	
	/**
	 * 交易历史列表
	 * 
	 * @return
	 */
	@RequestMapping("/tradeHistoryList.html")
	public String tradeHistoryList(HttpServletRequest request)
	{
		//参数
		String keyword = request.getParameter("keyword");
		String statusList = request.getParameter("statusList");
		String uaid = request.getParameter("uaid");
		String websiteId = request.getParameter("websiteId");
		String ordertypeList = request.getParameter("ordertypeList");
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		//分页
		String currentPage = request.getParameter("currentPage");
		String pageSize = request.getParameter("pageSize");
		
		if(keyword == null)
		{
			keyword = "" ; 
		}
		keyword = keyword.trim();
		if(statusList == null)
		{
			statusList = "" ; 
		}
		if(ordertypeList == null)
		{
			ordertypeList = "" ; 
		}
		if(uaid == null)
		{
			uaid = "" ; 
		}
		if(websiteId == null)
		{
			websiteId = "" ; 
		}
		if(st == null)
		{
			st = "" ; 
		}
		if(ed == null)
		{
			ed = "" ; 
		}
		//处理日期
		Date stDate = null ; 
		Date edDate = null ; 
		if(!"".equalsIgnoreCase(st) && !"".equalsIgnoreCase(ed))
		{
			stDate = this.dateUtil.parseDateTime(st);
			edDate = this.dateUtil.parseDateTime(ed);
		}
		//设置搜索条件
		PageInfoUtil pageInfo = this.setPageInfoCond(currentPage,pageSize);
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("keyword", keyword);
		condMap.put("status", statusList);
		if(!"".equalsIgnoreCase(uaid))
		{
			condMap.put("uaid", uaid);
		}
		if(!"".equalsIgnoreCase(ordertypeList))
		{
			condMap.put("ordertype", ordertypeList);
		}
		if(!"".equalsIgnoreCase(websiteId))
		{
			condMap.put("websiteId", websiteId);
		}
		condMap.put("stDate", stDate);
		condMap.put("edDate", edDate);
		List<ATradeHistory> tradeHistoryList = this.ordersService.findCondListTradeHistoryService(pageInfo, condMap);
		
		//存放数据
		request.setAttribute("tradeHistoryList", tradeHistoryList);
		request.setAttribute("pageInfo", pageInfo);
		
		//存储条件
		request.setAttribute("keyword", keyword);
		request.setAttribute("statusList", statusList);
		request.setAttribute("uaid", uaid);
		request.setAttribute("ordertypeList", ordertypeList);
		request.setAttribute("websiteId", websiteId);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		return "/head/orders/tradeHistoryList";
	}
	
	/**
	 * 交易历史添加
	 * 
	 * @return
	 */
	@RequestMapping("/tradeHistoryInsert.html")
	public String tradeHistoryInsert(Model model)
	{
		/* 查询所有启用的交易币种 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("status", ATradeTypeEnum.STATUS_ENABLE.getStatus());
		List<ATradeType> tradeTypeList = this.websiteService.findCondListTradeTypeService(null, condMap);
		model.addAttribute("tradeTypeList", tradeTypeList);
		
		return "/head/orders/tradeHistoryInsert";
	}
	
	/**
	 * 交易历史添加
	 * 
	 * @return
	 */
	@RequestMapping("/tradeHistoryInsertSubmit.html")
	public String tradeHistoryInsertSubmit(HttpServletRequest request ,ATradeHistory tradeHistory,Model model)
	{
		/* 系统指定 */
		tradeHistory.setStatus(ATradeHistoryEnum.STATUS_UNSTART.getStatus());
		
		/* 设置一下发布时间,就是交易时间 */
		String pubTimeStr = request.getParameter("pubTimeStr");
		tradeHistory.setPubtime(this.dateUtil.parseDateTime(pubTimeStr));
		
		tradeHistory.setCreatetime(new Date());
		tradeHistory.setUpdatetime(new Date());
		JSONObject resJSON = this.ordersService.saveOneTradeHistoryService(tradeHistory);
		ConstatFinalUtil.SYS_LOG.info(tradeHistory.getId() + ";" + tradeHistory.getStatusStr() + 
				"--交易历史添加成功--" + resJSON);
		/* 存储结果 */
		String info = resJSON.getString("info");
		model.addAttribute("info", info);
		model.addAttribute("tradeHistory", tradeHistory);
		return this.tradeHistoryInsert(model);
	}
	
	/**
	 * 交易历史更新
	 * 
	 * @return
	 */
	@RequestMapping("/tradeHistoryUpdate.html")
	public String tradeHistoryUpdate(HttpServletRequest request,Model model)
	{
		String thid = request.getParameter("thid");
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		condMap.clear();
		condMap.put("id", thid);
		ATradeHistory tradeHistory = this.ordersService.findOneTradeHistoryService(condMap);
		request.setAttribute("tradeHistory", tradeHistory);
		
		/* 查询一下实时的价格 */
		/*
		 * 取出交易网站
		 * */
		ITradeService tradeService = TradeUtil.tradeServiceMap.get(tradeHistory.getUsersAccountObj().getWbid() + "") ; 
		Map<String, String> paramsMap = new HashMap<String, String>();
		paramsMap.put("moneytype", tradeHistory.getCtName());
		JSONObject resultJSON = tradeService.queryDepth(paramsMap);
		request.setAttribute("resultJSON", resultJSON);
		
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			/* 查询所有启用的交易币种 */
			condMap.clear();
			condMap.put("status", ATradeTypeEnum.STATUS_ENABLE.getStatus());
			List<ATradeType> tradeTypeList = this.websiteService.findCondListTradeTypeService(null, condMap);
			model.addAttribute("tradeTypeList", tradeTypeList);
			
			return "/head/orders/tradeHistoryUpdate";
		}
		return "/head/orders/tradeHistoryInfo";
	}
	
	/**
	 * 交易历史更新
	 * @return
	 */
	@RequestMapping("/tradeHistoryUpdateSubmit.html")
	public String tradeHistoryUpdateSubmit(HttpServletRequest request,Model model)
	{
		String thid = request.getParameter("thid");
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.clear();
		condMap.put("id", thid);
		ATradeHistory tradeHistory = this.ordersService.findOneTradeHistoryService(condMap);
		
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			/* 接收参数 */
			String ctName = request.getParameter("ctName");
			String defprice = request.getParameter("defprice");
			String totalval = request.getParameter("totalval");
			String ordertype = request.getParameter("ordertype");
			String status = request.getParameter("status");
			String ordersn = request.getParameter("ordersn");
			
			/* 设置一下发布时间,就是交易时间 */
			String pubTimeStr = request.getParameter("pubTimeStr");
			tradeHistory.setPubtime(this.dateUtil.parseDateTime(pubTimeStr));
			
			tradeHistory.setOrdersn(ordersn);
			tradeHistory.setCtName(ctName);
			tradeHistory.setDefprice(Double.valueOf(defprice));
			tradeHistory.setTotalval(Double.valueOf(totalval));
			tradeHistory.setOrdertype(Byte.valueOf(ordertype));
			tradeHistory.setStatus(Byte.valueOf(status));
			
			tradeHistory.setUpdatetime(new Date());
			JSONObject resJSON = this.ordersService.updateOneTradeHistoryService(tradeHistory);
			ConstatFinalUtil.SYS_LOG.info(tradeHistory.getId() + ";" + tradeHistory.getStatusStr() + 
					"--交易历史更新成功--" + resJSON);
			
			/* 存储结果 */
			String info = resJSON.getString("info");
			model.addAttribute("info", info);
			model.addAttribute("tradeHistory", tradeHistory);
		}
		return this.tradeHistoryUpdate(request,model);
	}
	
	/**
	 * 交易历史更新
	 * @return
	 */
	@RequestMapping("/tradeHistoryBatch.html")
	public String tradeHistoryBatch(HttpServletRequest request)
	{
		String info = "" ; 
		int succedCount = 0 ; 
		Map<String, Object> condMap = new HashMap<String, Object>();
		Map<String, Object> paramsMap = new HashMap<String, Object>();
		String operType = request.getParameter("operType");
		String[] thids = request.getParameterValues("thids");
		if(thids != null)
		{
			String status = request.getParameter("status");
			/* 条件 */
			paramsMap.put("operType", operType);
			paramsMap.put("status", status);
			
			for (int i = 0; i < thids.length; i++)
			{
				String uaid = thids[i];
				
				//查询任务
				condMap.clear();
				condMap.put("id", uaid);
				ATradeHistory tradeHistory = this.ordersService.findOneTradeHistoryService(condMap);
				/* 原始状态 */
				paramsMap.put("souStatus", tradeHistory.getStatus());
				tradeHistory.setUpdatetime(new Date());
				JSONObject resDbJSON = this.ordersService.updateOneTradeHistoryService(tradeHistory,paramsMap);
				if(Integer.valueOf(resDbJSON.get("code") + "") > 0)
				{
					succedCount ++ ; 
				}
				
			}
		}
		info = "批量操作成功,影响条数:" + succedCount; 
		request.setAttribute("info", info);
		return this.tradeHistoryList(request);
	}
}