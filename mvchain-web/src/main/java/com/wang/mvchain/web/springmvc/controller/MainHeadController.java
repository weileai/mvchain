package com.wang.mvchain.web.springmvc.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.controller.BaseController;
import com.wang.mvchain.common.util.PageInfoUtil;
import com.wang.mvchain.outer.IOutTimerService;
import com.wang.mvchain.outer.service.ITradeService;
import com.wang.mvchain.outer.util.TradeUtil;
import com.wang.mvchain.system.pojo.ACoinrate;
import com.wang.mvchain.system.pojo.ADept;
import com.wang.mvchain.system.pojo.ATradeType;
import com.wang.mvchain.system.pojo.AWebsite;
import com.wang.mvchain.system.pojo.AWebsiteEnum;
import com.wang.mvchain.system.service.IWebsiteService;
import com.wang.mvchain.users.service.IOrdersService;


/**
 * 主要的controller
 * 
 * @author Administrator
 * 
 */
@Controller
@RequestMapping("/")
public class MainHeadController extends BaseController
{
	@Resource
	private IOrdersService ordersService;
	@Resource
	private IWebsiteService websiteService;
	@Resource
	private IOutTimerService outTimerService;
	
	/**
	 * 支持的网站列表
	 * 
	 * @return
	 */
	@RequestMapping("/websiteList")
	public String websiteList(HttpServletRequest request,Model model)
	{
		//参数
		String keyword = request.getParameter("keyword");
		String statusList = request.getParameter("statusList");
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		//分页
		String currentPage = request.getParameter("currentPage");
		String pageSize = request.getParameter("pageSize");
		
		if(keyword == null)
		{
			keyword = "" ; 
		}
		keyword = keyword.trim();
		//设置搜索条件
		PageInfoUtil pageInfo = this.setPageInfoCond(currentPage,pageSize);
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("keyword", keyword);
		condMap.put("status", statusList);
		condMap.put("st", st);
		condMap.put("ed", ed);
		List<AWebsite> websiteList = this.websiteService.findCondListWebsiteService(pageInfo,  condMap);
		
		//存放数据
		request.setAttribute("websiteList", websiteList);
		request.setAttribute("pageInfo", pageInfo);
		//存储条件
		request.setAttribute("keyword", keyword);
		request.setAttribute("statusList", statusList);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		return "/head/websiteList";
	}
	
	/**
	 * 币种列表页面
	 * @return
	 */
	@RequestMapping("/coinrateList.html")
	public String coinrateList(HttpServletRequest request,
			HttpServletResponse response)
	{
		
		String keyword = request.getParameter("keyword");
		String currentPage = request.getParameter("currentPage");
		String pageSize = request.getParameter("pageSize");
		
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		String prostatus = request.getParameter("prostatus");
		
		if(keyword == null)
		{
			keyword = "" ; 
		}
		keyword = keyword.trim() ; 
		if(prostatus == null)
		{
			prostatus = "" ; 
		}
		
		if(st == null)
		{
			st = "" ; 
		}
		if(ed == null)
		{
			ed = "" ; 
		}
		
		PageInfoUtil pageInfo = this.setPageInfoCond(currentPage , pageSize);
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.clear();
		condMap.put("keyword", keyword);
		condMap.put("status", "1");
		condMap.put("prostatus", prostatus);
		condMap.put("st", st);
		condMap.put("ed", ed);
		List<ACoinrate> coinrateList = this.websiteService.findCondListCoinrateService(pageInfo, condMap);
		request.setAttribute("coinrateList", coinrateList);
		request.setAttribute("pageInfo", pageInfo);
		
		request.setAttribute("keyword", keyword);
		request.setAttribute("prostatus", prostatus);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		return "/head/coinrateList";
	}
	
	/**
	 * 交易对列表页面
	 * @return
	 */
	@RequestMapping("/tradeTypeList.html")
	public String tradeTypeList(HttpServletRequest request,
			HttpServletResponse response)
	{
		
		String keyword = request.getParameter("keyword");
		String currentPage = request.getParameter("currentPage");
		String pageSize = request.getParameter("pageSize");
		
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		String status = request.getParameter("status");
		
		if(keyword == null)
		{
			keyword = "" ; 
		}
		keyword = keyword.trim() ; 
		if(status == null)
		{
			status = "" ; 
		}
		
		if(st == null)
		{
			st = "" ; 
		}
		if(ed == null)
		{
			ed = "" ; 
		}
		
		PageInfoUtil pageInfo = this.setPageInfoCond(currentPage , pageSize);
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.clear();
		condMap.put("keyword", keyword);
		condMap.put("status", "1");
		condMap.put("status", status);
		condMap.put("st", st);
		condMap.put("ed", ed);
		List<ATradeType> tradeTypeList = this.websiteService.findCondListTradeTypeService(pageInfo, condMap);
		request.setAttribute("tradeTypeList", tradeTypeList);
		request.setAttribute("pageInfo", pageInfo);
		
		/* 查询所有的交易网站 */
		condMap.clear();
		condMap.put("keyword", keyword);
		condMap.put("status", AWebsiteEnum.STATUS_ENABLE.getStatus());
		List<AWebsite> websiteList = this.websiteService.findCondListWebsiteService(null,  condMap);
		
		//存放数据
		request.setAttribute("websiteList", websiteList);
		
		request.setAttribute("keyword", keyword);
		request.setAttribute("status", status);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		return "/head/tradeTypeList";
	}
	
	/**
	 * 单独查询某个交易对的行情
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/tradeTypeDept.html")
	public String tradeTypeDept(HttpServletRequest request,
			HttpServletResponse response)
	{
		String tradeTyepId = request.getParameter("tradeTyepId");
		String wbid = request.getParameter("wbid");
		if(wbid == null)
		{
			wbid = "" ;
		}
		if(!"".equalsIgnoreCase(wbid))
		{
			Map<String, Object> condMap = new HashMap<String, Object>();
			condMap.clear();
			condMap.put("id", tradeTyepId);
			ATradeType tradeType = this.websiteService.findOneTradeTypeService(condMap);
			request.setAttribute("tradeType", tradeType);
			
			condMap.clear();
			condMap.put("id", wbid);
			AWebsite website = this.websiteService.findOneWebsiteService(condMap);
			request.setAttribute("website", website);
			/* 查询一下实时的价格 */
			/*
			 * 取出交易网站
			 * */
			ITradeService tradeService = TradeUtil.tradeServiceMap.get(website.getId() + "") ; 
			Map<String, String> paramsMap = new HashMap<String, String>();
			paramsMap.put("moneytype", tradeType.getName());
			JSONObject resultJSON = tradeService.queryDepth(paramsMap);
			request.setAttribute("resultJSON", resultJSON);
		}
		return "/head/tradeTypeDept";
	}

	/**
	 * 行情深度列表
	 * 
	 * @return
	 */
	@RequestMapping("/dept.html")
	public String dept(HttpServletRequest request, HttpServletResponse response)
	{
		String cid = request.getParameter("cid");

		if (cid == null || "".equalsIgnoreCase(cid))
		{
			cid = "";
		}
		
		//查询币种
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", cid);
		ACoinrate coinrate = this.websiteService.findOneCoinrateService(condMap);
		
		condMap.clear();
		condMap.put("cid", cid);
		// 查询交易历史数据
		Map<String, Object> resultMap = this.outTimerService
				.operCompareService("1" , condMap);
		request.setAttribute("resultMap", resultMap);
		request.setAttribute("cid", cid);
		request.setAttribute("coinrate", coinrate);
		return "/head/dept";
	}
	
	/**
	 * 行情深度网站比率汇总列表
	 * 
	 * @return
	 */
	@RequestMapping("/deptRate.html")
	public String deptRate(HttpServletRequest request, HttpServletResponse response)
	{
		String cid = request.getParameter("cid");

		if (cid == null || "".equalsIgnoreCase(cid))
		{
			cid = "";
		}
		
		//查询币种
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", cid);
		ACoinrate coinrate = this.websiteService.findOneCoinrateService(condMap);
		
		condMap.clear();
		condMap.put("cid", cid);
		// 查询交易历史数据
		Map<String, Object> resultMap = this.outTimerService.deptRateService("1", condMap);
		
		//将同一个网站的买单和卖单汇总一下
		List<Map<String,Object>> resultResList = new ArrayList<Map<String,Object>>();
		//数据汇总一下
		List<ADept> deptBuyList = (List<ADept>) resultMap.get("deptBuyList");
		List<ADept> deptSellList = (List<ADept>) resultMap.get("deptSellList");
		//list里面放MAP,同一个网站的买卖信息
		for (Iterator iterator = deptSellList.iterator(); iterator.hasNext();)
		{
			ADept sellDept = (ADept) iterator.next();
			
			for (Iterator iterator2 = deptBuyList.iterator(); iterator2.hasNext();)
			{
				ADept buyDept = (ADept) iterator2.next();
				
				Map<String, Object> resTempMap = new HashMap<String,Object>() ; 
				resTempMap.put("sellDept", sellDept);
				resTempMap.put("sellWebsite", sellDept.getWebsiteObj());
				resTempMap.put("buyDept", buyDept);
				resTempMap.put("buyWebsite", buyDept.getWebsiteObj());
				resultResList.add(resTempMap);
			}
		}
		
		request.setAttribute("resultMap", resultMap);
		request.setAttribute("resultResList", resultResList);
		request.setAttribute("coinrate", coinrate);
		return "/head/deptRate";
	}

}
