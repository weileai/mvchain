package com.wang.mvchain.web.springmvc.controller;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.controller.BaseController;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.common.util.EncryptUtil;
import com.wang.mvchain.outer.IOutTimerService;
import com.wang.mvchain.system.pojo.ACoinrate;
import com.wang.mvchain.system.pojo.ADept;
import com.wang.mvchain.system.pojo.ASyspro;
import com.wang.mvchain.system.pojo.ATradeType;
import com.wang.mvchain.system.service.ISystemService;
import com.wang.mvchain.system.service.IWebsiteService;

/**
 * 系统对外的接口
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/outer/system/")
public class SystemOuterController extends BaseController
{
	@Resource
	private IOutTimerService outTimerService;
	@Resource
	private IWebsiteService websiteService;
	@Resource
	private ISystemService systemService;
	
	/**
	 * 服务的借口
	 * var req = {
	  		//方法	
			"method" : "",
			//邮箱
			"email" : "",
			//唯一
			"nonce" : "",
			//公钥
			"key" : "",
			//加密结果
			"sign" : "",
			//数据
			"data" : {
				
			}
		}
	 * 
	 * @return
	 */
	@RequestMapping("/client.html")
	public String client(HttpServletRequest request,
			HttpServletResponse response)
	{
		long st = System.currentTimeMillis() ; 
		JSONObject resultJSON = new JSONObject();
		
		String json = request.getParameter("json");
		if(json == null)
		{
			json = ""; 
		}
		try
		{
			if("get".equalsIgnoreCase(json))
			{
				json = new String(json.getBytes("ISO8859-1"),"UTF-8");
			}
			
			JSONObject reqJSON = (JSONObject) JSON.parse(json);
			JSONObject dataReqJSON = (JSONObject) reqJSON.get("data");
			
			if("cancel".equalsIgnoreCase(reqJSON.getString("operType")))
			{
				/* 取消验证 */
				resultJSON = new JSONObject() ; 
				resultJSON.put("code", "0");
			}else
			{
				resultJSON = this.verify(reqJSON);
			}
			if("0".equalsIgnoreCase(resultJSON.get("code") + ""))
			{
				String method = reqJSON.get("method") + "" ; 
				if("coinrateList".equalsIgnoreCase(method))
				{
					resultJSON = this.coinrateList(reqJSON);
				}else if("deptList".equalsIgnoreCase(method))
				{
					resultJSON = this.deptList(reqJSON);
				}
			}
		}catch(Exception e)
		{
			ConstatFinalUtil.SYS_LOG.error("币种列表失败了;" + json , e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		
		long ed = System.currentTimeMillis() ; 
		resultJSON.put("consume", (ed - st) + "毫秒");
		this.printOut(request ,response, resultJSON + "");
		return null;
	}
	
	/**
	 * 币种列表
	 * 
	 * @return
	 */
	public JSONObject coinrateList(JSONObject reqJSON)
	{
		JSONObject resultJSON = new JSONObject() ; 
		long st = System.currentTimeMillis() ; 
		
		Map<String, Object> condMap = new HashMap<String, Object>(); 
		try
		{
			JSONObject dataReqJSON = (JSONObject) reqJSON.get("data");
			
			JSONObject dataResJSON = new JSONObject();
			JSONArray coinrateArr = new JSONArray();
			
			condMap.clear();
			condMap.put("status", "1");
			List<ACoinrate> coinrateList = this.websiteService.findCondListCoinrateService(null, condMap);
			for (Iterator iterator = coinrateList.iterator(); iterator.hasNext();)
			{
				ACoinrate coinrate = (ACoinrate) iterator.next();
				coinrateArr.add(coinrate.toJSON());
			}
			dataResJSON.put("coinrateArr", coinrateArr);
			
			resultJSON.put("data", dataResJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", "成功");
		}catch(Exception e)
		{
			ConstatFinalUtil.SYS_LOG.error("币种列表失败了;" + reqJSON , e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		
		long ed = System.currentTimeMillis() ; 
		resultJSON.put("consume", (ed - st) + "毫秒");
		return resultJSON;
	}
	
	/**
	 * 行情列表
	 * 
	 * @return
	 */
	public JSONObject deptList(JSONObject reqJSON)
	{
		JSONObject resultJSON = new JSONObject() ; 
		long st = System.currentTimeMillis() ; 
		
		Map<String, Object> condMap = new HashMap<String, Object>();
		try
		{
			JSONObject dataReqJSON = (JSONObject) reqJSON.get("data");
			
			JSONObject dataResJSON = new JSONObject();
			/* 币种id */
			String cid = dataReqJSON.get("cid") + "" ; 
			/* 网站id */
			String wbid = dataReqJSON.get("wbid") + "";
			/* 交易类型的id */
			String tradeTypeId = dataReqJSON.get("tradeTypeId") + "";
			if(dataReqJSON.get("tradeTypeId") != null && !"".equalsIgnoreCase(tradeTypeId))
			{
				/* 查询交易类型,获取from的coinid */
				condMap.clear();
				condMap.put("id", tradeTypeId);
				ATradeType tradeType = this.websiteService.findOneTradeTypeService(condMap);
				cid = tradeType.getFromcoinid() + ""; 
			}
			
			condMap.clear();
			condMap.put("cid", cid);
			condMap.put("wbid", wbid);
			// 查询交易历史数据
			Map<String, Object> resultMap = this.outTimerService
					.operCompareService("1" , condMap);
			
			//买一
			ADept buyDept = (ADept) resultMap.get("buyDept");
			if(buyDept != null)
			{
				dataResJSON.put("buyDept", buyDept.toJSON());
			}
			//卖一
			ADept sellDept = (ADept) resultMap.get("sellDept");
			if(sellDept != null)
			{
				dataResJSON.put("sellDept", sellDept.toJSON());
			}
			
			//买列表
			JSONArray deptBuyArr = new JSONArray();
			List<ADept> deptBuyList = (List<ADept>) resultMap.get("deptBuyList");
			for (Iterator iterator = deptBuyList.iterator(); iterator.hasNext();)
			{
				ADept dept = (ADept) iterator.next();
				deptBuyArr.add(dept.toJSON());
			}
			dataResJSON.put("deptBuyArr", deptBuyArr);
			
			//买列表
			JSONArray deptSellArr = new JSONArray();
			List<ADept> deptSellList = (List<ADept>) resultMap.get("deptSellList");
			for (Iterator iterator = deptSellList.iterator(); iterator.hasNext();)
			{
				ADept dept = (ADept) iterator.next();
				deptSellArr.add(dept.toJSON());
			}
			dataResJSON.put("deptSellArr", deptSellArr);
			
			resultJSON.put("data", dataResJSON);
			resultJSON.put("code", "0");
			resultJSON.put("info", "成功");
		}catch(Exception e)
		{
			ConstatFinalUtil.SYS_LOG.error("查询行情深度列表失败了;" + reqJSON , e);
			resultJSON.put("code", "-1");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("-1"));
		}
		
		long ed = System.currentTimeMillis() ; 
		resultJSON.put("consume", (ed - st) + "毫秒");
		return resultJSON;
	}
	
	/**
	 * 验证合法性
	 */
	private JSONObject verify(JSONObject reqJSON)
	{
		JSONObject resultJSON = new JSONObject();
		String method = reqJSON.get("method") + "";
		String email = reqJSON.get("email") + "";
		String nonce = reqJSON.get("nonce") + "";
		
		Map<String, Object> condMap = new HashMap<String,Object>();
		condMap.clear();
		condMap.put("name", "client.key");
		ASyspro sysproKey = this.systemService.findOneSysproService(condMap);
		String key = sysproKey.getVals() + "";
		
		condMap.clear();
		condMap.put("name", "client.secret");
		ASyspro sysproSecret = this.systemService.findOneSysproService(condMap);
		String secret = sysproSecret.getVals() + "";
		
		String souSign = reqJSON.get("sign") + "";
		
		String sign = method + email + nonce + key;
		sign = EncryptUtil.hmacSHA256(sign, secret);
		if(!souSign.equalsIgnoreCase(sign))
		{
			resultJSON.put("code", "2");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("2"));
		}else
		{
			resultJSON.put("code", "0");
			resultJSON.put("info", ConstatFinalUtil.INFO_JSON.get("0"));
		}
		return resultJSON ; 
	}
}
