package com.wang.mvchain.web.springmvc.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.controller.BaseController;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.common.util.HtmlParserUtil;
import com.wang.mvchain.common.util.PageInfoUtil;
import com.wang.mvchain.stat.pojo.AStatTask;
import com.wang.mvchain.stat.pojo.AStatTaskEnum;
import com.wang.mvchain.stat.service.IStatService;
import com.wang.mvchain.users.service.IUsersService;

/**
 * 系统模块的controller
 * 
 * @author Administrator
 * 
 */
@Controller
@RequestMapping("/head/stat/")
public class StatHeadController extends BaseController
{
	@Resource
	private IStatService statService;
	@Resource
	private IUsersService usersService;

	/**
	 * 用户任务
	 * 
	 * @return
	 */
	@RequestMapping("/statTaskList.html")
	public String statTaskList(HttpServletRequest request, HttpServletResponse response)
	{
		//参数
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		/* 标识 */
		String flag = request.getParameter("flag");
		//分页
		String currentPage = request.getParameter("currentPage");
		String pageSize = request.getParameter("pageSize");
		
		if(st == null)
		{
			st = "" ; 
		}
		if(ed == null)
		{
			ed = "" ; 
		}
		if(flag == null)
		{
			flag = "" ; 
		}
		//设置搜索条件
		PageInfoUtil pageInfo = this.setPageInfoCond(currentPage,pageSize);
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("st", st);
		condMap.put("ed", ed);
		condMap.put("status", AStatTaskEnum.STATUS_ENABLE.getStatus());
		condMap.put("flag", flag);
		condMap.put("usersid", ConstatFinalUtil.CONFIG_JSON.get("mvchain_id") + "");
		List<AStatTask> statTaskList = this.statService.findCondListStatTaskService(pageInfo, condMap);
		
		List<AStatTask> statTasksReverList = new ArrayList<AStatTask>();
		statTasksReverList.addAll(statTaskList);
		Collections.reverse(statTasksReverList);
		//存放数据
		request.setAttribute("statTaskList", statTaskList);
		request.setAttribute("statTasksReverList", statTasksReverList);
		request.setAttribute("pageInfo", pageInfo);
		//存储条件
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		request.setAttribute("flag", flag);
		return "/head/stat/statTaskList";
	}
	/**
	 * 用户任务统计图表
	 * 
	 * @return
	 */
	@RequestMapping("/statTaskListCharts.html")
	public String statTaskListCharts(HttpServletRequest request, HttpServletResponse response)
	{
		//参数
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		//分页
		String currentPage = request.getParameter("currentPage");
		String pageSize = request.getParameter("pageSize");
		
		//设置搜索条件
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("st", st);
		condMap.put("ed", ed);
		condMap.put("usersid", ConstatFinalUtil.CONFIG_JSON.get("mvchain_id") + "");
		List<AStatTask> statTaskList = this.statService.findCondListStatTaskService(null, condMap);
		if(statTaskList!=null && statTaskList.size()>0){
			Collections.sort(statTaskList, new Comparator<AStatTask>(){

				@Override
				public int compare(AStatTask o1, AStatTask o2)
				{
					return o1.getCreatetime().compareTo(o2.getCreatetime());
				}
				
			});
		}
		//TODO 总收益曲线数据和手续费曲线数据
		/*//TODO 模拟数据
		long currentTime =System.currentTimeMillis();
		for(int i=0;i<50;i++){
			AStatTask task = new AStatTask();
			task.setCreatetime(new Date(currentTime+(i*3600*1000) ));
			task.setPrift(Math.random()*100);
			task.setFee(Math.random()*10);
			statTaskList.add(task);
		}*/
		JSONArray series0Arr= new JSONArray();
		JSONArray series1Arr= new JSONArray();
		for(AStatTask task:statTaskList){
			JSONArray profitArr = new JSONArray();
			profitArr.add(0, task.getCreatetime().getTime());
			profitArr.add(1, task.getPrift());
			series0Arr.add(profitArr);
			//TODO 手续费
			JSONArray feeArr = new JSONArray();
			feeArr.add(0, task.getCreatetime().getTime());
			feeArr.add(1, task.getFee());
			series1Arr.add(feeArr);
		}
		//存放数据
		request.setAttribute("profitArray", series0Arr.toJSONString());
		request.setAttribute("feeArray", series1Arr.toJSONString());
		//存储条件
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		return "/head/stat/statTaskListCharts";
	}	
	/**
	 * 添加支持网站
	 * 
	 * @return
	 */
	@RequestMapping("/statTaskUpdate.html")
	public String statTaskUpdate(HttpServletRequest request, HttpServletResponse response)
	{
		String stid = request.getParameter("stid");
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", stid);
		AStatTask statTask = this.statService.findOneStatTaskService(condMap);
		request.setAttribute("statTask", statTask);
		
		//计算总资产
		condMap.clear();
		condMap.put("usersid", statTask.getUsersid() + "") ;
		condMap.put("status", "1") ;
		condMap.put("groupby", "1");
		List accountList = this.usersService.findUsersAccountStatListService(null, condMap);
		request.setAttribute("accountList", accountList);
		
		//查询美元汇率
		double usdRate = 1 ; 
		HtmlParserUtil htmlParserUtil = new HtmlParserUtil() ; 
		JSONObject resultJSON = htmlParserUtil.findCoinrateRate(new JSONObject());
		/* 键为币种的id */
		JSONObject usdJSON = (JSONObject) resultJSON.get("1");
		if(usdJSON != null)
		{
			usdRate = usdJSON.getDoubleValue("rate");
		}
		/*condMap.clear();
		condMap.put("tocoinid", TradeUtil.USD_COINID);
		List<ATradeType> tradeTypeList = this.websiteService.findCondListTradeTypeService(new PageInfoUtil(),condMap);
		if(tradeTypeList.size() > 0)
		{
			ATradeType tradeType = tradeTypeList.get(0);
			usdRate = tradeType.getRate();
		}*/
		request.setAttribute("usdRate", usdRate);
		return "/head/stat/statTaskInfo";
	}
}
