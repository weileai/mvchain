package com.wang.mvchain.web.springmvc.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.controller.BaseController;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.common.util.HtmlParserUtil;
import com.wang.mvchain.common.util.PageInfoUtil;
import com.wang.mvchain.outer.IOutTimerService;
import com.wang.mvchain.outer.service.ITradeService;
import com.wang.mvchain.outer.util.TradeUtil;
import com.wang.mvchain.stat.service.IStatService;
import com.wang.mvchain.system.pojo.ACoinrate;
import com.wang.mvchain.system.pojo.ATradeType;
import com.wang.mvchain.system.pojo.AWebsite;
import com.wang.mvchain.system.service.IWebsiteService;
import com.wang.mvchain.users.pojo.ATradeHistory;
import com.wang.mvchain.users.pojo.AUsers;
import com.wang.mvchain.users.pojo.AUsersAccount;
import com.wang.mvchain.users.pojo.AUsersTask;
import com.wang.mvchain.users.pojo.AUsersTaskOrder;
import com.wang.mvchain.users.service.IOrdersService;
import com.wang.mvchain.users.service.IUsersService;

/**
 * 主要的controller
 * 
 * @author Administrator
 * 
 */
@Controller
@RequestMapping("/head/users/")
public class UsersHeadController extends BaseController
{
	@Resource
	private IUsersService usersService;
	@Resource
	private IOrdersService ordersService;
	@Resource
	private IWebsiteService websiteService;
	@Resource
	private IOutTimerService outTimerService;
	@Resource
	private IStatService statService;

	/**
	 * 登陆后的首页
	 * 
	 * @return
	 */
	@RequestMapping("/main.html")
	public String main(HttpServletRequest request, HttpServletResponse response)
	{
		//计算总资产
		AUsers users = (AUsers) this.findObjfromSession(request, "1");
		
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.clear();
		condMap.put("usersid", ConstatFinalUtil.CONFIG_JSON.get("mvchain_id") + "") ;
		condMap.put("status", "1") ;
		condMap.put("groupby", "1");
		List accountList = this.usersService.findUsersAccountStatListService(null, condMap);
		request.setAttribute("accountList", accountList);
		
		//查询美元汇率
		double usdRate = 1 ; 
		HtmlParserUtil htmlParserUtil = new HtmlParserUtil() ; 
		JSONObject resultJSON = htmlParserUtil.findCoinrateRate(new JSONObject());
		/* 键为币种的id */
		JSONObject usdJSON = (JSONObject) resultJSON.get("1");
		if(usdJSON != null)
		{
			usdRate = usdJSON.getDoubleValue("rate");
		}
		/*condMap.clear();
		condMap.put("tocoinid", TradeUtil.USD_COINID);
		List<ATradeType> tradeTypeList = this.websiteService.findCondListTradeTypeService(new PageInfoUtil(),condMap);
		if(tradeTypeList.size() > 0)
		{
			ATradeType tradeType = tradeTypeList.get(0);
			usdRate = tradeType.getRate();
		}*/
		request.setAttribute("usdRate", usdRate);
		return "/head/users/main";
	}
	
	/**
	 * 用户更新
	 * 
	 * @return
	 */
	@RequestMapping("/usersUpdate.html")
	public String usersUpdate(HttpServletRequest request, HttpServletResponse response)
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.clear();
		condMap.put("id", ConstatFinalUtil.CONFIG_JSON.get("mvchain_id") + "" + "");
		condMap.put("extend", "true");
		AUsers users = this.usersService.findOneUsersService(condMap);
		request.setAttribute("users", users);
		
		/* 统计一下用户的总金额信息 */
		this.statService.updateStatTaskService(new Date());
		
		String operType = request.getParameter("operType");
		request.setAttribute("operType", operType);
		if("update".equalsIgnoreCase(operType))
		{
			condMap.clear();
			condMap.put("status", "1");
			List<ACoinrate> coinrateList = this.websiteService.findCondListCoinrateService(null, condMap);
			request.setAttribute("coinrateList", coinrateList);
			
			return "/head/users/usersUpdate";
		}
		return "/head/users/usersInfo";
	}
	
	/**
	 * 用户更新提交
	 * 
	 * @return
	 */
	@RequestMapping("/usersUpdateSubmit.html")
	public String usersUpdateSubmit(HttpServletRequest request, HttpServletResponse response)
	{
		String info = "" ; 
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.clear();
		condMap.put("id", ConstatFinalUtil.CONFIG_JSON.get("mvchain_id") + "" + "");
		AUsers users = this.usersService.findOneUsersService(condMap);
		
		//接收参数
		String prostatus = request.getParameter("prostatus");
		String maxtask = request.getParameter("maxtask");
		
		users.setProstatus(Byte.valueOf(prostatus));
		users.setMaxtask(Integer.valueOf(maxtask));
		
		//更新数据
		users.setUpdatetime(new Date());
		JSONObject resultDBJSON = this.usersService.updateOneUsersService(users);
		info = resultDBJSON.get("info") + "" ;
		
		//更新交易配置
		String coinid = request.getParameter("coinid");
		if(coinid == null)
		{
			coinid = "" ; 
		}
		
		if(!"".equalsIgnoreCase(coinid))
		{
			String trademinnum = request.getParameter("trademinnum");
			String trademaxnum = request.getParameter("trademaxnum");
			String prift = request.getParameter("prift");
					
			//更新交易的配置信息
			AUsersAccount usersAccount = new AUsersAccount() ; 
			usersAccount.setCoinid(Integer.valueOf(coinid));
			usersAccount.setTrademinnum(Double.valueOf(trademinnum));
			usersAccount.setTrademaxnum(Double.valueOf(trademaxnum));
			usersAccount.setPrift(Double.valueOf(prift));
			
			//更新最大交易量和最小收益
			resultDBJSON = this.usersService.updateBatchUsersAccountService("updatePrift" , usersAccount);
			info += ";" + resultDBJSON.get("info") + "" ;
		}
		
		request.setAttribute("info", info);
		return this.usersUpdate(request, response);
	}
	
	/**
	 * 支持的网站列表
	 * 
	 * @return
	 */
	@RequestMapping("/usersAccountList.html")
	public String usersAccountList(HttpServletRequest request, HttpServletResponse response)
	{
		String usersidList = ConstatFinalUtil.CONFIG_JSON.get("mvchain_id") + "" ;
		//参数
		String keyword = request.getParameter("keyword");
		String statusList = request.getParameter("statusList");
		String coinidList = request.getParameter("coinidList");
		String wbidList = request.getParameter("wbidList");
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		//分页
		String currentPage = request.getParameter("currentPage");
		String pageSize = request.getParameter("pageSize");
		
		if(keyword == null)
		{
			keyword = "" ; 
		}
		keyword = keyword.trim();
		//设置搜索条件
		PageInfoUtil pageInfo = this.setPageInfoCond(currentPage,pageSize);
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("keyword", keyword);
		condMap.put("status", statusList);
		condMap.put("usersid", usersidList);
		condMap.put("coinid", coinidList);
		condMap.put("wbid", wbidList);
		condMap.put("st", st);
		condMap.put("ed", ed);
		List<AUsersAccount> usersAccountList = this.usersService.findCondListUsersAccountService
				(pageInfo, condMap);
		
		//存放数据
		request.setAttribute("usersAccountList", usersAccountList);
		request.setAttribute("pageInfo", pageInfo);
		
		condMap.clear();
		condMap.put("status", "1");
		List<ACoinrate> coinrateList = this.websiteService.findCondListCoinrateService(null, condMap);
		request.setAttribute("coinrateList", coinrateList);
		
		condMap.clear();
		condMap.put("status", "1");
		List<AWebsite> websiteList = this.websiteService.findCondListWebsiteService(null, condMap);
		request.setAttribute("websiteList", websiteList);
		
		//存储条件
		request.setAttribute("keyword", keyword);
		request.setAttribute("statusList", statusList);
		request.setAttribute("coinidList", coinidList);
		request.setAttribute("wbidList", wbidList);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		return "/head/users/usersAccountList";
	}
	
	/**
	 * 添加支持网站账户
	 * 
	 * @return
	 */
	@RequestMapping("/usersAccountInsert.html")
	public String usersAccountInsert(HttpServletRequest request, HttpServletResponse response)
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		condMap.clear();
		condMap.put("status", "1");
		List<AWebsite> websiteList = this.websiteService.findCondListWebsiteService(null, condMap);
		request.setAttribute("websiteList", websiteList);
		return "/head/users/usersAccountInsert";
	}
	
	/**
	 * 添加支持网站账户提交
	 * 
	 * @return
	 */
	@RequestMapping("/usersAccountInsertSubmit.html")
	public String usersAccountInsertSubmit(HttpServletRequest request, HttpServletResponse response , AUsersAccount usersAccount)
	{
		String info = "添加失败" ; 
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		//查询网站id信息
		condMap.clear();
		condMap.put("id", usersAccount.getWbid() + "");
		AWebsite website = this.websiteService.findOneWebsiteService(condMap);
		JSONObject authInfoJSON = website.getAuthinfoJSON() ;
		for (Iterator iterator = authInfoJSON.entrySet().iterator(); iterator.hasNext();)
		{
			Entry me = (Entry) iterator.next();
			String key = me.getKey() + "" ;
			String value = me.getValue() + "" ;
			
			String valPara = request.getParameter(key);
			authInfoJSON.put(key, valPara);
		}
		
		usersAccount.setUsersid(Integer.valueOf(ConstatFinalUtil.CONFIG_JSON.get("mvchain_id") + ""));
		usersAccount.setAuthinfo(authInfoJSON + "");
		JSONObject resDBJSON = this.usersService.saveOneUsersAccountService(usersAccount);
		info = resDBJSON.get("info") + "";
		request.setAttribute("info", info);
		return usersAccountInsert(request, response);
	}
	
	/**
	 * 更新支持网站账户
	 * 
	 * @return
	 */
	@RequestMapping("/usersAccountUpdate.html")
	public String usersAccountUpdate(HttpServletRequest request, HttpServletResponse response)
	{
		String uaid = request.getParameter("uaid");
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		condMap.clear();
		condMap.put("id", uaid);
		AUsersAccount usersAccount = this.usersService.findOneUsersAccountService
				(condMap);
		request.setAttribute("usersAccount", usersAccount);
		
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			return "/head/users/usersAccountUpdate";
		}
		return "/head/users/usersAccountInfo";
	}
	
	/**
	 * 更新支持网站账户提交
	 * 
	 * @return
	 */
	@RequestMapping("/usersAccountUpdateSubmit.html")
	public String usersAccountUpdateSubmit(HttpServletRequest request, HttpServletResponse response)
	{
		String info = "更新失败" ; 
		String uaid = request.getParameter("uaid");
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		condMap.clear();
		condMap.put("id", uaid);
		AUsersAccount usersAccount = this.usersService.findOneUsersAccountService
				(condMap);
		
		String username = request.getParameter("username");
		usersAccount.setUsername(username);
		JSONObject authInfoJSON = usersAccount.getAuthinfoJSON() ;
		for (Iterator iterator = authInfoJSON.entrySet().iterator(); iterator.hasNext();)
		{
			Entry me = (Entry) iterator.next();
			String key = me.getKey() + "" ;
			String value = me.getValue() + "" ;
			
			String valPara = request.getParameter(key);
			authInfoJSON.put(key, valPara);
		}
		usersAccount.setAuthinfo(authInfoJSON + "");
		
		//根据网站id,更新用户和认证信息
		JSONObject resDBJSON = this.usersService.updateBatchUsersAccountService("updateAuditInfo" , usersAccount);
		info = resDBJSON.get("info") + "";
		
		/* 单独修改 */
		/* 接收其它的参数 */
		String trademinnum = request.getParameter("trademinnum");
		String trademaxnum = request.getParameter("trademaxnum");
		String prift = request.getParameter("prift");
		usersAccount.setTrademaxnum(Double.valueOf(trademaxnum));
		usersAccount.setTrademinnum(Double.valueOf(trademinnum));
		usersAccount.setPrift(Double.valueOf(prift));
		resDBJSON = this.usersService.updateOneUsersAccountService(usersAccount);
		info += resDBJSON.get("info") + "";
		
		request.setAttribute("info", info);
		return usersAccountUpdate(request, response);
	}
	
	/**
	 * 更新支持网站账户
	 * 
	 * @return
	 */
	@RequestMapping("/usersAccountBatch.html")
	public String usersAccountBatch(HttpServletRequest request, HttpServletResponse response)
	{
		String info = "" ; 
		int succedCount = 0 ; 
		Map<String, Object> condMap = new HashMap<String, Object>();
		String operType = request.getParameter("operType");
		String[] uaids = request.getParameterValues("uaids");
		if(uaids != null)
		{
			String status = request.getParameter("status");
			String pubtimeCond = request.getParameter("pubtimeCond");
			for (int i = 0; i < uaids.length; i++)
			{
				String uaid = uaids[i];
				
				//查询任务
				condMap.clear();
				condMap.put("id", uaid);
				AUsersAccount usersAccount = this.usersService.findOneUsersAccountService
						(condMap);
				if("1".equalsIgnoreCase(operType))
				{
					//修改状态为启用
					usersAccount.setStatus(Byte.valueOf(status));
				}else if("2".equalsIgnoreCase(operType))
				{
					//批量修改发布时间,用来排序
					usersAccount.setPubtime(this.dateUtil.parseDateTime(pubtimeCond));
				}
				usersAccount.setUpdatetime(new Date());
				JSONObject resDbJSON = this.usersService.updateOneUsersAccountService(usersAccount);
				if(Integer.valueOf(resDbJSON.get("code") + "") > 0)
				{
					succedCount ++ ; 
				}
				
			}
		}
		info = "批量操作成功,影响条数:" + succedCount; 
		request.setAttribute("info", info);
		return this.usersAccountList(request, response);
	}
	
	/**
	 * 任务列表
	 * 
	 * @return
	 */
	@RequestMapping("/usersTaskList.html")
	public String usersTaskList(HttpServletRequest request, HttpServletResponse response)
	{
		//参数
		String keyword = request.getParameter("keyword");
		String statusList = request.getParameter("statusList");
		String soutypeList = request.getParameter("soutypeList");
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		//分页
		String currentPage = request.getParameter("currentPage");
		String pageSize = request.getParameter("pageSize");
		
		if(keyword == null)
		{
			keyword = "" ; 
		}
		keyword = keyword.trim();
		if(statusList == null)
		{
			statusList = "" ; 
		}
		if(soutypeList == null)
		{
			soutypeList = "" ; 
		}
		if(st == null)
		{
			st = "" ; 
		}
		if(ed == null)
		{
			ed = "" ; 
		}
		//处理日期
		Date stDate = null ; 
		Date edDate = null ; 
		if(!"".equalsIgnoreCase(st) && !"".equalsIgnoreCase(ed))
		{
			stDate = this.dateUtil.parseDateTime(st);
			edDate = this.dateUtil.parseDateTime(ed);
		}
		//设置搜索条件
		PageInfoUtil pageInfo = this.setPageInfoCond(currentPage,pageSize);
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("keyword", keyword);
		condMap.put("status", statusList);
		condMap.put("usersid", ConstatFinalUtil.CONFIG_JSON.get("mvchain_id") + "");
		condMap.put("soutype", soutypeList);
		condMap.put("stDate", stDate);
		condMap.put("edDate", edDate);
		List<AUsersTask> usersTaskList = this.ordersService.findCondListUsersTaskService(pageInfo, condMap);
		
		//存放数据
		request.setAttribute("usersTaskList", usersTaskList);
		request.setAttribute("pageInfo", pageInfo);
		//存储条件
		request.setAttribute("keyword", keyword);
		request.setAttribute("statusList", statusList);
		request.setAttribute("soutypeList", soutypeList);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		return "/head/users/usersTaskList";
	}
	
	/**
	 * 添加
	 * 
	 * @return
	 */
	@RequestMapping("/usersTaskInsert.html")
	public String usersTaskInsert(HttpServletRequest request, HttpServletResponse response)
	{
		/* 查询所有启用的交易类型 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		//condMap.put("status", ATradeTypeEnum.STATUS_ENABLE.getStatus());
		List<ATradeType> tradeTypeList = this.websiteService.findCondListTradeTypeService(null, condMap);
		request.setAttribute("tradeTypeList", tradeTypeList);
		return "/head/users/usersTaskInsert";
	}
	
	/**
	 * 添加提交
	 * 
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping("/usersTaskInsertSubmit.html")
	public String usersTaskInsertSubmit(HttpServletRequest request, HttpServletResponse response , AUsersTask usersTask) throws IOException
	{
		String info = "添加失败" ; 
		String tradeTypeId = request.getParameter("tradeTypeId");
		String totalnum = request.getParameter("totalnum");
		String soutype = request.getParameter("soutype");
		String status = request.getParameter("status");
		usersTask.setBuyctid(Integer.valueOf(tradeTypeId));
		usersTask.setSellctid(Integer.valueOf(tradeTypeId));
		usersTask.setTotalnum(Double.valueOf(totalnum));
		usersTask.setSoutype(Byte.valueOf(soutype));
		usersTask.setStatus(Byte.valueOf(status));
		usersTask.setDeptnum(usersTask.getTotalnum());
		//usersTask.setSoutype(Byte.valueOf("1"));
		usersTask.setPriftcoinid(TradeUtil.USD_COINID);
		usersTask.setName(usersTask.getSoutypeStr() + "-" + tradeTypeId);
		usersTask.setContent(usersTask.getName());
		usersTask.setUsersid(Integer.valueOf(ConstatFinalUtil.CONFIG_JSON.get("mvchain_id") + ""));
		usersTask.setCreatetime(new Date());
		usersTask.setUpdatetime(new Date());
		usersTask.setFinishtime(new Date());
		
		JSONObject resDBJSON = this.ordersService.saveOneUsersTaskService(usersTask);
		info = resDBJSON.get("info") + "";
		request.setAttribute("info", info);
		response.sendRedirect(request.getContextPath() + "/head/users/usersTaskList.html");
		return null;
	}
	
	/**
	 * 任务更新
	 * 
	 * @return
	 */
	@RequestMapping("/usersTaskUpdate.html")
	public String usersTaskUpdate(HttpServletRequest request, HttpServletResponse response)
	{
		String utid = request.getParameter("utid");
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		condMap.clear();
		condMap.put("id", utid);
		AUsersTask usersTask = this.ordersService.findOneUsersTaskService(condMap);
		request.setAttribute("usersTask", usersTask);
		
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			/* 查询所有启用的交易类型 */
			condMap.clear();
			//condMap.put("status", ATradeTypeEnum.STATUS_ENABLE.getStatus());
			List<ATradeType> tradeTypeList = this.websiteService.findCondListTradeTypeService(null, condMap);
			request.setAttribute("tradeTypeList", tradeTypeList);
			
			return "/head/users/usersTaskUpdate";
		}
		return "/head/users/usersTaskInfo";
	}
	
	/**
	 * 任务提交
	 * 
	 * @return
	 */
	@RequestMapping("/usersTaskUpdateSubmit.html")
	public String usersTaskUpdateSubmit(HttpServletRequest request, HttpServletResponse response)
	{
		String info = "更新失败" ; 
		String utid = request.getParameter("utid");
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		condMap.clear();
		condMap.put("id", utid);
		AUsersTask usersTask = this.ordersService.findOneUsersTaskService(condMap);
		
		//接收参数
		String name = request.getParameter("name");
		String tradeTypeId = request.getParameter("tradeTypeId");
		String totalnum = request.getParameter("totalnum");
		String status = request.getParameter("status");
		String soutype = request.getParameter("soutype");
		
		usersTask.setBuyctid(Integer.valueOf(tradeTypeId));
		usersTask.setSellctid(Integer.valueOf(tradeTypeId));
		usersTask.setSoutype(Byte.valueOf(soutype));
		usersTask.setName(name);
		usersTask.setTotalnum(Double.valueOf(totalnum));
		usersTask.setDeptnum(usersTask.getTotalnum());
		usersTask.setStatus(Byte.valueOf(status));
		usersTask.setUpdatetime(new Date());
		
		JSONObject resDBJSON = this.ordersService.updateOneUsersTaskService(usersTask);
		info = resDBJSON.get("info") + "" ; 
		request.setAttribute("info", info);
		return this.usersTaskList(request, response);
	}
	
	/**
	 * 任务订单列表
	 * 
	 * @return
	 */
	@RequestMapping("/usersTaskOrderList.html")
	public String usersTaskOrderList(HttpServletRequest request, HttpServletResponse response)
	{
		//参数
		String keyword = request.getParameter("keyword");
		String statusList = request.getParameter("statusList");
		String utidList = request.getParameter("utidList");
		String uaid = request.getParameter("uaid");
		String websiteId = request.getParameter("websiteId");
		String ordertypeList = request.getParameter("ordertypeList");
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		//分页
		String currentPage = request.getParameter("currentPage");
		String pageSize = request.getParameter("pageSize");
		
		if(keyword == null)
		{
			keyword = "" ; 
		}
		keyword = keyword.trim();
		if(statusList == null)
		{
			statusList = "" ; 
		}
		if(utidList == null)
		{
			utidList = "" ; 
		}
		if(ordertypeList == null)
		{
			ordertypeList = "" ; 
		}
		if(uaid == null)
		{
			uaid = "" ; 
		}
		if(websiteId == null)
		{
			websiteId = "" ; 
		}
		if(st == null)
		{
			st = "" ; 
		}
		if(ed == null)
		{
			ed = "" ; 
		}
		//处理日期
		Date stDate = null ; 
		Date edDate = null ; 
		if(!"".equalsIgnoreCase(st) && !"".equalsIgnoreCase(ed))
		{
			stDate = this.dateUtil.parseDateTime(st);
			edDate = this.dateUtil.parseDateTime(ed);
		}
		//设置搜索条件
		PageInfoUtil pageInfo = this.setPageInfoCond(currentPage,pageSize);
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("keyword", keyword);
		condMap.put("status", statusList);
		if(!"".equalsIgnoreCase(utidList))
		{
			condMap.put("utid", utidList);
		}
		if(!"".equalsIgnoreCase(uaid))
		{
			condMap.put("uaid", uaid);
		}
		if(!"".equalsIgnoreCase(ordertypeList))
		{
			condMap.put("ordertype", ordertypeList);
		}
		if(!"".equalsIgnoreCase(websiteId))
		{
			condMap.put("websiteId", websiteId);
		}
		condMap.put("stDate", stDate);
		condMap.put("edDate", edDate);
		List<AUsersTaskOrder> usersTaskOrderList = this.ordersService.findCondListUsersTaskOrderService(pageInfo, condMap);
		
		//存放数据
		request.setAttribute("usersTaskOrderList", usersTaskOrderList);
		request.setAttribute("pageInfo", pageInfo);
		
		/* 查询一下任务 */
		condMap.clear();
		condMap.put("id", utidList);
		AUsersTask usersTask = this.ordersService.findOneUsersTaskService(condMap);
		request.setAttribute("usersTask", usersTask);
		
		if(usersTask != null && (usersTask.getBuywbid() != 0 || usersTask.getSellwbid() != 0) )
		{
			/* 查询一下实时的价格 */
			int webSiteIdRemote = usersTask.getBuywbid() ; 
			if(webSiteIdRemote == 0 )
			{
				webSiteIdRemote = usersTask.getSellwbid() ;
			}
			/* 交易类型 */
			String moneytype = usersTask.getBuyTradeTypeObj().getName() ; 
			/* 
			 * 取出交易网站
			 * */
			ITradeService tradeService = TradeUtil.tradeServiceMap.get(webSiteIdRemote + "") ; 
			Map<String, String> paramsMap = new HashMap<String, String>();
			paramsMap.put("moneytype", moneytype);
			JSONObject resultJSON = tradeService.queryDepth(paramsMap);
			request.setAttribute("resultJSON", resultJSON);
		}
		
		//存储条件
		request.setAttribute("keyword", keyword);
		request.setAttribute("statusList", statusList);
		request.setAttribute("utidList", utidList);
		request.setAttribute("uaid", uaid);
		request.setAttribute("ordertypeList", ordertypeList);
		request.setAttribute("websiteId", websiteId);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		return "/head/users/usersTaskOrderList";
	}
	
	/**
	 * 添加
	 * 
	 * @return
	 */
	@RequestMapping("/usersTaskOrderInsert.html")
	public String usersTaskOrderInsert(HttpServletRequest request, HttpServletResponse response)
	{
		String taskId = request.getParameter("taskId");
		/* 查询所有任务 */
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("id", taskId);
		AUsersTask usersTask = this.ordersService.findOneUsersTaskService(condMap);
		request.setAttribute("usersTask", usersTask);
		
		/* 查询所有启用的交易类型 */
		condMap.clear();
		List<ATradeType> tradeTypeList = this.websiteService.findCondListTradeTypeService(null, condMap);
		request.setAttribute("tradeTypeList", tradeTypeList);
		
		/* 查询指定的交易历史Id */
		String thId = request.getParameter("thId");
		condMap.put("id", thId);
		ATradeHistory tradeHistory = this.ordersService.findOneTradeHistoryService(condMap);
		request.setAttribute("tradeHistory", tradeHistory);
		return "/head/users/usersTaskOrderInsert";
	}
	
	/**
	 * 添加提交
	 * 
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping("/usersTaskOrderInsertSubmit.html")
	public String usersTaskOrderInsertSubmit(HttpServletRequest request, HttpServletResponse response , AUsersTaskOrder usersTaskOrder) throws IOException
	{
		String info = "添加失败" ; 
		String pubTimeStr = request.getParameter("pubTimeStr");
		usersTaskOrder.setPubtime(this.dateUtil.parseDateTime(pubTimeStr));
		
		usersTaskOrder.setTotalprice(usersTaskOrder.getPrice() * usersTaskOrder.getTradeedval());
		
		JSONObject resDBJSON = this.ordersService.saveOneUsersTaskOrderService(usersTaskOrder);
		info = resDBJSON.get("info") + "";
		request.setAttribute("info", info);
		response.sendRedirect(request.getContextPath() + "/head/users/usersTaskOrderList.html?utidList=" + usersTaskOrder.getUtid() );
		return null;
	}
	
	/**
	 * 任务订单更新
	 * 
	 * @return
	 */
	@RequestMapping("/usersTaskOrderUpdate.html")
	public String usersTaskOrderUpdate(HttpServletRequest request, HttpServletResponse response)
	{
		String utoid = request.getParameter("utoid");
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		condMap.clear();
		condMap.put("id", utoid);
		AUsersTaskOrder usersTaskOrder = this.ordersService.findOneUsersTaskOrderService(condMap);
		request.setAttribute("usersTaskOrder", usersTaskOrder);
		
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			/* 查询所有启用的交易类型 */
			condMap.clear();
			List<ATradeType> tradeTypeList = this.websiteService.findCondListTradeTypeService(null, condMap);
			request.setAttribute("tradeTypeList", tradeTypeList);
			
			return "/head/users/usersTaskOrderUpdate";
		}else if("tradeOrderInfo".equalsIgnoreCase(operType))
		{
			/*
			 * 取出交易网站
			 * */
			ITradeService tradeService = TradeUtil.tradeServiceMap.get(usersTaskOrder.getUsersAccountObj().getWbid() + "") ; 
			tradeService.setAuthJSON(usersTaskOrder.getUsersAccountObj().getAuthinfoJSON());
			/* 获取一些交易信息 */
			Map<String, String> tradeParamMap = new HashMap<String, String>();
			/* 查询订单的相关信息 */
			tradeParamMap.put("order_id", usersTaskOrder.getOrdersn());
			tradeParamMap.put("moneytype", usersTaskOrder.getTradeTypeObj().getName());
			tradeParamMap.put("amount", usersTaskOrder.getTotalval() + "");
			Map<String, Object> resultMap = this.outTimerService.findOrdersSingle(tradeService, tradeParamMap);
			/* 取出服务器返回的信息 */
			JSONObject resultJSON = (JSONObject) resultMap.get("resultJSON");
			request.setAttribute("resultJSON", resultJSON);
		}
		return "/head/users/usersTaskOrderInfo";
	}
	
	/**
	 * 任务订单更新
	 * 
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping("/usersTaskOrderUpdateSubmit.html")
	public String usersTaskOrderUpdateSubmit(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		String utoid = request.getParameter("utoid");
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.clear();
		condMap.put("id", utoid);
		AUsersTaskOrder usersTaskOrder = this.ordersService.findOneUsersTaskOrderService(condMap);
		
		boolean upflag = false ; 
		String operType = request.getParameter("operType");
		if("cancel".equalsIgnoreCase(operType))
		{
			upflag = this.outTimerService.cancelTaskOrdersService(usersTaskOrder);
		}else if("update".equalsIgnoreCase(operType))
		{
			/* 更新信息 */
			String uaid = request.getParameter("uaid");
			String ctid = request.getParameter("ctid");
			String ordersn = request.getParameter("ordersn");
			String souprice = request.getParameter("souprice");
			String price = request.getParameter("price");
			String tradePrice = request.getParameter("tradePrice");
			String tradeedval = request.getParameter("tradeedval");
			String totalval = request.getParameter("totalval");
			String fee = request.getParameter("fee");
			String ordertype = request.getParameter("ordertype");
			String status = request.getParameter("status");
			
			String pubTimeStr = request.getParameter("pubTimeStr");
			usersTaskOrder.setPubtime(this.dateUtil.parseDateTime(pubTimeStr));
			
			usersTaskOrder.setUaid(Integer.valueOf(uaid));
			usersTaskOrder.setCtid(Integer.valueOf(ctid));
			usersTaskOrder.setOrdersn(ordersn);
			usersTaskOrder.setSouprice(Double.valueOf(souprice));
			usersTaskOrder.setPrice(Double.valueOf(price));
			usersTaskOrder.setTradePrice(Double.valueOf(tradePrice));
			usersTaskOrder.setTradeedval(Double.valueOf(tradeedval));
			usersTaskOrder.setTotalval(Double.valueOf(totalval));
			usersTaskOrder.setFee(Double.valueOf(fee));
			usersTaskOrder.setOrdertype(Byte.valueOf(ordertype));
			usersTaskOrder.setStatus(Byte.valueOf(status));
			
			usersTaskOrder.setTotalprice(usersTaskOrder.getPrice() * usersTaskOrder.getTradeedval());
			
			upflag = true ;
		}
		
		if(upflag)
		{
			usersTaskOrder.setUpdatetime(new Date());
			JSONObject resDBJSON = this.ordersService.updateOneUsersTaskOrderService(usersTaskOrder);
			ConstatFinalUtil.SYS_LOG.info(usersTaskOrder.getId() + ";" + usersTaskOrder.getStatusStr() + 
					"--任务订单更新成功--" + resDBJSON);
			String info = resDBJSON.get("info") + "";
			request.setAttribute("info", info);
			response.sendRedirect(request.getContextPath() + "/head/users/usersTaskOrderList.html?utidList=" + usersTaskOrder.getUtid() );
			return null;
		}
		return this.usersTaskOrderUpdate(request, response);
	}
	
	/**
	 * 任务订单更新
	 * 
	 * @return
	 */
	@RequestMapping("/usersTaskOrderDelete.html")
	public String usersTaskOrderDelete(HttpServletRequest request, HttpServletResponse response)
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		condMap.clear();
		condMap.put("status", "1");
		JSONObject resultJSON = this.ordersService.deleteBatchUsersTaskOrderService(condMap);
		ConstatFinalUtil.SYS_LOG.info("批量清除撤单任务结果:{}",resultJSON);
		return "redirect:/head/users/usersTaskList.html" ; 
	}

	/**
	 * 登陆后的首页
	 * 
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping("/logout.html")
	public String logout(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		HttpSession session = request.getSession();
		AUsers users = (AUsers) this.findObjfromSession(request, "1");
		session.removeAttribute("users");
		
		/* 用户中心注销以后,返回的页面 */
		String returnUrl = ConstatFinalUtil.SYS_BUNDLE.getString("website.url");
		returnUrl = URLEncoder.encode(returnUrl,"UTF-8");
		
		/* 用户中心退出的页面 */
		String logoutUrl = ConstatFinalUtil.SYS_BUNDLE.getString("sso.users.logout") ; 
		if(returnUrl != null && !"".equalsIgnoreCase(logoutUrl))
		{
			logoutUrl += "&returnUrl=" + returnUrl ; 
		}
		response.sendRedirect(logoutUrl);
		return null;
	}
}
