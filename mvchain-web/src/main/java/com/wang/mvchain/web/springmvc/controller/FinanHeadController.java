package com.wang.mvchain.web.springmvc.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONObject;
import com.wang.mvchain.common.controller.BaseController;
import com.wang.mvchain.common.util.ConstatFinalUtil;
import com.wang.mvchain.common.util.PageInfoUtil;
import com.wang.mvchain.finan.pojo.AFinanAccount;
import com.wang.mvchain.finan.pojo.AFinanAccountEnum;
import com.wang.mvchain.finan.pojo.AFinanAsset;
import com.wang.mvchain.finan.pojo.AFinanHistory;
import com.wang.mvchain.finan.pojo.AFinanHistoryEnum;
import com.wang.mvchain.finan.service.IFinanService;
import com.wang.mvchain.outer.IOutTimerService;
import com.wang.mvchain.system.pojo.ACoinrate;
import com.wang.mvchain.system.pojo.ACoinrateEnum;
import com.wang.mvchain.system.pojo.ADept;
import com.wang.mvchain.system.pojo.AWebsite;
import com.wang.mvchain.system.pojo.AWebsiteEnum;
import com.wang.mvchain.system.service.IWebsiteService;
import com.wang.mvchain.users.pojo.AUsers;

/**
 * 资产前台的Controller
 * @author wangshMac
 */
@Controller
@RequestMapping("/head/finan/")
public class FinanHeadController extends BaseController
{
	@Resource
	private IFinanService finanService;
	@Resource
	private IWebsiteService websiteService;
	@Resource
	private IOutTimerService outTimerService;
	
	/*----资产模块开始----*/
	/**
	 * 资产列表
	 * @return
	 */
	@RequestMapping("/assetList")
	public String assetList(HttpServletRequest request, HttpSession session)
	{
		AUsers users = (AUsers) this.findObjfromSession(request, "1");
		String usersid = users.getId() + ""; 
		
		/* 接收参数 */
		String keyword = request.getParameter("keyword");
		String status = request.getParameter("status");
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		String coinid = request.getParameter("coinid");
		//分页
		String currentPage = request.getParameter("currentPage");
		String pageSize = request.getParameter("pageSize");
		
		if(keyword == null)
		{
			keyword = "" ; 
		}
		keyword = keyword.trim();
		if(status == null)
		{
			status = "" ; 
		}
		if(st == null)
		{
			st = "" ; 
		}
		if(ed == null)
		{
			ed = "" ;
		}
		if(coinid == null)
		{
			coinid = "" ; 
		}
		Date stDate = null ; 
		Date edDate = null ; 
		if(!"".equalsIgnoreCase(st) && !"".equalsIgnoreCase(ed))
		{
			stDate = this.dateUtil.parseDateTime(st);
			edDate = this.dateUtil.parseDateTime(ed);
		}
		
		//设置搜索条件
		PageInfoUtil pageInfo = this.setPageInfoCond(currentPage,pageSize);
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("keyword", keyword);
		if(!"".equalsIgnoreCase(status))
		{
			condMap.put("status", status);
		}
		if(!"".equalsIgnoreCase(coinid))
		{
			condMap.put("coinid", coinid);
		}
		if(!"".equalsIgnoreCase(usersid))
		{
			condMap.put("usersid", usersid);
		}
		condMap.put("stDate", stDate);
		condMap.put("edDate", edDate);
		List<AFinanAsset> assetList = this.finanService.findCondListFinanAssetService(pageInfo,  condMap);
		
		/*存放数据*/
		request.setAttribute("assetList", assetList);
		request.setAttribute("pageInfo", pageInfo);
		/* 查询所有启用的币种 */
		condMap.clear();
		condMap.put("status", ACoinrateEnum.STATUS_ENABLE.getStatus());
		List<ACoinrate> coinrateList = this.websiteService.findCondListCoinrateService(null, condMap);
		request.setAttribute("coinrateList", coinrateList);
		
		/*存储条件*/
		request.setAttribute("keyword", keyword);
		request.setAttribute("status", status);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		request.setAttribute("coinid", coinid);
		return "/head/finan/assetList";
	}
	/**
	 * 资产更新
	 * @return
	 */
	@RequestMapping("/assetUpdate")
	public String assetUpdate(String assetId,HttpServletRequest request)
	{
		Map<String, Object> condMap = new HashMap<String,Object>();
		
		/* 根据id查询资产 */
		condMap.clear();
		condMap.put("id", assetId);
		AFinanAsset asset = this.finanService.findOneFinanAssetService(condMap);
		request.setAttribute("asset", asset);
		
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			return "/head/finan/assetUpdate";
		}
		return "/head/finan/assetInfo";
	}
	/*----资产模块结束----*/
	/*----账户模块开始----*/
	/**
	 * 账户列表
	 * @return
	 */
	@RequestMapping("/accountList")
	public String accountList(HttpServletRequest request, HttpSession session)
	{
		AUsers users = (AUsers) this.findObjfromSession(request, "1");
		String usersid = users.getId() + ""; 
		
		/* 接收参数 */
		String keyword = request.getParameter("keyword");
		String status = request.getParameter("status");
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		String accountType = request.getParameter("accountType");
		//分页
		String currentPage = request.getParameter("currentPage");
		String pageSize = request.getParameter("pageSize");
		
		if(keyword == null)
		{
			keyword = "" ; 
		}
		keyword = keyword.trim();
		if(status == null)
		{
			status = "" ; 
		}
		if(st == null)
		{
			st = "" ; 
		}
		if(ed == null)
		{
			ed = "" ;
		}
		Date stDate = null ; 
		Date edDate = null ; 
		if(!"".equalsIgnoreCase(st) && !"".equalsIgnoreCase(ed))
		{
			stDate = this.dateUtil.parseDateTime(st);
			edDate = this.dateUtil.parseDateTime(ed);
		}
		
		//设置搜索条件
		PageInfoUtil pageInfo = this.setPageInfoCond(currentPage,pageSize);
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("keyword", keyword);
		if(!"".equalsIgnoreCase(status))
		{
			condMap.put("status", status);
		}
		if(!"".equalsIgnoreCase(accountType))
		{
			condMap.put("accountType", accountType);
		}
		if(!"".equalsIgnoreCase(usersid))
		{
			condMap.put("usersid", usersid);
		}
		condMap.put("stDate", stDate);
		condMap.put("edDate", edDate);
		List<AFinanAccount> accountList = this.finanService.findCondListFinanAccountService(pageInfo,  condMap);
		
		/*存放数据*/
		request.setAttribute("accountList", accountList);
		request.setAttribute("pageInfo", pageInfo);
		
		/*存储条件*/
		request.setAttribute("keyword", keyword);
		request.setAttribute("status", status);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		request.setAttribute("accountType", accountType);
		request.setAttribute("usersid", usersid);
		return "/head/finan/accountList";
	}
	/**
	 * 资产账户更新
	 * @return
	 */
	@RequestMapping("/accountInsert")
	public String accountInsert(HttpServletRequest request)
	{
		Map<String, Object> condMap = new HashMap<String,Object>();
		return "/head/finan/accountInsert";
	}
	/**
	 * 资产账户更新
	 * @return
	 */
	@RequestMapping("/accountInsertSubmit")
	public String accountInsertSubmit(AFinanAccount account,HttpServletRequest request)
	{
		String info = ConstatFinalUtil.INFO_JSON.getString("-2"); 
		
		AUsers users = (AUsers) this.findObjfromSession(request, "1");
		
		String pubTimeStr = request.getParameter("pubtime");
		account.setPubtime(this.dateUtil.parseDateTime(pubTimeStr));
		
		account.setUsersid(users.getId());
		account.setCreatetime(new Date());
		account.setUpdatetime(new Date());
		JSONObject resultDbJSON = this.finanService.saveOneFinanAccountService(account);
		
		info = resultDbJSON.getString("info");
		request.setAttribute("info", info);
		
		request.setAttribute("account", account);
		return this.accountInsert(request);
	}
	/**
	 * 资产账户更新
	 * @return
	 */
	@RequestMapping("/accountUpdate")
	public String accountUpdate(String accountId,HttpServletRequest request)
	{
		Map<String, Object> condMap = new HashMap<String,Object>();
		
		/* 根据id查询资产 */
		condMap.clear();
		condMap.put("id", accountId);
		AFinanAccount account = this.finanService.findOneFinanAccountService(condMap);
		request.setAttribute("account", account);
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			return "/head/finan/accountUpdate";
		}
		return "/head/finan/accountInfo";
	}
	/**
	 * 资产账户更新
	 * @return
	 */
	@RequestMapping("/accountUpdateSubmit")
	public String accountUpdateSubmit(String accountId,HttpServletRequest request)
	{
		Map<String, Object> condMap = new HashMap<String,Object>();
		
		String info = ConstatFinalUtil.INFO_JSON.getString("-2"); 
		/* 根据id查询资产 */
		condMap.clear();
		condMap.put("id", accountId);
		AFinanAccount account = this.finanService.findOneFinanAccountService(condMap);
		request.setAttribute("account", account);
		
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			String email = request.getParameter("email");
			String usersName = request.getParameter("usersName");
			String pubtimeStr = request.getParameter("pubtimeStr");
			String accountType = request.getParameter("accountType");
			String status = request.getParameter("status");
			String content = request.getParameter("content");
			
			account.setEmail(email);
			account.setUsersName(usersName);
			account.setPubtime(this.dateUtil.parseDateTime(pubtimeStr));
			account.setAccountType(Byte.valueOf(accountType));
			account.setStatus(Byte.valueOf(status));
			account.setContent(content);
			account.setUpdatetime(new Date());
			
			JSONObject resultDbJSON = this.finanService.updateOneFinanAccountService(account);
			info = resultDbJSON.getString("info");
		}
		request.setAttribute("info", info);
		return this.accountUpdate(accountId, request);
	}
	/*----账户模块结束----*/
	/*----历史模块开始----*/
	/**
	 * 历史列表
	 * @return
	 */
	@RequestMapping("/historyList")
	public String historyList(HttpServletRequest request, HttpSession session)
	{
		AUsers users = (AUsers) this.findObjfromSession(request, "1");
		String usersid = users.getId() + ""; 
		
		/* 接收参数 */
		String keyword = request.getParameter("keyword");
		String status = request.getParameter("status");
		String st = request.getParameter("st");
		String ed = request.getParameter("ed");
		
		String assetId = request.getParameter("assetId");
		String wbId = request.getParameter("wbId");
		String accountId = request.getParameter("accountId");
		String tradeType = request.getParameter("tradeType");
		String historyType = request.getParameter("historyType");
		String taskId = request.getParameter("taskId");
		//分页
		String currentPage = request.getParameter("currentPage");
		String pageSize = request.getParameter("pageSize");
		
		if(keyword == null)
		{
			keyword = "" ; 
		}
		keyword = keyword.trim();
		if(status == null)
		{
			status = "" ; 
		}
		if(st == null)
		{
			st = "" ; 
		}
		if(ed == null)
		{
			ed = "" ;
		}
		if(assetId == null)
		{
			assetId = "" ; 
		}
		if(wbId == null)
		{
			wbId = "" ; 
		}
		if(accountId == null)
		{
			accountId = "" ; 
		}
		if(tradeType == null)
		{
			tradeType = "" ; 
		}
		if(historyType == null)
		{
			historyType = "" ; 
		}
		Date stDate = null ; 
		Date edDate = null ; 
		if(!"".equalsIgnoreCase(st) && !"".equalsIgnoreCase(ed))
		{
			stDate = this.dateUtil.parseDateTime(st);
			edDate = this.dateUtil.parseDateTime(ed);
		}
		
		//设置搜索条件
		PageInfoUtil pageInfo = this.setPageInfoCond(currentPage,pageSize);
		Map<String, Object> condMap = new HashMap<String, Object>();
		condMap.put("keyword", keyword);
		if(!"".equalsIgnoreCase(status))
		{
			condMap.put("status", status);
		}
		if(!"".equalsIgnoreCase(assetId))
		{
			condMap.put("assetId", assetId);
		}else
		{
			condMap.put("usersId", usersid);
		}
		if(!"".equalsIgnoreCase(wbId))
		{
			condMap.put("wbId", wbId);
		}
		if(!"".equalsIgnoreCase(accountId))
		{
			condMap.put("accountId", accountId);
		}
		if(!"".equalsIgnoreCase(taskId))
		{
			condMap.put("taskId", taskId);
		}
		if(!"".equalsIgnoreCase(tradeType))
		{
			condMap.put("tradeType", tradeType);
		}
		if(!"".equalsIgnoreCase(historyType))
		{
			condMap.put("historyType", historyType);
		}
		condMap.put("stDate", stDate);
		condMap.put("edDate", edDate);
		List<AFinanHistory> historyList = this.finanService.findCondListFinanHistoryService(pageInfo,  condMap);
		
		/*存放数据*/
		request.setAttribute("historyList", historyList);
		request.setAttribute("pageInfo", pageInfo);
		/* 查询所有启用的币种 */
		condMap.clear();
		condMap.put("status", AWebsiteEnum.STATUS_ENABLE.getStatus());
		List<AWebsite> websiteList = this.websiteService.findCondListWebsiteService(null, condMap);
		request.setAttribute("websiteList", websiteList);
		
		/*存储条件*/
		request.setAttribute("keyword", keyword);
		request.setAttribute("status", status);
		request.setAttribute("st", st);
		request.setAttribute("ed", ed);
		request.setAttribute("usersAssetId", assetId);
		request.setAttribute("wbId", wbId);
		request.setAttribute("accountId", accountId);
		request.setAttribute("taskId", taskId);
		request.setAttribute("tradeType", tradeType);
		request.setAttribute("historyType", historyType);
		return "/head/finan/historyList";
	}
	
	/**
	 * 历史添加
	 * @return
	 */
	@RequestMapping("/historyInsert")
	public String historyInsert(HttpServletRequest request)
	{
		Map<String, Object> condMap = new HashMap<String, Object>();
		
		AUsers users = (AUsers) this.findObjfromSession(request, "1");
		String usersid = users.getId() + ""; 
		
		/* 查询所有启用的网站 */
		condMap.clear();
		condMap.put("status", AWebsiteEnum.STATUS_ENABLE.getStatus());
		List<AWebsite> websiteList = this.websiteService.findCondListWebsiteService(null, condMap);
		request.setAttribute("websiteList", websiteList);
		
		String assetId = request.getParameter("assetId");
		/* 根据id查询资产 */
		condMap.clear();
		condMap.put("id", assetId);
		AFinanAsset asset = this.finanService.findOneFinanAssetService(condMap);
		request.setAttribute("asset", asset);
		
		/* 查询所有启用的账户 */
		condMap.clear();
		condMap.put("status", AFinanAccountEnum.STATUS_ENABLE.getStatus());
		condMap.put("usersid", usersid);
		List<AFinanAccount> accountList = this.finanService.findCondListFinanAccountService(null, condMap);
		request.setAttribute("accountList", accountList);
		
		/* 币种id */
		String cid = asset.getCoinid() + "" ; 
		condMap.clear();
		condMap.put("cid", cid);
		// 查询交易历史数据
		Map<String, Object> resultMap = this.outTimerService.operCompareService("1" , condMap);
		ADept buyDept = (ADept) resultMap.get("buyDept");
		if(buyDept != null)
		{
			request.setAttribute("buyDept", buyDept.toJSON());
		}
		//卖一
		ADept sellDept = (ADept) resultMap.get("sellDept");
		if(sellDept != null)
		{
			request.setAttribute("sellDept", sellDept.toJSON());
		}
		double price = 0 ; 
		if(ConstatFinalUtil.CONFIG_JSON.getString("USD_ID").equals(asset.getCoinid() + ""))
		{
			/* 如果是美金就是:1 */
			price = 1 ; 
		}else if(sellDept != null)
		{
			price = sellDept.getSouprice() ; 
		}
		request.setAttribute("price", price);
		return "/head/finan/historyInsert";
	}
	
	/**
	 * 历史添加提交
	 * @return
	 */
	@RequestMapping("/historyInsertSubmit")
	public String historyInsertSubmit(AFinanHistory history , HttpServletRequest request,HttpServletResponse response)
	{
		String pubTimeStr = request.getParameter("pubTimeStr");
		history.setPubtime(this.dateUtil.parseDateTime(pubTimeStr));
		
		/* 取消,也是未处理 */
		history.setStatus(AFinanHistoryEnum.STATUS_CANCEL.getStatus());
		
		history.setCreatetime(new Date());
		history.setUpdatetime(new Date());
		JSONObject resultDbJSON = this.finanService.saveOneFinanHistoryService(history);
		String info = resultDbJSON.getString("info"); 
		request.setAttribute("info", info);
		return this.historyInsert(request) ; 
	}
	
	/**
	 * 历史更新
	 * @return
	 */
	@RequestMapping("/historyUpdate")
	public String historyUpdate(String historyId,HttpServletRequest request)
	{
		Map<String, Object> condMap = new HashMap<String,Object>();
		
		/* 根据id查询资产 */
		condMap.clear();
		condMap.put("id", historyId);
		AFinanHistory history = this.finanService.findOneFinanHistoryService(condMap);
		request.setAttribute("history", history);
		
		String operType = request.getParameter("operType");
		if("update".equalsIgnoreCase(operType))
		{
			AFinanAsset asset = history.getFinanAsset() ; 
			
			AUsers users = (AUsers) this.findObjfromSession(request, "1");
			String usersid = users.getId() + ""; 
			
			/* 查询所有启用的网站 */
			condMap.clear();
			condMap.put("status", AWebsiteEnum.STATUS_ENABLE.getStatus());
			List<AWebsite> websiteList = this.websiteService.findCondListWebsiteService(null, condMap);
			request.setAttribute("websiteList", websiteList);
			
			/* 查询所有启用的账户 */
			condMap.clear();
			condMap.put("status", AFinanAccountEnum.STATUS_ENABLE.getStatus());
			condMap.put("usersid", usersid);
			List<AFinanAccount> accountList = this.finanService.findCondListFinanAccountService(null, condMap);
			request.setAttribute("accountList", accountList);
			
			/* 币种id */
			String cid = asset.getCoinid() + "" ; 
			condMap.clear();
			condMap.put("cid", cid);
			// 查询交易历史数据
			Map<String, Object> resultMap = this.outTimerService.operCompareService("1" , condMap);
			ADept buyDept = (ADept) resultMap.get("buyDept");
			if(buyDept != null)
			{
				request.setAttribute("buyDept", buyDept.toJSON());
			}
			//卖一
			ADept sellDept = (ADept) resultMap.get("sellDept");
			if(sellDept != null)
			{
				request.setAttribute("sellDept", sellDept.toJSON());
			}
			double price = 0 ; 
			if(ConstatFinalUtil.CONFIG_JSON.getString("USD_ID").equals(asset.getCoinid() + ""))
			{
				/* 如果是美金就是:1 */
				price = 1 ; 
			}else if(sellDept != null)
			{
				price = sellDept.getSouprice() ; 
			}
			request.setAttribute("price", price);
			return "/head/finan/historyUpdate";
		}
		return "/head/finan/historyInfo";
	}
	
	/**
	 * 历史更新提交
	 * @return
	 */
	@RequestMapping("/historyUpdateSubmit")
	public String historyUpdateSubmit(String historyId,HttpServletRequest request,HttpServletResponse response)
	{
		Map<String, Object> condMap = new HashMap<String,Object>();
		
		/* 根据id查询资产 */
		condMap.clear();
		condMap.put("id", historyId);
		AFinanHistory finanHistory = this.finanService.findOneFinanHistoryService(condMap);
		
		String info = "" ; 
		/* 如果订单的状态已经变成了已处理,就无法操作了 */
		if(finanHistory.getStatus() != AFinanHistoryEnum.STATUS_PROCCED.getStatus())
		{
			String wbId = request.getParameter("wbId");
			String accountId = request.getParameter("accountId");
			String usersName = request.getParameter("usersName");
			String num = request.getParameter("num");
			String content = request.getParameter("content");
			String tradeType = request.getParameter("tradeType");
			
			finanHistory.setWbId(Integer.valueOf(wbId));
			finanHistory.setAccountId(Integer.valueOf(accountId));
			finanHistory.setUsersName(usersName);
			finanHistory.setNum(Double.valueOf(num));
			finanHistory.setContent(content);
			finanHistory.setTradeType(Byte.valueOf(tradeType));
			
			String pubTimeStr = request.getParameter("pubTimeStr");
			finanHistory.setPubtime(this.dateUtil.parseDateTime(pubTimeStr));
			
			finanHistory.setUpdatetime(new Date());
			JSONObject resultDbJSON = this.finanService.updateOneFinanHistoryService(finanHistory);
			info = resultDbJSON.getString("info"); 
		}else
		{
			/* "14" : "记录已经锁定,无法操作", */
			info = ConstatFinalUtil.INFO_JSON.getString("14"); 
		}
		request.setAttribute("info", info);
		return this.historyUpdate(historyId, request) ; 
	}
	/*----历史模块结束----*/
}
