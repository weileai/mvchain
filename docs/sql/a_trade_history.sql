/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50721
 Source Host           : localhost:3306
 Source Schema         : mvchain

 Target Server Type    : MySQL
 Target Server Version : 50721
 File Encoding         : 65001

 Date: 27/04/2018 17:09:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for a_trade_history
-- ----------------------------
DROP TABLE IF EXISTS `a_trade_history`;
CREATE TABLE `a_trade_history`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键,自动递增',
  `uaid` int(11) NOT NULL COMMENT '账户id',
  `ctid` int(11) NOT NULL COMMENT '交易类型id',
  `ctName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '交易类型名字(适用于不存在的名字)',
  `ordersn` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '下单的唯一标识',
  `defprice` decimal(20, 3) NOT NULL COMMENT '自定义价格',
  `tradePrice` decimal(20, 3) NOT NULL COMMENT '成交价格',
  `tradeedval` decimal(20, 3) NOT NULL COMMENT '成交数量',
  `totalval` decimal(20, 3) NOT NULL COMMENT '总数量',
  `totalprice` decimal(20, 3) NOT NULL COMMENT '总价格(买是负数,卖是正数),平均单价*成交数量',
  `ordertype` tinyint(20) NOT NULL COMMENT '类型:0:买,1:卖',
  `status` tinyint(4) NOT NULL COMMENT '状态:0:未开始,1:撤单;2,挂单;3:完成',
  `createtime` datetime(0) NOT NULL COMMENT '创建时间',
  `updatetime` datetime(0) NOT NULL COMMENT '更新时间',
  `pubtime` datetime(0) NOT NULL COMMENT '发布时间,用来排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '平台的交易历史表(a_users_task_order)' ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
