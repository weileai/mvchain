-- 查看行情的SQL语句
select wbid,ctid,count(*),website.name,trade_type.name from a_dept dept , a_website website, a_trade_type trade_type 
where dept.wbid = website.id and dept.ctid = trade_type.id 
 group by wbid,ctid ; 
 
-- 删除撤单的记录
select count(*) from a_users_task_order where status = 1 ; 
delete from a_users_task_order where status = 1 ;